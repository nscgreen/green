<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class About extends CI_Controller {
	
        public function index($page=NULL) {
            $data = array(
                'title' => 'NSBM',
                'mDescription' => 'Meta desctiptions goes hera',
                'mKeywords' => 'Meta Keywords goes hera',
            );
            $this->load->view('vheader_main', $data);
            
                switch ($page){
                case 'overview':
                    $this->load->view('main_all/vmain_overview');
                    break;
                
                case 'vision_mission':
                    $this->load->view('main_all/vmain_vision_mission');
                    break;
                
                case 'directors':
                    $this->load->view('main_all/vmain_directors');
                    break;
                
                case 'future_plans':
                    $this->load->view('main_all/vmain_future_plans');
                    break;
                
                case 'nsbm_nibm':
                    $this->load->view('main_all/vmain_nsbm_nibm');
                    break;
                
                case 'global':
                    $this->load->view('main_all/vmain_global');
                    break;
                
                default:
                    $this->load->view('vhome');
                    break;
                }
                
            $this->load->view('vfooter_main');
	}
        
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */