<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category extends CI_Controller {
    
        public function index()
	{
            $data = array(
                'title' => 'Admin',
                'mDescription' => 'Learning Management System of National School of business Management',
                'mKeywords' => 'LMS',
            );
            $this->load->helper('html');
            $this->load->helper('url');
            
                $this->load->view('vheader', $data);
                $this->load->view('lg_admin/cms/help/vcategories');
                $this->load->view('vfooter');
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */