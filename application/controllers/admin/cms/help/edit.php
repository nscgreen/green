<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Edit extends CI_Controller {

    public function article() {
        $data = array(
            'title' => 'Admin',
            'mDescription' => 'Learning Management System of National School of business Management',
            'mKeywords' => 'LMS',
        );
        $this->load->helper('html');
        $this->load->helper('url');

        if ($this->nativesession->get('edit_succes') == true) {
            $data['edit_success'] = true;
        }

        $this->nativesession->delete('edit_succes');

        $this->load->view('vheader', $data);
//            $this->load->view('backend/editArticle');
        $this->load->view('lg_admin/cms/help/vedit_article');
        $this->load->view('vfooter');
    }

    public function update($article_details) {
        $this->load->helper('html');
        $this->load->helper('url');

        $this->load->model('admin/cms/help/help_proccess');
        $unique_details['unique'] = $this->help_proccess->getUnique_ArticleDetails($article_details);
        $this->load->view('lg_admin/cms/help/vedit_article_ajax', $unique_details);
    }

    public function category_loadMenu($value) {
        $this->load->model('admin/cms/help/help_proccess');
        $unique_SlideName = $this->help_proccess->getUnique_ArticleTitles($value);


        echo '<select class="" id="" name="" >';
        echo'<option value="-" >Select Article Title:</option>';
        echo '</select>';

        foreach ($unique_SlideName->result() as $row) {
            echo'<option value="' . $row->id . '" > ' . $row->slide_title . '</option>';
        }
        echo '</select>';
    }

    function last_modify() {

        $id=$_POST['article_id'];

        if (!empty($_POST['new_article_title'])) {
            $article_title = $_POST['new_article_title'];
            $article_title = htmlentities($article_title);
        }

        if (!empty($_POST['new_article_category'])) {
            $article_category = $_POST['new_article_category'];
        }

        if (!empty($_POST['new_article_title'])) {
            $article_slideName = $_POST['new_article_title'];
            $article_slideName = htmlentities($article_slideName);
        }

        if (!empty($_POST['new_article_author'])) {
            $article_author = $_POST['new_article_author'];
            $article_author = htmlentities($article_author);
        }


        $article_timestamp = time();

        if (!empty($_POST['new_article_content'])) {
            $article_content = $_POST['new_article_content'];
            $article_content = htmlentities($article_content);
            $article_content = nl2br($article_content);
        }


        $this->load->model('admin/cms/help/help_proccess');

        $data = array(
            'title' => $article_title,
            'slide_title' => $article_slideName,
            'content' => $article_content,
            'category' => $article_category,
            'timestamp' => $article_timestamp
        );

        $last = $this->help_proccess->last_update($id, $data);
        if ($last == TRUE) {
            $this->nativesession->set('edit_succes', TRUE);
            redirect('admin/cms/help/edit/article');
        }
    }

}

/* End of file home.php */
    /* Location: ./application/controllers/home.php */    