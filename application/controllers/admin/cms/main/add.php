<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Add extends CI_Controller {

    public function article() {
        $data = array(
            'title' => 'Admin',
            'mDescription' => 'Learning Management System of National School of business Management',
            'mKeywords' => 'LMS',
        );
        
        if($this->nativesession->get('add_succes')==true){
            $data['added_success']=true;
        }
        
        $this->nativesession->delete('add_succes');
        
        $this->load->helper('html');
        $this->load->helper('url');

        $this->load->view('vheader', $data);
//                $this->load->view('backend/addArticle');
        $this->load->view('lg_admin/cms/help/vadd_article');
        $this->load->view('vfooter');
    }

    public function check_input() {
        $article_category = $this->input->post('article_category');
        $article_title = $this->input->post('article_title', TRUE);
        $menu_title = $this->input->post('menu_title', TRUE);
        $article_content = $this->input->post('article_content', TRUE);

        if (!empty($article_title) || !empty($article_category) ||
                !empty($menu_title) || !empty($article_content)) {

            $article_title = htmlentities($article_title);
            $menu_title = htmlentities($menu_title);
            $article_content = htmlentities($article_content);
            $article_content = nl2br($article_content);

            $this->load->model('admin/cms/help/help_proccess');

            $data = array(
                'category' => $article_category,
                'slide_title' => $menu_title,
                'title' => $article_title,
                'content' => $article_content,
                'timestamp' => time()
            );

            $return=$this->help_proccess->insert_entry($data);
            if($return==TRUE){
                $this->nativesession->set('add_succes',TRUE);
                redirect('admin/cms/help/add/article');
            }
        }
    }

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */