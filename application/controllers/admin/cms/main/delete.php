<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Delete extends CI_Controller {

    public function article() {
        $data = array(
            'title' => 'Admin',
            'mDescription' => 'Learning Management System of National School of business Management',
            'mKeywords' => 'LMS',
        );
        $this->load->helper('html');
        $this->load->helper('url');

        if($this->nativesession->get('delete_succes')==true){
            $data['delete_success']=true;
        }
        
        $this->nativesession->delete('delete_succes');
       

        $this->load->view('vheader', $data);
        $this->load->view('lg_admin/cms/help/vdelete_article');
        $this->load->view('vfooter');
    }

    public function category_loadMenu($value) {
        $this->load->model('admin/cms/help/help_proccess');
        $unique_SlideName = $this->help_proccess->getUnique_ArticleTitles($value);

        echo '<select class="" id="select_article" name="select_article" >';
        echo'<option value="-" >Select Article Title:</option>';
        echo '</select>';

        foreach ($unique_SlideName->result() as $row) {
            echo'<option value="' . $row->id . '" > ' . $row->slide_title . '</option>';
        }
        echo '</select>';
    }
    
    
      public function deleteArticle($id) {
            $this->load->model('admin/cms/help/help_proccess');
            $deleted = $this->help_proccess->delete_entry($id);
            if($deleted==TRUE){
                $this->nativesession->set('delete_succes',TRUE);
                redirect('admin/cms/help/delete/article');
            }
        }

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */