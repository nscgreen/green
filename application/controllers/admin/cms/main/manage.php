<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage extends CI_Controller {
    
        public function gallery()
	{
            $data = array(
                'title' => 'Admin',
                'mDescription' => 'Learning Management System of National School of business Management',
                'mKeywords' => 'LMS',
            );
            $this->load->helper('html');
            $this->load->helper('url');
            
                $this->load->view('vheader', $data);
                $this->load->view('lg_admin/cms/main/vgallery');
                $this->load->view('vfooter');
	}
        
        public function news()
	{
            $data = array(
                'title' => 'Admin',
                'mDescription' => 'Learning Management System of National School of business Management',
                'mKeywords' => 'LMS',
            );
            $this->load->helper('html');
            $this->load->helper('url');
            
                $this->load->view('vheader', $data);
                $this->load->view('lg_admin/cms/main/vnews');
                $this->load->view('vfooter');
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */