<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class home extends CI_Controller {
	
        public function index()
	{
            $this->load->helper('html');
            $this->load->helper('url');
            
            if($this->nativesession->sessionIsset('login_failed') && $this->nativesession->get('login_failed')===TRUE){
                $data['login_failed']=TRUE;
                $this->nativesession->delete('login_failed');
            }elseif($this->nativesession->sessionIsset('admin_is_logged') && $this->nativesession->get('admin_is_logged')==TRUE){
                redirect('admin/dashboard');
            }else{
                $data['login_failed']=FALSE;
            }
            
            
            
            $this->load->view('lg_admin/vad_login',$data);
	}
        
        public function login_validate()
	{
            $this->load->helper('html');
            $this->load->helper('url');
            
            $this->load->model('mlogin_user');

            $username=$this->input->post('username',TRUE);
            $password=$this->input->post('password',TRUE);
            
            $login_validation= $this->mlogin_user->validate_login($username,$password) ;
            
            if($login_validation){
                $this->nativesession->set('admin_is_logged',TRUE);
                if($this->nativesession->sessionIsset('login_failed')===TRUE){
                    $this->nativesession->delete('login_failed');
                }
                redirect('admin/dashboard');
            }else{
                $this->nativesession->set('login_failed',TRUE);
                if($this->nativesession->sessionIsset('admin_is_logged')===TRUE){
                    $this->nativesession->delete('admin_is_logged');
                }
                //redirect('admin');
                var_dump($login_validation);
            }
	}
        
        public function logout(){
            $this->nativesession->delete('admin_is_logged');
            redirect (base_url().'admin');
    }
        
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */