<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Schools extends CI_Controller {
    
    public function index()
    {
        $data = array(
            'title' => 'Admin',
            'mDescription' => 'Learning Management System of National School of business Management',
            'mKeywords' => 'LMS',
        );
        $this->load->helper('html');
        $this->load->helper('url');

        $this->load->model('admin/mschool','mschool');

        $dataall['schools']= $this->mschool->get_schools();
        //fulldetails
        if($this->nativesession->sessionIsset('school_added_successfully') && $this->nativesession->get('school_added_successfully')===TRUE){
            $this->nativesession->delete('school_added_successfully');
            $dataall['added_successfully']=TRUE;
            $dataall['updated_successfully']=FALSE;
            $dataall['edit_details']=FALSE;
        }elseif($this->nativesession->sessionIsset('school_updated_successfully') && $this->nativesession->get('school_updated_successfully')===TRUE){
            $this->nativesession->delete('school_updated_successfully');
            $dataall['updated_successfully']=TRUE;
            $dataall['added_successfully']=FALSE;
            $dataall['edit_details']=FALSE;
        }elseif(isset($_POST['type'])){
            
            if($this->input->post('type')==="edit"){
                //echo $_POST;
                $dataall['edit_details']=TRUE;
                $dataall['added_successfully']=FALSE;
                $dataall['updated_successfully']=FALSE;
            }

        }else{
            $dataall['edit_details']=FALSE;
            $dataall['added_successfully']=FALSE;
            $dataall['updated_successfully']=FALSE;
        }
            
        
        $this->load->view('vheader', $data);
        $this->load->view('lg_admin/vad_schools',$dataall);
        $this->load->view('vfooter');
    }
    
    public function add_school(){
        $this->load->helper('html');
        $this->load->helper('url');      


        $schooldetails['school_id']=$this->input->post('school_id', TRUE);
        $schooldetails['name']=$this->input->post('name', TRUE);
        $schooldetails['school_dean']=$this->input->post('school_dean_id', TRUE);
        $schooldetails['phone']=$this->input->post('phone', TRUE);
        $schooldetails['web_page']=$this->input->post('web_page', TRUE);
        $schooldetails['email']=$this->input->post('email', TRUE);

        $schooldetails['logo']=$this->input->post('logo',TRUE);

        $this->load->model('admin/mschool','mschool');

        $schooldetails['logo_file_name']=$schooldetails['name'];

        $folderupload="logos/schools/";
        $uploaddetails=$this->mschool->upload_logo($schooldetails['logo_file_name'],$folderupload,'logo');

         if(isset($_FILES['logo']['name']) && !empty($_FILES['logo']['name'])) {
             echo'no file';
         }
        
        if ($this->nativesession->get('uploadfailed')==TRUE) {
            redirect('admin/schools');  
        }

        $schooldetails['logo']= $uploaddetails['logopath'];

        //var_dump($schooldetails);
        $this->mschool->add_school($schooldetails);
        redirect('admin/schools');  
    }
    
    public function update_school(){
        $this->load->helper('html');
        $this->load->helper('url');      


        $schooldetails['school_id']=$this->input->post('school_id', TRUE);
        $schooldetails['name']=$this->input->post('name', TRUE);
        $schooldetails['school_dean']=$this->input->post('school_dean_id', TRUE);
        $schooldetails['phone']=$this->input->post('phone', TRUE);
        $schooldetails['web_page']=$this->input->post('web_page', TRUE);
        $schooldetails['email']=$this->input->post('email', TRUE);

        $schooldetails['logo']=$this->input->post('logo', TRUE);
        $schooldetails['logo_old']=$this->input->post('logo_old', TRUE);

        $this->load->model('admin/mschool','mschool');
        if(isset($_FILES['logo']['name']) && !empty($_FILES['logo']['name'])) {
            $this->load->model('mfile_handler');
            $this->mfile_handler->delete_file($schooldetails['logo_old']);

            $schooldetails['logo_file_name']=$schooldetails['name'];

            $folderupload="logos/schools/";
            $uploaddetails=$this->mschool->upload_logo($schooldetails['logo_file_name'],$folderupload,'logo');

            if ($this->nativesession->get('uploadfailed')==TRUE) {
                //redirect('admin/schools');  
            }
           
            //echo $uploaddetails['logopath'];
            $schooldetails['logo']= $uploaddetails['logopath'];
        }else{
            $schooldetails['logo']=$this->input->post('logo_old', TRUE);
        }

        $this->mschool->update_school($schooldetails);
        //var_dump($schooldetails);
        //redirect('admin/schools');  
    }
        
    
    
    
    
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */