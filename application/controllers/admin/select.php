<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Select extends CI_Controller {
        
        public function load_degrees_from_type($type)
	{
            $this->load->model('admin/mselect','mselect');
            $result=$this->mselect->load_degrees_from_type($type);
            
            echo '<label class="font-label">Course/Degree Name :</label>';
//            echo "<select class='form-control' id='degree' name='degree' onchange='talert();'>";
            echo "<select class='form-control' id='degree' name='degree' onchange='load_values(\"#batch_name\",\"".base_url()."admin/select/load_batches_from_degree\",\"#degree\");'>";
            echo'<option value="-" >Select the Degree</option>';
            
            foreach ($result as $degree) {
                echo'<option value="'.$degree->degree_id.'" >' . $degree->name . '</option>';
            }
            echo '</select>';
	}
        
        public function load_batches_from_degree($type)
	{
            $this->load->model('admin/mselect','mselect');
            $result=$this->mselect->load_batches_from_degree($type);
            
            echo '<label class="font-label">Batch Name :</label>';
            echo '<select class="form-control" id="batch" name="batch" >';
//            echo "<select class='form-control' id='degreed' name='degree' onchange='load_values(\"#batch_name\",\".".base_url()."admin/select/load_batches_from_degree\",\"#degree\");'>";
            echo'<option value="-" >Select the Batch</option>';
//            
            foreach ($result as $batch) {
                echo'<option value="'.$batch->batch_id.'" >' . $batch->name . '</option>';
            }
            echo '</select>';
	}
        
        public function load_degrees_from_type_for_edit($oldvalue,$type)
	{
            $this->load->model('admin/mselect','mselect');
            $result=$this->mselect->load_degrees_from_type($type);
            
            echo '<label class="font-label">Course/Degree Name :</label>';
//            echo "<select class='form-control' id='degree' name='degree' onchange='talert();'>";
            echo "<select class='form-control' id='degree' name='degree' onchange='load_values(\"#batch_name\",\"".base_url()."admin/select/load_batches_from_degree_for_edit\",\"#degree\");'>";
            echo'<option value="-" >Select the Degree</option>';
            
            foreach ($result as $degree) {
                if($degree->degree_id===$oldvalue){
                    echo'<option value="'.$degree->degree_id.'" selected>' . $degree->name . '</option>';
                }else{
                    echo'<option value="'.$degree->degree_id.'" >' . $degree->name . '</option>';
                }
                
            }
            echo '</select>';
	}
        
        public function load_batches_from_degree_for_edit($oldvalue,$type)
	{
            $this->load->model('admin/mselect','mselect');
            $result=$this->mselect->load_batches_from_degree($type);
            
            echo '<label class="font-label">Batch Name :</label>';
            echo '<select class="form-control" id="batch" name="batch" >';
//            echo "<select class='form-control' id='degreed' name='degree' onchange='load_values(\"#batch_name\",\".".base_url()."admin/select/load_batches_from_degree\",\"#degree\");'>";
            echo'<option value="-" >Select the Batch</option>';
//            
            foreach ($result as $batch) {
                if($batch->batch_id===$oldvalue){
                    echo'<option value="'.$batch->batch_id.'" selected>' . $batch->name . '</option>';
                }else{
                    echo'<option value="'.$batch->batch_id.'" >' . $batch->name . '</option>';
                }
            }
            echo '</select>';
	}
        
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */