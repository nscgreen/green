<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Students extends CI_Controller {
    
    public function register()
    {
        $data = array(
            'title' => 'Admin',
            'mDescription' => 'Learning Management System of National School of business Management',
            'mKeywords' => 'LMS',
        );
        $this->load->helper('html');
        $this->load->helper('url');
        
        if($this->nativesession->sessionIsset('student_added_successfully') && $this->nativesession->get('student_added_successfully')===TRUE){
            $this->nativesession->delete('student_added_successfully');
            $dataall['added_successfully']=TRUE; 
        }else{
            $dataall['added_successfully']=FALSE;
        }

        $this->load->view('vheader', $data);
        $this->load->view('lg_admin/vad_st-register',$dataall);
        $this->load->view('vfooter');
    }

    public function manage()
    {
        $this->load->helper('html');
        $this->load->helper('url');
        
        $data = array(
            'title' => 'Admin',
            'mDescription' => 'Learning Management System of National School of business Management',
            'mKeywords' => 'LMS',
        );
        
        if($this->nativesession->sessionIsset('student_updated_successfully') && $this->nativesession->get('student_updated_successfully')===TRUE){
            $dataall['updated_successfully']=TRUE;
            $dataall['edit_details']=FALSE;
            $dataall['view_fulldetails']=FALSE; 
            $dataall['delete']=FALSE;
        }elseif(isset($_POST['type']) && $_POST['type']==="edit"){
            $dataall['edit_details']=TRUE; 
            $dataall['updated_successfully']=FALSE; 
            $dataall['view_fulldetails']=FALSE; 
            $dataall['delete']=FALSE;
        }elseif(isset($_POST['type']) && $_POST['type']==="view"){            
            $dataall['view_fulldetails']=TRUE; 
            $dataall['edit_details']=FALSE; 
            $dataall['updated_successfully']=FALSE;
            $dataall['delete']=FALSE;
        }elseif(isset($_POST['type']) && $_POST['type']==="delete"){
            $dataall['delete']=TRUE;
            $dataall['view_fulldetails']=FALSE; 
            $dataall['edit_details']=FALSE; 
            $dataall['updated_successfully']=FALSE;
        }else{
            $dataall['delete']=FALSE;
            $dataall['edit_details']=FALSE;
            $dataall['updated_successfully']=FALSE;
            $dataall['view_fulldetails']=FALSE; 
        }

        $this->load->view('vheader', $data);
        $this->load->view('lg_admin/vad_st-manage',$dataall);
        $this->load->view('vfooter');
    }
    
    
    public function add_student(){
        $this->load->helper('html');
        $this->load->helper('url');  
        $this->load->model('admin/mstudents','mstudents');
        //Start Student Details
        $studentdetails['student_id']=$this->input->post('student_id', TRUE);
        $studentdetails['index_nm']=$this->input->post('index_nm', TRUE);
        $studentdetails['degree']=$this->input->post('degree', TRUE);
        $studentdetails['batch']=$this->input->post('batch', TRUE);
        
        $studentdetails['fname']=$this->input->post('fname', TRUE);
        $studentdetails['mname']=$this->input->post('mname', TRUE);
        $studentdetails['lname']=$this->input->post('lname', TRUE);
        $studentdetails['initials']=$this->input->post('initials', TRUE);
        $studentdetails['dob']=$this->input->post('dob', TRUE);
        $studentdetails['age']=$this->input->post('age', TRUE);
        $studentdetails['sex']=$this->input->post('sex', TRUE);
                
        $studentdetails['address_line1']=$this->input->post('address_line1', TRUE);
        $studentdetails['address_line2']=$this->input->post('address_line2', TRUE);
        $studentdetails['city']=$this->input->post('city', TRUE);
        $studentdetails['town']=$this->input->post('town', TRUE);
        $studentdetails['state']=$this->input->post('state', TRUE);
        $studentdetails['post_code']=$this->input->post('post_code', TRUE);
        $studentdetails['country']=$this->input->post('country', TRUE);
        //End Student Details
        
        //Start Guardian Details
        $studentdetails['guardian_id']=$this->input->post('student_id', TRUE);
        $studentdetails['g_title']=$this->input->post('g_title', TRUE);
        
        $studentdetails['g_fname']=$this->input->post('g_fname', TRUE);
        $studentdetails['g_lname']=$this->input->post('g_lname', TRUE);
        $studentdetails['g_initials']=$this->input->post('g_initials', TRUE);
        $studentdetails['g_relationship']=$this->input->post('g_relationship', TRUE);
        $studentdetails['g_tel_home']=$this->input->post('g_tel_home', TRUE);
        $studentdetails['g_tel_mobile']=$this->input->post('g_tel_mobile', TRUE);
        $studentdetails['g_email']=$this->input->post('g_email', TRUE);
                
        $studentdetails['g_address_line1']=$this->input->post('g_address_line1', TRUE);
        $studentdetails['g_address_line2']=$this->input->post('g_address_line2', TRUE);
        $studentdetails['g_town']=$this->input->post('g_town', TRUE);
        $studentdetails['g_city']=$this->input->post('g_city', TRUE);
        $studentdetails['g_state']=$this->input->post('g_state', TRUE);
        $studentdetails['g_post_code']=$this->input->post('g_post_code', TRUE);
        $studentdetails['g_country']=$this->input->post('g_country', TRUE);
        //End Guardian Details
        
//        $folderupload="logos/universities/";
//        $this->load->model('admin/mstudent','mstudent');
//        $uploaddetails=$this->mstudent->upload_logo($studentdetails['logo_file_name'],$folderupload,'logo');
//
//        if ($this->nativesession->get('uploadfailed')==TRUE) {
//            redirect('admin/student');  
//        }
//
//        $studentdetails['logo']= $uploaddetails['logopath'];

        var_dump($studentdetails);
        $this->mstudents->add_student($studentdetails);
        redirect('admin/students/register');  
    }
    
    public function update_student(){
        $this->load->helper('html');
        $this->load->helper('url');  
        $this->load->model('admin/mstudents','mstudents');
        //Start Student Details
        $studentdetails['student_id']=$this->input->post('student_id', TRUE);
                
        $studentdetails['fname']=$this->input->post('fname', TRUE);
        $studentdetails['mname']=$this->input->post('mname', TRUE);
        $studentdetails['lname']=$this->input->post('lname', TRUE);
        $studentdetails['initials']=$this->input->post('initials', TRUE);
                
        $studentdetails['address_line1']=$this->input->post('address_line1', TRUE);
        $studentdetails['address_line2']=$this->input->post('address_line2', TRUE);
        $studentdetails['city']=$this->input->post('city', TRUE);
        $studentdetails['town']=$this->input->post('town', TRUE);
        $studentdetails['state']=$this->input->post('state', TRUE);
        $studentdetails['post_code']=$this->input->post('post_code', TRUE);
        $studentdetails['country']=$this->input->post('country', TRUE);
        //End Student Details
        
        
        $this->mstudents->update_student($studentdetails);
        redirect('admin/students/manage');  
    }
        
    

    public function remove_student($student_id) {
        $this->load->helper('html');
        $this->load->helper('url');

        $this->load->database();

        $this->load->model('admin/mstudent','mstudent');
        $this->mstudent->remove_student($student_id);

        redirect('admin/students/manage');
    }
    
    public function load_student_by_id($student_id) {
        $this->load->helper('html');
        $this->load->helper('url');

//        $this->load->database();

        $this->load->model('admin/mstudents','mstudents');
        $row=$this->mstudents->get_student_by_id_for_view($student_id);
        echo '<table class="table table-hover" id="dev-table">
                    <thead>
                        <tr class="alert alert-info">
                            <th>#</th>
                            <th>Index</th>
                            <th>Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>';
        
        $i=1;
        foreach ($row as $student_details) {
                        echo '
                        <tr>
                        <td>' . $i . '</td>
                        <td>'.$student_details->index_nm.'</td>
                        <td>'.$student_details->initials.' '.$student_details->fname.' '.$student_details->mname.' '.$student_details->lname.'</td>
                        <td>
                            <ul class="list-inline" style="margin: 0; padding: 0;">
                                <form action="" method="post">
                                    <input type="hidden" name="type" value="edit"/>
                                    <input type="hidden" name="student_id" value="'.$student_details->student_id.'"/>
                                    <input type="hidden" name="fname" value="'.$student_details->fname.'"/>
                                    <input type="hidden" name="mname" value="'.$student_details->mname.'"/>
                                    <input type="hidden" name="lname" value="'.$student_details->lname.'"/>
                                    <input type="hidden" name="initials" value="'.$student_details->initials.'"/>
                                    <input type="hidden" name="address_line1" value="'.$student_details->address_line1.'"/>
                                    <input type="hidden" name="address_line2" value="'.$student_details->address_line2.'"/>
                                    <input type="hidden" name="city" value="'.$student_details->city.'"/>
                                    <input type="hidden" name="town" value="'.$student_details->town.'"/>
                                    <input type="hidden" name="state" value="'.$student_details->state.'"/>
                                    <input type="hidden" name="post_code" value="'.$student_details->post_code.'"/>
                                    <input type="hidden" name="country" value="'.$student_details->country.'"/>
                                        
                                <li><button type="submit" style="border: 0;background: none;outline: 0;"> <a class="" ><span class="glyphicon glyphicon-pencil" data-toggle="tooltip" data-placement="top" title="Update Details"></span></a></button></li>
                                </form>
                                
                                <form action="" method="post">
                                    <input type="hidden" name="type" value="delete"/>
                                    <input type="hidden" name="index_nm" value="'.$student_details->index_nm.'"/>
                                    <input type="hidden" name="student_id" value="'.$student_details->student_id.'"/>
                                    <input type="hidden" name="name" value="'.$student_details->initials.' '.$student_details->fname.' '.$student_details->mname.' '.$student_details->lname.'"/>                                      
                                <li><button type="submit" style="border: 0;background: none;outline: 0;"><a class="" type="submit"><span class="glyphicon glyphicon-remove" data-toggle="tooltip" data-placement="top" title="Remove Student Details"></span></a></button></li>
                                </form>
                                <!-- <li><a href="" class="" type="submit" data-toggle="modal" data-target="#message"><span class="glyphicon glyphicon-envelope" data-toggle="tooltip" data-placement="top" title="Send a Message"></span></a></li>-->
                                
                                <form action="" method="post">
                                    <input type="hidden" name="type" value="view"/>
                                    <input type="hidden" name="name" value="'.$student_details->initials.' '.$student_details->fname.' '.$student_details->mname.' '.$student_details->lname.'"/>
                                    <input type="hidden" name="degree_id" value="'.$student_details->stu_degree_id.'"/>
                                    <input type="hidden" name="mobile" value="'.$student_details->mobile.'"/>
                                    <input type="hidden" name="address" value="'.$student_details->address_line1.', '.$student_details->address_line2.', '.$student_details->town.', '.$student_details->city.', '.$student_details->state.', '.$student_details->country.', '.$student_details->post_code.'"/>
                                    <input type="hidden" name="email" value="'.$student_details->email.'"/>
                                    <input type="hidden" name="link_fb" value="'.$student_details->link_fb.'"/>
                                    <input type="hidden" name="link_ln" value="'.$student_details->link_ln.'"/>
                                    <input type="hidden" name="profile_pic" value="'.$student_details->profile_pic.'"/>
                                        
                                    <input type="hidden" name="g_name" value="'.$student_details->g_initial.' '.$student_details->fname.' '.$student_details->lname.'"/>
                                    <input type="hidden" name="g_phone" value="'.$student_details->g_tel_home.', '.$student_details->g_tel_home.'"/>
                                    <input type="hidden" name="g_email" value="'.$student_details->g_email.'"/>
                                    <input type="hidden" name="g_address" value="'.$student_details->g_address_line1.','.$student_details->g_address_line2.', '.$student_details->g_town.', '.$student_details->g_city.', '.$student_details->g_state.', '.$student_details->g_country.', '.$student_details->g_post_code.'"/>
                                    <input type="hidden" name="relationship" value="'.$student_details->g_relation.'"/>

                                <li><button type="submit" style="border: 0;background: none;outline: 0;"><a class="" ><span class="glyphicon glyphicon-user" data-toggle="tooltip" data-placement="top" title="Profile"></span></a></button></li>
                                </form>
                            </ul>
                        </td>
                        </tr>';
                        $i++;
        }                       
        
    }
    
    public function load_students_by_batch($batch_id) {
        $this->load->helper('html');
        $this->load->helper('url');

//        $this->load->database();

        $this->load->model('admin/mstudents','mstudents');
        $row=$this->mstudents->get_students_by_batch_for_view($batch_id);
        echo '<table class="table table-hover" id="dev-table">
                    <thead>
                        <tr class="alert alert-info">
                            <th>#</th>
                            <th>Index</th>
                            <th>Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>';
        
        $i=1;
        foreach ($row as $student_details) {
                        echo '
                        <tr>
                        <td>' . $i . '</td>
                        <td>'.$student_details->index_nm.'</td>
                        <td>'.$student_details->initials.' '.$student_details->fname.' '.$student_details->mname.' '.$student_details->lname.'</td>
                        <td>
                            <ul class="list-inline" style="margin: 0; padding: 0;">
                                <form action="" method="post">
                                    <input type="hidden" name="type" value="edit"/>
                                    <input type="hidden" name="student_id" value="'.$student_details->student_id.'"/>
                                    <input type="hidden" name="fname" value="'.$student_details->fname.'"/>
                                    <input type="hidden" name="mname" value="'.$student_details->mname.'"/>
                                    <input type="hidden" name="lname" value="'.$student_details->lname.'"/>
                                    <input type="hidden" name="initials" value="'.$student_details->initials.'"/>
                                    <input type="hidden" name="address_line1" value="'.$student_details->address_line1.'"/>
                                    <input type="hidden" name="address_line2" value="'.$student_details->address_line2.'"/>
                                    <input type="hidden" name="city" value="'.$student_details->city.'"/>
                                    <input type="hidden" name="town" value="'.$student_details->town.'"/>
                                    <input type="hidden" name="state" value="'.$student_details->state.'"/>
                                    <input type="hidden" name="post_code" value="'.$student_details->post_code.'"/>
                                    <input type="hidden" name="country" value="'.$student_details->country.'"/>
                                        
                                <li><button type="submit" style="border: 0;background: none;outline: 0;"> <a class="" ><span class="glyphicon glyphicon-pencil" data-toggle="tooltip" data-placement="top" title="Update Details"></span></a></button></li>
                                </form>
                                
                                <form action="" method="post">
                                    <input type="hidden" name="type" value="delete"/>
                                    <input type="hidden" name="index_nm" value="'.$student_details->index_nm.'"/>
                                    <input type="hidden" name="student_id" value="'.$student_details->student_id.'"/>
                                    <input type="hidden" name="name" value="'.$student_details->initials.' '.$student_details->fname.' '.$student_details->mname.' '.$student_details->lname.'"/>                                      
                                <li><button type="submit" style="border: 0;background: none;outline: 0;"><a class="" type="submit"><span class="glyphicon glyphicon-remove" data-toggle="tooltip" data-placement="top" title="Remove Student Details"></span></a></button></li>
                                </form>
                                <!-- <li><a href="" class="" type="submit" data-toggle="modal" data-target="#message"><span class="glyphicon glyphicon-envelope" data-toggle="tooltip" data-placement="top" title="Send a Message"></span></a></li>-->
                                
                                <form action="" method="post">
                                    <input type="hidden" name="type" value="view"/>
                                    <input type="hidden" name="name" value="'.$student_details->initials.' '.$student_details->fname.' '.$student_details->mname.' '.$student_details->lname.'"/>
                                    <input type="hidden" name="degree_id" value="'.$student_details->stu_degree_id.'"/>
                                    <input type="hidden" name="mobile" value="'.$student_details->mobile.'"/>
                                    <input type="hidden" name="address" value="'.$student_details->address_line1.','.$student_details->address_line2.','.$student_details->town.','.$student_details->city.','.$student_details->state.','.$student_details->country.','.$student_details->post_code.'"/>
                                    <input type="hidden" name="email" value="'.$student_details->email.'"/>
                                    <input type="hidden" name="link_fb" value="'.$student_details->link_fb.'"/>
                                    <input type="hidden" name="link_ln" value="'.$student_details->link_ln.'"/>
                                    <input type="hidden" name="profile_pic" value="'.$student_details->profile_pic.'"/>
                                        
                                    <input type="hidden" name="g_name" value="'.$student_details->g_initial.' '.$student_details->fname.' '.$student_details->lname.'"/>
                                    <input type="hidden" name="g_phone" value="'.$student_details->g_tel_home.', '.$student_details->g_tel_home.'"/>
                                    <input type="hidden" name="g_email" value="'.$student_details->g_email.'"/>
                                    <input type="hidden" name="g_address" value="'.$student_details->g_address_line1.','.$student_details->g_address_line2.','.$student_details->g_town.','.$student_details->g_city.','.$student_details->g_state.','.$student_details->g_country.','.$student_details->g_post_code.'"/>
                                    <input type="hidden" name="relationship" value="'.$student_details->g_relation.'"/>

                                <li><button type="submit" style="border: 0;background: none;outline: 0;"><a class="" ><span class="glyphicon glyphicon-user" data-toggle="tooltip" data-placement="top" title="Profile"></span></a></button></li>
                                </form>
                            </ul>
                        </td>
                        </tr>';
                        $i++;
        }                       
        
    }
       
    
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */

?>