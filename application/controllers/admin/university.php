<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class University extends CI_Controller {
    
        public function index()
	{
            $this->load->helper('html');
            $this->load->helper('url');
            
            $data = array(
                'title' => 'Admin',
                'mDescription' => 'Learning Management System of National School of business Management',
                'mKeywords' => 'LMS',
            );
            
            
            $this->load->model('admin/muniversity','muniversity');
            $dataall['nsbm_details']= $this->muniversity->get_nsbm();
            
            $dataall['universities']= $this->muniversity->get_universities();
            //fulldetails
            if($this->nativesession->sessionIsset('university_added_successfully') && $this->nativesession->get('university_added_successfully')===TRUE){
                $this->nativesession->delete('university_added_successfully');
                $dataall['added_successfully']=TRUE;   
                $dataall['updated_successfully']=FALSE;
                $dataall['view_fulldetails']=FALSE;
                $dataall['edit_details']=FALSE;
            }elseif($this->nativesession->sessionIsset('university_updated_successfully') && $this->nativesession->get('university_updated_successfully')===TRUE){
                $this->nativesession->delete('university_updated_successfully');
                $dataall['updated_successfully']=TRUE;
                $dataall['added_successfully']=FALSE;
                $dataall['view_fulldetails']=FALSE;
                $dataall['edit_details']=FALSE;
            }elseif(isset($_POST['type'])){
                if($this->input->post('type')==="fulldetails"){
                    $dataall['view_fulldetails']=TRUE;
                    $dataall['edit_details']=FALSE;
                    $dataall['added_successfully']=FALSE;
                    $dataall['updated_successfully']=FALSE;
                }elseif($this->input->post('type')==="edit"){
                    //echo $_POST;
                    $dataall['edit_details']=TRUE;
                    $dataall['view_fulldetails']=FALSE;
                    $dataall['added_successfully']=FALSE;
                    $dataall['updated_successfully']=FALSE;
                }
                
            }else{
                $dataall['view_fulldetails']=FALSE;
                $dataall['edit_details']=FALSE;
                $dataall['added_successfully']=FALSE;
                $dataall['updated_successfully']=FALSE;
            }
            
            $this->load->view('vheader', $data);
            $this->load->view('lg_admin/vad_university',$dataall);
            $this->load->view('vfooter');
	}
        
        public function add_university()
	{
            $this->load->helper('html');
            $this->load->helper('url');  
            
            $universitydetails['university_id']=$this->input->post('university_id', TRUE);
            $universitydetails['name']=$this->input->post('name', TRUE);
            $universitydetails['country']=$this->input->post('country', TRUE);
            $universitydetails['city']=$this->input->post('city', TRUE);
            
            $universitydetails['address']=$this->input->post('address', TRUE);
            $universitydetails['phone']=$this->input->post('phone', TRUE);
            $universitydetails['website']=$this->input->post('website', TRUE);
            $universitydetails['email']=$this->input->post('email', TRUE);
            
            $universitydetails['logo']=$this->input->post('logo', TRUE);
            
            $universitydetails['logo_file_name']=$universitydetails['name'];

            $folderupload="logos/universities/";
            $this->load->model('admin/muniversity','muniversity');
            $uploaddetails=$this->muniversity->upload_logo($universitydetails['logo_file_name'],$folderupload,'logo');

            if ($this->nativesession->get('uploadfailed')==TRUE) {
                redirect('admin/university');  
            }

            $universitydetails['logo']= $uploaddetails['logopath'];
            
            $this->muniversity->add_university($universitydetails);
            redirect('admin/university');  
	}
        
        public function update_university()
	{
            $this->load->helper('html');
            $this->load->helper('url');      
            
            
            $universitydetails['university_id']=$this->input->post('university_id', TRUE);
            $universitydetails['name']=$this->input->post('name', TRUE);
            $universitydetails['country']=$this->input->post('country', TRUE);
            $universitydetails['city']=$this->input->post('city', TRUE);
            
            $universitydetails['address']=$this->input->post('address', TRUE);
            $universitydetails['phone']=$this->input->post('phone', TRUE);
            $universitydetails['website']=$this->input->post('website', TRUE);
            $universitydetails['email']=$this->input->post('email', TRUE);
            
            $universitydetails['logo']=$this->input->post('logo', TRUE);
            $universitydetails['logo_old']=$this->input->post('logo_old', TRUE);
            
            $this->load->model('admin/muniversity','muniversity');
            if(isset($_FILES['logo']['name']) && !empty($_FILES['logo']['name'])) {
                $this->load->model('mfile_handler');
                $this->mfile_handler->delete_file($universitydetails['logo_old']);
                
                $universitydetails['logo_file_name']=$universitydetails['name'];
                
                $folderupload="logos/universities/";
                $uploaddetails=$this->muniversity->upload_logo($universitydetails['logo_file_name'],$folderupload,'logo');

                if ($this->nativesession->get('uploadfailed')==TRUE) {
                    redirect('admin/university');  
                }

                $universitydetails['logo']= $uploaddetails['logopath'];
            }else{
                $universitydetails['logo']=$this->input->post('logo_old', TRUE);
            }
            
            $this->muniversity->update_university($universitydetails);
            redirect('admin/university');  
	}
        
        public function remove_university($university_id) {
            $this->load->helper('html');
            $this->load->helper('url');

            $this->load->database();

            $this->load->model('admin/muniversity','muniversity');
            $this->muniversity->remove_university($university_id);

            redirect('admin/university');
        }
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */