<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Computing extends CI_Controller {
	
        public function index($page=NULL) {
            $data = array(
                'title' => 'NSBM',
                'mDescription' => 'Meta desctiptions goes hera',
                'mKeywords' => 'Meta Keywords goes hera',
            );
            
            $this->load->view('vheader_main', $data);
            
                switch ($page){
                case 'undergraduate':
                    $this->load->view('main_sc_com/vcom_undergraduate');
                    break;
                
                case 'postgraduate':
                    $this->load->view('main_sc_com/vcom_postgraduate');
                    break;
                
                case 'foundation':
                    $this->load->view('main_sc_com/vcom_foundation');
                    break;
                
                case 'registrations':
                    $this->load->view('vregistrations');
                    break;
                
                case 'school_union':
                    $this->load->view('main_sc_com/vcom_school_union');
                    break;
                
                case 'school_map':
                    $this->load->view('main_sc_com/vcom_school_map');
                    break;
                
                case 'services':
                    $this->load->view('main_sc_com/vcom_services');
                    break;
                
                case 'library':
                    $this->load->view('main_sc_com/vcom_library');
                    break;
                
                case 'accommodations':
                    $this->load->view('main_sc_com/vcom_accommodations');
                    break;
                
                case 'meet_coordinator':
                    $this->load->view('main_sc_com/vcom_meet_coordinator');
                    break;
                
                case 'faculty_staff':
                    $this->load->view('main_sc_com/vcom_faculty_staff');
                    break;
                
                case 'vacancies':
                    $this->load->view('main_sc_com/vcom_vacancies');
                    break;
                
                default:
                    $this->load->view('vcomputing');
                    break;
                }
                
            $this->load->view('vfooter_main');
	}
        
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */