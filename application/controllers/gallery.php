<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gallery extends CI_Controller {
	
        public function index()
	{
            $data = array(
                'title' => 'NSBM',
                'mDescription' => 'Meta desctiptions goes hera',
                'mKeywords' => 'Meta Keywords goes hera',
            );
            $this->load->helper('html');
            $this->load->helper('url');
            
                $this->load->view('vheader_main', $data);
                $this->load->view('vgallery');
                $this->load->view('vfooter_main');
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */