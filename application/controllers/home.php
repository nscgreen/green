<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	
        public function index(){
            $data = array(
                'title' => 'NSBM',
                'mDescription' => 'Meta desctiptions goes hera',
                'mKeywords' => 'Meta Keywords goes hera',
            );
            
                $this->load->view('vheader_main',$data);
                $this->load->view('vhome');
                $this->load->view('vfooter_main');
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */