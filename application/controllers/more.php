<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class More extends CI_Controller {
	
        public function index($page=NULL) {
            $data = array(
                'title' => 'NSBM',
                'mDescription' => 'Meta desctiptions goes hera',
                'mKeywords' => 'Meta Keywords goes hera',
            );
            $this->load->view('vheader_main', $data);
            
                switch ($page){
                case 'prospective_students':
                    $this->load->view('main_all/vmain_prospective_stu');
                    break;
                
                case 'current_students':
                    $this->load->view('main_all/vmain_current_stu');
                    break;
                
                case 'research':
                    $this->load->view('main_all/vmain_research');
                    break;
                
                case 'e-learning':
                    $this->load->view('main_all/vmain_e_learning');
                    break;
                
                case 'facilities':
                    $this->load->view('main_all/vmain_facilities');
                    break;
                
                case 'alumni_friends':
                    $this->load->view('main_all/vmain_alumni_fnds');
                    break;
                
                case 'sports_health':
                    $this->load->view('main_all/vmain_sports');
                    break;
                
                case 'library':
                    $this->load->view('main_all/vmain_library');
                    break;
                
                case 'well_wishes':
                    $this->load->view('main_all/vmain_wishes');
                    break;
                
                default:
                    $this->load->view('vhome');
                    break;
                }
                
            $this->load->view('vfooter_main');
	}
        
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */