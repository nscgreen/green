<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News extends CI_Controller {
	
        public function index()
	{
            $data = array(
                'title' => 'NSBM',
                'mDescription' => 'Meta desctiptions goes hera',
                'mKeywords' => 'Meta Keywords goes hera',
            );
            
                $this->load->view('vheader_main', $data);
                $this->load->view('vnews');
                $this->load->view('vfooter_main');
	}
        public function read($news_num=NULL){
            $data = array(
                'title' => 'NSBM',
                'mDescription' => 'Meta desctiptions goes hera',
                'mKeywords' => 'Meta Keywords goes hera',
            );
            
                $this->load->view('vheader_main', $data);
                if(isset($news_num)){
                    $this->load->view('main_all/vnews_single',$news_num);
                }
                else {
                    $this->load->view('vnews');
                }
                $this->load->view('vfooter_main');
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */