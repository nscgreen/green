<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jul 19, 2014, 6:14:01 PM
 */

if(!function_exists('create_breadcrumb')){
function create_breadcrumb(){
  $ci = &get_instance();
  $i=1;
  $uri = $ci->uri->segment($i);
  $link = NULL;
  //$link = '<ul>';
 
  while($uri != ''){
    $prep_link = '';
  for($j=1; $j<=$i;$j++){
    $prep_link .= $ci->uri->segment($j).'/';
  }
 
  if($ci->uri->segment($i+1) == ''){
    $link.='<li class="underline"><a class="text-muted" href="'.site_url($prep_link).'"><b>';
    $link.=$ci->uri->segment($i).'</b></a></li> ';
  }else{
    $link.='<li class="underline"> <a href="'.site_url($prep_link).'">';
    $link.=$ci->uri->segment($i).'</a></li> ';
  }
 
  $i++;
  $uri = $ci->uri->segment($i);
  }
    //$link .= '</ul>';
    return $link;
  }
}