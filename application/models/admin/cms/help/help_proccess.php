<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class help_proccess extends CI_Model {

    public function insert_entry($data) {
        if ($this->db->insert('help_cms', $data)){
            return TRUE;
        }  else {
            return FALSE;
        }
    }
    
    
    
    public function getUnique_ArticleTitles($article_category) {       
    $query="SELECT id,slide_title FROM help_cms where category='$article_category' ";
      $unique_SlideName=$this->db->query($query);
      return $unique_SlideName;
        
}


public function delete_entry($id){ 
    $this->db->where('id', $id);
    
    if($this->db->delete('help_cms')){
        return TRUE;
    }  
}



public function getUnique_ArticleDetails($id) {
    $query="SELECT * FROM help_cms Where id=?";
      $Edit_fullArticle=$this->db->query($query,$id);
     // $query = $this->db->query("YOUR QUERY");
     

foreach ($Edit_fullArticle->result() as $row)
{
    $row->id;
    $row->category;
    $row->slide_title;
    $row->title;
    $row->content;
}
      
      return $row;
}




public function last_update($id,$data) {  
    $this->db->where('id', $id);
    $this->db->update('help_cms', $data);
    return TRUE; 
}


}