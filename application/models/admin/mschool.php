<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mschool extends CI_Model {

    function get_schools() {
        $this->load->database();
        
        $this->db->select('s.*,s.school_dean as school_dean_id,p.initial,p.fname,p.lname');
        $this->db->from('school as s');
        $this->db->join('permenent_employee as p', 's.school_dean = p.employee_id');   
        $query = $this->db->get();
        $result=$query->result();
        
        $size=  sizeof($result);
        for($i=0;$i<$size;$i++){
            $result[$i]->school_dean=$result[$i]->initial.' '.$result[$i]->fname.' '.$result[$i]->lname;                           
        }        
        //var_dump($result);
        return $result;
        
    }
    
    public function upload_logo($logoname,$folderupload,$inputfieldname){
        $this->load->model('mfile_handler');

        $config['upload_path'] = './'.$folderupload;
        $config['allowed_types'] = 'gif|jpg|png';
        $config['file_name']	= $logoname;
//    		$config['max_size']	= '0';
        $config['overwrite']=TRUE;
        $this->load->model('mfile_handler');
        $logofilename=$this->mfile_handler->do_upload($inputfieldname,$config);

        $uploaddetails['logopath']= $folderupload.$logofilename;
        $uploaddetails['filename']= $logofilename;
        
        return $uploaddetails;
    }
    
    public function update_school($schooldetails){
        $this->load->database();        
        //Data set to insert into the Database
        $updatedata=array(
            'school_id'=>$schooldetails['school_id'],
            'name'=>$schooldetails['name'],
            'school_dean'=>$schooldetails['school_dean'],
            'phone'=>$schooldetails['phone'],
            'web_page'=>$schooldetails['web_page'],
            'email'=>$schooldetails['email'],
            'logo'=>$schooldetails['logo'],
        );
        //Update the Tute details in the database
        $this->db->where('school_id', $schooldetails['school_id']);
        $this->db->update('school', $updatedata);
        
        $this->nativesession->set('school_updated_successfully',TRUE);
    }
    
    public function add_school($schooldetails){
        $this->load->database();
        //Data set to insert into the Database
        $inserdata=array(
            'school_id'=>$schooldetails['school_id'],
            'name'=>$schooldetails['name'],
            'school_dean'=>$schooldetails['school_dean'],
            'phone'=>$schooldetails['phone'],
            'web_page'=>$schooldetails['web_page'],
            'email'=>$schooldetails['email'],
            'logo'=>$schooldetails['logo'],
        );   
        //Insert the Tute details in the database
        $this->db->insert('school', $inserdata);
        
        $this->nativesession->set('school_added_successfully',TRUE);
    }
    
}
    
