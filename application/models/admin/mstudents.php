<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mstudents extends CI_Model {

    function get_student_by_id($index_nm) {
        $this->load->database();
        
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where(array('index_nm'=>$index_nm));
        $query = $this->db->get();
        $result=$query->result();
        
        return $result;
    }

    function get_student_by_id_for_view($index_nm) {
        $this->load->database();
                
        $this->db->select("s.*,
                g.initial as g_initial,g.fname as g_fname,g.lname as g_lname,
                g.address_line1 as g_address_line1,g.address_line2 as g_address_line2,g.town as g_town,g.city as g_city,g.state as g_state,g.country as g_country,g.post_code as g_post_code,
                g.tel_home as g_tel_home,g.tel_mobile as g_tel_mobile,g.email as g_email,g.realation as g_relation,
                m.email,m.link_fb,m.link_ln,m.mobile,m.profile_pic");        
        $this->db->from('student as s');
        $this->db->join('my_nsbm_stu as m', 's.student_id = m.st_mynsbm_id');
        $this->db->join('guardian as g', 'g.guardian_id = s.student_id');
        $this->db->where('index_nm',$index_nm);
        $query = $this->db->get();
        $result=$query->result();
        //var_dump($result);
        return $result;
    }
    
    function get_students_by_batch_for_view($batch_id) {
        $this->load->database();
        
        $this->db->select("s.*,
                g.initial as g_initial,g.fname as g_fname,g.lname as g_lname,
                g.address_line1 as g_address_line1,g.address_line2 as g_address_line2,g.town as g_town,g.city as g_city,g.state as g_state,g.country as g_country,g.post_code as g_post_code,
                g.tel_home as g_tel_home,g.tel_mobile as g_tel_mobile,g.email as g_email,g.realation as g_relation,
                m.email,m.link_fb,m.link_ln,m.mobile,m.profile_pic");        
        $this->db->from('student as s');
        $this->db->join('my_nsbm_stu as m', 's.student_id = m.st_mynsbm_id');
        $this->db->join('guardian as g', 'g.guardian_id = s.student_id');
        $this->db->join('batch as b', 'b.batch_id = s.stu_batch_id');
        $this->db->where('stu_batch_id',$batch_id);
        
        $query = $this->db->get();
        $result=$query->result();
        //var_dump($result);
        return $result;
    }
    
    public function update_student($studentdetails){
        $this->load->database();        
        //Data set to insert into the Database
        $updatedata=array(
//            'student_id'=>$studentdetails['student_id'],
            'initials'=>$studentdetails['initials'],
            'fname'=>$studentdetails['fname'],
            'mname'=>$studentdetails['mname'],
            'lname'=>$studentdetails['lname'],
            
            'address_line1'=>$studentdetails['address_line1'],
            'address_line2'=>$studentdetails['address_line2'],
            'city'=>$studentdetails['city'],
            'town'=>$studentdetails['town'],
            'state'=>$studentdetails['state'],
            'post_code'=>$studentdetails['post_code'],
            'country'=>$studentdetails['country'],
        );
        //Update the Tute details in the database
        $this->db->where('student_id', $studentdetails['student_id']);
        $this->db->update('student', $updatedata);
        
        $this->nativesession->set('student_updated_successfully',TRUE);
    }
    
    public function add_student($studentdetails){
        $this->load->database();
        //Data set to insert into the Database
        $insertdata=array(
            'student_id'=>$studentdetails['student_id'],
            'index_nm'=>$studentdetails['index_nm'],
            'stu_degree_id'=>$studentdetails['degree'],
            'stu_batch_id'=>$studentdetails['batch'],
            'initials'=>$studentdetails['initials'],
            'fname'=>$studentdetails['fname'],
            'mname'=>$studentdetails['mname'],
            'lname'=>$studentdetails['lname'],
            'sex'=>$studentdetails['sex'],
            
            'dob'=>$studentdetails['dob'],
            'age'=>$studentdetails['age'],
            'address_line1'=>$studentdetails['address_line1'],
            'address_line2'=>$studentdetails['address_line2'],
            'city'=>$studentdetails['city'],
            'town'=>$studentdetails['town'],
            'state'=>$studentdetails['state'],
            'post_code'=>$studentdetails['post_code'],
            'country'=>$studentdetails['country'],
        );
        //Insert the details in the database
        $this->db->insert('student', $insertdata);
        
        //Guardian
        $insertdata2=array(
            'guardian_id'=>$studentdetails['guardian_id'],
            'realation'=>$studentdetails['g_relationship'],
            'title'=>$studentdetails['g_title'],
            'initial'=>$studentdetails['g_initials'],
            'fname'=>$studentdetails['g_fname'],
            'lname'=>$studentdetails['g_lname'],
            'email'=>$studentdetails['g_email'],
            
            'tel_home'=>$studentdetails['g_tel_home'],
            'tel_mobile'=>$studentdetails['g_tel_mobile'],
            'address_line1'=>$studentdetails['g_address_line1'],
            'address_line2'=>$studentdetails['g_address_line2'],
            'city'=>$studentdetails['g_city'],
            'town'=>$studentdetails['g_town'],
            'state'=>$studentdetails['g_state'],
            'post_code'=>$studentdetails['g_post_code'],
            'country'=>$studentdetails['g_country'],
        );
        //Insert the Tute details in the database
        $this->db->insert('guardian', $insertdata2);
        
        
        $this->nativesession->set('student_added_successfully',TRUE);
    }
    
    public function remove_student($student_id){
        $this->load->database();
       $query = $this->db->get_where('my_nsbm_stu', array(
                'st_mynsbm_id'=>$student_id
               ));
       $row=$query->row();
       $filepath=$row->profile_pic;

       $this->load->model('mfile_handler');
       $this->mfile_handler->delete_file($filepath);

       $this->db->where('student_id', $student_id);
       $this->db->delete('student');
       
    }
    
}
    
