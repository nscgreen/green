<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Muniversity extends CI_Model {

    function get_nsbm() {
        $this->load->database();
        
        $this->db->select('*');
        $this->db->from('university');
        $this->db->where(array('university_id'=>'UN-01'));
        $query = $this->db->get();
        $row=$query->row();
        
        return $row;
    }

    function get_universities() {
        $this->load->database();
        
        $this->db->select('*');
        $this->db->from('university');
        $this->db->where('university_id !=','UN-01');
        $query = $this->db->get();
        $result=$query->result();
        
        return $result;
    }
    
    public function upload_logo($logoname,$folderupload,$inputfieldname){
        $this->load->model('mfile_handler');

        $config['upload_path'] = './'.$folderupload;
        $config['allowed_types'] = 'gif|jpg|png';
        $config['file_name']	= $logoname;
    //		$config['max_size']	= '2048';
        $config['overwrite']=TRUE;
        $this->load->model('mfile_handler');
        $logofilename=$this->mfile_handler->do_upload($inputfieldname,$config);

        $uploaddetails['logopath']= $folderupload.$logofilename;
        $uploaddetails['filename']= $logofilename;

        return $uploaddetails;
    }
    
    public function update_university($universitydetails){
        $this->load->database();        
        //Data set to insert into the Database
        $updatedata=array(
            'university_id'=>$universitydetails['university_id'],
            'name'=>$universitydetails['name'],
            'country'=>$universitydetails['country'],
            'city'=>$universitydetails['city'],
            'address'=>$universitydetails['address'],
            'phone'=>$universitydetails['phone'],
            'website'=>$universitydetails['website'],
            'email'=>$universitydetails['email'],
            'logo'=>$universitydetails['logo'],
        );
        //Update the Tute details in the database
        $this->db->where('university_id', $universitydetails['university_id']);
        $this->db->update('university', $updatedata);
        
        $this->nativesession->set('university_updated_successfully');
    }
    
    public function add_university($universitydetails){
        $this->load->database();
        //Data set to insert into the Database
        $updatedata=array(
            'university_id'=>$universitydetails['university_id'],
            'name'=>$universitydetails['name'],
            'country'=>$universitydetails['country'],
            'city'=>$universitydetails['city'],
            'address'=>$universitydetails['address'],
            'phone'=>$universitydetails['phone'],
            'website'=>$universitydetails['website'],
            'email'=>$universitydetails['email'],
            'logo'=>$universitydetails['logo'],
        );
        //Insert the Tute details in the database
        $this->db->insert('university', $updatedata);
        
        $this->nativesession->set('university_added_successfully');
    }
    
    public function remove_university($university_id){
        $this->load->database();
       $query = $this->db->get_where('university', array(
                'university_id'=>$university_id
               ));
       $row=$query->row();
       $filepath=$row->logo;

       $this->load->model('mfile_handler');
       $this->mfile_handler->delete_file($filepath);

       $this->db->where('university_id', $university_id);
       $this->db->delete('university');
       
    }
    
}
    
