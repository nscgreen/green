<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mlogin_user extends CI_Model {

    function validate_login($username,$password) {
        $this->load->database();
        $this->db->where(array('user_name'=>$username,'password'=>$password));
        $query = $this->db->get('admin');
        
        If($query->num_rows == 1){
            return TRUE;
        }else {
            return FALSE;
        }
    }
    
    function validate_user_with_mynsbm($user_type,$login_index,$password) {
        $this->load->database();
        if($user_type==="CO" || $user_type==="PL" || $user_type==="VL" || $user_type==="PE" ){//If the user is a Permanent Employee or a Visiting Lecturer
           $query = $this->db->get_where('my_nsbm_pe', array('pe_mynsbm_id' => $login_index,'password' => $password));
           if ($query->num_rows == 1) {
               $row=$query->row();
               return $row->pe_mynsbm_id;
           }else{//If the user has no access
               return false;
           }
        }elseif($user_type==="ST"){//If the user is a Student $query = $this->db->get_where('my_nsbm_stu', array('st_index' => $login_index,'password' => $password));
           $query = $this->db->get_where('my_nsbm_stu', array('st_index' => $login_index,'password' => $password));
            if ($query->num_rows == 1) {
               $row=$query->row();
               return $row->st_mynsbm_id;
           }else{//If the user has no access
               return false;
           }
        }else{//If the user has no access
               return false;
        }
    }   
    

    function is_Log() {
        if ($this->session->userdata('is_logged_in')) {
            return true;
        }
        return false;
    }

}

