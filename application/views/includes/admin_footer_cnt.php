<!-- JavaScript -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script> <!-- Boostrap JS -->

    <script src="<?php echo base_url(); ?>assets/js/bst_formhelp.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bst_fileinput.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/admin/admin_snap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/admin/admin.js"></script>
<!-- Ajax Wrapper loader -->
    <script>
        function loadwrap($div, $page) {
            $($div).load($page);
        }
        function calajax(page) { // for calendar
            $('#cal').load(page);
        }
        function load_values(id,urlprefix,id2) {
            value = $(id2).val();
            url=urlprefix+'/'+value;
//            alert(url);
            if(value!=="-"){
               $(id).load(url); 
            }
            
        }
    </script>
<!-- Ajax Wrapper loader End -->