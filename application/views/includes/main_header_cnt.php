<!--Fonts-->
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Play' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'> <!--for h3 h4 h5 h6-->
<!--CSS-->
    <link rel="stylesheet" href="../../../assets/css/normalize.min.css">
    <link rel="stylesheet" href="../../../assets/css/reset.min.css">
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../../../assets/css/anime.css">
    <link rel="stylesheet" href="../../../assets/css/bst_formhelp.min.css">
    <link rel="stylesheet" href="../../../assets/css/bst_fileinput.min.css">
    
    <link rel="stylesheet" href="../../../assets/css/main/scrolling-nav.css">
    <link rel="stylesheet" href="../../../assets/css/main/style.css">
    <link rel="stylesheet" href="../../../assets/css/main/site_main.css">
    
    <link rel="stylesheet" href="http://frontend.reklamor.com/fancybox/jquery.fancybox.css" media="screen">

    <style>
        h1{
            font-family: 'Roboto Condensed', sans-serif;
        }
        h3{
            font-family: 'Play', sans-serif;
        }
        p{
            font-family: 'Roboto', sans-serif;
        }
    </style>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

<style>
    .destacados{
        padding: 20px 0;
        text-align: center;
    }
    .destacados > div > div{
        padding: 10px;
        border: 1px solid transparent;
        border-radius: 4px;
        transition: 0.2s;
    }
    .destacados > div:hover > div{
        margin-top: -10px;
        border: 1px solid rgb(200, 200, 200);
        box-shadow: rgba(0, 0, 0, 0.1) 0px 5px 5px 2px;
        background: rgba(200, 200, 200, 0.1);
        transition: 0.5s;
    }
</style>
