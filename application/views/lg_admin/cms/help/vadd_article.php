<?php
/*
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jun 13, 2014, 3:44:18 PM
 */
?>
<?php 
if($added_success==TRUE){
   
?>
    <body onload='loadmodal("#successfull");'>
<?php
    }else{
?>
    <body>        
<?php
    }
?>
<?php $this->load->view('lg_admin/vsnap_panel.php'); ?>  <!--include snap panel-->

    <div id="content" class="snap-content">
<?php $this->load->view('lg_admin/vad_topmenu.php'); ?> <!--include admin top menu-->

        <div class="row" style="margin: 0 10px">
            <div class="panel panel-body">
                <h1 class="text-primary font-play">Manage Help Site <small>- Add article</small>
                    <i class="fa fa-file-text fa-1x text-muted pull-right"></i>
                    <small><i class="fa fa-plus-circle text-muted pull-right"></i></small>
                </h1>
            </div>
            <div class="col-sm-8 well">
                <form class="" role="form" action="<?php echo base_url(); ?>admin/cms/help/add/check_input" method="post">
                    <div class="form-group">
                        <label class="font-label">Select Article Category:</label>
                        <select class="form-control" name="article_category">
                            <option value="main">NSBM main site</option>
                            <option value="mynsbm">my-NSBM</option>
                            <option value="lms">Learning Management System</option>
                            <option value="ems">Event Management System</option>
                            <option value="forum">Forum</option>
                            <option value="communities">Communities</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="font-label">Article Title:</label>
                        <input class="form-control " type="text" required="true" placeholder="Enter article title here" name="article_title" />
                    </div>
                    <div class="form-group">
                        <label class="font-label">Article Menu Title:</label>
                        <input class="form-control " type="text" required="true" placeholder="Enter article menu title here" name="menu_title"/>
                    </div>
                    <div class="form-group">
                        <label class="font-label">Article Content:</label>
                        <textarea class="form-control " type="text" required="true" rows="5" placeholder="Enter article menu title here" name="article_content"></textarea> 
                    </div>
                    <button type="submit" class="btn btn-primary" style="width: 100%">Save Article</button>
                </form>
            </div>

            <div class="col-sm-4">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Hint - Categories </h3>
                            </div>
                            <div class="panel-body" style="padding: 3px">
                                <img class="image-responsive img-polaroid" style="width: 100%" src="../../../../../assets/img/admin/cms/help/categories.jpg">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Hint - Article title </h3>
                            </div>
                            <div class="panel-body" style="padding: 3px">
                                <img class="image-responsive" style="width: 100%" src="../../../../../assets/img/admin/cms/help/categories.jpg">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Hint - Article menu title </h3>
                            </div>
                            <div class="panel-body" style="padding: 3px">
                                <img class="image-responsive img-polaroid" style="width: 100%" src="../../../../../assets/img/admin/cms/help/categories.jpg">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Hint - Article content</h3>
                            </div>
                            <div class="panel-body" style="padding: 3px">
                                <img class="image-responsive img-polaroid" style="width: 100%" src="../../../../../assets/img/admin/cms/help/categories.jpg">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            
            
           

           <!-- Registration Done Modal Start-->
<div class="modal fade" id="successfull" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 0;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #00cc66">Successfully Add Article</h4>
            </div>
            <div class="modal-body">
                <p>Done...!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Done !</button>
            </div>
        </div>
    </div>
</div><!-- Registration Done Modal End-->

<!-- Registration Failed Modal Start-->
<div class="modal fade" id="failed" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 0;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #ff0066">Registration failed</h4>
            </div>
            <div class="modal-body">
                <p>Error...!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-danger">Yes</button>
            </div>
        </div>
    </div>
</div><!-- Registration Failed Modal End-->
