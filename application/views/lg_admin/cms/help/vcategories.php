<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jun 13, 2014, 3:44:18 PM
 */

?>
<body>
    <?php $this->load->view('lg_admin/vsnap_panel.php'); ?>  <!--include snap panel-->
    
    <div id="content" class="snap-content">
        <?php $this->load->view('lg_admin/vad_topmenu.php'); ?> <!--include admin top menu-->
        
        <div class="row" style="margin: 0 10px">
            <div class="panel panel-body">
                <h1 class="text-primary font-play">Manage Help Site <small>- Manage Categories</small>
                    <i class="fa fa-th-list fa-1x text-muted pull-right"></i>
                    <small><i class="fa fa-plus-circle text-muted pull-right"></i></small>
                </h1>
            </div>
            <div class="col-sm-8">
                <div class="row well">
                    <h3 class="text-primary">Add a new category</h3>
                    <form class="" role="form" action="<?php echo base_url(); ?>article/check_addArticle" method="">
                        <div class="form-group">
                            <label class="font-label">Category Name:</label>
                            <input class="form-control " type="text" required="true" placeholder="Enter category name here" />
                        </div>
                        <button type="submit" class="btn btn-primary" style="width: 100%">Save Category</button>
                    </form>
                </div>
                
                <div class="row well">
                    <h3 class="text-primary">Category Details</h3>             
                    <table class="table table-hover table-responsive">
                        <thead>
                            <tr class="alert alert-info">
                                <td>ID</td>
                                <td>Category Name</td>
                                <td><i class="fa fa-pencil-square-o"></i></td>
                            </tr>
                        </thead>
                        <tr>
                            <td>1</td>
                            <td>NSBM-main</td>
                            <td>
                                <a href="" data-toggle="modal" data-target="#edit">&nbsp;<span class="glyphicon glyphicon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></span></a>
                                <a href="" data-toggle="modal" data-target="#delete">&nbsp;<span class="glyphicon glyphicon-remove" data-toggle="tooltip" data-placement="top" title="Delete"></span></a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            
            <div class="col-sm-4">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Hint - Categories </h3>
                            </div>
                            <div class="panel-body" style="padding: 3px">
                                <img class="image-responsive img-polaroid" style="width: 100%" src="../../../../../assets/img/admin/cms/help/categories.jpg">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Hint - Article title </h3>
                            </div>
                            <div class="panel-body" style="padding: 3px">
                                <img class="image-responsive" style="width: 100%" src="../../../../../assets/img/admin/cms/help/categories.jpg">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Hint - Article menu title </h3>
                            </div>
                            <div class="panel-body" style="padding: 3px">
                                <img class="image-responsive img-polaroid" style="width: 100%" src="../../../../../assets/img/admin/cms/help/categories.jpg">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Hint - Article content</h3>
                            </div>
                            <div class="panel-body" style="padding: 3px">
                                <img class="image-responsive img-polaroid" style="width: 100%" src="../../../../../assets/img/admin/cms/help/categories.jpg">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<!-- Edit Modal Start-->
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 0;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #277dcf">Update Category name</h4>
            </div>
            <div class="modal-body">
                <form class="" role="form" action="<?php echo base_url(); ?>article/check_addArticle" method="">
                    <div class="form-group">
                        <label class="font-label">Category Name:</label>
                        <input class="form-control " type="text" required="true" placeholder="Enter category name here" />
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-success">Update</button>
                </form>
            </div>
        </div>
    </div>
</div><!-- Edit Modal End-->

<!-- Delete Modal Start-->
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 0;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #ff0066">Do you want to delete this university details..?</h4>
            </div>
            <div class="modal-body">
                <p>If you delete this Record data will no longer display any more, and it will remove from whole database</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-danger">Yes</button>
            </div>
        </div>
    </div>
</div><!-- Delete Modal End-->

<!-- Registration Done Modal Start-->
<div class="modal fade" id="successfull" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 0;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #00cc66">Successfully registerd</h4>
            </div>
            <div class="modal-body">
                <p>Done...!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Done !</button>
            </div>
        </div>
    </div>
</div><!-- Registration Done Modal End-->

<!-- Registration Failed Modal Start-->
<div class="modal fade" id="failed" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 0;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #ff0066">Registration failed</h4>
            </div>
            <div class="modal-body">
                <p>Error...!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-danger">Yes</button>
            </div>
        </div>
    </div>
</div><!-- Registration Failed Modal End-->
