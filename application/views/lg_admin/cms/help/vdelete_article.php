<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jun 13, 2014, 3:44:18 PM
 */

?>



<?php 
if(isset($_POST['select_article'])){
$id= $_POST['select_article']; 
?>
    <body onload='loadmodal("#delete");'>
<?php

    }elseif($delete_success==TRUE){
   
?>
    <body onload='loadmodal("#delete_successfull");'>
<?}else{
?>
    <body>        
<?php
    }
?>
    <?php $this->load->view('lg_admin/vsnap_panel.php'); ?>  <!--include snap panel-->
    
    <div id="content" class="snap-content">
        <?php $this->load->view('lg_admin/vad_topmenu.php'); ?> <!--include admin top menu-->
        
        <div class="row" style="margin: 0 10px">
            <div class="panel panel-body">
                <h1 class="text-primary font-play">Manage Help Site <small>- Delete article</small>
                    <i class="fa fa-file-text fa-1x text-muted pull-right"></i>
                    <small><i class="fa fa-minus-circle text-muted pull-right"></i></small>
                </h1>
            </div>
            <div class="col-sm-8">
                <div class="row well">
                    <form class="" role="form" action="#" method="GET">
                        <div class="form-group">
                            <label class="font-label">Select Article Category:</label>
                            <select id="select_category" class="form-control" name="article_category" onchange='for_edit("#select_article","<?php echo base_url(); ?>admin/cms/help/delete/category_loadMenu","#select_category" );'>
                                <option value="main">NSBM main site</option>
                                <option value="mynsbm">my-NSBM</option>
                                <option value="lms">Learning Management System</option>
                                <option value="ems">Event Management System</option>
                                <option value="forum">Forum</option>
                                <option value="communities">Communities</option>
                            </select>
                        </div>
                       
                    </form>
                </div>
                <div class="row well">
                    <form class="" role="form" action="" method="POST" >
                        <div class="form-group">
                            <label class="font-label">Select Article Title:</label>
                            <select class="form-control" name="select_article" id="select_article">
<!--                                <option value="main">NSBM main site</option>
                                <option value="myNsbm">my-NSBM</option>
                                <option value="eLearning">Learning Management System</option>
                                <option value="ems">Event Management System</option>
                                <option value="forum">Forum</option>
                                <option value="communities">Communities</option>-->
                            </select>
                        </div>
                        <button type="submit" class="btn btn-danger"  style="width: 100%">Delete</button>
                    </form>
                </div>
                
                <div id="edit-article-load" class=""></div>
            </div>
            
            <div class="col-sm-4">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Hint - Categories </h3>
                            </div>
                            <div class="panel-body" style="padding: 3px">
                                <img class="image-responsive img-polaroid" style="width: 100%" src="../../../../../assets/img/admin/cms/help/categories.jpg">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Hint - Article title </h3>
                            </div>
                            <div class="panel-body" style="padding: 3px">
                                <img class="image-responsive" style="width: 100%" src="../../../../../assets/img/admin/cms/help/categories.jpg">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Hint - Article menu title </h3>
                            </div>
                            <div class="panel-body" style="padding: 3px">
                                <img class="image-responsive img-polaroid" style="width: 100%" src="../../../../../assets/img/admin/cms/help/categories.jpg">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Hint - Article content</h3>
                            </div>
                            <div class="panel-body" style="padding: 3px">
                                <img class="image-responsive img-polaroid" style="width: 100%" src="../../../../../assets/img/admin/cms/help/categories.jpg">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            
            
       
<!-- Modal -->
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Are You Sure...</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
<!--        <button type="button" class="btn btn-primary">Yes</button>-->
        <a href="<?php echo base_url(); ?>admin/cms/help/delete/deleteArticle/<?php echo $id; ?> " class="btn btn-primary">Yes</a>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="delete_successfull" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 0;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #00cc66">Successfully Delete Article</h4>
            </div>
            <div class="modal-body">
                <p>Done...!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Done !</button>
            </div>
        </div>
    </div>
</div>