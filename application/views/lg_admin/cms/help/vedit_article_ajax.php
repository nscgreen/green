<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jun 13, 2014, 3:44:18 PM
 */

?>
<div class="row well">
    <form class="" role="form" action="<?php echo base_url(); ?>admin/cms/help/edit/last_modify" method="post" >
        <input type="hidden" value="<?php echo $unique->id; ?>" name="article_id" >
                        <div class="form-group">
                            <label class="font-label">Select Article Category:</label>
                            <select id="select_category" class="form-control" name="new_article_category">
                                <option value="<?php echo $unique->category ?>">-- Select --</option>
                                <option value="main">NSBM main site</option>
                                <option value="mynsbm">my-NSBM</option>
                                <option value="lms">Learning Management System</option>
                                <option value="ems">Event Management System</option>
                                <option value="forum">Forum</option>
                                <option value="communities">Communities</option>
                            </select>
                        </div>
        <div class="form-group">
            <label class="font-label">Article Title:</label>
            <input class="form-control " type="text" name="new_article_title" required="true" value="<?php echo $unique->title;?>" />
        </div>
        <div class="form-group">
            <label class="font-label">Article Menu Title:</label>
            <input class="form-control " type="text" name="new_menu_title" required="true" value="<?php echo $unique->slide_title; ?>" />
        </div>
        <div class="form-group">
            <label class="font-label">Article Content:</label>
            <textarea class="form-control " type="text" name="new_article_content" required="true" rows="5" placeholder="Enter article menu title here">
                <?php echo $unique->content; ?>
            </textarea>
        </div>
        <button type="submit" class="btn btn-primary"  style="width: 100%">Edit Article</button>
    </form>
</div>