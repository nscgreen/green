<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jun 13, 2014, 3:44:18 PM
 */

?>
<body>
    <?php $this->load->view('lg_admin/vsnap_panel.php'); ?>  <!--include snap panel-->
    
    <div id="content" class="snap-content">
        <?php $this->load->view('lg_admin/vad_topmenu.php'); ?> <!--include admin top menu-->
        
        <div class="row" style="margin: 0 10px">
            <div class="panel panel-body">
                <h1 class="text-primary font-play">Manage Help Site <small>Here you can manage CMS of help.nsbm.lk</small></h1>
            </div>
            <div class="col-sm-3">
                <div class="thumbnail" style="padding: 0;">
                    <div class="caption" style="text-align: center">
                        <i class="fa fa-plus-circle fa-3x"></i><br /><br />
                        <i class="fa fa-file-text fa-5x"></i>
                        <h1 class="text-info">Add</h1>
                        <h3 class="text-primary">Article</h3>
                        <a href="<?php echo base_url(); ?>admin/cms/help/add/article" class="btn btn-primary btn-lg" style="width: 100%" role="button">Manage</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="thumbnail" style="padding: 0;">
                    <div class="caption" style="text-align: center">
                        <i class="fa fa-pencil-square-o fa-3x"></i><br /><br />
                        <i class="fa fa-file-text fa-5x"></i>
                        <h1 class="text-info">Edit</h1>
                        <h3 class="text-primary">Article</h3>
                        <a href="<?php echo base_url(); ?>admin/cms/help/edit/article" class="btn btn-primary btn-lg" style="width: 100%" role="button">Manage</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="thumbnail" style="padding: 0;">
                    <div class="caption" style="text-align: center">
                        <i class="fa fa-minus-circle fa-3x"></i><br /><br />
                        <i class="fa fa-file-text fa-5x"></i>
                        <h1 class="text-info">Delete</h1>
                        <h3 class="text-primary">Article</h3>
                        <a href="<?php echo base_url(); ?>admin/cms/help/delete/article" class="btn btn-primary btn-lg" style="width: 100%" role="button">Manage</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="thumbnail" style="padding: 0;">
                    <div class="caption" style="text-align: center">
                        <i class="fa fa-pencil-square-o fa-3x"></i><br /><br />
                        <i class="fa fa-th-list fa-5x"></i>
                        <h1 class="text-info">Manage</h1>
                        <h3 class="text-primary">Menu item</h3>
                        <a href="<?php echo base_url(); ?>admin/cms/help/category" class="btn btn-primary btn-lg" style="width: 100%" role="button">Manage</a>
                    </div>
                </div>
            </div>
        </div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>