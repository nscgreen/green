<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Add Article</title>
    </head>
    <body>
        <h1 align="center">Add Article</h1><br><br>

        <form method="post" action="<?php echo base_url(); ?>article/check_addArticle">


            <table>

                <tr>
                    <td>Select Category:</td>
                    <td>
                        <select name="article_category">
                            
                            <option value="main">main</option>
                            <option value="myNsbm">My-NSBM</option>
                            <option value="eLearning">E-Learning</option>
                            <option value="ems">EMS</option>
                            <option value="communities">Communities</option>
                            <option value="forum">Forum</option>
                        </select>

                    </td>
                <hr>
                </tr>

                <tr>
                    <td>Article Title:</td>
                    <td><input type="text" name="article_title"  required=""></td>
                </tr>

                <tr>
                    <td>Slider Name:</td>
                    <td><input type="text" name="article_slideName"  required=""></td>
                </tr>

                <tr>
                    <td>Article Author:</td>
                    <td><input type="text" name="article_author" required=""></td>
                </tr>
                
                <tr>
                    <td>Article Content:</td>
                    <td><textarea  name="article_content"   cols="30" rows="15"></textarea>
                    </td>
                </tr>

                <tr>
                    <td></td>
                    <td><input type="submit" value="Add Article"></td>
                </tr>
            </table>
      
            

        </form>

    </body>
</html>
