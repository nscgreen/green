<?php
/*
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jun 13, 2014, 3:44:18 PM
 */
?>
<?php 
if($added_success==TRUE){
   
?>
    <body onload='loadmodal("#successfull");'>
<?php
    }else{
?>
    <body>        
<?php
    }
?>
<?php $this->load->view('lg_admin/vsnap_panel.php'); ?>  <!--include snap panel-->

    <div id="content" class="snap-content">
<?php $this->load->view('lg_admin/vad_topmenu.php'); ?> <!--include admin top menu-->

        <div class="row" style="margin: 0 10px">
            <div class="panel panel-body">
                <h1 class="text-success font-play">Manage Main Site <small>- Manage gallery</small>
                    <i class="fa fa-file-text fa-1x text-muted pull-right"></i>
                    <small><i class="fa fa-plus-circle text-muted pull-right"></i></small>
                </h1>
            </div>
            <div class="col-sm-6">
                <form class="well" role="form" action="<?php echo base_url(); ?>admin/cms/help/add/check_input" method="post">
                    <h2 class="text-info">Add a new Gallery</h2>
                    <div class="form-group">
                        <label class="font-label">Gallery Title:</label>
                        <input class="form-control " type="text" required="true" placeholder="Enter article title here" name="article_title" />
                    </div>
                    <div class="form-group">
                        <label class="font-label">Select a gallery main image:</label>
                        <input id="input-2" type="file" class="file" data-show-upload="false" data-show-caption="true" name="logo" required>
                    </div>
                    <button type="submit" class="btn btn-success" style="width: 100%">Add a Gallery</button>
                </form>
                <div class="row" style="margin: 0px;">
                    <div class="col-sm-12" style="padding: 0">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h4 class="text-success">Published Gallery List</h4>
                            </div>
                            <table class="table table-hover table-responsive">
                                <thead>
                                    <tr class="alert alert-success">
                                        <td>ID</td>
                                        <td>Title</td>
                                        <td>#images</td>
                                        <td><i class="fa fa-pencil-square-o"></i></td>
                                    </tr>
                                </thead>
                                <tr>
                                    <td>01</td>
                                    <td>BSc (Hons) in Management Information Systems (University College of Dublin)</td>
                                    <td>50</td>
                                    <td>
                                        <a href="" data-toggle="modal" data-target="#fulldetails"><span class="fa fa-info" data-toggle="tooltip" data-placement="top" title="Full details"></span></a>
                                        <a href="" data-toggle="modal" data-target="#edit">&nbsp;<span class="glyphicon glyphicon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></span></a>
                                        <a href="" data-toggle="modal" data-target="#delete">&nbsp;<span class="glyphicon glyphicon-remove" data-toggle="tooltip" data-placement="top" title="Delete"></span></a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Hint - Article title </h3>
                            </div>
                            <div class="panel-body">
                                <h2 class="text-info">Add new images to a gallery</h2>
                        <form class="" role="form" action="<?php echo base_url(); ?>admin/cms/help/add/check_input" method="post">
                            <div class="form-group">
                                <label class="font-label">Select a Gallery:</label>
                                <select class="form-control" name="article_category">
                                    <option value="main">NSBM main site</option>
                                    <option value="mynsbm">my-NSBM</option>
                                    <option value="lms">Learning Management System</option>
                                    <option value="ems">Event Management System</option>
                                    <option value="forum">Forum</option>
                                    <option value="communities">Communities</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="font-label">Select image:</label>
                                <input id="input-2" type="file" class="file" data-show-upload="false" data-show-caption="true" name="logo" required>
                            </div>
                            <div class="form-group">
                                <label class="font-label">Select image:</label>
                                <input id="input-2" type="file" class="file" data-show-upload="false" data-show-caption="true" name="logo" required>
                            </div>
                            <div class="form-group">
                                <label class="font-label">Select image:</label>
                                <input id="input-2" type="file" class="file" data-show-upload="false" data-show-caption="true" name="logo" required>
                            </div>
                            <div class="form-group">
                                <label class="font-label">Select image:</label>
                                <input id="input-2" type="file" class="file" data-show-upload="false" data-show-caption="true" name="logo" required>
                            </div>
                            <div class="form-group">
                                <label class="font-label">Select image:</label>
                                <input id="input-2" type="file" class="file" data-show-upload="false" data-show-caption="true" name="logo" required>
                            </div>
                            <button type="submit" class="btn btn-success" style="width: 100%">Add images</button>
                        </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            
            
           

<!-- Registration Done Modal Start-->
<div class="modal fade" id="successfull" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 0;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #00cc66">Successfully Add Article</h4>
            </div>
            <div class="modal-body">
                <p>Done...!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Done !</button>
            </div>
        </div>
    </div>
</div><!-- Registration Done Modal End-->

<!-- Registration Failed Modal Start-->
<div class="modal fade" id="failed" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 0;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #ff0066">Registration failed</h4>
            </div>
            <div class="modal-body">
                <p>Error...!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-danger">Yes</button>
            </div>
        </div>
    </div>
</div><!-- Registration Failed Modal End-->
