<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jun 13, 2014, 3:44:18 PM
 */

?>
<body>
    <?php $this->load->view('lg_admin/vsnap_panel.php'); ?>  <!--include snap panel-->
    
    <div id="content" class="snap-content">
        <?php $this->load->view('lg_admin/vad_topmenu.php'); ?> <!--include admin top menu-->
        
        <div class="row" style="margin: 0 10px">
            <div class="panel panel-body">
                <h1 class="text-success font-play">Manage NSBM-Main Site <small>Here you can manage CMS of www.nsbm.lk</small></h1>
            </div>
            <div class="col-sm-3">
                <div class="thumbnail" style="padding: 0;">
                    <div class="caption" style="text-align: center">
                        <i class="fa fa-plus-circle fa-3x"></i><br /><br />
                        <i class="fa fa-file-text fa-5x"></i>
                        <h3 class="text-primary">Manage</h3>
                        <h1 class="text-info">Gallery</h1>
                        <a href="<?php echo base_url();?>admin/cms/main/manage/gallery" class="btn btn-success btn-lg" style="width: 100%" role="button">Manage</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="thumbnail" style="padding: 0;">
                    <div class="caption" style="text-align: center">
                        <i class="fa fa-pencil-square-o fa-3x"></i><br /><br />
                        <i class="fa fa-file-text fa-5x"></i>
                        <h3 class="text-primary">Manage</h3>
                        <h1 class="text-info">News</h1>
                        <a href="<?php echo base_url();?>admin/cms/main/manage/news" class="btn btn-success btn-lg" style="width: 100%" role="button">Manage</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="thumbnail" style="padding: 0;">
                    <div class="caption" style="text-align: center">
                        <i class="fa fa-minus-circle fa-3x"></i><br /><br />
                        <i class="fa fa-file-text fa-5x"></i>
                        <h3 class="text-primary">Manage</h3>
                        <h1 class="text-info">Articles</h1>
                        <a href="#" class="btn btn-success btn-lg" style="width: 100%" role="button">Manage</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="thumbnail" style="padding: 0;">
                    <div class="caption" style="text-align: center">
                        <i class="fa fa-pencil-square-o fa-3x"></i><br /><br />
                        <i class="fa fa-th-list fa-5x"></i>
                        <h3 class="text-primary">Course</h3>
                        <h1 class="text-info">Commencement</h1>
                        <a href="#" class="btn btn-success btn-lg" style="width: 100%" role="button">Manage</a>
                    </div>
                </div>
            </div>
        </div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="well">
                </div>
            </div>
            <div class="col-lg-4">
                <div class="well">
                </div>
            </div>  
        </div>