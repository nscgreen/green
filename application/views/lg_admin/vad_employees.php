<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jun 13, 2014, 3:44:18 PM
 */

?>
<body>
    <?php include 'vsnap_panel.php';?> <!--include snap panel-->
    
    <div id="content" class="snap-content">
        <?php include 'vad_topmenu.php';?> <!--include admin top menu-->
        
        <div class="row" style="margin: 0 10px">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4 class="text-info">Manage Employees <i class="fa fa-users fa-2x pull-right"></i></h4>
                    </div>
                    <table id="dev-table" class="table table-hover table-responsive">
                        <thead>
                            <tr class="alert alert-info">
                                <td>Emp-id</td>
                                <td>Type</td>
                                <td>Name</td>
                                <td>Gender</td>
                                <td>Email</td>
                                <td>Phone</td>
                                <td><i class="fa fa-pencil-square-o"></i></td>
                            </tr>
                        </thead>
                        <tr>
                            <td>UD-001</td>
                            <td>BSc (Hons) in Management Information Systems (University College of Dublin)</td>
                            <td>2 yr</td>
                            <td>2 yr</td>
                            <td>Computing</td>
                            <td>Univercity Collage Dublin</td>
                            <td>
                                <a href="" data-toggle="modal" data-target="#fulldetails"><span class="fa fa-info" data-toggle="tooltip" data-placement="top" title="Full details"></span></a>
                                <a href="" data-toggle="modal" data-target="#edit">&nbsp;<span class="glyphicon glyphicon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></span></a>
                                <a href="" data-toggle="modal" data-target="#delete">&nbsp;<span class="glyphicon glyphicon-remove" data-toggle="tooltip" data-placement="top" title="Delete"></span></a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        
        <div class="row" style="margin: 0 10px">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title font-roboto">Add new Employee</h3>
                        <span class="pull-right clickable-panel" style="margin-top: -23px; font-size: 15px; margin-right: -9px;" data-toggle="tooltip" data-placement="top" title="Click">
                            <i class="glyphicon glyphicon-minus"></i></span>
                    </div>
                    <div class="panel-body">
                        <form class="" role="form" action="" method="" autocomplete="off">
                            <div class="form-group">
                                <label class="font-label">Employee First Name:</label>
                                <input type="text" class="form-control" required="true" placeholder="Enter employee name here" />
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label class="font-label">Initials:</label>
                                        <input type="text" class="form-control" placeholder="Enter initials here">
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="font-label">Employee Last Name:</label>
                                        <input type="text" class="form-control" placeholder="Enter last name here">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label class="font-label">Gender:</label>
                                        <select class="form-control">
                                            <option>Male</option>
                                            <option>Female</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="font-label">Employee ID:</label>
                                        <input type="text" class="form-control" placeholder="Enter employee id here (Ex: EI-001)">
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="font-label">Employee Type:</label>
                                        <select class="form-control">
                                            <option>DN (for Dean)</option>
                                            <option>CO (for Coordinator)</option>
                                            <option>PL (for Permanent lecturer)</option>
                                            <option>EM (for Normal Employee)</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label class="font-label">Email:</label>
                                        <input type="text" class="form-control" placeholder="Enter email here">
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="font-label">Phone:</label>
                                        <input type="text" class="form-control" placeholder="Enter phone number here">
                                    </div>
                                </div>
                            </div>
                        </form>             
                    </div>
                    <div class="panel-footer">
                        <div class="pull-right">
                            <button class="btn btn-default" data-toggle="modal" data-target="#failed">Close</button>
                            <button type="submit" class="btn btn-success" data-toggle="modal" data-target="#successfull">Register</button>
                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
              
<!-- Full Details Modal Start-->
<div id="fulldetails" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #00cc66">University Profile</h4>
            </div>
            <div class="modal-body">
                <img class="image-responsive img-circle center-block" src="../../../logos/nsbmwhite.gif">
                <div style="text-align: center">
                <h2>University Collage Dublin</h2>
                <h4>Country</h4>
                <h4>City</h4>
                <h4>Web : <a href="">http://www.ucd.com</a></h4>
                <h4>E-mail : sfjafkak@knkf.com</h4>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div><!-- Full Details Modal End-->
        
<!-- Degree-Edit Modal Start-->
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 0;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #277dcf">Update university details</h4>
            </div>
            <div class="modal-body">
                <form class="" role="form" action="" method="">
                    <div class="form-group">
                        <label class="font-label">Degree Name:</label>
                        <input type="text" class="form-control" placeholder="Enter degree name here">
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="font-label">Degree Type:</label>
                                <select class="form-control">
                                    <option>Undergraduate Degree</option>
                                    <option>Postgraduate Degree</option>
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <label class="font-label">Degree Course Period:</label>
                                <select class="form-control">
                                    <?php
                                    for ($i = 1; $i < 5; $i++) {
                                        echo'<option>' . $i . ' Year</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="font-label">School:</label>
                                <select class="form-control">
                                    <option>Business</option>
                                    <option>Computing</option>
                                    <option>Engineering</option>
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <label class="font-label">University:</label>
                                <select class="form-control">
                                    <option>University College Dublin</option>
                                    <option>University College Dublin</option>
                                    <option>Engineering</option>
                                </select>
                            </div>
                        </div>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-success">Update</button>
                </form>
            </div>
        </div>
    </div>
</div><!-- Degree-Edit Modal End-->

<!-- Degree-Delete Modal Start-->
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 0;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #ff0066">Do you want to delete this degree details..?</h4>
            </div>
            <div class="modal-body">
                <p>If you delete this Record data will no longer display any more, and it will remove from whole database</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-danger">Yes</button>
            </div>
        </div>
    </div>
</div><!-- Delete Modal End-->

<!-- Registration Done Modal Start-->
<div class="modal fade" id="successfull" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 0;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #00cc66">Successfully registerd</h4>
            </div>
            <div class="modal-body">
                <p>Done...!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Done !</button>
            </div>
        </div>
    </div>
</div><!-- Registration Done Modal End-->

<!-- Registration Failed Modal Start-->
<div class="modal fade" id="failed" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 0;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #ff0066">Registration failed</h4>
            </div>
            <div class="modal-body">
                <p>Error...!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-danger">Yes</button>
            </div>
        </div>
    </div>
</div><!-- Registration Failed Modal End-->