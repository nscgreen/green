<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jun 13, 2014, 3:44:18 PM
 */

?>
<body>
    <?php include 'vsnap_panel.php';?> <!--include snap panel-->
    
    <div id="content" class="snap-content">
        <?php include 'vad_topmenu.php';?> <!--include admin top menu-->
        
        <div class="row" style="margin: 0 10px">
            <div class="col-sm-5" style="">
            <h2 class="font-roboto h1-shadow">Manage University & Schools</h2>
            <hr />
                <div class="row alert alert-info" style="">
                    <div class="col-xs-3" style="text-align: center">
                        <span class="fa fa-university fa-4x text-primary"></span>
                    </div>
                    <div class="col-xs-6" style="">
                        <h3 class="font-play underline"><a href="<?php echo base_url(); ?>admin/university"><span  class="text-info">Manage University</span></a></h3>
                    </div>
                    <div class="col-xs-3" style="">
                        <a href="<?php echo base_url(); ?>admin/university" class="text-muted"><span class="fa fa-angle-double-right fa-4x"></span></a>
                    </div>
                </div>
                <div class="row alert alert-info" style="">
                    <div class="col-xs-3" style="text-align: center">
                        <span class="fa fa-bookmark-o fa-4x text-primary"></span>
                    </div>
                    <div class="col-xs-6" style="">
                        <h3 class="font-play underline"><a href="<?php echo base_url(); ?>admin/schools"><span  class="text-info">Manage Schools</span></a></h3>
                    </div>
                    <div class="col-xs-3" style="">
                        <a href="<?php echo base_url(); ?>admin/schools" class="text-muted"><span class="fa fa-angle-double-right fa-4x"></span></a>
                    </div>
                </div>
            <h2 class="font-roboto h1-shadow">Manage Students</h2>
            <hr />
                <div class="row alert alert-success" style="">
                    <div class="col-xs-3" style="text-align: center">
                        <span class="fa fa-pencil fa-4x text-success"></span>
                    </div>
                    <div class="col-xs-6" style="">
                        <h3 class="font-play underline"><a href="<?php echo base_url(); ?>admin/students/register"><span  class="text-info">Students Register</span></a></h3>
                    </div>
                    <div class="col-xs-3" style="">
                        <a href="<?php echo base_url(); ?>admin/students/register" class="text-muted"><span class="fa fa-angle-double-right fa-4x"></span></a>
                    </div>
                </div>
                <div class="row alert alert-success" style="">
                    <div class="col-xs-3" style="text-align: center">
                        <span class="fa fa-star-o fa-4x text-success"></span>
                    </div>
                    <div class="col-xs-6" style="">
                        <h3 class="font-play underline"><a href="<?php echo base_url(); ?>admin/students/manage"><span  class="text-info">Manage Students</span></a></h3>
                    </div>
                    <div class="col-xs-3" style="">
                        <a href="<?php echo base_url(); ?>admin/students/manage" class="text-muted"><span class="fa fa-angle-double-right fa-4x"></span></a>
                    </div>
                </div>
            </div>
            <div class="col-xs-1"></div>
            <div class="col-sm-6" style="">
                <div class="row" style="">
                    <h2 class="font-roboto h1-shadow">Manage Degrees, Batches & Modules</h2>
                    <hr />
                    <div class="col-sm-3" style="">
                        <div class="row alert alert-warning" style=" padding: 0">
                            <div class="col-xs-12" style="padding-left: 17%; padding-top: 5%">
                                <span class="fa fa-graduation-cap fa-3x text-warning"></span>
                            </div><br />
                            <div class="col-xs-9" style="">
                                <h3 class="font-play underline"><a href="<?php echo base_url(); ?>admin/degree"><span  class="text-warning">Degrees</span></a></h3>
                            </div>
                            <div class="col-xs-3" style="padding-top: 10px">
                                <a href="<?php echo base_url(); ?>admin/degree" class="text-muted"><span class="fa fa-angle-double-right fa-3x"></span></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-1"></div>
                    <div class="col-sm-3" style="">
                        <div class="row alert alert-warning" style=" padding: 0">
                            <div class="col-xs-12" style="padding-left: 17%; padding-top: 5%">
                                <span class="fa fa-tags fa-3x text-warning"></span>
                            </div><br />
                            <div class="col-xs-9" style="">
                                <h3 class="font-play underline"><a href="<?php echo base_url(); ?>admin/batches"><span  class="text-warning">Batches</span></a></h3>
                            </div>
                            <div class="col-xs-3" style="padding-top: 10px">
                                <a href="<?php echo base_url(); ?>admin/batches" class="text-muted"><span class="fa fa-angle-double-right fa-3x"></span></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-1"></div>
                    <div class="col-sm-3" style="">
                        <div class="row alert alert-warning" style=" padding: 0">
                            <div class="col-xs-12" style="padding-left: 17%; padding-top: 5%">
                                <span class="fa fa-book fa-3x text-warning"></span>
                            </div><br />
                            <div class="col-xs-9" style="">
                                <h3 class="font-play underline"><a href="<?php echo base_url(); ?>admin/modules"><span  class="text-warning">Modules</span></a></h3>
                            </div>
                            <div class="col-xs-3" style="padding-top: 10px">
                                <a href="<?php echo base_url(); ?>admin/modules" class="text-muted"><span class="fa fa-angle-double-right fa-3x"></span></a>
                            </div>
                        </div>
                    </div>
                    <h2 class="font-roboto h1-shadow">Manage Employees</h2>
            <hr />
            <div class="col-sm-3" style="">
                <div class="row well well-sm" style="">
                    <div class="col-xs-12" style="">
                        <span class="fa fa-users fa-2x text-primary"></span>
                    </div><br />
                    <div class="col-xs-9" style="">
                        <h4 class="font-play underline"><a href="<?php echo base_url(); ?>admin/employees"><span  class="text-info">All</span></a></h4>
                    </div>
                    <div class="col-xs-3" style="padding-top: 7px">
                        <a href="<?php echo base_url(); ?>admin/employees" class="text-muted"><span class="fa fa-angle-double-right fa-2x"></span></a>
                    </div>
                </div>
            </div>
            <div class="col-xs-1"></div>
            <div class="col-sm-3" style="">
                <div class="row well well-sm" style="">
                    <div class="col-xs-12" style="">
                        <span class="fa fa-user fa-2x text-primary"></span>
                    </div><br />
                    <div class="col-xs-9" style="">
                        <h4 class="font-play underline"><a href="<?php echo base_url(); ?>admin/employees/permanent_lecturers"><span  class="text-info">Permanent</span></a></h4>
                    </div>
                    <div class="col-xs-3" style="padding-top: 7px">
                        <a href="<?php echo base_url(); ?>admin/employees/permanent_lecturers" class="text-muted"><span class="fa fa-angle-double-right fa-2x"></span></a>
                    </div>
                </div>
            </div>
            <div class="col-xs-1"></div>
            <div class="col-sm-3" style="">
                <div class="row well well-sm" style="">
                    <div class="col-xs-12" style="">
                        <span class="fa fa-user fa-2x text-primary"></span>
                    </div><br />
                    <div class="col-xs-9" style="">
                        <h4 class="font-play underline"><a href="<?php echo base_url(); ?>admin/employees/visiting_lecturers"><span  class="text-info">Visiting</span></a></h4>
                    </div>
                    <div class="col-xs-3" style="padding-top: 7px">
                        <a href="<?php echo base_url(); ?>admin/employees/visiting_lecturers" class="text-muted"><span class="fa fa-angle-double-right fa-2x"></span></a>
                    </div>
                </div>
            </div>
            
            <h2 class="font-roboto h1-shadow">Manage CMS Sites</h2>
            <hr />
            <div class="col-sm-5" style="">
                <div class="row alert alert-danger" style="padding: 0">
                    <div class="col-xs-12" style="padding-top: 5%">
                        <span class="fa fa-list-alt fa-2x text-error"></span>
                    </div><br />
                    <div class="col-xs-9" style="">
                        <h4 class="font-play underline"><a href="<?php echo base_url();?>admin/cms/main"><span class="text-warning">NSBM CMS</span></a></h4>
                    </div>
                    <div class="col-xs-3" style="margin-top: -5px">
                        <a href="" class="text-muted"><span class="fa fa-angle-double-right fa-2x"></span></a>
                    </div>
                </div>
            </div>
            <div class="col-xs-1"></div>
            <div class="col-sm-5" style="">
                <div class="row alert alert-danger" style="padding: 0">
                    <div class="col-xs-12" style="padding-top: 5%">
                        <span class="fa fa-list-alt fa-2x text-error"></span>
                    </div><br />
                    <div class="col-xs-9" style="">
                        <h4 class="font-play underline"><a href="<?php echo base_url(); ?>admin/cms/help"><span  class="text-warning">Help CMS</span></a></h4>
                    </div>
                    <div class="col-xs-3" style="margin-top: -5px">
                        <a href="<?php echo base_url(); ?>admin/cms/help" class="text-muted"><span class="fa fa-angle-double-right fa-2x"></span></a>
                    </div>
                </div>
            </div>
                </div>
            </div>
        </div>
        