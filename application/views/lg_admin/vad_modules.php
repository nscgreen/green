<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jun 13, 2014, 3:44:18 PM
 */

?>
<body>
    <?php include 'vsnap_panel.php';?> <!--include snap panel-->
    
    <div id="content" class="snap-content">
        <?php include 'vad_topmenu.php';?> <!--include admin top menu-->
        
        <div class="row" style="margin: 0 10px">
            <div class="col-sm-12">
                <div class=" well well-sm">
                    <h3 class="font-play text-muted">School of Business module details</h3>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="media">
                                <a class="pull-left" href="#">
                                    <img class="media-object" src="<?php echo base_url() ?>logos/management-logo.png" style="width: 80px;"/>
                                </a>
                                <div class="media-body">
                                    <h4 class="text-info">Undergraduate Modules</h4>
                                </div>
                            </div>
                            <span class="pull-right clickable-panel" style="margin-top: -40px; font-size: 15px; margin-right: -9px;" data-toggle="tooltip" data-placement="top" title="Click">
                                <i class="glyphicon glyphicon-minus"></i></span>
                        </div>
                        <div class="panel-body" style="padding: 0">
                            <table id="dev-table" class="table table-hover table-responsive">
                                <thead>
                                    <tr class="alert alert-success">
                                        <td>M-Code</td>
                                        <td>Name</td>
                                        <td>Degree Id</td>
                                        <td>Academic Year</td>
                                        <td>Semester</td>
                                        <td><i class="fa fa-pencil-square-o"></i></td>
                                    </tr>
                                </thead>
                                <tr>
                                    <td>BSC-001</td>
                                    <td>C Language</td>
                                    <td>UN-001</td>
                                    <td>1 Year</td>
                                    <td>2 Semester</td>
                                    <td>
                                        <a href="" data-toggle="modal" data-target="#edit">&nbsp;<span class="glyphicon glyphicon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></span></a>
                                        <a href="" data-toggle="modal" data-target="#delete">&nbsp;<span class="glyphicon glyphicon-remove" data-toggle="tooltip" data-placement="top" title="Delete"></span></a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="media">
                                <a class="pull-left" href="#">
                                    <img class="media-object" src="<?php echo base_url() ?>logos/management-logo.png" style="width: 80px;"/>
                                </a>
                                <div class="media-body">
                                    <h4 class="text-info">Postgraduate Modules</h4>
                                </div>
                            </div>
                            <span class="pull-right clickable-panel" style="margin-top: -40px; font-size: 15px; margin-right: -9px;" data-toggle="tooltip" data-placement="top" title="Click">
                                <i class="glyphicon glyphicon-minus"></i></span>
                        </div>
                        <div class="panel-body" style="padding: 0">
                            <table id="dev-table" class="table table-hover table-responsive">
                                <thead>
                                    <tr class="alert alert-success">
                                        <td>M-Code</td>
                                        <td>Name</td>
                                        <td>Degree Id</td>
                                        <td>Academic Year</td>
                                        <td>Semester</td>
                                        <td><i class="fa fa-pencil-square-o"></i></td>
                                    </tr>
                                </thead>
                                <tr>
                                    <td>BSC-001</td>
                                    <td>C Language</td>
                                    <td>UN-001</td>
                                    <td>1 Year</td>
                                    <td>2 Semester</td>
                                    <td>
                                        <a href="" data-toggle="modal" data-target="#edit">&nbsp;<span class="glyphicon glyphicon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></span></a>
                                        <a href="" data-toggle="modal" data-target="#delete">&nbsp;<span class="glyphicon glyphicon-remove" data-toggle="tooltip" data-placement="top" title="Delete"></span></a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin: 0 10px">
            <div class="col-sm-12">
                <div class=" well well-sm">
                    <h3 class="font-play text-muted">School of Computing module details</h3>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="media">
                                <a class="pull-left" href="#">
                                    <img class="media-object" src="<?php echo base_url() ?>logos/computing-logo.png" style="width: 80px;"/>
                                </a>
                                <div class="media-body">
                                    <h4 class="text-info">Undergraduate Modules</h4>
                                </div>
                            </div>
                            <span class="pull-right clickable-panel" style="margin-top: -40px; font-size: 15px; margin-right: -9px;" data-toggle="tooltip" data-placement="top" title="Click">
                                <i class="glyphicon glyphicon-minus"></i></span>
                        </div>
                        <div class="panel-body" style="padding: 0">
                            <table id="dev-table" class="table table-hover table-responsive">
                                <thead>
                                    <tr class="alert alert-info">
                                        <td>M-Code</td>
                                        <td>Name</td>
                                        <td>Degree Id</td>
                                        <td>Academic Year</td>
                                        <td>Semester</td>
                                        <td><i class="fa fa-pencil-square-o"></i></td>
                                    </tr>
                                </thead>
                                <tr>
                                    <td>BSC-001</td>
                                    <td>C Language</td>
                                    <td>UN-001</td>
                                    <td>1 Year</td>
                                    <td>2 Semester</td>
                                    <td>
                                        <a href="" data-toggle="modal" data-target="#edit">&nbsp;<span class="glyphicon glyphicon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></span></a>
                                        <a href="" data-toggle="modal" data-target="#delete">&nbsp;<span class="glyphicon glyphicon-remove" data-toggle="tooltip" data-placement="top" title="Delete"></span></a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="media">
                                <a class="pull-left" href="#">
                                    <img class="media-object" src="<?php echo base_url() ?>logos/computing-logo.png" style="width: 80px;"/>
                                </a>
                                <div class="media-body">
                                    <h4 class="text-info">Postgraduate Modules</h4>
                                </div>
                            </div>
                            <span class="pull-right clickable-panel" style="margin-top: -40px; font-size: 15px; margin-right: -9px;" data-toggle="tooltip" data-placement="top" title="Click">
                                <i class="glyphicon glyphicon-minus"></i></span>
                        </div>
                        <div class="panel-body" style="padding: 0">
                            <table id="dev-table" class="table table-hover table-responsive">
                                <thead>
                                    <tr class="alert alert-info">
                                        <td>M-Code</td>
                                        <td>Name</td>
                                        <td>Degree Id</td>
                                        <td>Academic Year</td>
                                        <td>Semester</td>
                                        <td><i class="fa fa-pencil-square-o"></i></td>
                                    </tr>
                                </thead>
                                <tr>
                                    <td>BSC-001</td>
                                    <td>C Language</td>
                                    <td>UN-001</td>
                                    <td>1 Year</td>
                                    <td>2 Semester</td>
                                    <td>
                                        <a href="" data-toggle="modal" data-target="#edit">&nbsp;<span class="glyphicon glyphicon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></span></a>
                                        <a href="" data-toggle="modal" data-target="#delete">&nbsp;<span class="glyphicon glyphicon-remove" data-toggle="tooltip" data-placement="top" title="Delete"></span></a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin: 0 10px">
            <div class="col-sm-12">
                <div class=" well well-sm">
                    <h3 class="font-play text-muted">School of Engineering module details</h3>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="media">
                                <a class="pull-left" href="#">
                                    <img class="media-object" src="<?php echo base_url() ?>logos/engineering-logo.png" style="width: 80px;"/>
                                </a>
                                <div class="media-body">
                                    <h4 class="text-info">Undergraduate Modules</h4>
                                </div>
                            </div>
                            <span class="pull-right clickable-panel" style="margin-top: -40px; font-size: 15px; margin-right: -9px;" data-toggle="tooltip" data-placement="top" title="Click">
                                <i class="glyphicon glyphicon-minus"></i></span>
                        </div>
                        <div class="panel-body" style="padding: 0">
                            <table id="dev-table" class="table table-hover table-responsive">
                                <thead>
                                    <tr class="alert alert-warning">
                                        <td>M-Code</td>
                                        <td>Name</td>
                                        <td>Degree Id</td>
                                        <td>Academic Year</td>
                                        <td>Semester</td>
                                        <td><i class="fa fa-pencil-square-o"></i></td>
                                    </tr>
                                </thead>
                                <tr>
                                    <td>BSC-001</td>
                                    <td>C Language</td>
                                    <td>UN-001</td>
                                    <td>1 Year</td>
                                    <td>2 Semester</td>
                                    <td>
                                        <a href="" data-toggle="modal" data-target="#edit">&nbsp;<span class="glyphicon glyphicon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></span></a>
                                        <a href="" data-toggle="modal" data-target="#delete">&nbsp;<span class="glyphicon glyphicon-remove" data-toggle="tooltip" data-placement="top" title="Delete"></span></a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="media">
                                <a class="pull-left" href="#">
                                    <img class="media-object" src="<?php echo base_url() ?>logos/engineering-logo.png" style="width: 80px;"/>
                                </a>
                                <div class="media-body">
                                    <h4 class="text-info">Postgraduate Modules</h4>
                                </div>
                            </div>
                            <span class="pull-right clickable-panel" style="margin-top: -40px; font-size: 15px; margin-right: -9px;" data-toggle="tooltip" data-placement="top" title="Click">
                                <i class="glyphicon glyphicon-minus"></i></span>
                        </div>
                        <div class="panel-body" style="padding: 0">
                            <table id="dev-table" class="table table-hover table-responsive">
                                <thead>
                                    <tr class="alert alert-warning">
                                        <td>M-Code</td>
                                        <td>Name</td>
                                        <td>Degree Id</td>
                                        <td>Academic Year</td>
                                        <td>Semester</td>
                                        <td><i class="fa fa-pencil-square-o"></i></td>
                                    </tr>
                                </thead>
                                <tr>
                                    <td>BSC-001</td>
                                    <td>C Language</td>
                                    <td>UN-001</td>
                                    <td>1 Year</td>
                                    <td>2 Semester</td>
                                    <td>
                                        <a href="" data-toggle="modal" data-target="#edit">&nbsp;<span class="glyphicon glyphicon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></span></a>
                                        <a href="" data-toggle="modal" data-target="#delete">&nbsp;<span class="glyphicon glyphicon-remove" data-toggle="tooltip" data-placement="top" title="Delete"></span></a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                
        <div class="row" style="margin: 0 10px">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title font-roboto">Add new module</h3>
                        <span class="pull-right clickable-panel" style="margin-top: -23px; font-size: 15px; margin-right: -9px;" data-toggle="tooltip" data-placement="top" title="Click">
                            <i class="glyphicon glyphicon-minus"></i></span>
                    </div>
                    <div class="panel-body">
                        <form class="" role="form" action="" method="">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label class="font-label">Module Name:</label>
                                        <input type="text" class="form-control" placeholder="Enter degree name here">
                                    </div>
                                    <div class="col-sm-2">
                                        <label class="font-label">Module-Code:</label>
                                        <input type="text" class="form-control" placeholder="Enter degree name here">
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="font-label">Degree Id:</label>
                                        <select class="form-control">
                                            <option>Business</option>
                                            <option>Computing</option>
                                            <option>Engineering</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label class="font-label">Academic Year:</label>
                                        <select class="form-control">
                                            <?php
                                            for ($i = 1; $i < 6; $i++) {
                                                echo'<option>'.$i.' Year</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="font-label">Semester:</label>
                                        <select class="form-control">
                                            <?php
                                            for ($i = 1; $i < 3; $i++) {
                                                echo'<option>Semester '.$i.'</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="font-label">Description:</label>
                                <textarea class="form-control" rows="3" placeholder="Enter module description here"></textarea>
                            </div>
                            </form>
                    </div>
                    <div class="panel-footer">
                        <div class="pull-right">
                            <button type="reset" class="btn btn-default">Close</button>
                            <button class="btn btn-success" data-toggle="modal" data-target="#successfull">Add Module</button>
                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row" style="margin: 0 10px">
            <div class="col-sm-2">
                <div class="thumbnail" style="padding: 5px;">
                    <img class="image-responsive" style="width: 50%" src="<?php echo base_url()?>logos/computing-logo.png" />
                    <div class="caption" style="text-align: center">
                        <h4 class="font-roboto text-primary">Foundation  for Computer Science</h4>
                    </div>
                    <div class="caption">
                        <table class="table table-responsive">
                            <tbody>
                                <tr>
                                    <td>Type:</td>
                                    <td>Undergraduate</td>
                                </tr>
                                <tr>
                                    <td>Code:</td>
                                    <td>BSC-003</td>
                                </tr>
                            </tbody>
                        </table>
                        <a href="#" data-toggle="modal" data-target="#editnsbm" class="btn btn-default btn-sm" style="width: 100%" role="button"><span class="fa fa-info text-info"></span></a>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="thumbnail" style="padding: 5px;">
                    <img class="image-responsive" style="width: 50%" src="<?php echo base_url()?>logos/computing-logo.png" />
                    <div class="caption" style="text-align: center">
                        <h4 class="font-roboto text-primary">Foundation  for Computer Science</h4>
                    </div>
                    <div class="caption">
                        <table class="table table-responsive">
                            <tbody>
                                <tr>
                                    <td>Type:</td>
                                    <td>Undergraduate</td>
                                </tr>
                                <tr>
                                    <td>Code:</td>
                                    <td>BSC-003</td>
                                </tr>
                            </tbody>
                        </table>
                        <a href="#" data-toggle="modal" data-target="#editnsbm" class="btn btn-default" style="width: 100%" role="button"><span class="fa fa-info-circle fa-1x text-info"></span></a>
                    </div>
                </div>
            </div>
        </div> <!--Modules row finish-->
        
<!-- Full Details Modal Start-->
<div id="fulldetails" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #00cc66">University Profile</h4>
            </div>
            <div class="modal-body">
                <img class="image-responsive img-circle center-block" src="../../../logos/nsbmwhite.gif">
                <div style="text-align: center">
                <h2>University Collage Dublin</h2>
                <h4>Country</h4>
                <h4>City</h4>
                <h4>Web : <a href="">http://www.ucd.com</a></h4>
                <h4>E-mail : sfjafkak@knkf.com</h4>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div><!-- Full Details Modal End-->

<!-- Edit Modal Start-->
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 0;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #277dcf">Update module details</h4>
            </div>
            <div class="modal-body">
                <form class="" role="form" action="" method="">
                    <div class="form-group">
                        <label class="font-label">Module Name:</label>
                        <input type="text" class="form-control" placeholder="Enter degree name here">
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-4">
                                <label class="font-label">Module-Code:</label>
                                <input type="text" class="form-control" placeholder="Enter degree name here">
                            </div>
                            <div class="col-sm-8">
                                <label class="font-label">Degree Id:</label>
                                <select class="form-control">
                                    <option>Business</option>
                                    <option>Computing</option>
                                    <option>Engineering</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="font-label">Academic Year:</label>
                                <select class="form-control">
                                    <?php
                                    for ($i = 1; $i < 6; $i++) {
                                        echo'<option>' . $i . ' Year</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <label class="font-label">Semester:</label>
                                <select class="form-control">
                                    <?php
                                    for ($i = 1; $i < 3; $i++) {
                                        echo'<option>Semester ' . $i . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="font-label">Description:</label>
                        <textarea class="form-control" rows="3" placeholder="Enter module description here"></textarea>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-success">Update</button>
                </form>
            </div>
        </div>
    </div>
</div><!-- Edit Modal End-->

<!-- NSBM-Edit Modal Start-->
<div class="modal fade" id="editnsbm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 0;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #277dcf">Update Details</h4>
            </div>
            <div class="modal-body">
                <form class="" role="form" action="" method="">
                    <div class="form-group">
                        <label class="font-label">University Name :</label>
                        <input type="text" class="form-control" placeholder="Enter university name here">
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="font-label">Country :</label>
                                <input type="text" class="form-control" placeholder="Country">
                            </div>
                            <div class="col-sm-6">
                                <label class="font-label">City :</label>
                                <input type="text" class="form-control" placeholder="City">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="font-label">Address:</label>
                        <input type="text" class="form-control" value="Enter university name here">
                    </div>
                    <div class="form-group">
                        <label class="font-label">Web Site:</label>
                        <input type="text" class="form-control" placeholder="http:// ...">
                    </div>
                    <div class="form-group">
                        <label class="font-label">Email:</label>
                        <input type="text" class="form-control" placeholder="University email">
                    </div>
                    <div class="form-group">
                        <label for="fileName" class="font-label">Logo: </label>
                        <input id="input-2" type="file" class="file" data-show-upload="false" data-show-caption="true">
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger">Update</button>
                </form>
            </div>
        </div>
    </div>
</div><!-- NSBM-Edit Modal End-->

<!-- Delete Modal Start-->
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 0;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #ff0066">Do you want to delete this university details..?</h4>
            </div>
            <div class="modal-body">
                <p>If you delete this Record data will no longer display any more, and it will remove from whole database</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-danger">Yes</button>
            </div>
        </div>
    </div>
</div><!-- Delete Modal End-->

<!-- Registration Done Modal Start-->
<div class="modal fade" id="successfull" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 0;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #00cc66">Successfully registerd</h4>
            </div>
            <div class="modal-body">
                <p>Done...!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Done !</button>
            </div>
        </div>
    </div>
</div><!-- Registration Done Modal End-->

<!-- Registration Failed Modal Start-->
<div class="modal fade" id="failed" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 0;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #ff0066">Registration failed</h4>
            </div>
            <div class="modal-body">
                <p>Error...!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-danger">Yes</button>
            </div>
        </div>
    </div>
</div><!-- Registration Failed Modal End-->