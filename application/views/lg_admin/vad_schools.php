<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jun 13, 2014, 3:44:18 PM
 */

?>
<?php if($added_successfully==TRUE){ ?>
<body onload='loadmodal("#successfull");'>
<?php }elseif($updated_successfully==TRUE){ ?>
<body onload='loadmodal("#updated");'>
<?php }elseif($edit_details==TRUE){ ?>
<body onload='loadmodal("#edit");'>
<?php }else{ ?>
<body>
<?php } ?>
    <?php include 'vsnap_panel.php';?> <!--include snap panel-->
    
    <div id="content" class="snap-content">
        <?php include 'vad_topmenu.php';?> <!--include admin top menu-->
        
        
        
<!------------ Start View current Schools ----------->
<?php 
$i=1;
foreach ($schools as $school_details) {
?>
        <?php if($i===4) { ?>
        <div class="row" style="margin: 0 10px">
        <?php } ?>    
            <div class="col-sm-4">
                <div class="thumbnail" style="padding: 5px;">
                    <img class="image-responsive" src="<?php echo base_url().'./'.$school_details->logo ?>" />
                    <div class="caption" style="text-align: center">
                        <h3 class="font-roboto text-primary"><?php echo strtoupper($school_details->name); ?></h3>
                    </div>
                    <div class="caption">
                        <table class="table table-responsive">
                            <tbody>
                                <tr>
                                    <td>School ID:</td>
                                    <td><?php echo $school_details->school_id; ?></td>
                                </tr>
                                <tr>
                                    <td>Dean:</td>
                                    <td><?php echo $school_details->school_dean; ?></td>
                                </tr>
                                <tr>
                                    <td>Phone:</td>
                                    <td><?php echo $school_details->phone; ?></td>
                                </tr>
                                <tr>
                                    <td>Web:</td>
                                    <td><a href="<?php echo $school_details->web_page; ?>" target="_blank"><?php echo $school_details->web_page; ?></a></td>
                                </tr>
                                <tr>
                                    <td>Email:</td>
                                    <td><a href="mailto:<?php echo $school_details->email; ?>"><?php echo $school_details->email; ?></a></td>
                                </tr>
                            </tbody>
                        </table>
                        <form action="" method="post">
                                    <input type="hidden" name="type" value="edit"/>
                                    <input type="hidden" name="school_id" value="<?php echo $school_details->school_id ?>"/>
                                    <input type="hidden" name="name" value="<?php echo $school_details->name ?>"/>
                                    <input type="hidden" name="school_dean_id" value="<?php echo $school_details->school_dean_id ?>"/>
                                    <input type="hidden" name="phone" value="<?php echo $school_details->phone ?>"/>
                                    <input type="hidden" name="web_page" value="<?php echo $school_details->web_page ?>"/>
                                    <input type="hidden" name="email" value="<?php echo $school_details->email ?>"/>
                                    <input type="hidden" name="logo" value="<?php echo $school_details->logo ?>"/>
                                    
                                    <button type="submit" class="btn btn-default btn-lg" style="width: 100%" role="button">Update</button>
                        </form>
                        
                    </div>
                </div>
            </div>
            
        <?php if($i===4) { 
            //echo 'always inside end';
            ?>    
        </div>    
        <?php } ?>
<?php
        if($i===4){
            $i=1;
        }else{
        $i++;  
        }
}
?>

<!------------ End View current Schools ----------->

        
        <div class="row" style="margin: 0 10px">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title font-roboto">Add new School</h3>
                        <span class="pull-right clickable-panel" style="margin-top: -23px; font-size: 15px; margin-right: -9px;" data-toggle="tooltip" data-placement="top" title="Click">
                            <i class="glyphicon glyphicon-minus"></i></span>
                    </div>
                    <div class="panel-body">
                        <form class="" role="form" action="<?php echo base_url().'admin/schools/add_school';?>" method="POST" enctype="multipart/form-data">
                            <div class="form-group">
                                <label class="font-label">School ID:</label>
                                <input type="text" class="form-control" placeholder="School ID" name="school_id" required>
                            </div>
                            <div class="form-group">
                                <label class="font-label">School Name:</label>
                                <input type="text" class="form-control" placeholder="School Name" name="name" required>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label class="font-label">Dean:</label>
                                        <input type="text" class="form-control" placeholder="Employee id of Dean" name="school_dean_id" required>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="font-label">Phone:</label>
                                        <input type="text" class="form-control" placeholder="Phone" name="phone" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label class="font-label">Web:</label>
                                        <input type="text" class="form-control" placeholder="http:// ..." name="web_page" required>
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="font-label">Email:</label>
                                        <input type="text" class="form-control" placeholder="School email" name="email" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="fileName" class="font-label">Logo: </label>
                                <input id="input-2" type="file" class="file" data-show-upload="false" data-show-caption="true" name="logo" required>
                            </div>
                            
                    </div>
                    <div class="panel-footer">
                        <div class="pull-right">
                            <!--<input class="btn btn-success" type="submit" value="Register"/>-->                        
                            <button class="btn btn-success" type="submit">Register</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                
            
        


<!-- School-Edit Modal Start-->

<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 0;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #277dcf">Update Details</h4>
            </div>
            <?php if($edit_details==TRUE){ ?>
            <div class="modal-body">
                <form class="" role="form" action="<?php echo base_url().'admin/schools/update_school';?>" method="POST" enctype="multipart/form-data">
                    <?php $_POST; ?>
                    <input type="hidden" name="school_id" value="<?php echo $_POST['school_id'] ?>"/>
                    <input type="hidden" name="logo_old" value="<?php echo $_POST['logo'] ?>"/>
                    
                    <div class="form-group">
                        <label class="font-label">School Name:</label>
                        <input type="text" class="form-control" placeholder="School Name" name="name" value="<?php echo $_POST['name'] ?>" required>
                    </div>
                    <div class="form-group">
                        <label class="font-label">Dean:</label>
                        <input type="text" class="form-control" placeholder="Employee id of Dean" name="school_dean_id" value="<?php echo $_POST['school_dean_id'] ?>" required>
                    </div>
                    <div class="form-group">
                        <label class="font-label">Phone:</label>
                        <input type="text" class="form-control" placeholder="City" name="phone" value="<?php echo $_POST['phone'] ?>" required>

                    </div>
                    <div class="form-group">
                        <label class="font-label">Web:</label>
                        <input type="text" class="form-control" placeholder="http:// ..." name="web_page" value="<?php echo $_POST['web_page'] ?>" required>
                    </div>
                    <div class="form-group">
                        <label class="font-label">Email:</label>
                        <input type="text" class="form-control" placeholder="University email" name="email" value="<?php echo $_POST['email'] ?>" required>
                    </div>
                    <div class="form-group">
                        <label for="fileName" class="font-label">Logo: </label>
                        <input id="input-2" type="file" class="file" data-show-upload="false" data-show-caption="true" name="logo">
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-success">Update</button>
                </form>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
<!-- School Edit Modal End-->

<!-- Delete Modal Start-->
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 0;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #ff0066">Do you want to delete this university details..?</h4>
            </div>
            <div class="modal-body">
                <p>If you delete this Record data will no longer display any more, and it will remove from whole database</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-danger">Yes</button>
            </div>
        </div>
    </div>
</div>
<!-- Delete Modal End-->

<!-- Registration Done Modal Start-->
<div class="modal fade" id="successfull" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #00cc66">School Successfully registered</h4>
            </div>
            <div class="modal-body">
                <p>School updated successfully. Click Update if anything need to be changed.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Done !</button>
            </div>
        </div>
    </div>
</div>
<!-- Registration Done Modal End-->

<!-- Update Done Modal Start-->
<div class="modal fade" id="updated" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #00cc66">School Successfully updated</h4>
            </div>
            <div class="modal-body">
                <p>School updated successfully. Click Update if anything need to be changed.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Done !</button>
            </div>
        </div>
    </div>
</div>
<!-- Update Done Modal End-->

<!-- Registration Failed Modal Start-->
<div class="modal fade" id="failed" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 0;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #ff0066">School Registration or Update failed</h4>
            </div>
            <div class="modal-body">
                <p>Error...!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-danger">Yes</button>
            </div>
        </div>
    </div>
</div>
<!-- Registration Failed Modal End-->