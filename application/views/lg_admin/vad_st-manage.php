<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jun 9, 2014, 12:33:43 AM
 */

?>
<?php if($updated_successfully==TRUE){ ?>
<body onload='loadmodal("#updated");'>
<?php }elseif($view_fulldetails==TRUE){ ?>
<body onload='loadmodal("#profile");'>
<?php }elseif($edit_details==TRUE){ ?>
<body onload='loadmodal("#edit");'>
<?php }elseif($delete==TRUE){ ?>
<body onload='loadmodal("#delete");'>
<?php }else{ ?>
<body>
<?php } ?>
    <?php include 'vsnap_panel.php';?> <!--include snap panel-->
    
    <div id="content" class="snap-content">
        <?php include 'vad_topmenu.php';?> <!--include admin top menu-->

    <!--Student Register Section-->
    <div class="row" style="margin: 0 10px">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h2 class="font-play">Manage Students <i class="fa fa-pencil fa-2x pull-right"></i></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin: 0 10px">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading" style="padding: 0;">
                    <h4 class="alert alert-info"  style="margin: 0;">Filter sudent by ID</h4>
                </div>
                <div class="panel-body">
                    <form class="" role="form" action="" method="">
                        <div class="form-group">
                            <label class="font-label">Index no :</label>
                            <input type="text" class="form-control" placeholder="Index no : (ex: BSC-UCD-CSC-13.1-008)" id="student_id">
                        </div>
                    </form>
                </div>
                <div class="panel-footer">
                    <div class="pull-right">
                        <button class="btn btn-info" onclick='load_values("#student_details","<?php echo base_url(); ?>admin/students/load_student_by_id","#student_id");'>Find</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin: 0 10px">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading" style="padding: 0;">
                    <h4 class="alert alert-info"  style="margin: 0;">Filter sudents by batch</h4>
                </div>
                <div class="panel-body">
                    <form class="" role="form" action="" method="">
                        <div class="form-group">
                            <label class="font-label">Batch ID:</label>
                            <input type="text" class="form-control" placeholder="Batch ID : (ex: UG-01-0001)" id="batch_id">
                        </div>
                    </form>
                </div>
                <div class="panel-footer">
                    <div class="pull-right">
                        <button class="btn btn-info" onclick='load_values("#student_details","<?php echo base_url(); ?>admin/students/load_students_by_batch","#batch_id");'>Find</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row" style="margin: 0 10px">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Students Details</h3>
                    <div class="pull-right" style="margin-top: -18px; font-size: 15px;">
                        <a href="" type="submit" class="" style="color: #ffffff" data-toggle="modal" data-target="#fullmodel">
                            <span class="glyphicon glyphicon-resize-full text-muted"  data-toggle="tooltip" data-placement="top" title="Pull Details"></span>
                        </a>
                    </div>
                </div>
                <div class="panel-body" style="display: none;">
                    <input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-table" placeholder="Filter Students" />
                </div>
                <div id="student_details">
                <table class="table table-hover" id="dev-table">
                    <thead>
                        <tr class="alert alert-info">
                            <th>#</th>
                            <th>Index</th>
                            <th>Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
    
<!--Student full details model-->
<div class="modal fade" id="fullmodel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width: 100%;">
    <div class="modal-dialog modal-lg" style="width: 100%">
        <div class="modal-content" style="width: 100%;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Students Full Details</h4>
            </div>
            <div class="modal-body">
                <div class="panel panel-primary" style="margin: 0">
                    <div class="panel-heading">
                        <h3 class="panel-title">Students Full Details</h3>
                    </div>
                    <table class="table table-hover table-responsive" id="dev-table">
                        <thead>
                            <tr class="alert alert-info">
                                <th>#</th>
                                <th>Index</th>
                                <th>Name</th>
                                <th>Name</th>
                                <th>Name</th>
                                <th>Name</th>
                                <th>E-mail</th>
                                <th>Phone</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            for ($i = 0; $i < 40; $i++) {
                                echo'
                                    <tr>
                                    <td>' . $i . '</td>
                                    <td>BSC-UCD-CSC-13.1-008</td>
                                    <td>H.W.S.P Fonseka</td>
                                    <td>s.priyanga22@gmail.com</td>
                                    <td>s.priyanga22@gmail.com</td>
                                    <td>s.priyanga22@gmail.com</td>
                                    <td>s.priyanga22@gmail.com</td>
                                    <td>0779093013</td>
                                    </tr>
                                    ';
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!--Student full details model END-->

<!--Student details edit model-->
<div id="edit" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Update Student Details</h4>
            </div>
            <?php if($edit_details==TRUE){ ?>
            <div class="modal-body">
                <div class="panel panel-primary" style="margin: 0 auto">
                    <div class="panel-heading">
                        <h3 class="panel-title">Edit Details</h3>
                    </div>
                    
                    <div class="panel-body">
                        <form class="" role="form" action="<?php echo base_url().'admin/students/update_student' ?>" method="POST">
<!--                            <div class="form-group">
                                <label class="font-label">Index no :</label>
                                <input type="text" class="form-control" placeholder="Index no : (ex: BSC-UCD-CSC-13.1-008)" value="<?php //echo $_POST['index_nm']?>" name="index_nm" required>
                            </div>-->
                            <input type="hidden" value="<?php echo $_POST['student_id']?>" name="student_id" required/>
<!--                            <div class="form-group">
                                <div class="row">-->
<!--                                    <div class="col-sm-3">
                                        <label class="font-label">Index no :</label>
                                        <input type="text" class="form-control" placeholder="Index no : (ex: BSC-UCD-CSC-13.1-008)" value="<?php //echo $_POST['index_nm']?>" name="index_nm" required>
                                    </div>-->
<!--                                    <div class="col-sm-2">
                                        <label class="font-label">Course/Degree Type :</label>
                                         <select id="degree_type" name="degree_type" class="form-control" onchange='alert_test();'>
                                        <select id="degree_type" name="degree_type" class="form-control" onchange='load_values("#degree_name","<?php //echo base_url(); ?>admin/select/load_degrees_from_type_for_edit/<?php echo $_POST['lname'] ?>","#degree_type");'>
                                            <option value="-">Select the type</option>
                                            <?php // if($_POST['degree_type']==="FD"){?>
                                            <option value="FD" selected>Foundation Programmes</option>
                                            <?php // }else{ ?>
                                            <option value="FD" >Foundation Programmes</option>
                                            <?php // } ?>
                                            <?php // if($_POST['degree_type']==="UG"){?>
                                            <option value="UG" selected>Undergraduate Programmes</option>
                                            <?php // }else{ ?>
                                            <option value="UG">Undergraduate Programmes</option>
                                            <?php // } ?>
                                            <?php // if($_POST['degree_type']==="PG"){?>
                                            <option value="PG" selected>Postgraduate Programmes</option>
                                            <?php // }else{ ?>
                                            <option value="PG">Postgraduate Programmes</option> 
                                            <?php // } ?>                                     
                                        </select>
                                    </div>-->
<!--                                    <div class="col-sm-4">
                                        <label class="font-label">Last Name :</label>
                                        <input type="text" class="form-control" placeholder="Enter student's last name here" value="<?php //echo $_POST['lname']?>" name="lname" required>
                                    </div>-->
<!--                                </div>
                            </div>-->
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label class="font-label">First Name :</label>
                                        <input type="text" class="form-control" placeholder="Enter student's first name here" value="<?php echo $_POST['fname']?>" name="fname" required>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="font-label">Middle Name :</label>
                                        <input type="text" class="form-control" placeholder="Enter student's last name here" value="<?php echo $_POST['mname']?>" name="mname" >
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="font-label">Last Name :</label>
                                        <input type="text" class="form-control" placeholder="Enter student's last name here" value="<?php echo $_POST['lname']?>" name="lname" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label class="font-label">Initials :</label>
                                        <input type="text" class="form-control" placeholder="Enter student's initials here" value="<?php echo $_POST['initials']?>" name="initials" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="font-label">Address line 1 :</label>
                                <input type="text" class="form-control" placeholder="Enter student's Address line 1 here" value="<?php echo $_POST['address_line1']?>" name="address_line1" required>
                            </div>
                            <div class="form-group">
                                <label class="font-label">Address line 2 :</label>
                                <input type="text" class="form-control" placeholder="Enter student's Address line 1 here" value="<?php echo $_POST['address_line2']?>" name="address_line2" required>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label class="font-label" for="textinput">City</label>
                                        <input type="text" placeholder="City" class="form-control" value="<?php echo $_POST['city']?>" name="city" required>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="font-label" for="textinput">Town</label>
                                        <input type="text" placeholder="City" class="form-control" value="<?php echo $_POST['town']?>" name="town" required>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="font-label" for="textinput">State</label>
                                        <input type="text" placeholder="State" class="form-control" value="<?php echo $_POST['state']?>" name="state" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group"> <!-- Text input-->
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label class="font-label" for="textinput">Postcode</label>
                                        <input type="text" placeholder="Post Code" class="form-control" value="<?php echo $_POST['post_code']?>" name="post_code" required>    
                                    </div>
                                    <div class="col-sm-6"> <!-- Text input-->
                                        <label class="font-label" for="textinput">Country</label>
                                        <input type="text" placeholder="Country" class="form-control" value="<?php echo $_POST['country']?>" name="country" required>
                                    </div>
                                </div>
                            </div>
                        
                    </div>
                </div>
            </div>
            <?php } ?>
            <div class="modal-footer">
                <div class="pull-right">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
                    <button type="submit" class="btn btn-success">Register</button>
                </div>
                </form>
            </div>
            
        </div>
    </div>
</div> 
<!--Student details edit model END-->

<!--Delete Student model-->
<div id="delete" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #ff0066">Do you want to delete this Student..?</h4>
            </div>
            <?php if($delete==TRUE){ ?>
            <form class="" role="form" action="<?php echo base_url().'admin/students/remove_student/'.$_POST['student_id']; ?>" method="">
            <div class="modal-body">
                <h4><small>Index :</small><?php echo $_POST['index_nm'] ; ?></h4>
<!--                <h4><small>Batch :</small> 13.1 Computing</h4>-->
                <h4><small>Name :</small><?php echo $_POST['name'] ; ?></h4>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-danger">Yes</button>
            </div>
            </form>
            <?php } ?>
        </div>
    </div>
</div> 
<!--Delete Student model END-->

<!--Student Message model-->
<div id="message" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #ff0066">Type a Message</h4>
            </div>
            <div class="modal-body">
                <form class="form" role="form" action="" method="">
                        <div class="form-group">
                            <h4>Title</h4>
                            <div class="input-group">
                                <input type="text" class="form-control"/>
                                <span class="input-group-addon danger"><span class="glyphicon glyphicon-star"></span></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <h4>To</h4>
                            <div class="input-group">
                                <input type="text" class="form-control" value="BSC-UCD-SCS-13.1-008 - Fname" disabled/>
                                <span class="input-group-addon info"><span class="glyphicon glyphicon-user"></span></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="message" placeholder="Type in your message" rows="5" style="margin-bottom:10px;"></textarea>
                        </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-info center-block" style="width: 100%">Send</button>
            </div>
        </div>
    </div>
</div> 
<!--Student message model END-->

<!--Student Profile model-->
<!--<div id="profile" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 0;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #00ccff">Student Profile</h4>
            </div>
            <div class="modal-body">-->
                
<div id="profile" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #00ccff">Student Profile</h4>
            </div>
            <?php if($view_fulldetails==TRUE){ ?>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-8 col-xs-8">
                        <h4>Name : <span class="prf-name"> &nbsp; <?php echo $_POST['name'] ; ?></span></h4>
                        <div class="prf-contact">
                        <h5>Degree ID : <span class=""> &nbsp; <?php echo $_POST['degree_id'] ; ?></span></h5>
                        <h5>Voice : <span class=""> &nbsp; <?php echo $_POST['mobile'] ; ?></span></h5>
                        <h5>Address : <span class=""> &nbsp; <?php echo $_POST['address'] ; ?></span></h5>
                        <h5>Email : <span class=""> &nbsp; <?php echo $_POST['email'] ; ?></span></h5>
                        <h5>FB : <span class=""> &nbsp; <?php echo $_POST['link_fb'] ; ?></span></h5>
                        <h5>Linkedin : <span class=""> &nbsp; <?php echo $_POST['link_ln'] ; ?></span></h5>
                        
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-4">
                        <img class="image-responsive img-circle" src="<?php echo $_POST['profile_pic'] ; ?>">
                    </div>
                </div>
                <div class="row">
                    <hr class="hr-style-two">
                    <div class="col-sm-12 col-xs-12">
                        <h4>Guardian Details :</h4>
                        <h5>Name : <span class=""> &nbsp; <?php echo $_POST['g_name'] ; ?></span></h5>
                        <h5>Voice : <span class=""> &nbsp; <?php echo $_POST['g_phone'] ; ?></span></h5>
                        <h5>Email : <span class=""> &nbsp; <?php echo $_POST['g_email'] ; ?></span></h5>
                        <h5>Address : <span class=""> &nbsp; <?php echo $_POST['g_address'] ; ?></span></h5>
                        <h5>Relationship : <span class=""> &nbsp; <?php echo $_POST['relationship'] ; ?></span></h5>
                    </div>
                </div>
            </div>
            <?php } ?>
            <div class="modal-footer">
                <button class="btn btn-info center-block" style="width: 100%" data-dismiss="modal" aria-hidden="true">Close</button>
            </div>
        </div>
    </div>
</div> 
<!--Student Profile model END-->

<!-- Update Done Modal Start-->
<div class="modal fade" id="successfull" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #00cc66">Student Successfully updated.</h4>
            </div>
            <div class="modal-body">
                <p>Student updated successfully. Click on the edit button again if anything needs to be changed.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Done !</button>
            </div>
        </div>
    </div>
</div>
<!-- Update Done Modal End-->