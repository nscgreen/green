<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jun 9, 2014, 12:33:43 AM
 */

?>
<?php if($added_successfully==TRUE){ ?>
<body onload='loadmodal("#successfull");'>
<?php }else{ ?>
<body>
<?php } ?>
    <?php include 'vsnap_panel.php';?> <!--include snap panel-->
    
    <div id="content" class="snap-content">
        <?php include 'vad_topmenu.php';?> <!--include admin top menu-->

    <!--Student Register Section-->
    <div class="row" style="margin: 0 10px">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h2 class="font-play">Register a new Student <i class="fa fa-pencil fa-2x pull-right"></i></h2>
                </div>
                <div class="panel-heading">
                    <h4 class="alert alert-info">Fill the student details</h4>
                </div>
                <div class="panel-body">
                    <form class="" role="form" action="<?php echo base_url().'admin/students/add_student';?>" method="POST">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-2">
                                    <label class="font-label">Student ID :</label>
                                    <input type="text" class="form-control" placeholder="Student ID : (ex: ST-000001)" name="student_id" required>

                                </div>
                                <div class="col-sm-4">
                                    <label class="font-label">Course/Degree Type :</label>
<!--                                    <select id="degree_type" name="degree_type" class="form-control" onchange='alert_test();'>-->
                                    <select id="degree_type" name="degree_type" class="form-control" onchange='load_values("#degree_name","<?php echo base_url(); ?>admin/select/load_degrees_from_type","#degree_type");'>
                                        <option value="-">Select the type</option>
                                        <option value="FD">Foundation Programmes</option>
                                        <option value="UG">Undergraduate Programmes</option>
                                        <option value="PG">Postgraduate Programmes</option>                                        
                                    </select>
                                </div>
                                <div class="col-sm-6" id="degree_name" >
                                    <label class="font-label">Course/Degree Name :</label>
                                    <select class="form-control" id="degree" name="degree">
                                        <option value="-" >Select the Degree</option>
                                    </select>
                                </div>
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6" id="batch_name">
                                    <label class="font-label">Batch Name :</label>
                                    <select class="form-control" name="batch">
                                        <option value="-" >Select the Batch</option>
                                    </select>
                                </div>
                                <div class="col-sm-6" id="batch_name">
                                    <label class="font-label">Index No :</label>
                                    <input type="text" class="form-control" placeholder="Index no : (ex: BSC-UCD-CSC-13.1-008)" name="index_nm" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label class="font-label">First Name :</label>
                                    <input type="text" class="form-control" placeholder="Enter student's first name here" name="fname" required>
                                </div>
                                <div class="col-sm-4">
                                    <label class="font-label">Middle Name :</label>
                                    <input type="text" class="form-control" placeholder="Enter student's first name here" name="mname" >
                                </div>
                                <div class="col-sm-4">
                                    <label class="font-label">Last Name :</label>
                                    <input type="text" class="form-control" placeholder="Enter student's last name here" name="lname" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label class="font-label">Initials :</label>
                                    <input type="text" class="form-control" placeholder="Enter student's initials here" name="initials" required>
                                </div>
                                <div class="col-sm-3">
                                    <label for="fileName" class="font-label">Date of Birth</label><br />
                                    <div class="bfh-datepicker" data-format="y-m-d" data-date="today" style="float: left" data-name="dob" required></div>
                                </div>
                                <div class="col-sm-3">
                                    <label for="fileName" class="font-label">Age</label><br />
                                    <input type="text" class="form-control" placeholder="Enter student's age here" name="age" required>
                                </div>
                                <div class="col-sm-3">
                                    <label class="font-label">Gender :</label>
                                    <select class="form-control" name="sex" required>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                    </select>
                                </div>
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="font-label">Address line 1 :</label>
                            <input type="text" class="form-control" placeholder="Address Line 1 here" name="address_line1" required>
                        </div>
                        <div class="form-group">
                            <label class="font-label">Address line 2 :</label>
                            <input type="text" class="form-control" placeholder="Address Line 2 here" name="address_line2">
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label class="font-label" for="textinput">City</label>
                                    <input type="text" placeholder="City" class="form-control" name="city" required>
                                </div>
                                <div class="col-sm-4">
                                    <label class="font-label" for="textinput">Town</label>
                                    <input type="text" placeholder="State" class="form-control" name="town" required>
                                </div>
                                <div class="col-sm-4">
                                    <label class="font-label" for="textinput">State</label>
                                    <input type="text" placeholder="State" class="form-control" name="state" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group"> <!-- Text input-->
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="font-label" for="textinput">Postcode</label>
                                    <input type="text" placeholder="Post Code" class="form-control" name="post_code" required>    
                                </div>
                                <div class="col-sm-6"> <!-- Text input-->
                                    <label class="font-label" for="textinput">Country</label>
                                    <input type="text" placeholder="Country" class="form-control" name="country" required>
                                </div>
                            </div>
                        </div>


                        <h4 class="alert alert-info">Fill details of the guardian</h4>

                        <div class="form-group">
                            <div class="row">
                                
                                <div class="col-sm-2">
                                    <label class="font-label">Title :</label>
                                    <select class="form-control" name="g_title" required>
                                        <option value="Mr.">Mr.</option>
                                        <option value="Mrs.">Mrs.</option>
                                        <option value="Miss.">Miss.</option>
                                    </select>
                                </div>
                                <div class="col-sm-5">
                                    <label class="font-label">First Name :</label>
                                    <input type="text" class="form-control" placeholder="Enter guardian's first name here" name="g_fname" required>
                                </div>
                                <div class="col-sm-5">
                                    <label class="font-label">Last Name :</label>
                                    <input type="text" class="form-control" placeholder="Enter guardian's last name here" name="f_fname" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-2">
                                    <label class="font-label">Initials :</label>
                                    <input type="text" class="form-control" placeholder="Enter guardian's initials here" name="g_initials" required>
                                </div>
                                <div class="col-sm-2">
                                    <label class="font-label">Relationship :</label>
                                    <input type="text" class="form-control" placeholder="ex: Father/Mother" name="g_relationship" required>
                                </div>                                
                                <div class="col-sm-2">
                                    <label class="font-label">Phone(Home) :</label>
                                    <input type="text" class="form-control" placeholder="Enter guardian's email here" name="g_tel_home" required>
                                </div>
                                <div class="col-sm-2">
                                    <label class="font-label">Phone(Mobile) :</label>
                                    <input type="text" class="form-control" placeholder="Enter guardian's email here" name="g_tel_mobile" required>
                                </div>
                                <div class="col-sm-4">
                                    <label class="font-label">Email :</label>
                                    <input type="text" class="form-control" placeholder="Enter guardian's email here" name="g_email" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="font-label">Address line 1 :</label>
                            <input type="text" class="form-control" placeholder="Enter guardian's address here" name="g_address_line1" required>
                        </div>
                        <div class="form-group">
                            <label class="font-label">Address line 2 :</label>
                            <input type="text" class="form-control" placeholder="Enter guardian's address here" name="g_address_line2" required>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label class="font-label" for="textinput">Town</label>
                                    <input type="text" placeholder="City" class="form-control" name="g_town" required>
                                </div>
                                <div class="col-sm-4">
                                    <label class="font-label" for="textinput">City</label>
                                    <input type="text" placeholder="City" class="form-control" name="g_city" required>
                                </div>
                                <div class="col-sm-4">
                                    <label class="font-label" for="textinput">State</label>
                                    <input type="text" placeholder="State" class="form-control" name="g_state" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group"> <!-- Text input-->
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="font-label" for="textinput">Postcode</label>
                                    <input type="text" placeholder="Post Code" class="form-control" name="g_post_code" required>    
                                </div>
                                <div class="col-sm-6"> <!-- Text input-->
                                    <label class="font-label" for="textinput">Country</label>
                                    <input type="text" placeholder="Country" class="form-control" name="g_country" required>
                                </div>
                            </div>
                        </div>

                </div>
                <div class="panel-footer">
                    <div class="pull-right">
                        <input type="reset" class="btn btn-default" value="Reset" style="width:80px">
                        <button type="submit" class="btn btn-success">Register</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
</div>
    
    
<!-- Registration Done Modal Start-->
<div class="modal fade" id="successfull" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #00cc66">Student Successfully registered</h4>
            </div>
            <div class="modal-body">
                <p>Student registered successfully. Go to Manage Students if anything need to be changed.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Done !</button>
            </div>
        </div>
    </div>
</div>
<!-- Registration Done Modal End-->