<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jun 21, 2014, 11:58:10 PM
 */

?>

<nav class="navbar navbar-default" role="navigation" style="margin: 0 -10px 20px -10px; border-radius: 0">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" id="open-left"><i class="glyphicon glyphicon-th"></i></a>
            <a class="navbar-brand" href="<?php echo base_url();?>admin/dashboard"><span class="font-roboto">Admin Dashboard</span></a>
<!--            <a class="navbar-brand" href=""><img src="http://placehold.it/25x25" /></a>-->
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <form class="navbar-form navbar-right" role="search">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search">
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>

            <ul class="nav navbar-nav navbar-right">
                <li class="menu-links"><a href="<?php echo base_url();?>admin/dashboard" class="ubuntu"><i class="glyphicon glyphicon-home"></i> Home</a></li>
                <li class="menu-links"><a href="#" data-toggle="modal" data-target="#notifi" class="ubuntu"><i class="glyphicon glyphicon-star-empty"></i> Notifications</a></li>
                <li class="menu-links"><a href="#" data-toggle="modal" data-target="#config" class="ubuntu"><i class="glyphicon glyphicon-cog"></i> Setting</a></li>
                <li class="menu-links"><a href="<?php echo base_url()?>admin/home/logout "><i class="glyphicon glyphicon-off"></i> Log out</a></li>
            </ul>
        </div>
    </div>
</nav>

<!-- Degree-Edit Modal Start-->
<div class="modal fade" id="config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 1;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #277dcf">Change Password</h4>
            </div>
            <div class="modal-body">
                <form class="" role="form" action="" method="">
                    <div class="form-group">
                        <label class="font-label">Current Password:</label>
                        <input type="text" class="form-control" placeholder="Enter degree name here">
                    </div>
                    <div class="form-group">
                        <label class="font-label">New Password:</label>
                        <input type="text" class="form-control" placeholder="Enter degree name here">
                    </div>
                    <div class="form-group">
                        <label class="font-label">Confirm Password:</label>
                        <input type="text" class="form-control" placeholder="Enter degree name here">
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-success">Change Password</button>
                </form>
            </div>
        </div>
    </div>
</div><!-- Degree-Edit Modal End-->

<!-- Notification Modal Start-->
<div class="modal fade" id="notifi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 1;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #277dcf">Notifications</h4>
            </div>
            <div class="modal-body">
                <p>There is no new notifications</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-success">Change Password</button>
                </form>
            </div>
        </div>
    </div>
</div><!-- Notification Modal End-->