<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jun 13, 2014, 3:44:18 PM
 */

?>
<?php if($added_successfully==TRUE){ ?>
<body onload='loadmodal("#successfull");'>
<?php }elseif($updated_successfully==TRUE){ ?>
<body onload='loadmodal("#updated");'>
<?php }elseif($view_fulldetails==TRUE){ ?>
<body onload='loadmodal("#fulldetails");'>
<?php }elseif($edit_details==TRUE){ ?>
<body onload='loadmodal("#edit");'>
<?php }else{ ?>
<body>
<?php } ?>
    
    <?php include 'vsnap_panel.php';?> <!--include snap panel-->
    
    <div id="content" class="snap-content">
        <?php include 'vad_topmenu.php';?> <!--include admin top menu-->
        
        <div class="row" style="margin: 0 10px">
            <div class="col-sm-4">
                <div class="thumbnail" style="padding: 5px;">
                    <img class="image-responsive" src="<?php echo base_url().'./'.$nsbm_details->logo ?>" />
                    <div class="caption" style="text-align: center">
                        <h3 class="font-roboto text-primary"><?php echo $nsbm_details->name ?></h3>
                    </div>
                    <div class="caption">
                        <table class="table table-responsive">
                            <tbody>
                                <tr>
                                    <td>Address:</td>
                                    <td><?php echo $nsbm_details->address ?></td>
                                </tr>
                                <tr>
                                    <td>Phone:</td>
                                    <td><?php echo $nsbm_details->phone ?></td>
                                </tr>
                                <tr>
                                    <td>Web:</td>
                                    <td><a href="<?php echo $nsbm_details->website ?>" target="_blank"><?php echo $nsbm_details->website ?></a></td>
                                </tr>
                                <tr>
                                    <td>Email:</td>
                                    <td><a href="mailto:<?php echo $nsbm_details->email ?>"><?php echo $nsbm_details->email ?></a></td>
                                </tr>
                            </tbody>
                        </table>
                        <a href="#" data-toggle="modal" data-target="#editnsbm" class="btn btn-default btn-lg" style="width: 100%" role="button">Update</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class=" well well-sm">
                    <h3 class="font-roboto text-muted">Affiliated University Details</h3>                
                    <table class="table table-hover table-responsive">
                        <thead>
                            <tr class="alert alert-success">
                                <td>ID</td>
                                <td>Name</td>
                                <td>Country</td>
                                <td>Email</td>
                                <td><i class="fa fa-pencil-square-o"></i></td>
                            </tr>
                        </thead>
                        
                        <?php 
                        if(!empty($universities)){ 
                           foreach ($universities as $uni_details){ 
                        ?>
                        <tr>
                            <td><?php echo $uni_details->university_id ?></td>
                            <td><?php echo $uni_details->name ?></td>
                            <td><?php echo $uni_details->country ?></td>
                            <td><?php echo $uni_details->email ?></td>
                            <td>
                                <form action="" method="post">
                                    <input type="hidden" name="type" value="fulldetails"/>
                                    
                                    <input type="hidden" name="logo" value="<?php echo $uni_details->logo ?>"/>
                                    <input type="hidden" name="name" value="<?php echo $uni_details->name ?>"/>
                                    <input type="hidden" name="country" value="<?php echo $uni_details->country ?>"/>
                                    <input type="hidden" name="city" value="<?php echo $uni_details->city ?>"/>
                                    <input type="hidden" name="website" value="<?php echo $uni_details->website ?>"/>
                                    <input type="hidden" name="email" value="<?php echo $uni_details->email ?>"/>
                                    
                                    <button type="submit" style="border: 0;background: none;outline: 0;"><a href="" ><span class="fa fa-info" data-toggle="tooltip" data-placement="top" title="Full details"></span></a></button>
                                </form>
                                
                                <form action="" method="post">
                                    <input type="hidden" name="type" value="edit"/>
                                    <input type="hidden" name="university_id" value="<?php echo $uni_details->university_id ?>"/>
                                    <input type="hidden" name="name" value="<?php echo $uni_details->name ?>"/>
                                    <input type="hidden" name="country" value="<?php echo $uni_details->country ?>"/>
                                    <input type="hidden" name="city" value="<?php echo $uni_details->city ?>"/>
                                    <input type="hidden" name="address" value="<?php echo $uni_details->address ?>"/>
                                    <input type="hidden" name="phone" value="<?php echo $uni_details->phone ?>"/>
                                    <input type="hidden" name="website" value="<?php echo $uni_details->website ?>"/>
                                    <input type="hidden" name="email" value="<?php echo $uni_details->email ?>"/>
                                    <input type="hidden" name="logo" value="<?php echo $uni_details->logo ?>"/>
                                    
                                    <button type="submit" style="border: 0;background: none;outline: 0;"><a href="" >&nbsp;<span class="glyphicon glyphicon-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></span></a></button>
                                </form>
                                <!-- Remove University-->
<!--                                    <button type="button" style="border: 0;background: none;outline: 0;"  >-->
                                <a onclick='loadmodalwith_formaction("#delete","#mdeleteform","<?php echo base_url().'admin/university/remove_university/'.$uni_details->university_id; ?>");' style="cursor: pointer;" >&nbsp;<span class="glyphicon glyphicon-remove" data-toggle="tooltip" data-placement="top" title="Delete"></span></a>
<!--                                        </button>-->
                                <!-- Remove University-->
                                
                                
                            </td>
                        </tr>
                        <?php } 
                           }
                        ?>
                    </table>
                </div>
                
                <div class="row" style="padding: 0 15px">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title font-roboto">Add new university</h3>
                            <span class="pull-right clickable-panel" style="margin-top: -23px; font-size: 15px; margin-right: -9px;" data-toggle="tooltip" data-placement="top" title="Click">
                                <i class="glyphicon glyphicon-minus"></i></span>
                        </div>
                        <div class="panel-body">
                            <form class="" role="form" action="<?php echo base_url().'admin/university/add_university';?>" method="POST" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label class="font-label">University ID :</label>
                                    <input type="text" class="form-control" placeholder="Enter University ID here" name="university_id" required>
                                </div>
                                <div class="form-group">
                                    <label class="font-label">University Name :</label>
                                    <input type="text" class="form-control" placeholder="Enter University name here" name="name" required>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label class="font-label">Country :</label>
                                            <input type="text" class="form-control" placeholder="Country" name="country" required>
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="font-label">City :</label>
                                            <input type="text" class="form-control" placeholder="City" name="city" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="font-label">Address :</label>
                                    <input type="text" class="form-control" placeholder="Address" name="address" required>
                                </div>
                                <div class="form-group">
                                    <label class="font-label">Phone :</label>
                                    <input type="text" class="form-control" placeholder="Phone number" name="phone" required>
                                </div>
                                <div class="form-group">
                                    <label class="font-label">Web Site :</label>
                                    <input type="text" class="form-control" placeholder="http:// ..." name="website" required>
                                </div>
                                <div class="form-group">
                                    <label class="font-label">E-mail :</label>
                                    <input type="text" class="form-control" placeholder="University email" name="email" required>
                                </div>
                                <div class="form-group">
                                    <label for="fileName" class="font-label">Logo : </label>
                                    <input id="input-2" type="file" class="file" data-show-upload="false" data-show-caption="true" name="logo" required>
                                </div>
                            
                        </div>
                        <div class="panel-footer">
                            <div class="pull-right">                                
                                <button class="btn btn-success" type="submit">Register</button>
                            </div>
                           </form>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        
<!-- Full Details Modal Start-->
<div id="fulldetails" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #00cc66">University Profile</h4>
            </div>
            <div class="modal-body">
                <?php if($view_fulldetails==TRUE){ ?>
                <img class="image-responsive img-circle center-block" src="<?php echo base_url().'./'.$_POST['logo'] ?>">
                <div style="text-align: center">
                <h2><?php echo $_POST['name'] ?></h2>
                <h4><?php echo $_POST['country'] ?></h4>
                <h4><?php echo $_POST['city'] ?></h4>
                <h4>Web : <a href=""><?php echo $_POST['website'] ?></a></h4>
                <h4>E-mail : <?php echo $_POST['email'] ?></h4>
                </div>
                <?php } ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- Full Details Modal End-->

<!-- Edit Modal Start-->
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 0;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #277dcf">Update university details</h4>
            </div>
            <div class="modal-body">
                <?php if($edit_details==TRUE){ ?>
                <form class="" role="form" action="<?php echo base_url().'admin/university/update_university';?>" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="university" value="affiliated"/>
                    <input type="hidden" name="university_id" value="<?php echo $_POST['university_id'] ;?>"/>
                    <input type="hidden" name="logo_old" value="<?php echo $_POST['logo'] ;?>"/>
                    <div class="form-group">
                        <label class="font-label">University Name :</label>
                        <input type="text" class="form-control" placeholder="Enter university name here" name="name" required value="<?php echo $_POST['name'] ;?>">
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="font-label">Country :</label>
                                <input type="text" class="form-control" placeholder="Country" name="country" required value="<?php echo $_POST['country'] ;?>">
                            </div>
                            <div class="col-sm-6">
                                <label class="font-label">City :</label>
                                <input type="text" class="form-control" placeholder="City" name="city" required value="<?php echo $_POST['city'] ;?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="font-label">Address:</label>
                        <input type="text" class="form-control" placeholder="Enter university address here" required value="<?php echo $_POST['address'] ;?>" name="address">
                    </div>
                    <div class="form-group">
                        <label class="font-label">Phone:</label>
                        <input type="text" class="form-control" placeholder="Enter phone number here" required value="<?php echo $_POST['phone'] ;?>" name="phone">
                    </div>
                    <div class="form-group">
                        <label class="font-label">Web Site :</label>
                        <input type="text" class="form-control" placeholder="http:// ..." name="website" required value="<?php echo $_POST['website'] ;?>">
                    </div>
                    <div class="form-group">
                        <label class="font-label">E-mail :</label>
                        <input type="text" class="form-control" placeholder="University email" name="email" required value="<?php echo $_POST['email'] ;?>">
                    </div>
                    <div class="form-group">
                        <label for="fileName" class="font-label">Logo : </label>
                        <input id="input-2" type="file" class="file" data-show-upload="false" data-show-caption="true" name="logo" >
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-success">Update</button>
                
                <?php } ?>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Edit Modal End-->

<!-- NSBM-Edit Modal Start-->
<div class="modal fade" id="editnsbm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #277dcf">Update Details</h4>
            </div>
            <div class="modal-body">
                <form class="" role="form" action="university/update_university" method="POST" enctype="multipart/form-data" >
                    <input type="hidden" name="university" value="nsbm"/>
                    <div class="form-group">
                        <label class="font-label">University Name :</label>
                        <input type="text" class="form-control" placeholder="Enter university name here" value="<?php echo $nsbm_details->name ?>" name="name">
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="font-label">Country :</label>
                                <input type="text" class="form-control" placeholder="Country" value="<?php echo $nsbm_details->country ?>" name="country">
                            </div>
                            <div class="col-sm-6">
                                <label class="font-label">City :</label>
                                <input type="text" class="form-control" placeholder="City" value="<?php echo $nsbm_details->city ?>" value="city">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="font-label">Address:</label>
                        <input type="text" class="form-control" placeholder="Enter university address here" value="<?php echo $nsbm_details->address ?>" name="address">
                    </div>
                    <div class="form-group">
                        <label class="font-label">Phone:</label>
                        <input type="text" class="form-control" placeholder="Enter phone number here" value="<?php echo $nsbm_details->phone ?>" name="phone">
                    </div>
                    <div class="form-group">
                        <label class="font-label">Web Site:</label>
                        <input type="text" class="form-control" placeholder="http:// ..." value="<?php echo $nsbm_details->website ?>" name="website">
                    </div>
                    <div class="form-group">
                        <label class="font-label">Email:</label>
                        <input type="text" class="form-control" placeholder="University email" value="<?php echo $nsbm_details->email ?>" name="email">
                    </div>
                    <div class="form-group">
                        <label for="fileName" class="font-label">Logo: </label>
                        <input id="input-2" type="file" class="file" data-show-upload="false" data-show-caption="true" name="logo">
                        
                        <input type="hidden" name="logo_old" value="<?php echo $nsbm_details->logo ?>"/>
                        
                        <input type="hidden" name="university_id" value="<?php echo $nsbm_details->university_id ?>"/>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-danger">Update</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- NSBM-Edit Modal End-->

<!-- Delete Modal Start-->
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #ff0066">Do you want to delete this University details..?</h4>
            </div>
            <div class="modal-body">
                <p>If you delete this Record data will no longer display any more, and it will remove from whole database</p>
            </div>
            <div class="modal-footer">
            <form method="post" action="" id="mdeleteform">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-danger">Yes</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Delete Modal End-->

<!-- Registration Done Modal Start-->
<div class="modal fade" id="successfull" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #00cc66">University Successfully registered</h4>
            </div>
            <div class="modal-body">
                <p>University registered successfully. Click Update if anything need to be changed</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Done !</button>
            </div>
        </div>
    </div>
</div>
<!-- Registration Done Modal End-->

<!-- Update Done Modal Start-->
<div class="modal fade" id="successfull" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #00cc66">University Successfully updated</h4>
            </div>
            <div class="modal-body">
                <p>University updated successfully. Click Update if anything need to be changed again.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Done !</button>
            </div>
        </div>
    </div>
</div>
<!-- Update Done Modal End-->

<!-- Registration Failed Modal Start-->
<div class="modal fade" id="failed" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 0;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #ff0066">University Registration or Update failed</h4>
            </div>
            <div class="modal-body">
                <p>Error...!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-danger">Yes</button>
            </div>
        </div>
    </div>
</div>
<!-- Registration Failed Modal End-->