<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jun 21, 2014, 11:58:10 PM
 */

?>

<div class="snap-drawers">
    <div class="snap-drawer snap-drawer-left">
        <div class="">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="http://main.d2real.com"><img style="margin: 5px auto auto auto; width: 90%; height: 90%;" class=" center-block image-responsive" src="<?php echo base_url();?>logos/nsbm.gif"></a>
                        <div style="text-align: center; margin-left: -25px">
                            <h3 style="color: #009999; font-weight: 600">Admin</h3>
                            <h4><i class="glyphicon glyphicon-stats"></i> Dashboard</h4>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="<?php echo base_url(); ?>admin/university">
                                <span class="fa fa-university"></span> University
                            </a>
                        </h4>
                    </div>
                    <div id="collapseone" class="panel-collapse collapse">
                        <ul class="list-group">
                            <li class="list-group-item"><span class="glyphicon glyphicon-pencil text-primary"></span><a href="http://fb.com/moinakbarali"> Add</a></li>
                        </ul>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapsetwo"><span class="fa fa-bookmark-o">
                                </span> &nbsp;&nbsp;Schools</a>
                        </h4>
                    </div>
                    <div id="collapsetwo" class="panel-collapse collapse">
                        <ul class="list-group">
                            <li class="list-group-item"><span class="fa fa-asterisk text-info"></span><a href="<?php echo base_url(); ?>admin/schools"> &nbsp;Manage Schools</a></li>
                            <li class="list-group-item"><span class="fa fa-graduation-cap text-info"></span><a href="<?php echo base_url(); ?>admin/degree"> Degree</a></li>
                            <li class="list-group-item"><span class="glyphicon glyphicon-tags text-info"></span><a href="<?php echo base_url(); ?>admin/batches"> &nbsp;Batches</a></li>
                            <li class="list-group-item"><span class="fa fa-book text-info"></span><a href="<?php echo base_url(); ?>admin/modules"> &nbsp;Modules</a></li>
                        </ul>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading"> 
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapsethree"><span class="fa fa-user">
                                </span> &nbsp;Students</a>
                        </h4>
                    </div>
                    <div id="collapsethree" class="panel-collapse collapse">
                        <ul class="list-group">
                            <li class="list-group-item"><span class="glyphicon glyphicon-pencil text-info"></span><a href="<?php echo base_url(); ?>admin/students/register"> Register</a></li>
                            <li class="list-group-item"><span class="glyphicon glyphicon-star-empty text-info"></span><a href="<?php echo base_url(); ?>admin/students/manage"> Student Manage</a></li>
                        </ul>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapsefour"><span class="fa fa-users">
                                </span> Employees</a>
                        </h4>
                    </div>
                    <div id="collapsefour" class="panel-collapse collapse">
                        <ul class="list-group">
                            <li class="list-group-item"><span class="fa fa-users text-info"></span><a href="<?php echo base_url(); ?>admin/employees"> Manage Employees</a></li>
                            <li class="list-group-item"><span class="glyphicon glyphicon-user text-info"></span><a href="<?php echo base_url(); ?>admin/employees/permanent_lecturers"> &nbsp;Permanent lecturers</a></li>
                            <li class="list-group-item"> <span class="fa fa-user text-info"></span><a href="<?php echo base_url(); ?>admin/employees/visiting_lecturers"> &nbsp;Visiting lecturers</a></li>
                        </ul>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapsefive"><span class="fa fa-sitemap">
                                </span> Sites CMS</a>
                        </h4>
                    </div>
                    <div id="collapsefive" class="panel-collapse collapse">
                        <ul class="list-group">
                            <li class="list-group-item"><span class="glyphicon glyphicon-list-alt text-primary"></span><a href="<?php echo base_url(); ?>admin/cms/main"> NSBM site manage </a><a data-toggle="collapse" data-target="#cms-nsbm"><span class="fa fa-chevron-down pull-right"></span></a>
                                <div id="cms-nsbm" class="list-group collapse" style="margin-top: 10px">
                                    <a class="list-group-item list-group-item-success" href="<?php echo base_url(); ?>admin/cms/main/manage/gallery"><span class="fa fa-plus-circle text-success"></span> Manage gallery</a>
                                    <a class="list-group-item list-group-item-success" href="<?php echo base_url(); ?>admin/cms/main/manage/news"><span class="fa fa-pencil-square-o text-success"></span> Manage news</a>
                                    <a class="list-group-item list-group-item-success"><span class="fa fa-minus-circle text-success"></span> Delete article</a>
                                    <a class="list-group-item list-group-item-success"><span class="fa fa-th-list text-success"></span> Add menu item</a>
                                </div>
                            </li>
                            <li class="list-group-item"><span class="glyphicon glyphicon-list-alt text-primary"></span><a href="<?php echo base_url(); ?>admin/cms/help"> Help site manage </a><a data-toggle="collapse" data-target="#cms-help"><span class="fa fa-chevron-down pull-right"></span></a>
                                <div id="cms-help" class="list-group collapse" style="margin-top: 10px">
                                    <a class="list-group-item list-group-item-info" href="<?php echo base_url(); ?>admin/cms/help/add/article"><span class="fa fa-plus-circle text-info"></span> Add article</a>
                                    <a class="list-group-item list-group-item-info" href="<?php echo base_url(); ?>admin/cms/help/edit/article"><span class="fa fa-pencil-square-o text-info"></span> Edit article</a>
                                    <a class="list-group-item list-group-item-info" href="<?php echo base_url(); ?>admin/cms/help/delete/article"><span class="fa fa-minus-circle text-info"></span> Delete article</a>
                                    <a class="list-group-item list-group-item-info" href="<?php echo base_url(); ?>admin/cms/help/category"><span class="fa fa-th-list text-info"></span> Manage category</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                
                <footer class="footer">
                    <div class="row" style="padding: 0; margin: 0">
                        <div class="col-xs-10" style="padding: 0; margin: 25px 15px 0 20px">
                        <p>Copyright &COPY; National School of Business Management</p>
                        <ul class="footer-links">
                            <li><a href="#"  style="text-decoration: none;">NSBM</a></li>
                            <li class="muted">&middot;</li>
                            <li><a href="#"  style="text-decoration: none;">LMS</a></li>
                            <li class="muted">&middot;</li>
                            <li><a href="#"  style="text-decoration: none;">Forum</a></li>
                            <li class="muted">&middot;</li>
                            <li><a href="#"  style="text-decoration: none;">EMS</a></li>
                            <li class="muted">&middot;</li>
                            <li><a href="#"  style="text-decoration: none;">Communities</a></li>
                            <li class="muted">&middot;</li>
                            <li><a href="#"  style="text-decoration: none;">Help</a></li>
                        </ul>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    </div>
    
    <div class="snap-drawer snap-drawer-right">
<!--        <div class="panel-body">
            <div class="row">
                <div class="col-xs-6 col-md-6">
                    <a href="#" class="btn btn-danger btn-lg" role="button"><span class="glyphicon glyphicon-list-alt"></span> <br/>Apps</a>
                    <a href="#" class="btn btn-warning btn-lg" role="button"><span class="glyphicon glyphicon-bookmark"></span> <br/>Bookmarks</a>
                    <a href="#" class="btn btn-primary btn-lg" role="button"><span class="glyphicon glyphicon-signal"></span> <br/>Reports</a>
                    <a href="#" class="btn btn-primary btn-lg" role="button"><span class="glyphicon glyphicon-comment"></span> <br/>Comments</a>
                </div>
                <div class="col-xs-6 col-md-6">
                    <a href="#" class="btn btn-success btn-lg" role="button"><span class="glyphicon glyphicon-user"></span> <br/>Users</a>
                    <a href="#" class="btn btn-info btn-lg" role="button"><span class="glyphicon glyphicon-file"></span> <br/>Notes</a>
                    <a href="#" class="btn btn-primary btn-lg" role="button"><span class="glyphicon glyphicon-picture"></span> <br/>Photos</a>
                    <a href="#" class="btn btn-primary btn-lg" role="button"><span class="glyphicon glyphicon-tag"></span> <br/>Tags</a>
                </div>
            </div>
            <a href="http://www.jquery2dotnet.com/" class="btn btn-success btn-lg btn-block" role="button"><span class="glyphicon glyphicon-globe"></span> Website</a>
        </div>-->
    </div>
</div>