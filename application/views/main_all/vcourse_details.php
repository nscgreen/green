<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jul 17, 2014, 12:54:26 PM
 */

?>
<body class="pages-body">
    <?php $this->load->view('main_all/vmain_all_top_menu'); ?>  <!--include menu and header-->
    <div class="container pages-container">
        <div id="content" class="row-fluid">
            <div class="well col-md-3 col-sm-4 sidebar-main">
                <?php $this->load->view('main_all/vmain_side_menu.php') ?>
            </div>

            <div class="col-md-9 col-sm-8 right-content">
                <!--School of Computing main page content-->
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url();?>"><span class="glyphicon glyphicon-home"></span></a></li>
                            <?php echo create_breadcrumb(); ?>
                        </ol>
                        <h1 class="text-info">Course Details</h1>
                        <h3 class="text-success">BSc in Computer Science <small>(University College of Dublin)</small></h3>
                        <table class="table table-responsive font-label table-bordered">
                            <tbody>
                                <tr>
                                    <td class="label-success font-play"><span style="color: #faf8f8">Course Name</span></td>
                                    <td>BSc in Computer Science (University College of Dublin)</td>
                                </tr>
                                <tr>
                                    <td class="font-play active">Objectives</td>
                                    <td class="text-justify">A University Level Degree that provides participants with comprehensive knowledge of Computer Science</td>
                                </tr>
                                <tr>
                                    <td class="font-play active">Entry Requirements</td>
                                    <td>
                                        <ul>
                                            <li>2 Passes at GCE A/L (Science Stream)</li>
                                            <li>5 Credits at GCE O/L including Maths and English</li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="font-play active">Contents</td>
                                    <td>
                                        <table class="table table-responsive table-condensed">
                                            <tr>
                                                <td>
                                                    <h4>Year 1</h4><br />
                                                    <div style="padding-left: 20px;">
                                                        <ul style="list-style: circle;">
                                                            <li>Formal Foundation</li>
                                                            <li>Foundation of Mathematics for Computer Science I</li>
                                                            <li>Computer Programming I</li>
                                                            <li>Algorithmic Problem Solving</li>
                                                            <li>Computer Science in Practice</li>
                                                            <li>Foundation of Mathematics for Computer Science II</li>
                                                        </ul>
                                                    </div>
                                                </td>
                                                <td>
                                                    <h4>Year 2</h4><br />
                                                    <div style="padding-left: 20px;">
                                                        <ul style="list-style: circle;">
                                                            <li>Formal Foundation</li>
                                                            <li>Foundation of Mathematics for Computer Science I</li>
                                                            <li>Computer Programming I</li>
                                                            <li>Algorithmic Problem Solving</li>
                                                            <li>Computer Science in Practice</li>
                                                            <li>Foundation of Mathematics for Computer Science II</li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <h4>Year 3</h4><br />
                                                    <div style="padding-left: 20px;">
                                                        <ul style="list-style: circle;">
                                                            <li>Formal Foundation</li>
                                                            <li>Foundation of Mathematics for Computer Science I</li>
                                                            <li>Computer Programming I</li>
                                                            <li>Algorithmic Problem Solving</li>
                                                            <li>Computer Science in Practice</li>
                                                            <li>Foundation of Mathematics for Computer Science II</li>
                                                        </ul>
                                                    </div>
                                                </td>
                                                <td>
                                                    <h4>Year 4</h4><br />
                                                    <div style="padding-left: 20px;">
                                                        <ul style="list-style: circle;">
                                                            <li>Formal Foundation</li>
                                                            <li>Foundation of Mathematics for Computer Science I</li>
                                                            <li>Computer Programming I</li>
                                                            <li>Algorithmic Problem Solving</li>
                                                            <li>Computer Science in Practice</li>
                                                            <li>Foundation of Mathematics for Computer Science II</li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="font-play active">Duration</td>
                                    <td>+94 (11) 544 5000</td>
                                </tr>
                                <tr>
                                    <td class="label-warning font-play"><span style="color: #faf8f8">Commencement</span></td>
                                    <td>+94 (11) 544 5000</td>
                                </tr>
                                <tr>
                                    <td class="font-play active">Lectures & Practicals</td>
                                    <td>+94 (11) 544 5000</td>
                                </tr>
                                <tr>
                                    <td class="font-play active">Course Director(s)</td>
                                    <td>+94 (11) 544 5000</td>
                                </tr>
                                <tr>
                                    <td class="label-danger font-play"><span style="color: #faf8f8">Apply</span></td>
                                    <td class="underline">
                                        <a class="text-info" href="<?php echo base_url();?>registrations"><b>Online Registrations</b></a> &nbsp;Or
                                        <a class="btn btn-info btn-sm" style="margin-left: 10px" href=""><span style="color: #faf8f8">Download Application</span></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
