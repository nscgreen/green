<?php
/*
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jul 15, 2014, 11:03:19 AM
 */
?>
<div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12" >
            <nav id="menu-background" class="navbar navbar-default navbar-fixed-top" role="navigation" style="padding: 0 10px; width: 100%">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="visible-xs navbar-brand" href="#page-top">NSBM</a>
                </div>
                <div  class=" collapse navbar-collapse navbar-ex1-collapse" style="padding: 0 10px;">
                    <ul class="nav navbar-nav">               
                        <li class="hidden">
                            <a href="#page-top"></a>
                        </li>
                        <li class="page-scroll" style="padding: 0px">
                            <a href="<?php echo base_url() ?>" style="padding: 5px 0 5px 0"><img style="height: 40px; padding: 0; margin: 0;" src="<?php echo base_url() . 'assets/img/main/nsbm.gif'; ?>"/></a>
                        </li>
                        <li class="page-scroll">
                            <a href="<?php echo base_url();?>#intro">Welcome</a>
                        </li>
                        <li class="dropdown page-scroll">
                            <a href="<?php echo base_url();?>#schools" class="dropdown-toggle">Schools</a>
                            <ul class="dropdown-menu hidden-sm hidden-xs">          
                                <li><a href="<?php echo base_url(); ?>computing">School of Computing</a></li>          
                                <li><a href="<?php echo base_url(); ?>management">School of Management</a></li>
                                <li><a href="<?php echo base_url(); ?>engineering">School of Engineering</a></li>
                            </ul>
                        </li>
                        <li class="dropdown page-scroll">
                            <a href="<?php echo base_url();?>#mynsbm" class="dropdown-toggle">My NSBM</a>
                            <ul class="dropdown-menu hidden-xs">          
                                <li><a href="http://lms.d2real.com" target="_blank">L.M.S</a></li>
                                <li><a href="http://events.d2real.com" target="_blank">Events</a></li>
                                <li><a href="http://forum.d2real.com" target="_blank">Forum</a></li>
                                <li><a href="http://communities.d2real.com" target="_blank">Communities</a></li>
                            </ul>
                        </li>
                        <li class="dropdown page-scroll">
                            <a href="<?php echo base_url();?>news" class="dropdown-toggle">News</a>
                            <ul class="dropdown-menu hidden-xs">          
                                <li><a href="<?php echo base_url(); ?>news">More <i class="fa fa-angle-double-right"></i></a></li>
                            </ul>
                        </li>
                        <li class="dropdown page-scroll">
                            <a href="<?php echo base_url();?>#quick" class="dropdown-toggle">Quick links</a>
                            <ul class="dropdown-menu hidden-xs">          
                                <li><a href="<?php echo base_url(); ?>about/future_plans">Accommodations</a></li>
                                <li><a href="<?php echo base_url(); ?>more/meet_coordinator">Meet Coordinator</a></li>
                                <li><a href="<?php echo base_url(); ?>about/nsbm_nibm">NSBM & NIBM</a></li>
                                <li><a href="<?php echo base_url(); ?>about/future_plans">Future Plans</a></li>
                                <li><a href="<?php echo base_url(); ?>more/e-learning">e-Learning</a></li>
                                <li><a href="<?php echo base_url(); ?>more/library">Library</a></li>
                            </ul>
                        </li>
                        <li class="dropdown page-scroll">
                            <a href="<?php echo base_url();?>gallery" class="dropdown-toggle">Gallery</a>
                            <ul class="dropdown-menu hidden-xs">          
                                <li><a href="<?php echo base_url(); ?>gallery">More <i class="fa fa-angle-double-right"></i></a></li>
                            </ul>
                        </li>               
                        <li class="page-scroll">
                            <a href="#contacts">Contacts</a>
                        </li>
                        <li class="underline">
                            <a style="color: #018ab8" href="<?php echo base_url(); ?>registrations">Register</a>
                        </li>
                    </ul>

                    <ul id="top-menu2" class="hidden-sm hidden-xs nav navbar-nav navbar-right">
                        <li class="underline">
                            <a href="http://connect.d2real.com" target="_blank"><span class="glyphicon glyphicon-user"> Login</a></li>
                        <li class="underline"><a href="http://mynsbm.d2real.com" target="_blank">My NSBM</a></li>
                        <li class="underline"><a href="http://lms.d2real.com" target="_blank">L.M.S</a></li>
                        <li class="underline"><a href="http://events.d2real.com" target="_blank">Events</a></li>
                        <li class="underline"><a href="http://help.d2real.com" target="_blank">Help</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</div>

<div class="container" style=" width: 100%;">
    <div class="row">
        <div id="welcome" class="bs-docs-header" id="content">
            <div style="padding: 30px;" class="container">
                <div class="col-md-7 col-sm-7">
                    <h1><font style="color: rgba(32, 76, 147, 1);" class="animated zoomIn">National School of Business Management</font></h1>
                    <h3><small><font style="color: black;" class="animated pulse">Welcome to The NSBM One of the South Asia's Top Leading Universities...</font></small><h3>
                </div>
                <div  class="col-md-5 col-sm-5 push-right">

                    <div class="row visible-lg visible-xs" style="margin-top: 0px; padding: 0px">
                        <center>
                            <a href="<?php echo base_url();?>"><img style="margin: 0px 0px 30px 0px" class="nsbmlogo animated fadeInDown" src="<?php echo base_url(); ?>logos/nsbmwhite.gif"/></a>
                        </center>
                    </div>
                    <div class="row visible-sm visible-md">
                        <center>
                            <a href="<?php echo base_url();?>"><img style="margin-top: 30px" class="nsbmlogo animated fadeInDown" src="<?php echo base_url(); ?>logos/nsbmwhite.gif"/></a>
                        </center>
                    </div>
                    <div class="row" style="margin-bottom: -20px; margin-top: 5px">
                        <div class="col-xs-4">
                            <a href="<?php echo base_url(); ?>computing"><img class="animated flipInX school-logo image-responsive center-block" src="../../logos/schools/batch/computing-logo.png"></a>
                        </div>
                        <div class="col-xs-4">
                            <a href="<?php echo base_url(); ?>management"><img class="animated flipInX school-logo image-responsive center-block" src="../../logos/schools/batch/management-logo.png"></a>
                        </div>
                        <div class="col-xs-4">
                            <a href="<?php echo base_url(); ?>engineering"><img class="animated flipInX school-logo image-responsive center-block" src="../../logos/schools/batch/engineering-logo.png"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

                                <!--                            <div class="row"  style="margin: -30px -130px 20px auto; padding: 0">
                                                                <div class="col-sm-12">
                                                                    <div class="pull-right animated fadeInRight">
                                                                        <div class="col-sm-2">
                                                                            <a rel="me" href="https://twitter.com/mynsbmsrilanka" target="_blank">
                                                                                <img src="<?php // echo base_url() . 'assets/img/main/facebook.png';   ?>" class="social-img" title="NSBM Twitter account" height="48" width="48" alt="NSBM Twitter account">
                                                                            </a>
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <a rel="me" href="http://www.facebook.com/nsbm.lk" target="_blank">
                                                                                <img src="<?php // echo base_url() . 'assets/img/main/flickr.png';   ?>" class="social-img" title="NSBM Facebook Page" height="48" width="48" alt="NSBM Facebook page">
                                                                            </a>    
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <a rel="me" href="http://www.youtube.com/user/mynsbm" target="_blank">
                                                                                <img src="<?php // echo base_url() . 'assets/img/main/twitter.png';   ?>" class="social-img" title="NSBM YouTube channel" height="48" width="48" alt="NSBM YouTube channel">
                                                                            </a>    
                                                                        </div>
                                                                        <div class="col-sm-2">
                                                                            <a rel="me" href="http://www.flickr.com/photos/nsbm/sets/" target="_blank">
                                                                                <img src="<?php // echo base_url() . 'assets/img/main/googleplusalt.png';   ?>" class="social-img" title="NSBM SoundCloud channel" height="48" width="48" alt="NSBM Flicker channel">
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>-->