<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jul 17, 2014, 12:54:26 PM
 */

?>
<body class="pages-body">
    <?php $this->load->view('main_all/vmain_all_top_menu'); ?>  <!--include menu and header-->
    <div class="container pages-container">
        <div id="content" class="row-fluid">
            <div class="well col-md-3 col-sm-4 sidebar-main">
                <?php $this->load->view('main_all/vmain_side_menu.php') ?>
            </div>

            <div class="col-md-9 col-sm-8 right-content">
                <!--School of Computing main page content-->
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url();?>"><span class="glyphicon glyphicon-home"></span></a></li>
                            <?php echo create_breadcrumb(); ?>
                        </ol>
                        <h1 class="text-info">Alumni and Friends</h1>
                        <p class="text-justify text-info font-schools">
                            The NSBM Alumni aim to help you stay in touch with NSBM, your college and other colleagues, wherever you are in the world.
                        </p>
                        <p class="text-justify text-info font-schools">
                            The role of the Alumni is to establish and enhance a continuing relationship between NSBM and its alumni. There are already more than thousand students and this number is ever growing as we are expanding extensively with more national as well as international students both at undergraduate and graduate level. Alumni aspire to provide all the students, regardless of their school or subject affiliations, with an alumnus experience commensurate with their world-class education and offer a varied programme of benefits, events and communications to help keep you in touch and involved more with NSBM.
                        </p>
                        <h3 class="font-roboto text-info" style="font-weight: 700">THE OBJECTIVES OF THE ASSOCIATION</h3>
                        <ul class="font-schools">
                            <li><i class="glyphicon glyphicon-star-empty text-muted"></i> Engage in the transformation of lives</li>
                            <li><i class="glyphicon glyphicon-star-empty text-muted"></i> To encourage, foster and promote close relations between the NSBM and its Alumni</li>
                            <li><i class="glyphicon glyphicon-star-empty text-muted"></i> To promote the interest of the alumni body in the affairs of the NSBM</li>
                            <li><i class="glyphicon glyphicon-star-empty text-muted"></i> To assist the alumni in promoting the general interest and well-being of NSBM</li>
                            <li><i class="glyphicon glyphicon-star-empty text-muted"></i> To provide and disseminate information regarding NSBM, its facilities, staff, graduates and students</li>
                            <li><i class="glyphicon glyphicon-star-empty text-muted"></i> To promote the study and development of every member and to bring together members of the same field</li>
                            <li><i class="glyphicon glyphicon-star-empty text-muted"></i> To safeguard the profession and foster more interest amongst persons practicing this profession</li>
                            <li><i class="glyphicon glyphicon-star-empty text-muted"></i> Offer assistance with your class reunion</li>
                        </ul>
                        <br />
                        <p class="text-justify text-info font-schools">
                            Through us you can reconnect with old friends, network at events, read about the exciting happenings at NSBM and of our alumni and much more.
                        </p>
                        <!--<img class="pull-right" style="z-index:-1; opacity: 0.4; filter: alpha(opacity=40); width:60%;" src="http://www.sandersontestprep.com/wp-content/uploads/2013/12/slider-2-student-girl1.png"/>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
