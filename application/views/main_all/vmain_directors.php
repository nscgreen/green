<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jul 17, 2014, 12:54:26 PM
 */

?>
<body class="pages-body">
    <?php $this->load->view('main_all/vmain_all_top_menu'); ?>  <!--include menu and header-->
    <div class="container pages-container">
        <div id="content" class="row-fluid">
            <div class="well col-md-3 col-sm-4 sidebar-main">
                <?php $this->load->view('main_all/vmain_side_menu.php') ?>
            </div>

            <div class="col-md-9 col-sm-8 right-content">
                <!--School of Computing main page content-->
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url();?>"><span class="glyphicon glyphicon-home"></span></a></li>
                            <?php echo create_breadcrumb(); ?>
                        </ol>
                        <h1 class="text-info">Directors</h1>
                        <div class="row">
                            <div class="col-sm-12">
                                <img class="thumbnail pull-left" style=" width: 110px; height: 140px; margin-right:10px;" src="../../../assets/img/main/directors/LPJayatissa.jpg">
                                <div class="text-justify p-news" style="margin-top: 50px">
                                    <p>Prof. L. P. Jayatissa</p>
                                    <p>Chairman - National School of Business Management</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <img class="thumbnail pull-left" style=" width: 110px; height: 140px; margin-right:10px;" src="../../../assets/img/main/directors/vice_chancellor.jpg">
                                <div class="text-justify p-news" style="margin-top: 50px">
                                    <p>Dr. E. A Weerasinghe</p>
                                    <p>CEO - National School of Business Management</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <img class="thumbnail pull-left" style=" width: 110px; height: 140px; margin-right:10px;" src="../../../assets/img/main/directors/head_mgt.jpg">
                                <div class="text-justify p-news" style="margin-top: 50px">
                                    <p>Mr. D. M. A Kulasooriya</p>
                                    <p>Dean - School of Business National School of Business Management</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <img class="thumbnail pull-left" style=" width: 110px; height: 140px; margin-right:10px;" src="../../../assets/img/main/directors/head_cs.jpg">
                                <div class="text-justify p-news" style="margin-top: 50px">
                                    <p>Mrs. Kishani Wijesiriwardhane</p>
                                    <p>Dean - School of Computing National School of Business Management</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <img class="thumbnail pull-left" style=" width: 110px; height: 140px; margin-right:10px;" src="../../../assets/img/main/directors/BernardSilva.jpg">
                                <div class="text-justify p-news" style="margin-top: 50px">
                                    <p>Mr. G. Bernard Silva</p>
                                    <p>Director - Finance National Institute of Business Management</p>
                                </div>
                            </div>
                        </div>
                        <!--<img class="pull-right" style="z-index:-1; opacity: 0.4; filter: alpha(opacity=40); width:60%;" src="http://www.sandersontestprep.com/wp-content/uploads/2013/12/slider-2-student-girl1.png"/>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
