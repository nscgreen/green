<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jul 17, 2014, 12:54:26 PM
 */

?>
<body class="pages-body">
    <?php $this->load->view('main_all/vmain_all_top_menu'); ?>  <!--include menu and header-->
    <div class="container pages-container">
        <div id="content" class="row-fluid">
            <div class="well col-md-3 col-sm-4 sidebar-main">
                <?php $this->load->view('main_all/vmain_side_menu.php') ?>
            </div>

            <div class="col-md-9 col-sm-8 right-content">
                <!--School of Computing main page content-->
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url();?>"><span class="glyphicon glyphicon-home"></span></a></li>
                            <?php echo create_breadcrumb(); ?>
                        </ol>
                        <h1 class="text-info">Future Plans</h1>
                        <p class="text-justify text-info font-schools">
                            Inspired by the vision to make Sri Lanka the 'Knowledge Hub', NSBM is geared towards constructing the first ever 'Green University Town' of the region.
                        </p>
                        <p class="text-justify text-info font-schools">
                            NSBM will be far ahead with its physical facilities with its new fully fledged university complex, which is under construction at Pitipana, Homagama and to be completed at the end of 2014. The objective for constructing NSBM Green University is to provide the school with not only permanent spaces to conduct its day to day activities efficiently and provide the ideal environment for academics and students to conduct their work on “teach & learn”. Hence an ideal environment could be created by providing both the physical and physiological comfort not only for effective teaching but also for productive learning.
                        </p>
                        <p class="text-justify text-info font-schools">
                            The design of the complex is not only to create an environment which caters to the physical and physiological comfort of both students and academics, but also to create buildings that would harmonize with the existing physical and natural environment, whilst being techno and environment friendly and built with 21st century technology and modern materials requiring a less maintenance effort. There are facilities for both commuting and residing; for maximizing the use of teaching and learning resources and for developing academic research and leisure pursuits , , so as to create graduates with a well-rounded personality to pursue successful careers as professionals in business and industry. The university complex also contains a comfortable, eco-friendly, aesthetically pleasing and self-contained atmosphere, a conference/convention centre for national and international gatherings, commercial and industrial centres for the benefit of students, professionals and their broader community.
                        </p>
                        <h1 class="text-center"><small>NSBM Green University Virtual Tour</small></h1>
                        <div class="row visible-lg visible-md" style="margin-bottom: 20px">
                            <div class="col-sm-12" align="center">
                                <iframe width="600" height="350" src="//www.youtube.com/embed/sYW65_W4RYU" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>
                        <div class="row visible-sm" style="margin-bottom: 20px">
                            <div class="col-sm-12" align="center">
                                <iframe width="460" height="315" src="//www.youtube.com/embed/sYW65_W4RYU" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>
                        <div class="row visible-xs" style="margin-bottom: 20px">
                            <div class="col-sm-12" align="center">
                                <iframe width="400" height="250" src="//www.youtube.com/embed/sYW65_W4RYU" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>
                        <!--<img class="pull-right" style="z-index:-1; opacity: 0.4; filter: alpha(opacity=40); width:60%;" src="http://www.sandersontestprep.com/wp-content/uploads/2013/12/slider-2-student-girl1.png"/>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
