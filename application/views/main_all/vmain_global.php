<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jul 17, 2014, 12:54:26 PM
 */

?>
<body class="pages-body">
    <?php $this->load->view('main_all/vmain_all_top_menu'); ?>  <!--include menu and header-->
    <div class="container pages-container">
        <div id="content" class="row-fluid">
            <div class="well col-md-3 col-sm-4 sidebar-main">
                <?php $this->load->view('main_all/vmain_side_menu.php') ?>
            </div>

            <div class="col-md-9 col-sm-8 right-content">
                <!--School of Computing main page content-->
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url();?>"><span class="glyphicon glyphicon-home"></span></a></li>
                            <?php echo create_breadcrumb(); ?>
                        </ol>
                        <h1 class="text-info">NSBM Global</h1>
                        <p class="text-justify text-info font-schools">
                            Sri Lanka is booming towards becoming a vibrant, innovative leader in Asia for a knowledge-based economy. To achieve this ultimate intent NSBM is working at its full capacity to integrate innovation and entrepreneurship, with a culturally and ethnically diverse workforce as well as international engagement of expertise in the fields of Information Technology and Management. It will assist to build a world- class School of Business integrating leadership, ethics, global thinking, core management skills, and leading edge technological innovations.
                        </p>
                        <p class="text-justify text-info font-schools">
                            As a forward thinking school in higher education NSBM assures the vital need to make learning relevant to industry expectations. NSBM has forged strategic collaborations with leading universities in the world that enable its students to expose themselves to global thinking and best practices in global industries.
                        </p>
                        <p class="text-justify text-info font-schools">
                            NSBM offers internationally recognized degree programmes. . Towards this end, NSBM has developed long- term partnerships with highly reputed and globally ranked universities and degree awarding institutions established in Europe and Asia.
                        </p>
                        <h1><small>Affiliated Universities</small></h1>
                        <div class="row">
                            <div class="col-sm-2 pull-left">
                                <img style="width: 120px; height: 160px;" class="image-responsive" src="../../../logos/universities/ucd.png">
                            </div>
                            <div class="col-sm-9 col-xs-offset-1 underline" style="padding-top: 70px">
                                <a class="font-schools pull-left" href="http://www.ucd.ie/" target="_blank"> University College of Dublin (UCD)- Ireland</a>
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-sm-2 pull-left">
                                <img style="width: 120px; margin-right: 5px" class="image-responsive" src="../../../logos/new/plymouth.png">
                            </div>
                            <div class="col-sm-9 col-xs-offset-1 underline" style="padding-top: 20px">
                                <a class="font-schools pull-left" href="http://www.plymouth.ac.uk/" target="_blank"> Plymouth University - United Kingdom</a>
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-sm-2 pull-left">
                                <img style="width: 120px; margin-right: 5px" class="image-responsive" src="../../../logos/new/linkm.png">
                            </div>
                            <div class="col-sm-9 col-xs-offset-1 underline" style="padding-top: 40px">
                                <a class="font-schools pull-left" href="http://www.limkokwing.net/" target="_blank"> Limkokwing University of Creative Technology-Malaysia</a>
                            </div>
                        </div>
                        <br/>
                        <!--<img class="pull-right" style="z-index:-1; opacity: 0.4; filter: alpha(opacity=40); width:60%;" src="http://www.sandersontestprep.com/wp-content/uploads/2013/12/slider-2-student-girl1.png"/>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
