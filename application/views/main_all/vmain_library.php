<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jul 17, 2014, 12:54:26 PM
 */

?>
<body class="pages-body">
    <?php $this->load->view('main_all/vmain_all_top_menu'); ?>  <!--include menu and header-->
    <div class="container pages-container">
        <div id="content" class="row-fluid">
            <div class="well col-md-3 col-sm-4 sidebar-main">
                <?php $this->load->view('main_all/vmain_side_menu.php') ?>
            </div>

            <div class="col-md-9 col-sm-8 right-content">
                <!--School of Computing main page content-->
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url();?>"><span class="glyphicon glyphicon-home"></span></a></li>
                            <?php echo create_breadcrumb(); ?>
                        </ol>
                        <h1 class="text-info">Library</h1>
                        <p class="text-justify text-info font-schools">
                            The NSBM Library is a source of information. It plays a vital role in catering to the information and educational needs of both course participants and professional staff of NSBM. The fully-fledged library is well stocked with over 23000 books and selected periodicals. Among the special collections of the library are the Training Manuals, Company Annual Reports, Periodicals Holdings, Socio-Economic statistics, NSBM Consultancy and Student Project Reports and the Sri Lanka Collection.
                        </p>
                        <p class="text-justify text-info font-schools">
                            Services provided by the NSBM library include inquiry, reference & lending services, library information services, the library database, inter-library loan facility, photocopying facility, journal, contents page service and bibliographical assistance to the undergraduates, academic staff and non academic staff at NSBM.
                        </p>
                        <p class="text-justify text-info font-schools">
                            NSBM Library is open seven days a week from 0800 hrs to 1600 hrs, except on pubic holidays.
                        </p>
                        <h1><small>Library and Information Resources</small></h1>
                        <div class="row">
                            <div class="col-sm-4 col-xs-4">
                                <a class="thumbnail fancybox" rel="gallery1" href="../../../assets/img/main/facilities/lib/1.jpg">
                                    <img alt="" src="../../../assets/img/main/facilities/lib/1.jpg" />
                                </a>
                            </div>
                            <div class="col-sm-4 col-xs-4">
                                <a class="thumbnail fancybox" rel="gallery1" href="../../../assets/img/main/facilities/lib/2.jpg">
                                    <img alt="" src="../../../assets/img/main/facilities/lib/2.jpg" />
                                </a>
                            </div>
                            <div class="col-sm-4 col-xs-4">
                                <a class="thumbnail fancybox" rel="gallery1" href="../../../assets/img/main/facilities/lib/3.jpg">
                                    <img alt="" src="../../../assets/img/main/facilities/lib/3.jpg" />
                                </a>
                            </div>
                        </div>
                        <!--<img class="pull-right" style="z-index:-1; opacity: 0.4; filter: alpha(opacity=40); width:60%;" src="http://www.sandersontestprep.com/wp-content/uploads/2013/12/slider-2-student-girl1.png"/>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
