<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jul 17, 2014, 12:54:26 PM
 */

?>
<body class="pages-body">
    <?php $this->load->view('main_all/vmain_all_top_menu'); ?>  <!--include menu and header-->
    <div class="container pages-container">
        <div id="content" class="row-fluid">
            <div class="well col-md-3 col-sm-4 sidebar-main">
                <?php $this->load->view('main_all/vmain_side_menu.php') ?>
            </div>

            <div class="col-md-9 col-sm-8 right-content">
                <!--School of Computing main page content-->
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url();?>"><span class="glyphicon glyphicon-home"></span></a></li>
                            <?php echo create_breadcrumb(); ?>
                        </ol>
                        <h1 class="text-info">NSBM and NIBM</h1>
                        <p class="text-justify text-info font-schools">
                            National School of Business Management (NSBM) is established as the degree awarding school of NIBM, offering undergraduate and postgraduate degree programmes in the fields of Business, Computing and Engineering. Recognizing the trends in technology and globalization, NIBM launched NSBM as an alternative system of higher education to breed a generation of knowledge based employees.
                        </p>
                        <p class="text-justify text-info font-schools">
                            Taking higher education to its next level , NSBM is inspired to transform Sri Lanka to a 'Knowledge Hub' as declared in 'Mahinda Chnthana' with its 'Green University Town '. Constructing the first ever Green University Town of its own kind in the South Asia, NSBM is proud to enhance the levels and the quality of education offered locally to its next level. Fully equipped gymnasiums, state of the art class rooms, student accommodation, shopping complexes, banking facilities, open theaters, library areas & free study areas will be a few of the facilities that are going to get underway with the NSBM Green University Town in future.
                        </p>
                        <p class="text-justify text-info font-schools">
                            Establish in 1968, NIBM has experience over four decades in the industry. It functions under the purview of the Ministry of Youth Affairs and Skills Development. NIBM undoubtedly has given an impetus to the nation through its progressive march towards development. The institute is well equipped with modern teaching aids and a conducive learning environment for students to acquire knowledge without any disturbance. The organization has rendered a great service to the nation through education, consultancy and training where it will take continuous efforts to enhance the productivity of Sri Lankan work force. The National Institute of Business Management – (NIBM) keeps abreast of global trends and constantly upgrades their training programs to suit today’s needs. Over the years we have developed our own character while proving to be a leader in training. We have helped thousands to carve out a better future for themselves.
                        </p>
                        <!--<img class="pull-right" style="z-index:-1; opacity: 0.4; filter: alpha(opacity=40); width:60%;" src="http://www.sandersontestprep.com/wp-content/uploads/2013/12/slider-2-student-girl1.png"/>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
