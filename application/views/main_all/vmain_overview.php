<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jul 17, 2014, 12:54:26 PM
 */

?>
<body class="pages-body">
    <?php $this->load->view('main_all/vmain_all_top_menu'); ?>  <!--include menu and header-->
    <div class="container pages-container">
        <div id="content" class="row-fluid">
            <div class="well col-md-3 col-sm-4 sidebar-main">
                <?php $this->load->view('main_all/vmain_side_menu.php') ?>
            </div>

            <div class="col-md-9  col-sm-8 right-content">
                <!--School of Computing main page content-->
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url();?>"><span class="glyphicon glyphicon-home"></span></a></li>
                            <?php echo create_breadcrumb(); ?>
                        </ol>
                        <h1 class="text-info">Overview</h1>
                        <p class="text-justify text-info font-schools">
                            NSBM will integrate innovation and entrepreneurship with a culturally and ethnically diverse workforce and an international engagement of expertise in the fields of Business Management, Computing and Engineering. NSBM has designed world class degree programmes integrating leadership, ethics, global thinking, core management skills and leading edge technological innovations. We focus on producing 'Global Graduates' and postgraduate professionals here at NSBM, offering world renowned education as a 360 degree solution provider in education.
                        </p>
                        <p class="text-justify text-info font-schools">
                            As defined in our mission statement, it is a creative and innovative school of future generations. NSBM will drive under the themes of innovation, entrepreneurship, technology and globalization to direct the country towards a knowledge based globalized economy, while creating synergies with existing bodies of knowledge after considering the needs of 21st century Management and Information Technology education.
                        </p>
                        <p class="text-justify text-info font-schools">
                            Spearheading to a future of higher education, NSBM assures the vital need to make learning relevant to industry expectations. NSBM has formed strategic collaborations with leading universities in the world that enable our students to expose themselves to global thinking and best practices in global industries.
                        </p>
                        <!--<img class="pull-right" style="z-index:-1; opacity: 0.4; filter: alpha(opacity=40); width:60%;" src="http://www.sandersontestprep.com/wp-content/uploads/2013/12/slider-2-student-girl1.png"/>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
