<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jul 17, 2014, 12:54:26 PM
 */

?>
<body class="pages-body">
    <?php $this->load->view('main_all/vmain_all_top_menu'); ?>  <!--include menu and header-->
    <div class="container pages-container">
        <div id="content" class="row-fluid">
            <div class="well col-md-3 col-sm-4 sidebar-main">
                <?php $this->load->view('main_all/vmain_side_menu.php') ?>
            </div>

            <div class="col-md-9  col-sm-8 right-content">
                <!--School of Computing main page content-->
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url();?>"><span class="glyphicon glyphicon-home"></span></a></li>
                            <?php echo create_breadcrumb(); ?>
                        </ol>
                        <h1 class="text-info">Prospective Students</h1>
                        <h1><small>Join with Us to Experience the Future of Higher Education</small></h1>
                        <p class="text-justify text-info font-schools">
                            In the midst of an enduring dedication to the pursuit of excellence, NSBM offers incomparable student experiences across a broad spectrum of academic environments. Whether you're a prospective graduate or undergraduate student, school leaver or even if you are just after your O/Ls NSBM is rich with opportunities for your future.
                        </p>
                        <p class="text-justify text-info font-schools">
                            Through the schools of Business, Computing and Engineering, NSBM offers focused disciplines in the fine arts, business, technology and the sciences, with the opportunity to mould your personality and to sharpen your knowledge. All the faculties are thriving harder to achieve NSBM’s prime objective of nurturing an employable graduate.
                        </p>
                        <p class="text-justify text-info font-schools">
                            NSBM offers students with the option of following non-traditional degree programmes which are more industry specific. Curricula of the degree programmes are designed to reflect the changing socio economic requirements with due considerations on the demands of the industry. All programmes, while specializing in a specific field of study, will have an appropriate inter-disciplinary focus which will eventually help a prospective degree holder to easily assimilate in any business or industry environment.
                        </p>
                        <!--<img class="pull-right" style="z-index:-1; opacity: 0.4; filter: alpha(opacity=40); width:60%;" src="http://www.sandersontestprep.com/wp-content/uploads/2013/12/slider-2-student-girl1.png"/>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
