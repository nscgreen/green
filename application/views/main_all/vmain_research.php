<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jul 17, 2014, 12:54:26 PM
 */

?>
<body class="pages-body">
    <?php $this->load->view('main_all/vmain_all_top_menu'); ?>  <!--include menu and header-->
    <div class="container pages-container">
        <div id="content" class="row-fluid">
            <div class="well col-md-3  col-sm-4 sidebar-main">
                <?php $this->load->view('main_all/vmain_side_menu.php') ?>
            </div>

            <div class="col-md-9  col-sm-8 right-content">
                <!--School of Computing main page content-->
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url();?>"><span class="glyphicon glyphicon-home"></span></a></li>
                            <?php echo create_breadcrumb(); ?>
                        </ol>
                        <h1 class="text-info">Research</h1>
                        <p class="text-justify text-info font-schools">
                            Research methodology is a compulsory module for all postgraduate and undergraduate programs at NSBM. All students should complete a comprehensive research project under the guidance of assigned lecturers. Generally undergraduate students work in groups to fulfill this compulsory requirement while postgraduate students take up individual projects. NSBM, managed by a committee of senior academics/ researchers, provides facilities to carry out each student's research to bring out the researcher’s best insight.
                        </p>
                        <h1><small>RESOURCES</small></h1>
                        <p class="text-justify text-info font-schools">
                            NSBM is comprised of WiFi enabled computers, online references and free access to online journal articles, research papers and other various academic papers required to conduct a research. To guide each individual in their research platform, NSBM comprises a well qualified and experienced academic staff. We make every possible effort to develop a vibrant research culture where numerous initiatives are in place for research among the academic and student communities.
                        </p>
                        <h1><small>GRANTS</small></h1>
                        <p class="text-justify text-info font-schools">
                            NSBM provides a substantial research grant scheme to encourage high calibre research among its academics. All academics are clustered into a research team headed by a senior researcher. The senior researchers, as the principle investigators, are eligible for the NSBM annual research grants per project. These grants can be freely utilized for research related purposes such as accessing useful research resources and participation in conferences.
                        </p>
                        <!--<img class="pull-right" style="z-index:-1; opacity: 0.4; filter: alpha(opacity=40); width:60%;" src="http://www.sandersontestprep.com/wp-content/uploads/2013/12/slider-2-student-girl1.png"/>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
