<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jul 17, 2014, 10:27:10 PM
 */

?>
<!--<a href="<?php echo base_url(); ?>computing" ><img class="center-block" src="../../logos/universities/nsbm.gif"/></a>-->
<center><h3>Quick links</h3></center>
<div id="sidebar-main-menu" class="">
    <ul>
        <li><a class="btn btn-default" href='<?php echo base_url(); ?>gallery'>Gallery</a></li>
        <li><a class="btn btn-default" href='<?php echo base_url(); ?>news'>News</a></li>
        <li><a class="btn btn-default" href='<?php echo base_url(); ?>more/library'>Library</a></li>
        <li><a class="btn btn-default" href='<?php echo base_url(); ?>more/e-learning'>e-Learning</a></li>
        <li><a class="btn btn-default" href='<?php echo base_url(); ?>more/facilities'>NSBM Facilities</a></li>
        <li><a class="btn btn-default" href='<?php echo base_url(); ?>more/prospective_students'>Prospective Students</a></li>
        <li><a class="btn btn-default" href='<?php echo base_url(); ?>more/current_students'>Current Students</a></li>
        <li><a class="btn btn-default" href='<?php echo base_url(); ?>more/alumni_friends'>Alumni & Friends</a></li>
        <li><a class="btn btn-default" href='<?php echo base_url(); ?>more/sports_health'>Sport & Health</a></li>
        <li><a class="btn btn-default" href='<?php echo base_url(); ?>more/research'>Research</a></li>
        <li><a class="btn btn-default" href='<?php echo base_url(); ?>more/well_wishes'>Well Wishes</a></li>
        <li><a class="btn btn-danger" href='<?php echo base_url(); ?>registrations'>Online Registrations</a></li>
    </ul>
</div>
<center><h3>About Us</h3></center>
<div id="sidebar-main-menu">
    <ul>
        <li><a class="btn btn-default" href='<?php echo base_url(); ?>about/overview'>Overview</a></li>
        <li><a class="btn btn-default" href='<?php echo base_url(); ?>about/vision_mission'>Vision & Mission</a></li>
        <li><a class="btn btn-default" href='<?php echo base_url(); ?>about/directors'>Boad of Directors</a></li>
        <li><a class="btn btn-default" href='<?php echo base_url(); ?>about/future_plans'>Future Planes</a></li>
        <li><a class="btn btn-default" href='<?php echo base_url(); ?>about/nsbm_nibm'>NSBM & NIBM</a></li>
        <li><a class="btn btn-default" href='<?php echo base_url(); ?>about/global'>NSBM Global</a></li>
    </ul>
</div>