<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jul 17, 2014, 12:54:26 PM
 */

?>
<body class="pages-body">
    <?php $this->load->view('main_all/vmain_all_top_menu'); ?>  <!--include menu and header-->
    <div class="container pages-container">
        <div id="content" class="row-fluid">
            <div class="well col-md-3 col-sm-4 sidebar-main">
                <?php $this->load->view('main_all/vmain_side_menu.php') ?>
            </div>

            <div class="col-md-9 col-sm-8 right-content">
                <!--School of Computing main page content-->
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url();?>"><span class="glyphicon glyphicon-home"></span></a></li>
                            <?php echo create_breadcrumb(); ?>
                        </ol>
                        <h1 class="text-info">Sports and Health</h1>
                        <h1><small>Sports</small></h1>
                        <p class="text-justify text-info font-schools">
                            The environment of NSBM provides a vibrant social life to all young undergraduates. Annual sports meets and inter batch sports activities are organized and raised by NSBM and the student council in each part of the year. Not only does NSBM provide required facilities to these sports events but it also supplies qualified coaches to keep the spirit up of the young students to balance their studies with extra curricular activities.
                        </p>
                        <h1><small>Health</small></h1>
                        <p class="text-justify text-info font-schools">
                            NSBM consider health as a vital factor to be a productive entity. A full-time medical consultancy service is available free of charge at NSBM for the benefit of students and staff.
                        </p>
                        <!--<img class="pull-right" style="z-index:-1; opacity: 0.4; filter: alpha(opacity=40); width:60%;" src="http://www.sandersontestprep.com/wp-content/uploads/2013/12/slider-2-student-girl1.png"/>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
