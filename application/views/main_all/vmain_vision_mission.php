<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jul 17, 2014, 12:54:26 PM
 */

?>
<body class="pages-body">
    <?php $this->load->view('main_all/vmain_all_top_menu'); ?>  <!--include menu and header-->
    <div class="container pages-container">
        <div id="content" class="row-fluid">
            <div class="well col-md-3 col-sm-4 sidebar-main">
                <?php $this->load->view('main_all/vmain_side_menu.php') ?>
            </div>

            <div class="col-md-9 col-sm-8 right-content">
                <!--School of Computing main page content-->
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url();?>"><span class="glyphicon glyphicon-home"></span></a></li>
                            <?php echo create_breadcrumb(); ?>
                        </ol>
                        <h1 class="text-info">Vision</h1>
                        <p class="text-justify text-info font-schools">
                            To be the foremost business school to create future leaders.	
                        </p>
                        <h1 class="text-info">Mission</h1>
                        <p class="text-justify text-info font-schools">
                            Contribute to building a prosperous society through education, learning and research.
                        </p>
                        <h1 class="text-info">Values</h1>
                        <ul class="font-schools">
                            <li><i class="glyphicon glyphicon-star-empty text-muted"></i> Engaging in the transformation of lives.</li>
                            <li><i class="glyphicon glyphicon-star-empty text-muted"></i> Achieving academic excellence.</li>
                            <li><i class="glyphicon glyphicon-star-empty text-muted"></i> Research orientation.</li>
                            <li><i class="glyphicon glyphicon-star-empty text-muted"></i> Networking across the globe.</li>
                            <li><i class="glyphicon glyphicon-star-empty text-muted"></i> Inspiration to learn.</li>
                            <li><i class="glyphicon glyphicon-star-empty text-muted"></i> Nurturing innovation and change.</li>
                            <li><i class="glyphicon glyphicon-star-empty text-muted"></i> Growing with the community.</li>
                        </ul>
                        <!--<img class="pull-right" style="z-index:-1; opacity: 0.4; filter: alpha(opacity=40); width:60%;" src="http://www.sandersontestprep.com/wp-content/uploads/2013/12/slider-2-student-girl1.png"/>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
