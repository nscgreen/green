<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jul 17, 2014, 12:54:26 PM
 */

?>
<body class="pages-body">
    <?php $this->load->view('main_all/vmain_all_top_menu'); ?>  <!--include menu and header-->
    <div class="container pages-container">
        <div id="content" class="row-fluid">
            <div class="well col-md-3 col-sm-4 sidebar-main">
                <?php $this->load->view('main_all/vmain_side_menu.php') ?>
            </div>

            <div class="col-md-9 col-sm-8 right-content">
                <!--School of Computing main page content-->
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url(); ?>"><span class="glyphicon glyphicon-home"></span></a></li>
                            <?php echo create_breadcrumb(); ?>
                        </ol>
                        <h1 class="text-info">Well Wishes</h1>
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                            <h4 class="text-info">Message from the CEO</h4>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <p class="text-justify text-info font-schools">
                                            Welcome to the National School of Business Management. At NSBM we aim to introduce novelty and innovation to the field of higher education. NSBM, the degree school of NIBM offers academic programmes aimed at producing skillful graduates who can uplift the performance of businesses and the economy. We always attempt to play a distinctive role in furthering the nation’s economic development. Degree Programmes offered by NSBM are designed to cater to the requirements of the business community and the economy. We have invested in state-of-the-art learning facilities to provide our undergraduates with a new experience in learning and sharing knowledge. Our strategic plan of establishing an array of academic programmes and facilities in the next five years will further strengthen our journey towards serving the field of higher education. Our Academic Departments consist of members with strong academic backgrounds, appropriately blended with industry experience. Extensive quality control procedures implemented will ensure the delivery of programmes at international standards. Our long-term affiliations with world renowned universities such as University College Dublin- Ireland and Limkokwing University will indubitably strengthen our capacity to offer world-class qualifications at NSBM. We are inspired to redefine the field of higher education to deliver the promise of providing a conducive academic environment and a world of opportunities to shape the future of our next generation. I warmly invite you to join us to experience the future of higher education.
                                        </p>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <img class="thumbnail pull-left" style=" width: 110px; height: 140px; margin-right:10px;" src="../../../assets/img/main/directors/vice_chancellor.jpg">
                                                <div class="text-justify p-news" style="margin-top: 50px">
                                                    <p>Dr. E. A Weerasinghe</p>
                                                    <p>CEO - National School of Business Management</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                            <h4 class="text-info">Message from the Dean – School of Business</h4>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p class="text-justify text-info font-schools">
                                            Welcome to NSBM School of Business and to the start of an exciting and enriching journey of knowlledge. You will feel inspired and challenged while studying with us. Besides classroom learning, you will have the opportunity to work in different industries with our industry partners and get to think and write about your real life experiences. During your study, you will learn new skills through a variety of degree programmes as well as create a good network of colleagues and professionals. You will meet our committed and caring staff and lecturers who are academically and professionally qualified to go an extra mile to mentor and guide you. Our facilities are new and have been designed to create a conducive study environment as well as a place to simply enjoy your academic life. We are committed to be a socially responsible business school, delivering our expertise to clients that can be best used in working with businesses, public sector and the private sector organisations to address their challenges. A cornerstone of this social responsibility is a commitment to offer high-quality higher education to the most capable students, regardless of their background. With new corporate models merging, we orient our students to incorporate requirements and to groom them into global business personalities who would not only contribute to the development of their organizations but would also provide leadership to society to make the world a better place for living. We hope you will enjoy your time at NSBM School of Business and also succeed in your learning journey with us.
                                        </p>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <img class="thumbnail pull-left" style=" width: 110px; height: 140px; margin-right:10px;" src="../../../assets/img/main/directors/head_mgt.jpg">
                                                <div class="text-justify p-news" style="margin-top: 50px">
                                                    <p>Mr. D. M. A Kulasooriya</p>
                                                    <p>Dean - School of Business National School of Business Management</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                            <h4 class="text-info">Message from the Dean – School of Computing</h4>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <p class="text-justify text-info font-schools">
                                            It’s my distinct pleasure to welcome you to the School of Computing of National School of Business Management. We are inspired by the common desire to make learning and sharing of knowledge to be blended with innovation to deliver value to every aspect of businesses and communities. At School of Computing we are committed to provide superior undergraduate and postgraduate programmes that prepare our participants to face challenges in diverse and technologically challenging workplaces. Our affiliations with world renowned universities pave the opportunity for our participants to experience latest trends in the fields of Computing and Information Technology. Our passion for ensuring quality in every activity at School of Computing will enable you to experience undisrupted learning environment with international standards of teaching and evaluation. Also we continuously review our programmes and facilities to cope with the global dynamics in the fields of Computing and Information Technology and constantly respond to the needs of businesses. Our exceptional faculty and staff are committed to guide and assist you throughout your academic life. It’s indeed an honour and a pleasure for me to invite you to the dynamic environment of this amazing learning community. Join us to experience the true sense of learning through our approach of reflection, discovery and advancement.
                                        </p>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <img class="thumbnail pull-left" style=" width: 110px; height: 140px; margin-right:10px;" src="../../../assets/img/main/directors/head_cs.jpg">
                                                <div class="text-justify p-news" style="margin-top: 50px">
                                                    <p>Mrs. Kishani Wijesiriwardhane</p>
                                                    <p>Dean - School of Computing National School of Business Management</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

<!--<img class="pull-right" style="z-index:-1; opacity: 0.4; filter: alpha(opacity=40); width:60%;" src="http://www.sandersontestprep.com/wp-content/uploads/2013/12/slider-2-student-girl1.png"/>-->
                    </div>
                </div>
            </div>
        </div>
    </div>

    
