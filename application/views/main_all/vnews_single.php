<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jul 17, 2014, 12:54:26 PM
 */

?>
<body class="pages-body">
    <?php $this->load->view('main_all/vmain_all_top_menu'); ?>  <!--include menu and header-->
    <div class="container pages-container">
        <div id="content" class="row-fluid">
            <div class="well col-md-3 col-sm-4 sidebar-main">
                <?php $this->load->view('main_all/vmain_side_menu.php') ?>
            </div>

            <div class="col-md-9 col-sm-8 right-content">
                <!--School of Computing main page content-->
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url();?>"><span class="glyphicon glyphicon-home"></span></a></li>
                            <?php echo create_breadcrumb(); ?>
                        </ol>
                        <h1 class="text-info">NSBM News</h1>
                        <h3 class="text-success">Progress Review and Media Brief on Green University Town</h3>
                        <img class="thumbnail pull-left" style=" width: 200px; height: 150px; margin-right:10px;" src="http://www.nsbm.lk/4togallery/Progress_Review/1.jpg">
                        <p class="font-schools text-justify">A media briefing and a progress review of the construction of NSBM Green University town, a venture inspired by the vision of Mahinda Chintanaya to develop Sri Lanka to be the ‘Knowledge Hub’....
                        A media briefing and a progress review of the construction of NSBM Green University town, a venture inspired by the vision of Mahinda Chintanaya to develop Sri Lanka to be the ‘Knowledge Hub’....
                        A media briefing and a progress review of the construction of NSBM Green University town, a venture inspired by the vision of Mahinda Chintanaya to develop Sri Lanka to be the ‘Knowledge Hub’....
                        A media briefing and a progress review of the construction of NSBM Green University town, a venture inspired by the vision of Mahinda Chintanaya to develop Sri Lanka to be the ‘Knowledge Hub’....
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
