<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jul 17, 2014, 10:27:10 PM
 */

?>
<a href="<?php echo base_url(); ?>computing" ><img class="center-block" src="http://www.nsbm.lk/nsbm_img/icon/cs_schoollogo.png"/></a>
<center><h3>Future Students</h3></center>
<div id="sidebar-com-menu">
    <ul>
        <li><a class="btn btn-info" href='<?php echo base_url(); ?>computing/undergraduate'>Undergraduate</a></li>
        <li><a class="btn btn-info" href='<?php echo base_url(); ?>computing/postgraduate'>Postgraduate</a></li>
        <li><a class="btn btn-info" href='<?php echo base_url(); ?>computing/foundation'>Foundation Courses</a></li>
        <li><a class="btn btn-danger" href='<?php echo base_url(); ?>registrations'>Online Registration</a></li>
    </ul>
</div>
<center><h3>Student life</h3></center>
<div id="sidebar-com-menu" class="">
    <ul>
        <li><a class="btn btn-default" href='<?php echo base_url(); ?>computing/school_union'>School Union</a></li>
        <li><a class="btn btn-default" href='<?php echo base_url(); ?>computing/school_map'>School Map</a></li>
        <li><a class="btn btn-default" href='<?php echo base_url(); ?>computing/services'>Services</a></li>
        <li><a class="btn btn-default" href='<?php echo base_url(); ?>more/library'>Library</a></li>
    </ul>
</div>
<center><h3>Quick links</h3></center>
<div id="sidebar-com-menu" class="">
    <ul>
        <li><a class="btn btn-default" href='<?php echo base_url(); ?>about/future_plans'>Accommodations</a></li>
        <li><a class="btn btn-default" href='<?php echo base_url(); ?>computing/meet_coordinator'>Meet Coordinator</a></li>
        <li><a class="btn btn-default" href='<?php echo base_url(); ?>computing/faculty_staff'>Faculty & Staff</a></li>
        <li><a class="btn btn-default" href='<?php echo base_url(); ?>computing/vacancies'>Vacancies</a></li>
    </ul>
</div>