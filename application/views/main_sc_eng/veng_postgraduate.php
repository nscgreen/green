<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jul 17, 2014, 12:54:26 PM
 */

?>
<body class="pages-body">
    <?php $this->load->view('vmenu'); ?>  <!--include menu-->
    <div class="container pages-container">
        <div id="content" class="row-fluid">
            <div class="well col-md-3 sidebar-eng">
                <?php $this->load->view('main_sc_eng/veng_side_menu.php') ?>
            </div>

            <div class="col-md-9 right-content">
                <!--School of Computing main page content-->
                <div class="row">
                    <div class="col-sm-12">
                        <h1 class="text-info">Postgraduate Programmes</h1>
                        <p class="text-justify text-info font-schools">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>
                        <ul>
                            <li><i class="fa fa-graduation-cap text-muted"></i><a class="font-schools" href="target_cs.php?eid=undergraduate_ucd_mis" target="_self"> MSc in Information Technology (UCD)</a></li>
                            <li><i class="fa fa-graduation-cap text-muted"></i><a class="font-schools" href="target_cs.php?eid=undergraduate_ucd_cs" target="_self"> Postgraduate Diploma In Software Engineering</a></li>
                            <li><i class="fa fa-graduation-cap text-muted"></i><a class="font-schools" href="target_cs.php?eid=undergraduate_plm_se" target="_self"> Postgraduate Diploma In Computer Networks</a></li>
                        </ul>
                        <img style="z-index:-1; opacity: 0.3; filter: alpha(opacity=40); padding-top:0px; width:100%;" src="http://www.degreefinders.com/assets/images/human-resource-degrees.jpg"/>
                    </div>
                </div>
            </div>
        </div>
    </div>

