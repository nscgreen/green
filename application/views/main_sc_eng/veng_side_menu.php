<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jul 17, 2014, 10:27:10 PM
 */

?>
<a href="<?php echo base_url(); ?>engineering" ><img class="center-block" src="http://www.nsbm.lk/nsbm_img/icon/eng_schoollogo.png"/></a>
<center><h3>Future Students</h3></center>
<div id="sidebar-eng-menu">
    <ul>
        <li><a class="btn btn-warning" href='<?php echo base_url(); ?>engineering'>Undergraduate</a></li>
        <li><a class="btn btn-warning" href='<?php echo base_url(); ?>engineering'>Postgraduate</a></li>
        <li><a class="btn btn-warning" href='<?php echo base_url(); ?>engineering'>Foundation Courses</a></li>
        <li><a class="btn btn-danger" href='<?php echo base_url(); ?>registrations'>Online Registration</a></li>
    </ul>
</div>
<center><h3>Student life</h3></center>
<div id="sidebar-eng-menu" class="">
    <ul>
        <li><a class="btn btn-default" href='<?php echo base_url(); ?>engineering'>School Union</a></li>
        <li><a class="btn btn-default" href='<?php echo base_url(); ?>engineering'>School Map</a></li>
        <li><a class="btn btn-default" href='<?php echo base_url(); ?>engineering'>Services</a></li>
        <li><a class="btn btn-default" href='<?php echo base_url(); ?>more/library'>Library</a></li>
    </ul>
</div>
<center><h3>Quick links</h3></center>
<div id="sidebar-eng-menu" class="">
    <ul>
        <li><a class="btn btn-default" href='<?php echo base_url(); ?>about/future_plans'>Accommodations</a></li>
        <li><a class="btn btn-default" href='<?php echo base_url(); ?>engineering'>Meet Coordinator</a></li>
        <li><a class="btn btn-default" href='<?php echo base_url(); ?>engineering'>Faculty & Staff</a></li>
        <li><a class="btn btn-default" href='<?php echo base_url(); ?>engineering'>Vacancies</a></li>
    </ul>
</div>