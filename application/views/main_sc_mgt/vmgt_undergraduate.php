<?php
/*
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jul 17, 2014, 12:54:26 PM
 */
?>
<body class="pages-body">
    <?php $this->load->view('main_all/vmain_all_top_menu'); ?>  <!--include menu and header-->
    <div class="container pages-container">
        <div id="content" class="row-fluid">
            <div class="well col-md-3 col-sm-4 sidebar-mng">
                <?php $this->load->view('main_sc_mgt/vmgt_side_menu.php') ?>
            </div>

            <div class="col-md-9 col-sm-8 right-content">
                <!--School of Computing main page content-->
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url();?>"><span class="glyphicon glyphicon-home"></span></a></li>
                            <?php echo create_breadcrumb(); ?>
                        </ol>
                        <h1 class="text-info">Undergraduate Degree Programmes</h1>
                        <p class="text-justify text-info font-schools">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>

                        <h1><small>Offered by National School of Business Management</small></h1>
                        <ul>
                            <li><i class="fa fa-graduation-cap text-muted"></i><a class="font-schools" href="target_cs.php?eid=undergraduate_ugc_mis" target="_self"> BSc in Human Resource Management - Special (University Grant Commission Approved)</a></li>
                            <li><i class="fa fa-graduation-cap text-muted"></i><a class="font-schools" href="target_cs.php?eid=undergraduate_ugc_mis" target="_self"> BSc in Logistics Management - Special (University Grant Commission Approved)</a></li>
                            <li><i class="fa fa-graduation-cap text-muted"></i><a class="font-schools" href="target_cs.php?eid=undergraduate_ugc_mis" target="_self"> BSc in Industrial Management - Special (University Grant Commission Approved)</a></li>
                            <li><i class="fa fa-graduation-cap text-muted"></i><a class="font-schools" href="target_cs.php?eid=undergraduate_ugc_mis" target="_self"> BSc in Project Management - Special (University Grant Commission Approved)</a></li>
                        </ul>

                        <h1><small>Offered by University College of Dublin</small></h1>
                        <ul>
                            <li><i class="fa fa-graduation-cap text-muted"></i><a class="font-schools" href="target_cs.php?eid=undergraduate_ucd_mis" target="_self"> BSc (Hons) in Management</a></li>
                            <li><i class="fa fa-graduation-cap text-muted"></i><a class="font-schools" href="target_cs.php?eid=undergraduate_ucd_cs" target="_self"> BSc (Hons) in Human Resource Management</a></li>
                        </ul>

                        <h1><small>Offered by Plymouth University-United Kingdom</small></h1>
                        <ul>
                            <li><i class="fa fa-graduation-cap text-muted"></i><a class="font-schools" href="target_cs.php?eid=undergraduate_ucd_mis" target="_self"> BSc (Hons) Marketing Management</a></li>
                            <li><i class="fa fa-graduation-cap text-muted"></i><a class="font-schools" href="target_cs.php?eid=undergraduate_ucd_cs" target="_self"> BSc (Hons) Accounting & Finance</a></li>
                            <li><i class="fa fa-graduation-cap text-muted"></i><a class="font-schools" href="target_cs.php?eid=undergraduate_plm_se" target="_self"> BSc (Hons) Operations and Logistics Management</a></li>
                            <li><i class="fa fa-graduation-cap text-muted"></i><a class="font-schools" href="target_cs.php?eid=undergraduate_plm_se" target="_self"> BSc (Hons) International Management and Business</a></li>
                            <li><i class="fa fa-graduation-cap text-muted"></i><a class="font-schools" href="target_cs.php?eid=undergraduate_plm_se" target="_self"> BSc (Hons) Events, Tourism and Hospitality</a></li>
                        </ul>

                        <h1><small>Offered by Limkokwing University of Creative Technology-Malaysia</small></h1>
                        <ul>
                            <li><i class="fa fa-graduation-cap text-muted"></i><a class="font-schools" href="target_cs.php?eid=undergraduate_ucd_mis" target="_self"> BA in Interior Design</a></li>
                        </ul>
                        <br/>
                    </div>
                </div>
            </div>
        </div>
    </div>
