<body class="pages-body">
    <?php $this->load->view('main_all/vmain_all_top_menu'); ?>  <!--include menu and header-->
    <div class="container pages-container">
        <div id="content" class="row-fluid">
            <div class="well col-md-3 col-sm-4 sidebar-com">
                <?php $this->load->view('main_sc_com/vcom_side_menu.php') ?>
            </div>

            <div class="col-md-9 col-sm-8 right-content">
                <!--School of Computing main page content-->
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url();?>"><span class="glyphicon glyphicon-home"></span></a></li>
                            <?php echo create_breadcrumb(); ?>
                        </ol>
                        <center>
                        <img style="width: 256px" src="http://www.nsbm.lk/nsbm_img/icon/cs_schoollogo.png"/>
                        </center>
                        <br/>
                        <h1 class="text-center">Welcome to NSBM School of Computing</h1><br/>
                        <p class="text-justify text-info font-schools">
                            NSBM has well facilitated building for computing section. It provides well equiped computer laboratories, network
                            lab. Well knowledged lecturers are always prepared to inpart their knowledge to students. Computing building 
                            has well conditioned lecture halls with free wifi acess to students. Many renowned IT professionals and Software
                            Engineers come from NSBM.
                        </p>
                        <h1><small>Learning Information Technology through NSBM</small></h1>
                        <p class="text-justify text-info font-schools">
                            NSBM, the business school of Sri Lanka will integrate innovation, entrepreneurship with a culturally and ethnically diverse workforce, international engagement of expertise in the field of management, IT Education.
                        </p>
                        <h1><small>IT Division</small></h1>
                        <p class="text-justify text-info font-schools">
                            As forward thinking school in higher education NSBM assures the vital need to make learning relevant to industry expectations. NSBM has forged strategic collaborations with leading universities in the world that enable its students to expose themselves with global thinking and best in global industry.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>