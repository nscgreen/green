<body class="pages-body">
    <?php $this->load->view('main_all/vmain_all_top_menu'); ?>  <!--include menu and header-->
    <div class="container pages-container">
        <div id="content" class="row-fluid">
            <div class="well col-md-3 col-sm-4 sidebar-eng">
                <?php $this->load->view('main_sc_eng/veng_side_menu.php') ?>
            </div>

            <div class="col-md-9 col-sm-8 right-content">
                <!--School of Computing main page content-->
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url();?>"><span class="glyphicon glyphicon-home"></span></a></li>
                            <?php echo create_breadcrumb(); ?>
                        </ol>
                        <center>
                        <img style="width: 256px" src="http://www.nsbm.lk/nsbm_img/icon/eng_schoollogo.png"/>
                        </center>
                        <br/>
                        <h1 class="text-center">Welcome to NSBM School of Engineering</h1><br/>
                        <p class="text-justify text-info font-schools">
                            Much anticipated School of Engineering of NSBM will be opened in the premises of Green University in Homagama near future. School of Engineering will have advance buildings with qualified lecturers. Green University will open at the end of next year.
                        </p>
                        <h1><small>Aims of the School</small></h1>
                        <p class="text-justify text-info font-schools">
                            The primary aim of the Schoolis forming engineers of the highest quality who, with experience, should be able to hold responsible positions at the highest levels of the profession, possessing the wisdom to recognize their professional development.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>