<section id="contacts" class="fotter-section">
   <div class="container">
       <div class="row">
           <!-- CONTAC HEADER-->
           <div class="text-center">
               <div id="foot-head">
                   <div class="col-md-12">
                       <h3>
                           <div class="page-header">
                               <font style="color: white"><h1>Contact Information </font><small><font style="color: white">Any time...</font></small></h1>
                           </div>
                       </h3>
                   </div>
               </div>	
           </div>
       </div>
       <!--/ CONTAC HEADER-->
       <div class="row">
           <div align="left" class="col-md-9">
               <h2 class="text-info font-play">We are here</h2>
               <div class="row">
                   <div class="col-sm-4">
                       <p class="connect-submenu"><strong>School Of Computing</strong></p>
                       <h4 class="h5-contact"><span>Tel:</span> +94(11) 544 6000</h4>
                       <h4 class="h5-contact"><span>Address:</span><br/>
                       318, High Level Road, Colombo 05, Sri Lanka.
                       </h4>
                   </div>
                   <div class="col-sm-4">
                       <p class="connect-submenu"><strong>NSBM Library</strong></p>
                       <h4 class="h5-contact"><span>Tel:</span> +94(11) 528 8827</h4>
                   </div>
                   <div class="col-sm-4">
                       <p class="connect-submenu"><strong>NSBM Exam Division </strong></p>
                       <h4 class="h5-contact"><span>Tel:</span> +94(11) 544 5004</h4>
                       <h4 class="h5-contact"><span>E-mail :</span> info@nsbm.lk</h4>
                   </div>
               </div>
           </div>
           
           <div class="col-md-3">
               <h2 class="text-info font-play">Keep in touch with us</h2>
               <ul class="centered" style="margin: 0 auto; padding-left: 20%; text-align: left">
                   <li><a href="" class="con-social-links"><img src="<?php echo base_url();?>assets/img/main/facebook.png"> Facebook</a></li>
                   <li><a href="" class="con-social-links"><img src="<?php echo base_url();?>assets/img/main/twitter.png"> Twitter</a></li>
                   <li><a href="" class="con-social-links"><img src="<?php echo base_url();?>assets/img/main/googleplusalt.png"> Google+</a></li>
                   <li><a href="" class="con-social-links"><img src="<?php echo base_url();?>assets/img/main/flickr.png"> Flicker</a></li>
               </ul>
           </div>
       </div><!-- / ROW-->
       <div class="row">
           <h2 class="text-info font-play">Connect with NSBM Network</h2>
           <div class="row" style="">
                <ul class="list-inline" style="display: inline">
                    <li class="connect-links underline"><a href="http://mynsbm.d2real.com" target="_blank"><span class="glyphicon glyphicon-link"></span> My-NSBM</a></li>
                    <li class="connect-links underline"><a href="http://lms.d2real.com" target="_blank"><span class="glyphicon glyphicon-link"></span> Learning Management System</a></li>
                    <li class="connect-links underline"><a href="http://forum.d2real.com" target="_blank"><span class="glyphicon glyphicon-link"></span> Forum</a></li>
                    <li class="connect-links underline"><a href="http://events.d2real.com" target="_blank"><span class="glyphicon glyphicon-link"></span> Event Management System</a></li>
                    <li class="connect-links underline"><a href="http://communities.d2real.com" target="_blank"><span class="glyphicon glyphicon-link"></span> Communities</a></li>
                    <li class="connect-links underline"><a href="http://help.d2real.com" target="_blank"><span class="glyphicon glyphicon-link"></span> Help</a></li>
                </ul>
            </div>
       </div>
       
       <hr>
       <div class="row">
           <center>
               <div style="background: #ffffff; width: 100%">
               <img class="img-responsive" src="http://www.nsbm.lk/lib/images/content/footer_logo.jpg">
               </div>
               <hr>
               <address>
                   <strong>National School of Business Management Ltd.</strong><br>
                   <i class="fa-icon-map-marker"></i> 309, High Level Rd, Colombo 05, Sri Lanka<br>
                   <i class="fa-icon-phone-sign"></i> +94 (11) 544 5000
                   <div class="foot-line"></div>
               </address>
               <p>Copyright © National School of Business Management</p>
           </center>
       </div>
   </div>
   <br>
   <iframe width="100%" height="100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d990.2877812584475!2d79.88354246951941!3d6.87248899402029!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xadb9d7e5a5710f63!2sNational+School+of+Business+Management+Library!5e0!3m2!1sen!2sus!4v1405451160770"></iframe>
</section>

<?php include 'includes/main_footer_cnt.php'; ?>
</body>
</html>