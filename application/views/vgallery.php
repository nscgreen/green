<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jul 17, 2014, 12:54:26 PM
 */

?>
<body class="pages-body">
    <?php $this->load->view('main_all/vmain_all_top_menu'); ?>  <!--include menu and header-->
    <div class="container pages-container">
        <div id="content" class="row-fluid">
            <div class="well col-md-3 col-sm-4 sidebar-main">
                <?php $this->load->view('main_all/vmain_side_menu.php') ?>
            </div>

            <div class="col-md-9 col-sm-8 right-content">
                <!--School of Computing main page content-->
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url();?>"><span class="glyphicon glyphicon-home"></span></a></li>
                            <?php echo create_breadcrumb(); ?>
                        </ol>
                        <h1 class="text-info">Gallery <small>find more videos and photos..</small></h1>
                        <div class="row">
                            <div class='col-sm-4 col-xs-6 underline'>
                                <a class="thumbnail fancybox" style="height: 280px; padding: 0; width: 200px" rel="gallery1" href="http://www.nsbm.lk/4togallery/Convocation_2014_BA_in_Interior_Architecture/2.jpg">
                                    <img alt="" src="http://www.nsbm.lk/4togallery/Convocation_2014_BA_in_Interior_Architecture/2.jpg" />
                                    <br />
                                    <h4 class='text-center text-muted font-play'>CONVOCATION 2014 Interior Architecture – Batch 02</h4>
                                </a>
                                <a class="hidden thumbnail fancybox image-responsive" rel="gallery1" href="http://www.nsbm.lk/4togallery/Convocation_2014_BA_in_Interior_Architecture/1.jpg" title="CONVOCATION 2014 Interior Architecture – Batch 02">
                                    <img src="http://www.nsbm.lk/4togallery/Convocation_2014_BA_in_Interior_Architecture/1.jpg" alt="" />
                                </a>
                                <a class="hidden thumbnail fancybox image-responsive" rel="gallery1" href="http://www.nsbm.lk/4togallery/Convocation_2014_BA_in_Interior_Architecture/3.jpg" title="CONVOCATION 2014 Interior Architecture – Batch 02">
                                    <img src="http://www.nsbm.lk/4togallery/Convocation_2014_BA_in_Interior_Architecture/3.jpg" alt="" />
                                </a>
                                <a class="hidden thumbnail fancybox image-responsive" rel="gallery1" href="http://www.nsbm.lk/4togallery/Convocation_2014_BA_in_Interior_Architecture/4.jpg" title="CONVOCATION 2014 Interior Architecture – Batch 02">
                                    <img src="http://www.nsbm.lk/4togallery/Convocation_2014_BA_in_Interior_Architecture/4.jpg" alt="" />
                                </a>
                                <a class="hidden thumbnail fancybox image-responsive" rel="gallery1" href="http://www.nsbm.lk/4togallery/Convocation_2014_BA_in_Interior_Architecture/5.jpg" title="CONVOCATION 2014 Interior Architecture – Batch 02">
                                    <img src="http://www.nsbm.lk/4togallery/Convocation_2014_BA_in_Interior_Architecture/5.jpg" alt="" />
                                </a>
                            </div>
                            <div class='col-sm-4 col-xs-6 underline'>
                                <a class="thumbnail fancybox" style="height: 280px; padding: 0; width: 200px" rel="gallery2" href="http://www.nsbm.lk/icludes/news/images/NSBM_Software_Competition/2.jpg">
                                    <img alt="" src="http://www.nsbm.lk/icludes/news/images/NSBM_Software_Competition/2.jpg" />
                                    <br />
                                    <h4 class='text-center text-muted font-play'>NSBM Software Competition (NSC) 2014</h4>
                                </a>
                                <a class="hidden thumbnail fancybox image-responsive" rel="gallery2" href="http://www.nsbm.lk/icludes/news/images/NSBM_Software_Competition/3.jpg" title="NSBM Software Competition (NSC) 2014">
                                    <img src="http://www.nsbm.lk/icludes/news/images/NSBM_Software_Competition/3.jpg" alt="" />
                                </a>
                                <a class="hidden thumbnail fancybox image-responsive" rel="gallery2" href="http://www.nsbm.lk/icludes/news/images/NSBM_Software_Competition/5.jpg" title="NSBM Software Competition (NSC) 2014">
                                    <img src="http://www.nsbm.lk/icludes/news/images/NSBM_Software_Competition/5.jpg" alt="" />
                                </a>
                                <a class="hidden thumbnail fancybox image-responsive" rel="gallery2" href="http://www.nsbm.lk/icludes/news/images/NSBM_Software_Competition/6.jpg" title="NSBM Software Competition (NSC) 2014">
                                    <img src="http://www.nsbm.lk/icludes/news/images/NSBM_Software_Competition/6.jpg" alt="" />
                                </a>
                                <a class="hidden thumbnail fancybox image-responsive" rel="gallery2" href="http://www.nsbm.lk/icludes/news/images/NSBM_Software_Competition/8.jpg" title="NSBM Software Competition (NSC) 2014">
                                    <img src="http://www.nsbm.lk/icludes/news/images/NSBM_Software_Competition/8.jpg" alt="" />
                                </a>
                            </div>
                            <div class='col-sm-4 col-xs-6 underline'>
                                <a class="thumbnail fancybox" style="height: 280px; padding: 0; width: 200px" rel="gallery1" href="http://www.nsbm.lk/4togallery/Engage_Youth_Workshop/1.jpg">
                                    <img alt="" src="http://www.nsbm.lk/4togallery/Engage_Youth_Workshop/1.jpg" />
                                    <br />
                                    <h4 class='text-center text-muted font-play'>Engage Youth - Workshop by NSAC</h4>
                                </a>
                                <a class="hidden thumbnail fancybox image-responsive" rel="gallery1" href="http://www.nsbm.lk/4togallery/Engage_Youth_Workshop/2.jpg" title="Engage Youth - Workshop by NSAC">
                                    <img src="http://www.nsbm.lk/4togallery/Engage_Youth_Workshop/2.jpg" alt="" />
                                </a>
                                <a class="hidden thumbnail fancybox image-responsive" rel="gallery1" href="http://www.nsbm.lk/4togallery/Engage_Youth_Workshop/3.jpg" title="Engage Youth - Workshop by NSAC">
                                    <img src="http://www.nsbm.lk/4togallery/Engage_Youth_Workshop/3.jpg" alt="" />
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class='col-sm-4 col-xs-6 underline'>
                                <a class="thumbnail fancybox" style="height: 280px; padding: 0; width: 200px" rel="gallery1" href="http://www.nsbm.lk/4togallery/Progress_Review/1.jpg">
                                    <img alt="" src="http://www.nsbm.lk/4togallery/Progress_Review/1.jpg" />
                                    <br />
                                    <h4 class='text-center text-muted font-play'>Progress Review of Construction</h4>
                                </a>
                                <a class="hidden thumbnail fancybox image-responsive" rel="gallery1" href="http://www.nsbm.lk/4togallery/Progress_Review/2.jpg" title="Progress Review of Construction">
                                    <img src="http://www.nsbm.lk/4togallery/Progress_Review/2.jpg" alt="" />
                                </a>
                                <a class="hidden thumbnail fancybox image-responsive" rel="gallery1" href="http://www.nsbm.lk/4togallery/Progress_Review/3.jpg" title="Progress Review of Construction">
                                    <img src="http://www.nsbm.lk/4togallery/Progress_Review/3.jpg" alt="" />
                                </a>
                            </div>
                            <div class='col-sm-4 col-xs-6 underline'>
                                <a class="thumbnail fancybox" style="height: 280px; padding: 0; width: 200px" rel="gallery1" href="http://www.nsbm.lk/4togallery/Foundation_Drama/1.jpg">
                                    <img alt="" src="http://www.nsbm.lk/4togallery/Foundation_Drama/1.jpg" />
                                    <br />
                                    <h4 class='text-center text-muted font-play'>Foundation Batch in class mini Drama Fest</h4>
                                </a>
                                <a class="hidden thumbnail fancybox image-responsive" rel="gallery1" href="http://www.nsbm.lk/4togallery/Foundation_Drama/3.jpg" title="Foundation Batch in class mini Drama Fest">
                                    <img src="http://www.nsbm.lk/4togallery/Foundation_Drama/3.jpg" alt="" />
                                </a>
                                <a class="hidden thumbnail fancybox image-responsive" rel="gallery1" href="http://www.nsbm.lk/4togallery/Foundation_Drama/4.jpg" title="Foundation Batch in class mini Drama Fest">
                                    <img src="http://www.nsbm.lk/4togallery/Foundation_Drama/4.jpg" alt="" />
                                </a>
                                <a class="hidden thumbnail fancybox image-responsive" rel="gallery1" href="http://www.nsbm.lk/4togallery/Foundation_Drama/7.jpg" title="Foundation Batch in class mini Drama Fest">
                                    <img src="http://www.nsbm.lk/4togallery/Foundation_Drama/7.jpg" alt="" />
                                </a>
                            </div>
                            <div class='col-sm-4 col-xs-6 underline'>
                                <a class="thumbnail fancybox" style="height: 280px; padding: 0; width: 200px" rel="gallery1" href="http://www.nsbm.lk/4togallery/Mozila_day_at_NSBM_photos/1.jpg">
                                    <img alt="" src="http://www.nsbm.lk/4togallery/Mozila_day_at_NSBM_photos/1.jpg" />
                                    <br />
                                    <h4 class='text-center text-muted font-play'>Mozila day at NSBM</h4>
                                </a>
                                <a class="hidden thumbnail fancybox image-responsive" rel="gallery1" href="http://www.nsbm.lk/4togallery/Mozila_day_at_NSBM_photos/3.jpg" title="Mozila day at NSBM">
                                    <img src="http://www.nsbm.lk/4togallery/Mozila_day_at_NSBM_photos/3.jpg" alt="" />
                                </a>
                                <a class="hidden thumbnail fancybox image-responsive" rel="gallery1" href="http://www.nsbm.lk/4togallery/Mozila_day_at_NSBM_photos/4.jpg" title="WMozila day at NSBM">
                                    <img src="http://www.nsbm.lk/4togallery/Mozila_day_at_NSBM_photos/4.jpg" alt="" />
                                </a>
                            </div>
                        </div>
<!--                        <div class="row">
                            <div class='col-sm-4 col-xs-6 underline'>
                                <a class="thumbnail fancybox" style="height: 280px; padding: 0; width: 200px" rel="gallery1" href="http://farm6.staticflickr.com/5471/9036958611_fa1bb7f827_b.jpg">
                                    <img alt="" src="http://farm6.staticflickr.com/5471/9036958611_fa1bb7f827_b.jpg" />
                                    <br />
                                    <h4 class='text-center text-muted font-play'>CONVOCATION 2014 Interior Architecture – Batch 02</h4>
                                </a>
                                <a class="hidden thumbnail fancybox image-responsive" rel="gallery1" href="http://farm6.staticflickr.com/5471/9036958611_fa1bb7f827_b.jpg" title="Westfield Waterfalls - Middletown CT Lower (Graham_CS)">
                                    <img src="http://farm6.staticflickr.com/5471/9036958611_fa1bb7f827_m.jpg" alt="" />
                                </a>
                                <a class="hidden thumbnail fancybox image-responsive" rel="gallery1" href="http://farm6.staticflickr.com/5471/9036958611_fa1bb7f827_b.jpg" title="Westfield Waterfalls - Middletown CT Lower (Graham_CS)">
                                    <img src="http://farm6.staticflickr.com/5471/9036958611_fa1bb7f827_m.jpg" alt="" />
                                </a>
                                <a class="hidden thumbnail fancybox image-responsive" rel="gallery1" href="http://farm4.staticflickr.com/3824/9041440555_2175b32078_b.jpg" title="Calm Before The Storm (One Shoe Photography Ltd.)">
                                    <img src="http://farm4.staticflickr.com/3824/9041440555_2175b32078_m.jpg" alt="" />
                                </a>
                                <a class="hidden thumbnail fancybox image-responsive" rel="gallery1" href="http://farm3.staticflickr.com/2870/8985207189_01ea27882d_b.jpg" title="Lambs Valley (JMImagesonline.com)">
                                    <img src="http://farm3.staticflickr.com/2870/8985207189_01ea27882d_m.jpg" alt="" />
                                </a>
                                <a class="hidden thumbnail fancybox image-responsive" rel="gallery1" href="http://farm4.staticflickr.com/3677/8962691008_7f489395c9_b.jpg" title="Grasmere Lake (Phil 'the link' Whittaker (gizto29))">
                                    <img src="http://farm4.staticflickr.com/3677/8962691008_7f489395c9_m.jpg" alt="" />
                                </a>
                            </div>
                            <div class='col-sm-4 col-xs-6 underline'>
                                <a class="thumbnail fancybox" style="height: 280px; padding: 0; width: 200px" rel="gallery1" href="http://farm6.staticflickr.com/5471/9036958611_fa1bb7f827_b.jpg">
                                    <img alt="" src="http://farm6.staticflickr.com/5471/9036958611_fa1bb7f827_b.jpg" />
                                    <br />
                                    <h4 class='text-center text-muted font-play'>CONVOCATION 2014 Interior Architecture – Batch 02</h4>
                                </a>
                                <a class="hidden thumbnail fancybox image-responsive" rel="gallery1" href="http://farm6.staticflickr.com/5471/9036958611_fa1bb7f827_b.jpg" title="Westfield Waterfalls - Middletown CT Lower (Graham_CS)">
                                    <img src="http://farm6.staticflickr.com/5471/9036958611_fa1bb7f827_m.jpg" alt="" />
                                </a>
                                <a class="hidden thumbnail fancybox image-responsive" rel="gallery1" href="http://farm6.staticflickr.com/5471/9036958611_fa1bb7f827_b.jpg" title="Westfield Waterfalls - Middletown CT Lower (Graham_CS)">
                                    <img src="http://farm6.staticflickr.com/5471/9036958611_fa1bb7f827_m.jpg" alt="" />
                                </a>
                                <a class="hidden thumbnail fancybox image-responsive" rel="gallery1" href="http://farm4.staticflickr.com/3824/9041440555_2175b32078_b.jpg" title="Calm Before The Storm (One Shoe Photography Ltd.)">
                                    <img src="http://farm4.staticflickr.com/3824/9041440555_2175b32078_m.jpg" alt="" />
                                </a>
                                <a class="hidden thumbnail fancybox image-responsive" rel="gallery1" href="http://farm3.staticflickr.com/2870/8985207189_01ea27882d_b.jpg" title="Lambs Valley (JMImagesonline.com)">
                                    <img src="http://farm3.staticflickr.com/2870/8985207189_01ea27882d_m.jpg" alt="" />
                                </a>
                                <a class="hidden thumbnail fancybox image-responsive" rel="gallery1" href="http://farm4.staticflickr.com/3677/8962691008_7f489395c9_b.jpg" title="Grasmere Lake (Phil 'the link' Whittaker (gizto29))">
                                    <img src="http://farm4.staticflickr.com/3677/8962691008_7f489395c9_m.jpg" alt="" />
                                </a>
                            </div>
                            <div class='col-sm-4 col-xs-6 underline'>
                                <a class="thumbnail fancybox" style="height: 280px; padding: 0; width: 200px" rel="gallery1" href="http://farm6.staticflickr.com/5471/9036958611_fa1bb7f827_b.jpg">
                                    <img alt="" src="http://farm6.staticflickr.com/5471/9036958611_fa1bb7f827_b.jpg" />
                                    <br />
                                    <h4 class='text-center text-muted font-play'>CONVOCATION 2014 Interior Architecture – Batch 02</h4>
                                </a>
                                <a class="hidden thumbnail fancybox image-responsive" rel="gallery1" href="http://farm6.staticflickr.com/5471/9036958611_fa1bb7f827_b.jpg" title="Westfield Waterfalls - Middletown CT Lower (Graham_CS)">
                                    <img src="http://farm6.staticflickr.com/5471/9036958611_fa1bb7f827_m.jpg" alt="" />
                                </a>
                                <a class="hidden thumbnail fancybox image-responsive" rel="gallery1" href="http://farm6.staticflickr.com/5471/9036958611_fa1bb7f827_b.jpg" title="Westfield Waterfalls - Middletown CT Lower (Graham_CS)">
                                    <img src="http://farm6.staticflickr.com/5471/9036958611_fa1bb7f827_m.jpg" alt="" />
                                </a>
                                <a class="hidden thumbnail fancybox image-responsive" rel="gallery1" href="http://farm4.staticflickr.com/3824/9041440555_2175b32078_b.jpg" title="Calm Before The Storm (One Shoe Photography Ltd.)">
                                    <img src="http://farm4.staticflickr.com/3824/9041440555_2175b32078_m.jpg" alt="" />
                                </a>
                                <a class="hidden thumbnail fancybox image-responsive" rel="gallery1" href="http://farm3.staticflickr.com/2870/8985207189_01ea27882d_b.jpg" title="Lambs Valley (JMImagesonline.com)">
                                    <img src="http://farm3.staticflickr.com/2870/8985207189_01ea27882d_m.jpg" alt="" />
                                </a>
                                <a class="hidden thumbnail fancybox image-responsive" rel="gallery1" href="http://farm4.staticflickr.com/3677/8962691008_7f489395c9_b.jpg" title="Grasmere Lake (Phil 'the link' Whittaker (gizto29))">
                                    <img src="http://farm4.staticflickr.com/3677/8962691008_7f489395c9_m.jpg" alt="" />
                                </a>
                            </div>
                        </div>-->
                          
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
