<nav class="navbar navbar-default navbar-fixed-top" role="navigation" style=" background: linear-gradient(rgb(223, 223, 223), rgba(148, 148, 148, 1)); box-shadow: 0 0 50px rgba(0,0,0,2);">
    <div class="container">
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#page-top">National School of Business Management</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div id="" class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav pull-right">
                <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                <li class="hidden">
                    <a href="#page-top"></a>
                </li>
                <li class="page-scroll">
                    <a href="<?php echo base_url(); ?>home">Home Page</a>
                </li>
                <li class="page-scroll">
                    <a href="<?php echo base_url(); ?>schools">Schools</a>
                </li>
                <li class="page-scroll">
                    <a href="#">My NSBM</a>
                </li>
                <li class="page-scroll">
                    <a href="<?php echo base_url(); ?>news">NSBM News</a>
                </li>
                <li class="page-scroll">
                    <a href="<?php echo base_url(); ?>gallery">NSBM Gallery</a>
                </li>
                <li class="page-scroll">
                    <a href="<?php echo base_url(); ?>aboutus">Quick links</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<div class='container' style="padding-top:200px;">
    <div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                        CONVOCATION 2014 Interior Architecture – Batch 02
                    </a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body">
                    <link rel="stylesheet" href="http://frontend.reklamor.com/fancybox/jquery.fancybox.css" media="screen">
                    <script src="http://frontend.reklamor.com/fancybox/jquery.fancybox.js"></script>


                    <div class='list-group gallery'>
                        <div class='col-lg-2 col-sm-3 col-xs-4'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="http://www.nsbm.lk/4togallery/Convocation_2014_BA_in_Interior_Architecture/6.jpg">
                                <img class="img-responsive" alt="" src="http://www.nsbm.lk/4togallery/Convocation_2014_BA_in_Interior_Architecture/6.jpg" />
                                <div class='text-right'>

                                </div> <!-- text-right / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-lg-2 col-sm-3 col-xs-4'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="http://www.nsbm.lk/4togallery/Convocation_2014_BA_in_Interior_Architecture/5.jpg">
                                <img class="img-responsive" alt="" src="http://www.nsbm.lk/4togallery/Convocation_2014_BA_in_Interior_Architecture/5.jpg" />
                                <div class='text-right'>

                                </div> <!-- text-right / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-lg-2 col-sm-3 col-xs-4'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="http://www.nsbm.lk/4togallery/Convocation_2014_BA_in_Interior_Architecture/4.jpg">
                                <img class="img-responsive" alt="" src="http://www.nsbm.lk/4togallery/Convocation_2014_BA_in_Interior_Architecture/4.jpg" />
                                <div class='text-right'>

                                </div> <!-- text-right / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-lg-2 col-sm-3 col-xs-4'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="http://www.nsbm.lk/4togallery/Convocation_2014_BA_in_Interior_Architecture/3.jpg">
                                <img class="img-responsive" alt="" src="http://www.nsbm.lk/4togallery/Convocation_2014_BA_in_Interior_Architecture/3.jpg" />
                                <div class='text-right'>

                                </div> <!-- text-right / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-lg-2 col-sm-3 col-xs-4'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="http://www.nsbm.lk/4togallery/Convocation_2014_BA_in_Interior_Architecture/2.jpg">
                                <img class="img-responsive" alt="" src="http://www.nsbm.lk/4togallery/Convocation_2014_BA_in_Interior_Architecture/2.jpg" />
                                <div class='text-right'>

                                </div> <!-- text-right / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-lg-2 col-sm-3 col-xs-4'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="http://www.nsbm.lk/4togallery/Convocation_2014_BA_in_Interior_Architecture/1.jpg">
                                <img class="img-responsive" alt="" src="http://www.nsbm.lk/4togallery/Convocation_2014_BA_in_Interior_Architecture/1.jpg" />
                                <div class='text-right'>

                                </div> <!-- text-right / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                        NSBM Software Competition (NSC) 2014
                    </a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body">
                    <link rel="stylesheet" href="http://frontend.reklamor.com/fancybox/jquery.fancybox.css" media="screen">
                    <script src="http://frontend.reklamor.com/fancybox/jquery.fancybox.js"></script>


                    <div class='list-group gallery'>
                        <div class='col-lg-2 col-sm-3 col-xs-4'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="http://www.nsbm.lk/icludes/news/images/NSBM_Software_Competition/1.jpg">
                                <img class="img-responsive" alt="" src="http://www.nsbm.lk/icludes/news/images/NSBM_Software_Competition/1.jpg" />
                                <div class='text-right'>

                                </div> <!-- text-right / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-lg-2 col-sm-3 col-xs-4'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="http://www.nsbm.lk/icludes/news/images/NSBM_Software_Competition/2.jpg">
                                <img class="img-responsive" alt="" src="http://www.nsbm.lk/icludes/news/images/NSBM_Software_Competition/2.jpg" />
                                <div class='text-right'>

                                </div> <!-- text-right / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-lg-2 col-sm-3 col-xs-4'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="http://www.nsbm.lk/icludes/news/images/NSBM_Software_Competition/3.jpg">
                                <img class="img-responsive" alt="" src="http://www.nsbm.lk/icludes/news/images/NSBM_Software_Competition/3.jpg" />
                                <div class='text-right'>

                                </div> <!-- text-right / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-lg-2 col-sm-3 col-xs-4'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="http://www.nsbm.lk/icludes/news/images/NSBM_Software_Competition/6.jpg">
                                <img class="img-responsive" alt="" src="http://www.nsbm.lk/icludes/news/images/NSBM_Software_Competition/6.jpg" />
                                <div class='text-right'>

                                </div> <!-- text-right / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-lg-2 col-sm-3 col-xs-4'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="http://www.nsbm.lk/icludes/news/images/NSBM_Software_Competition/7.jpg">
                                <img class="img-responsive" alt="" src="http://www.nsbm.lk/icludes/news/images/NSBM_Software_Competition/7.jpg" />
                                <div class='text-right'>

                                </div> <!-- text-right / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-lg-2 col-sm-3 col-xs-4'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="http://www.nsbm.lk/icludes/news/images/NSBM_Software_Competition/8.jpg">
                                <img class="img-responsive" alt="" src="http://www.nsbm.lk/icludes/news/images/NSBM_Software_Competition/8.jpg" />
                                <div class='text-right'>

                                </div> <!-- text-right / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                    </div> <!-- list-group / end -->
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                        Engage Youth - Workshop by NSAC
                    </a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body">
                    <link rel="stylesheet" href="http://frontend.reklamor.com/fancybox/jquery.fancybox.css" media="screen">
                    <script src="http://frontend.reklamor.com/fancybox/jquery.fancybox.js"></script>


                    <div class='list-group gallery'>
                        <div class='col-lg-2 col-sm-3 col-xs-4'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="http://www.nsbm.lk/4togallery/Engage_Youth_Workshop/1.jpg">
                                <img class="img-responsive" alt="" src="http://www.nsbm.lk/4togallery/Engage_Youth_Workshop/1.jpg" />
                                <div class='text-right'>
                                </div> <!-- text-right / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-lg-2 col-sm-3 col-xs-4'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="http://www.nsbm.lk/4togallery/Engage_Youth_Workshop/2.jpg">
                                <img class="img-responsive" alt="" src="http://www.nsbm.lk/4togallery/Engage_Youth_Workshop/2.jpg" />
                                <div class='text-right'>

                                </div> <!-- text-right / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-lg-2 col-sm-3 col-xs-4'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="http://www.nsbm.lk/4togallery/Engage_Youth_Workshop/3.jpg">
                                <img class="img-responsive" alt="" src="http://www.nsbm.lk/4togallery/Engage_Youth_Workshop/3.jpg" />
                                <div class='text-right'>

                                </div> <!-- text-right / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-lg-2 col-sm-3 col-xs-4'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="http://www.nsbm.lk/4togallery/Engage_Youth_Workshop/4.jpg">
                                <img class="img-responsive" alt="" src="http://www.nsbm.lk/4togallery/Engage_Youth_Workshop/4.jpg" />
                                <div class='text-right'>

                                </div> <!-- text-right / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-lg-2 col-sm-3 col-xs-4'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="http://www.nsbm.lk/4togallery/Engage_Youth_Workshop/5.jpg">
                                <img class="img-responsive" alt="" src="http://www.nsbm.lk/4togallery/Engage_Youth_Workshop/5.jpg" />
                                <div class='text-right'>

                                </div> <!-- text-right / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-lg-2 col-sm-3 col-xs-4'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="http://www.nsbm.lk/4togallery/Engage_Youth_Workshop/6.jpg">
                                <img class="img-responsive" alt="" src="http://www.nsbm.lk/4togallery/Engage_Youth_Workshop/6.jpg" />
                                <div class='text-right'>

                                </div> <!-- text-right / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                    </div> <!-- list-group / end -->
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                        Progress Review of Construction
                    </a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse">
                <div class="panel-body">
                    <link rel="stylesheet" href="http://frontend.reklamor.com/fancybox/jquery.fancybox.css" media="screen">
                    <script src="http://frontend.reklamor.com/fancybox/jquery.fancybox.js"></script>


                    <div class='list-group gallery'>
                        <div class='col-lg-2 col-sm-3 col-xs-4'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="http://www.nsbm.lk/4togallery/Progress_Review/1.jpg">
                                <img class="img-responsive" alt="" src="http://www.nsbm.lk/4togallery/Progress_Review/1.jpg" />
                                <div class='text-right'>
                                </div> <!-- text-right / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-lg-2 col-sm-3 col-xs-4'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="http://www.nsbm.lk/4togallery/Progress_Review/3.jpg">
                                <img class="img-responsive" alt="" src="http://www.nsbm.lk/4togallery/Progress_Review/3.jpg" />
                                <div class='text-right'>

                                </div> <!-- text-right / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-lg-2 col-sm-3 col-xs-4'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="http://www.nsbm.lk/4togallery/Progress_Review/5.jpg">
                                <img class="img-responsive" alt="" src="http://www.nsbm.lk/4togallery/Progress_Review/5.jpg" />
                                <div class='text-right'>

                                </div> <!-- text-right / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-lg-2 col-sm-3 col-xs-4'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="http://www.nsbm.lk/4togallery/Progress_Review/4.jpg">
                                <img class="img-responsive" alt="" src="http://www.nsbm.lk/4togallery/Progress_Review/4.jpg" />
                                <div class='text-right'>

                                </div> <!-- text-right / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-lg-2 col-sm-3 col-xs-4'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="http://placehold.it/300x320.png">
                                <img class="img-responsive" alt="" src="http://placehold.it/320x320" />
                                <div class='text-right'>

                                </div> <!-- text-right / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-lg-2 col-sm-3 col-xs-4'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="http://placehold.it/300x320.png">
                                <img class="img-responsive" alt="" src="http://placehold.it/320x320" />
                                <div class='text-right'>

                                </div> <!-- text-right / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                    </div> <!-- list-group / end -->

                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                        Foundation Batch in class mini Drama Fest
                    </a>
                </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse">
                <div class="panel-body">
                    <link rel="stylesheet" href="http://frontend.reklamor.com/fancybox/jquery.fancybox.css" media="screen">
                    <script src="http://frontend.reklamor.com/fancybox/jquery.fancybox.js"></script>


                    <div class='list-group gallery'>
                        <div class='col-lg-2 col-sm-3 col-xs-4'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="http://placehold.it/300x320.png">
                                <img class="img-responsive" alt="" src="http://placehold.it/320x320" />
                                <div class='text-right'>
                                </div> <!-- text-right / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-lg-2 col-sm-3 col-xs-4'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="http://placehold.it/300x320.png">
                                <img class="img-responsive" alt="" src="http://placehold.it/320x320" />
                                <div class='text-right'>

                                </div> <!-- text-right / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-lg-2 col-sm-3 col-xs-4'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="http://placehold.it/300x320.png">
                                <img class="img-responsive" alt="" src="http://placehold.it/320x320" />
                                <div class='text-right'>

                                </div> <!-- text-right / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-lg-2 col-sm-3 col-xs-4'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="http://placehold.it/300x320.png">
                                <img class="img-responsive" alt="" src="http://placehold.it/320x320" />
                                <div class='text-right'>

                                </div> <!-- text-right / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-lg-2 col-sm-3 col-xs-4'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="http://placehold.it/300x320.png">
                                <img class="img-responsive" alt="" src="http://placehold.it/320x320" />
                                <div class='text-right'>

                                </div> <!-- text-right / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                        <div class='col-lg-2 col-sm-3 col-xs-4'>
                            <a class="thumbnail fancybox" rel="ligthbox" href="http://placehold.it/300x320.png">
                                <img class="img-responsive" alt="" src="http://placehold.it/320x320" />
                                <div class='text-right'>

                                </div> <!-- text-right / end -->
                            </a>
                        </div> <!-- col-6 / end -->
                    </div> <!-- list-group / end -->
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
                                Mozila day at NSBM
                            </a>
                        </h4>
                    </div>
                    <div id="collapseSix" class="panel-collapse collapse">
                        <div class="panel-body">
                            <link rel="stylesheet" href="http://frontend.reklamor.com/fancybox/jquery.fancybox.css" media="screen">
                            <script src="http://frontend.reklamor.com/fancybox/jquery.fancybox.js"></script>

                            <div class='list-group gallery'>
                                <div class='col-lg-2 col-sm-3 col-xs-4'>
                                    <a class="thumbnail fancybox" rel="ligthbox" href="http://placehold.it/300x320.png">
                                        <img class="img-responsive" alt="" src="http://placehold.it/320x320" />
                                        <div class='text-right'>
                                        </div> <!-- text-right / end -->
                                    </a>
                                </div> <!-- col-6 / end -->
                                <div class='col-lg-2 col-sm-3 col-xs-4'>
                                    <a class="thumbnail fancybox" rel="ligthbox" href="http://placehold.it/300x320.png">
                                        <img class="img-responsive" alt="" src="http://placehold.it/320x320" />
                                        <div class='text-right'>

                                        </div> <!-- text-right / end -->
                                    </a>
                                </div> <!-- col-6 / end -->
                                <div class='col-lg-2 col-sm-3 col-xs-4'>
                                    <a class="thumbnail fancybox" rel="ligthbox" href="http://placehold.it/300x320.png">
                                        <img class="img-responsive" alt="" src="http://placehold.it/320x320" />
                                        <div class='text-right'>

                                        </div> <!-- text-right / end -->
                                    </a>
                                </div> <!-- col-6 / end -->
                                <div class='col-lg-2 col-sm-3 col-xs-4'>
                                    <a class="thumbnail fancybox" rel="ligthbox" href="http://placehold.it/300x320.png">
                                        <img class="img-responsive" alt="" src="http://placehold.it/320x320" />
                                        <div class='text-right'>

                                        </div> <!-- text-right / end -->
                                    </a>
                                </div> <!-- col-6 / end -->
                                <div class='col-lg-2 col-sm-3 col-xs-4'>
                                    <a class="thumbnail fancybox" rel="ligthbox" href="http://placehold.it/300x320.png">
                                        <img class="img-responsive" alt="" src="http://placehold.it/320x320" />
                                        <div class='text-right'>

                                        </div> <!-- text-right / end -->
                                    </a>
                                </div> <!-- col-6 / end -->
                                <div class='col-lg-2 col-sm-3 col-xs-4'>
                                    <a class="thumbnail fancybox" rel="ligthbox" href="http://placehold.it/300x320.png">
                                        <img class="img-responsive" alt="" src="http://placehold.it/320x320" />
                                        <div class='text-right'>
                                        </div> <!-- text-right / end -->
                                    </a>
                                </div> <!-- col-6 / end -->
                            </div>
                        </div>
                    </div>
                </div>


