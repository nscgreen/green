<body>
       <?php $this->load->view('vmenu'); ?>  <!--include menu-->
        <div class="container" style=" width: 100%;">
            <div class="row">
                <div id="welcome" class="bs-docs-header" id="content">
                    <div style="padding: 30px;" class="container">
                       <div class="col-md-7 col-sm-7">
                            <h1><font style="color: rgba(32, 76, 147, 1);" class="animated zoomIn">National School of Business Management</font></h1>
                            <h3><small><font style="color: black;" class="animated pulse">Welcome to The NSBM One of the South Asia's Top Leading Universities...</font></small><h3>
                        </div>
                        <div  class="col-md-5 col-sm-5 push-right">
                            <div class="row visible-lg visible-xs" style="margin-top: 0px; padding: 0px">
                                <center>
                                    <a href="<?php echo base_url();?>"><img style="margin: 0px 0px 30px 0px" class="nsbmlogo animated fadeInDown" src="../../logos/nsbmwhite.gif"/></a>
                                </center>
                            </div>
                            <div class="row visible-sm visible-md">
                                <center>
                                    <a href="<?php echo base_url();?>"><img style="margin-top: 30px" class="nsbmlogo animated fadeInDown" src="../../logos/nsbmwhite.gif"/></a>
                                </center>
                            </div>
                            <div class="row" style="margin-bottom: -20px; margin-top: 5px">
                                <div class="col-xs-4">
                                    <a href="<?php echo base_url();?>computing"><img class="animated flipInX school-logo image-responsive center-block" src="../../logos/schools/batch/computing-logo.png"></a>
                                </div>
                                <div class="col-xs-4">
                                    <a href="<?php echo base_url();?>management"><img class="animated flipInX school-logo image-responsive center-block" src="../../logos/schools/batch/management-logo.png"></a>
                                </div>
                                <div class="col-xs-4">
                                    <a href="<?php echo base_url();?>engineering"><img class="animated flipInX  school-logo image-responsive center-block" src="../../logos/schools/batch/engineering-logo.png"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


<section id="intro" class="intro-section">
   <div class="container">
       <div class="row">
           <div class="col-md-7">
               <div class="row" style="padding-bottom: 5px;">
                   <div class="col-md-4 col-sm-4 col-xs-3"><center><img style="max-height:120px;" class="img-responsive" src="<?php echo base_url();?>logos/gov.png"/></center></div>
                   <div class="col-md-4 col-sm-4 col-xs-3"><center><img style="max-height:120px; margin-top: 20px" class="img-responsive" src="<?php echo base_url();?>logos/ugc.png"/></center></div>
                   <div class="col-md-4 col-sm-4 col-xs-3"><center><img style="max-height:120px; margin-top: 20px" class="img-responsive" src="<?php echo base_url();?>logos/nibm.png"/></center></div>
               </div>
               <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                   <!-- Indicators -->
                   <ol class="carousel-indicators">
                       <li data-target="#carousel-example-generic" style="background-color: #ffffff;" data-slide-to="0" class="active"></li>
                       <li data-target="#carousel-example-generic" style="background-color: #ffffff;" data-slide-to="1"></li>
                       <li data-target="#carousel-example-generic" style="background-color: #ffffff;" data-slide-to="2"></li>
                   </ol>
                   <!-- Wrapper for slides -->
                   <div class="carousel-inner">
                       <div class="item active">
                           <img src="<?php echo base_url();?>uploads_main/slider_images/1.jpg" alt="...">
                           <div class="carousel-caption">
                               <!--<h2 style="color: #660066">Progress of Green university</h2>-->
                           </div>
                       </div>
                       <div class="item">
                           <img src="<?php echo base_url();?>uploads_main/slider_images/2.jpg" alt="...">
                           <div class="carousel-caption">
                               <!--<h2 style="color: #660066">Apply Now...</h2>-->
                           </div>
                       </div>
                       <div class="item">
                           <img src="<?php echo base_url();?>uploads_main/slider_images/3.jpg" alt="...">
                           <div class="carousel-caption">
                               <!--<h2 style="color: #660066">Progress of Green university</h2>-->
                           </div>
                       </div>
                       <div class="item">
                           <img src="<?php echo base_url();?>uploads_main/slider_images/4.jpg" alt="...">
                           <div class="carousel-caption">
                               <!--<h2 style="color: #660066">Progress of Green university</h2>-->
                           </div>
                       </div>
                       <div class="item">
                           <img src="<?php echo base_url();?>uploads_main/slider_images/5.jpg" alt="...">
                           <div class="carousel-caption">
                               <!--<h2 style="color: #660066">Progress of Green university</h2>-->
                           </div>
                       </div>
                   </div>
                   <!-- Controls -->
                   <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                       <span class="glyphicon glyphicon-chevron-left"></span>
                   </a>
                   <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                       <span class="glyphicon glyphicon-chevron-right"></span>
                   </a>
               </div>
           </div>
           <div class="col-md-5" style="border-left-width:4px; border-left-style:solid; border-left-color:#999;">
               <h2 style="color: #0099cc; padding-left: 15px;" class="underline"><a>Welcome <small>To the NSBM.</small></a></h2>
               <p style="padding-left: 15px; text-align:justify; font-size: 17px" class="text-info">National School of Business Management situated in Nugegoda is a pinnacle of higher education. NSBM works with severel of the famouse forigen 
                   univerties in the world. University of Plymouth in England, University of Dubline and Limkokwing University of Creative Technology in Malaysia are the 
                   universities affliated with NSBM. NSBM caters the undergraduates and the postgraduates study courses in the fields of Management, Computing and in 
                   near future the field of Engineering will be introduced. NSBM also provides library facilities, sporting facilities, career guidance, clubs and 
                   communities. </p>
           </div>
       </div>
   </div>
</section>

       
<section id="schools" class="about-section">
   <div class="container">
       <div class="page-header" style="border: 0">
           <h1 class="">Degree Schools <small>Choose your way...</small></h1>
           <hr class="hr-style"/>
       </div>
       <div class="container">
           <div class="row destacados">
               <div class="col-md-4">
                   <div>
                       <img src="http://www.nsbm.lk/nsbm_img/icon/cs_schoollogo.png" alt="" class="img-rounded img-thumbnail">
                       <h2 class="text-primary">School of Computing</h2>
                       <p class="font-schools">NSBM has well facilitated building for computing section. It provides well equiped computer laboratories, network
                           lab. Well knowledged lecturers are always prepared to inpart their knowledge to students. Computing building 
                           has well conditioned lecture halls with free wifi acess to students. Many renowned IT professionals and Software
                           Engineers come from NSBM.</p>
                       <a href="<?php echo base_url(); ?>computing" class="btn btn-info btn-lg" title="Click here to see the full details of Computing cources">More »</a>
                   </div>
               </div>
               <div class="col-md-4">
                   <div>
                       <img src="http://www.nsbm.lk/nsbm_img/icon/mgt_schoollogo.png" alt="" class="img-rounded img-thumbnail">
                       <h2 class="text-success">School of Management</h2>
                       <p class="font-schools">School of Business provides the Management Students a well equiped building for their higher education. And also 
                           they are facilitated with qualified lecturers to carry out the studies. Students are motivated to develop their 
                           educational and social skill with guidance of the lecturer. Further students are trained for analytical skills, 
                           human skills, leadership qualities, orientation and communication skills.</p>
                       <a href="<?php echo base_url(); ?>management" class="btn btn-success btn-lg" title="Click here to see the full details of Management cources">More »</a>
                   </div>
               </div>
               <div class="col-md-4">
                   <div>
                       <img src="http://www.nsbm.lk/nsbm_img/icon/eng_schoollogo.png" alt="" class="img-rounded img-thumbnail">
                       <h2 class="text-info">School of Engineering</h2>
                       <p class="font-schools">Much anticipated School of Engineering of NSBM will be opened in the premises of Green University in Homagama near future. School of Engineering 
                           will have advance buildings with qualified lecturers. Green University will open at the end of next year.</p>
                       <a href="<?php echo base_url(); ?>engineering" class="btn btn-primary btn-lg" title="Click here to see the full details of Engineering cources">More »</a>
                   </div>
               </div>
           </div>
       </div>
       <div style="padding: 20px;" class="row">
           <h1 class="underline"><a href="<?php echo base_url(); ?>computing"><small class="text-muted">Schools</small></a></h1>
           <a href="<?php echo base_url(); ?>computing"><img style="max-width: 50px;" claas="img-responsive" src="<?php echo base_url() . 'assets/img/main/right_round.png'; ?>"/></a>
       </div>
   </div>
</section>

       
<section id="mynsbm" class="services-section">
   <div class="container">
       <div class="page-header" style="border: 0; margin-bottom: -20px">
           <h1>My NSBM <small>More ways in one place...</small></h1>
           <hr class="hr-style"/>
       </div>
       <div class="legend">
           <p class="text-info" style="font-size: 17px">My NSBM is a place where all of you will be able to access NSBM 
               LMS, NSBM EMS, My Nsbm and NSBM Forum. From My NSBM you can create the University Profile
               where all of your information is saved. This feature will help outside personals to gain
               your educational qualifications and information reguarding projects you have done. Join
               to University Profile to day and enjoy perquisites..</p>
       </div>
       <div class="row">
           <div class="col-md-3 col-md-offset-1 text-right">
               <div class="feature-item">
                   <h3 class="underline"><a href="" class="text-muted"><i class="fa fa-angle-double-left"></i> L.M.S</a></h3>
                   <div>
                       <p>NSBM learning management system . This provides self learning facilities to all the students in NSBM. All of the assignments, projects and important news events will be displayed here. Academic staff members will facilitate you through this system 24/7.</p>
                   </div>
               </div>
               <div class="feature-item">							
                   <h3 class="underline"><a href="" class="text-muted"><i class="fa fa-angle-double-left"></i> Communities</a></h3>								
                   <div>
                       <p>
                           NSBM Community helps the vast amount of communities that have been introduced by NSBM students to gather in a one place. A place where all the communities are held, so the students can easily find information about on going communities and participate accordingly to their likes. A place all the messages and notices can be added and viewed. Network with NSBM communities now..</p>
                   </div>
               </div>
           </div>
           <div class="col-md-4 text-right feature-picture">
               <center>
                   <img class="img-responsive" style="margin-top: 46px;" src="<?php echo base_url();?>assets/img/main/student.png">
               </center> 
           </div>
           <div class="col-md-3 text-left">
               <div class="feature-item">
                   <h3 class="underline"><a href="" class="text-muted">Forum <i class="fa fa-angle-double-right"></i></a></h3>	
                   <div>
                       <p>NSBM Forum is a new feature that we are going to intorduce the main website. This is to increase the communication between NSBM students and the communication between students and lecturers. From this forum both students and lecturers will reap many uses, like exchange of ideas, couse materials, discussions or even use as a bulletin board. Our developing team will hope to launch the NSBM forum soon enough.</p>
                   </div>
               </div>
               <div class="feature-item">
                   <h3 class="underline"><a href="" class="text-muted">E.M.S <i class="fa fa-angle-double-right"></i></a></h3>
                   <div><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500.</p></div>
               </div>
           </div>
       </div>
   </div>
</section>

       
<section id="news" class="contact-section">
   <div class="container">
       <div class="row">
           <div class="page-header" style="border: 0">
               <h1>News of NSBM <small>Do you know what happen??...</small></h1>
               <hr class="hr-style"/>
           </div>
           <div class="row">
               <div  class="col-xs-6 col-sm-3 col-md-3">
                    <div style="max-height: 440px; padding: 0" class="thumbnail">
                       <img src="http://www.nsbm.lk/icludes/news/images/The_Orator/1.jpg" alt="">
                       <div class="caption">
                           <h4 class="text-primary">‘The Orator’ from NSBM.</h4>
                           <p class="text-justify">NSBM students swept the board at “The Orator”: the speech competition organized by NIBM. Amidst contestants that represented NSBM and island wide centres of NIBM 5 students from NSBM were among the 12 finalists at the final round held on 25th June at NIBM Auditorium....</p>
                           <p class="text-center"><a href="<?php echo base_url();?>news" class="btn btn-info btn-xs" role="button">Read More</a></p>
                       </div>
                   </div>
               </div>

               <div  class="col-xs-6 col-sm-3 col-md-3">
                   <div style="height: 440px; padding: 0" class="thumbnail">
                       <img src="http://www.nsbm.lk/4togallery/Convocation_2014_BA_in_Interior_Architecture/1.jpg" alt="">
                       <div class="caption">
                           <h4 class="text-primary">CONVOCATION 2014 Interior Architecture – Batch 02</h4>
                           <p>Convocation of the 2nd batch of Interior architecture was held on 24th June 2014 at BMICH with the participation of Hon. Dullas Alahapperuma, Minister of Youth Affairs & Skills Development, Vice President Limkokwing University....</p>
                           <br /><br /><br />
                           <p><a href="<?php echo base_url();?>news" class="btn btn-info btn-xs" role="button">Read More</a></p>
                       </div>
                   </div>
               </div>

               <div class="col-xs-6 col-sm-3 col-md-3">
                   <div style="max-height: 440px; padding: 0" class="thumbnail">
                       <img src="http://www.nsbm.lk/4togallery/Progress_Review/1.jpg" alt="">
                       <div class="caption">
                           <h4 class="text-primary">Progress Review and Media Brief on Green University Town</h4>
                           <p>A media briefing and a progress review of the construction of NSBM Green University town, a venture inspired by the vision of Mahinda Chintanaya to develop Sri Lanka to be the ‘Knowledge Hub’...</p>
                           <p><a href="<?php echo base_url();?>news" class="btn btn-info btn-xs" role="button">Read More</a></p>
                       </div>
                   </div>
               </div>

               <div class="col-xs-6 col-sm-3 col-md-3">
                   <div style="max-height: 440px; padding: 0" class="thumbnail">
                       <img src="http://www.nsbm.lk/icludes/news/images/Introduction_to_Android/1.jpg" alt="">
                       <div class="caption">
                           <h4 class="text-primary">Introduction to Android Development (NSBM DevOps).</h4>
                           <p>Mobile development can be considered as a trending area within developers around the globe. Learning about mobile development in relation to computing by any undergraduate can be considered a timely necessity.....</p>
                           <p><a href="<?php echo base_url();?>news" class="btn btn-info btn-xs" role="button">Read More</a></p>
                       </div>
                   </div>
               </div>
           </div>
       </div><!-- End row -->
       <div class="row">
           <h1 class="underline"><a href="<?php echo base_url(); ?>news"><small>More News</small></a></h1>
           <a href="<?php echo base_url(); ?>news"><img style="max-width: 50px;" claas="img-responsive" src="<?php echo base_url() . 'assets/img/main/right_round.png'; ?>"/></a>
       </div>
   </div>
</section>

       
<section id="quick" class="quick-section">
    <div class="container">
        <div class="page-header" style="border: 0">
            <h1>Quick links <small>Everything is here...</small></h1>
            <hr class="hr-style"/>
        </div>
        <div class="tabbable tabs-right">
            <ul class="nav nav-tabs btn-group">
                <li class="active"><a href="#1" data-toggle="tab">Prospective Students</a></li>
                <li><a href="#2" data-toggle="tab">Current Students</a></li>
                <li><a href="#3" data-toggle="tab">Research</a></li>
                <li><a href="#4" data-toggle="tab">e-Learning</a></li>

                <li><a href="#6" data-toggle="tab">Alumni & Friends</a></li>
                <li><a href="#7" data-toggle="tab">Sport & Health</a></li>
                <li><a href="#8" data-toggle="tab">Library</a></li>
                <li><a href="#9" data-toggle="tab">Well Wishes</a></li>
            </ul>

            <div class="tab-content"style="background-color:white;">
                <div class="tab-pane active" id="1">
                    <br/>
                    <h1 style="padding: 0; margin: 0"><font style="color: rgb(16, 132, 148)">Prospective Students</font></h1>
                    <br><h3>Join with Us to Experience the Future of Higher Education</h3>
                    <br><p class="font-schools" style="padding: 10px">
                        In the midst of an enduring dedication to the pursuit of excellence, NSBM offers incomparable student experiences across a broad spectrum of academic environments. Whether you're a prospective graduate or undergraduate student, school leaver or even if you are just after your O/Ls NSBM is rich with opportunities for your future.
                        Through the schools of Business, Computing and Engineering, NSBM offers focused disciplines in the fine arts, business, technology and the sciences, with the opportunity to mould your personality and to sharpen your knowledge. All the faculties are thriving harder to achieve NSBM’s prime objective of nurturing an employable graduate.
                        NSBM offers students with the option of following non-traditional degree programmes which are more industry specific. Curricula of the degree programmes are designed to reflect the changing socio economic requirements with due considerations on the demands of the industry. All programmes, while specializing in a specific field of study, will have an appropriate inter-disciplinary focus which will eventually help a prospective degree holder to easily assimilate in any business or industry environment.
                    </p><br />
                </div>
                <div class="tab-pane" id="2">
                    <br />
                    <h1 style="padding: 0; margin: 0"><font style="color: rgb(16, 132, 148)">Current Students</font></h1>
                    <br><h3>Don't forget your student card</h3>
                    <br>
                    <td style="border:groove" width="100" height="100">
                        <img src="http://www.nsbm.lk/nsbm_img/images/current1.jpg" alt="current1" width="160" height="100"></td>
                    <p class="font-schools" style="padding: 10px">
                        Make sure you familiarise yourself with the exam hall procedures (student card, mobile phones and unauthorised materials)
                    </p><br />
                </div>
                <div class="tab-pane" id="3">
                    <br/>
                    <h1 style="padding: 0; margin: 0"><font style="color: rgb(16, 132, 148)">Research</font></h1>
                    <br><p class="font-schools" style="padding: 10px">
                        Research methodology is a compulsory module for all postgraduate and undergraduate programs at NSBM. All students should complete a comprehensive research project under the guidance of assigned lecturers. Generally undergraduate students work in groups to fulfill this compulsory requirement while postgraduate students take up individual projects. NSBM, managed by a committee of senior academics/ researchers, provides facilities to carry out each student's research to bring out the researcher’s best insight.
                    </p>
                    <br><h3>RESOURCES</h3>
                    <br><p class="font-schools" style="padding: 10px">
                        NSBM is comprised of WiFi enabled computers, online references and free access to online journal articles, research papers and other various academic papers required to conduct a research. To guide each individual in their research platform, NSBM comprises a well qualified and experienced academic staff. We make every possible effort to develop a vibrant research culture where numerous initiatives are in place for research among the academic and student communities.
                    </p>
                    <br><h3>GRANTS</h3>
                    <br><p class="font-schools" style="padding: 10px">
                        NSBM provides a substantial research grant scheme to encourage high calibre research among its academics. All academics are clustered into a research team headed by a senior researcher. The senior researchers, as the principle investigators, are eligible for the NSBM annual research grants per project. These grants can be freely utilized for research related purposes such as accessing useful research resources and participation in conferences.
                    </p><br />
                </div>
                <div class="tab-pane" id="4">
                    <br/>
                    <h1 style="padding: 0; margin: 0"><font style="color: rgb(16, 132, 148)">e-Learning</font></h1>
                    <br><p class="font-schools" style="padding: 10px">
                        Recognizing the trends in technology and globalization, NSBM has designed an alternative and innovative approach to higher education to prepare young people to face new challenges of the world. As a degree school which enables its students to experience the future of higher education NSBM is encouraging their students to deal with sophisticated technology throughout their academic life.
                    </p>
                    <br><p class="font-schools" style="padding: 10px">
                        “e-Learning Management System” is the novel initiative taken up by NSBM to give hi-tech exposure to the students in order to pursue their studies in virtual terms. Through this learning process learners can communicate with their lecturers, their peers, and access learning materials, over the Internet or other computer networks. This area provides you with a wealth of information and resources designed to enrich student’s experience at NSBM. From information on registration, fees and exams to student support services and who to contact for academic advice, you’ll find it all here in one place.
                    </p><br />
                </div>

                <div class="tab-pane" id="6">
                    <br />
                    <h1 style="padding: 0; margin: 0"><font style="color: rgb(16, 132, 148)">Alumni & Friends</font></h1>
                    <br><p class="font-schools" style="padding: 10px">
                        The NSBM Alumni aim to help you stay in touch with NSBM, your college and other colleagues, wherever you are in the world.
                        The role of the Alumni is to establish and enhance a continuing relationship between NSBM and its alumni. There are already more than thousand students and this number is ever growing as we are expanding extensively with more national as well as international students both at undergraduate and graduate level. Alumni aspire to provide all the students, regardless of their school or subject affiliations, with an alumnus experience commensurate with their world-class education and offer a varied programme of benefits, events and communications to help keep you in touch and involved more with NSBM.
                    </p>
                    <br><h3>THE OBJECTIVES OF THE ASSOCIATION</h3>
                    <p class="font-schools" style="padding: 10px">
                        * Engage in the transformation of lives
                        <br>* To encourage, foster and promote close relations between the NSBM and its Alumni
                        <br>* To promote the interest of the alumni body in the affairs of the NSBM
                        <br>* To assist the alumni in promoting the general interest and well-being of NSBM
                        <br>* To provide and disseminate information regarding NSBM, its facilities, staff, graduates and students
                        <br>* To promote the study and development of every member and to bring together members of the same field
                        <br>* To safeguard the profession and foster more interest amongst persons practicing this profession
                        <br>* Offer assistance with your class reunion
                    </P>
                    <p class="font-schools" style="padding: 10px">
                        Through us you can reconnect with old friends, network at events, read about the exciting happenings at NSBM and of our alumni and much more.
                    </P><br />
                </div>
                <div class="tab-pane" id="7">
                    <br />
                    <h1 style="padding: 0; margin: 0"><font style="color: rgb(16, 132, 148)">Spots and Health</font></h1>
                    <br><h3>Sport</h3>
                    <br><p class="font-schools" style="padding: 10px">
                        The environment of NSBM provides a vibrant social life to all young undergraduates. Annual sports meets and inter batch sports activities are organized and raised by NSBM and the student council in each part of the year. Not only does NSBM provide required facilities to these sports events but it also supplies qualified coaches to keep the spirit up of the young students to balance their studies with extra curricular activities.
                    </p>
                    <h3>Health</h3>
                    <p class="font-schools" style="padding: 10px">
                        NSBM consider health as a vital factor to be a productive entity. A full-time medical consultancy service is available free of charge at NSBM for the benefit of students and staff.
                    </p><br />
                </div>
                <div class="tab-pane" id="8">
                    <br />
                    <h1 style="padding: 0; margin: 0"><font style="color: rgb(16, 132, 148)">Library</font></h1>
                    <br><p class="font-schools" style="padding: 10px">
                        The NSBM Library is a source of information. It plays a vital role in catering to the information and educational needs of both course participants and professional staff of NSBM. The fully-fledged library is well stocked with over 23000 books and selected periodicals. Among the special collections of the library are the Training Manuals, Company Annual Reports, Periodicals Holdings, Socio-Economic statistics, NSBM Consultancy and Student Project Reports and the Sri Lanka Collection.
                        Services provided by the NSBM library include inquiry, reference & lending services, library information services, the library database, inter-library loan facility, photocopying facility, journal, contents page service and bibliographical assistance to the undergraduates, academic staff and non academic staff at NSBM.
                        NSBM Library is open seven days a week from 0800 hrs to 1600 hrs, except on pubic holidays.
                    </p>
                    <br />
                </div>
                <div class="tab-pane" id="9">
                    <br />
                    <h1 style="padding: 0; margin: 0"><font style="color: rgb(16, 132, 148)">Well Wishes</font></h1>
                    <br><h3>Message from the CEO</h3>
                    <br><p class="font-schools" style="padding: 10px">
                        Welcome to the National School of Business Management. At NSBM we aim to introduce novelty and innovation to the field of higher education. NSBM, the degree school of NIBM offers academic programmes aimed at producing skillful graduates who can uplift the performance of businesses and the economy. We always attempt to play a distinctive role in furthering the nation’s economic development. Degree Programmes offered by NSBM are designed to cater to the requirements of the business community and the economy. We have invested in state-of-the-art learning facilities to provide our undergraduates with a new experience in learning and sharing knowledge. Our strategic plan of establishing an array of academic programmes and facilities in the next five years will further strengthen our journey towards serving the field of higher education. Our Academic Departments consist of members with strong academic backgrounds, appropriately blended with industry experience. Extensive quality control procedures implemented will ensure the delivery of programmes at international standards. Our long-term affiliations with world renowned universities such as University College Dublin- Ireland and Limkokwing University will indubitably strengthen our capacity to offer world-class qualifications at NSBM. We are inspired to redefine the field of higher education to deliver the promise of providing a conducive academic environment and a world of opportunities to shape the future of our next generation. I warmly invite you to join us to experience the future of higher education.
                    </p>
                    <br />
                </div>
            </div>
        </div>
        <div style="padding: 20px;" class="row">
            <h1 class="underline"><a href="<?php echo base_url(); ?>about/overview"><small>About US</small></a></h1>
            <a href="<?php echo base_url(); ?>about/overview"><img style="max-width: 50px;" claas="img-responsive" src="<?php echo base_url() . 'assets/img/main/right_round.png'; ?>"/></a>
        </div>
    </div>
</section>

       
<section id="gallery" class="gallery-section">
   <div class="container">
       <div class="row">
           <div class="page-header" style="border: 0">
               <h1>Gallery <small>Talents on NSBM...</small></h1>
               <hr class="hr-style"/>
           </div>
           
           <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3 underline'>
               <a style="height: 307px; padding: 0" class="thumbnail" href="<?php echo base_url();?>gallery">
                   <img class="img-responsive" alt="" src="http://www.nsbm.lk/4togallery/Convocation_2014_BA_in_Interior_Architecture/2.jpg" />
                   <br />
                   <h4 class='text-muted font-play'>CONVOCATION 2014 Interior Architecture – Batch 02</h4>
               </a>
           </div> 
           
           
           <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3 underline'>
               <a style="height: 307px; padding: 0" class="thumbnail" href="<?php echo base_url();?>gallery">
                   <img class="img-responsive" alt="" src="http://www.nsbm.lk/icludes/news/images/NSBM_Software_Competition/2.jpg" />
                   <div class=''>
                       <h4 class='text-muted'>NSBM Software Competition (NSC) 2014</h4>
                   </div> <!-- text-right / end -->
               </a>
           </div> 
           <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3 underline'>
               <a style="height: 307px; padding: 0" class="thumbnail" href="<?php echo base_url();?>gallery">
                   <img class="img-responsive" alt="" src="http://www.nsbm.lk/4togallery/Engage_Youth_Workshop/7.jpg" />
                   <div class=''>
                       <h4 class='text-muted'>Engage Youth - Workshop by NSAC</h4>
                   </div> <!-- text-right / end -->
               </a>
           </div> 
           <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3 underline'>
               <a style="height: 307px; padding: 0" class="thumbnail" href="<?php echo base_url();?>gallery">
                   <img class="img-responsive" alt="" src="http://www.nsbm.lk/4togallery/Progress_Review/3.jpg" />
                   <div class=''>
                       <h4 class='text-muted'>Progress Review of Construction</h4>
                   </div> <!-- text-right / end -->
               </a>
           </div> <!-- col-6 / end -->
       </div>
       <!-- list-group / end -->
       <div style="padding: 20px;" class="row">
           <h1 class="underline"><a href="<?php echo base_url(); ?>gallery"><small>More Albums</small></a></h1>
           <a href="<?php echo base_url(); ?>gallery"><img style="max-width: 50px;" claas="img-responsive" src="<?php echo base_url() . 'assets/img/main/right_round.png'; ?>"/></a>
       </div>
   </div>
</section>
       
<!-- These scripts are most wanted for IE error fixing Because don't delete -->
<script>
    (function() {
        if (!window.console) {
            window.console = {};
        }
        // union of Chrome, FF, IE, and Safari console methods
        var m = [
            "log", "info", "warn", "error", "debug", "trace", "dir", "group",
            "groupCollapsed", "groupEnd", "time", "timeEnd", "profile", "profileEnd",
            "dirxml", "assert", "count", "markTimeline", "timeStamp", "clear"
        ];
        // define undefined methods as noops to prevent errors
        for (var i = 0; i < m.length; i++) {
            if (!window.console[m[i]]) {
                window.console[m[i]] = function() {
                };
            }
        }
    })();
</script>

<script src="<?php echo base_url(); ?>assets/js/main/jquery-1.10.2.js"></script>

