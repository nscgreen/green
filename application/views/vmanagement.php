<body class="pages-body">
    <?php $this->load->view('main_all/vmain_all_top_menu'); ?>  <!--include menu and header-->
    <div class="container pages-container">
        <div id="content" class="row-fluid">
            <div class="well col-md-3 col-sm-4 sidebar-mng">
                <?php $this->load->view('main_sc_mgt/vmgt_side_menu.php');?>
            </div>

            <div class="col-md-9 col-sm-8 right-content">
                <!--School of Computing main page content-->
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url();?>"><span class="glyphicon glyphicon-home"></span></a></li>
                            <?php echo create_breadcrumb(); ?>
                        </ol>
                        <center>
                            <img style="width: 256px" src="http://www.nsbm.lk/nsbm_img/icon/mgt_schoollogo.png"/>
                        </center>
                        <br/>
                        <h1 class="text-center">Welcome to NSBM School of Management</h1><br/>
                        <p class="text-justify text-info font-schools">
                            School of Business provides the Management Students a well equiped building for their higher education. And also 
                            they are facilitated with qualified lecturers to carry out the studies. Students are motivated to develop their 
                            educational and social skill with guidance of the lecturer. Further students are trained for analytical skills, 
                            human skills, leadership qualities, orientation and communication skills.
                        </p>
                        <h1><small>Productive way of learning Management</small></h1>
                        <p class="text-justify text-info font-schools">
                            Here is a task to cooperate by both faculty members as well as the students in the school. The teaching, learning process at the school conceivably is a novel one analogues to the latest technological know-how and adoptions.
                        </p>
                        <h1><small>Career Development</small></h1>
                        <p class="text-justify text-info font-schools">
                            Career Development Programmes are an inclusive part of the institutional development. The students are motivated for upgrading their skills and personality on a customary basis. Particularly, the students are treated and trained for analytical skills, human skills, leadership qualities and orientations, communication skills.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>