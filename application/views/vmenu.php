<?php
/*
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jul 15, 2014, 11:03:19 AM
 */
?>
<div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12" >
            <nav id="menu-background" class="navbar navbar-default navbar-fixed-top" role="navigation" style="padding: 0 10px; width: 100%">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="visible-xs navbar-brand" href="#page-top">NSBM</a>
                </div>
                <div  class=" collapse navbar-collapse navbar-ex1-collapse" style="padding: 0 10px;">
                    <ul class="nav navbar-nav">               
                        <li class="hidden">
                            <a href="#page-top"></a>
                        </li>
                        <li class="page-scroll" style="padding: 0px">
                            <a href="<?php echo base_url() ?>" style="padding: 5px 0 5px 0"><img style="height: 40px; padding: 0; margin: 0;" src="<?php echo base_url() . 'assets/img/main/nsbm.gif'; ?>"/></a>
                        </li>
                        <li class="page-scroll">
                            <a href="#intro">Welcome</a>
                        </li>
                        <li class="dropdown page-scroll">
                            <a href="#schools" class="dropdown-toggle">Schools</a>
                            <ul class="dropdown-menu hidden-sm hidden-xs">          
                                <li><a href="<?php echo base_url(); ?>computing">School of Computing</a></li>          
                                <li><a href="<?php echo base_url(); ?>management">School of Management</a></li>
                                <li><a href="<?php echo base_url(); ?>engineering">School of Engineering</a></li>
                            </ul>
                        </li>
                        <li class="dropdown page-scroll">
                            <a href="#mynsbm" class="dropdown-toggle">My NSBM</a>
                            <ul class="dropdown-menu hidden-xs">          
                                <li><a href="http://lms.d2real.com" target="_blank">L.M.S</a></li>
                                <li><a href="http://events.d2real.com" target="_blank">Events</a></li>
                                <li><a href="http://forum.d2real.com" target="_blank">Forum</a></li>
                                <li><a href="http://communities.d2real.com" target="_blank">Communities</a></li>
                            </ul>
                        </li>
                        <li class="dropdown page-scroll">
                            <a href="#news" class="dropdown-toggle">News</a>
                            <ul class="dropdown-menu hidden-xs">          
                                <li><a href="<?php echo base_url(); ?>news">More <i class="fa fa-angle-double-right"></i></a></li>
                            </ul>
                        </li>
                        <li class="dropdown page-scroll">
                            <a href="#quick" class="dropdown-toggle">Quick links</a>
                            <ul class="dropdown-menu hidden-xs">          
                                <li><a href="<?php echo base_url(); ?>about/future_plans">Accommodations</a></li>
                                <li><a href="<?php echo base_url(); ?>more/meet_coordinator">Meet Coordinator</a></li>
                                <li><a href="<?php echo base_url(); ?>about/nsbm_nibm">NSBM & NIBM</a></li>
                                <li><a href="<?php echo base_url(); ?>about/future_plans">Future Plans</a></li>
                                <li><a href="<?php echo base_url(); ?>more/e-learning">e-Learning</a></li>
                                <li><a href="<?php echo base_url(); ?>more/library">Library</a></li>
                            </ul>
                        </li>
                        <li class="dropdown page-scroll">
                            <a href="#gallery" class="dropdown-toggle">Gallery</a>
                            <ul class="dropdown-menu hidden-xs">          
                                <li><a href="<?php echo base_url(); ?>gallery">More <i class="fa fa-angle-double-right"></i></a></li>
                            </ul>
                        </li>               
                        <li class="page-scroll">
                            <a href="#contacts">Contacts</a>
                        </li>
                        <li class="underline">
                            <a style="color: #018ab8" href="<?php echo base_url(); ?>registrations">Register</a>
                        </li>
                    </ul>

                    <ul id="top-menu2" class="hidden-sm hidden-xs nav navbar-nav navbar-right">
                        <li class="underline">
                            <a href="http://connect.d2real.com" target="_blank"><span class="glyphicon glyphicon-user"> Login</a></li>
                        <li class="underline"><a href="http://mynsbm.d2real.com" target="_blank">My NSBM</a></li>
                        <li class="underline"><a href="http://lms.d2real.com" target="_blank">L.M.S</a></li>
                        <li class="underline"><a href="http://events.d2real.com" target="_blank">Events</a></li>
                        <li class="underline"><a href="http://help.d2real.com" target="_blank">Help</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</div>