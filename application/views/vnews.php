<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jul 17, 2014, 12:54:26 PM
 */

?>
<body class="pages-body">
    <?php $this->load->view('main_all/vmain_all_top_menu'); ?>  <!--include menu and header-->
    <div class="container pages-container">
        <div id="content" class="row-fluid">
            <div class="well col-md-3 col-sm-4 sidebar-main">
                <?php $this->load->view('main_all/vmain_side_menu.php') ?>
            </div>

            <div class="col-md-9 col-sm-8 right-content">
                <!--School of Computing main page content-->
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url();?>"><span class="glyphicon glyphicon-home"></span></a></li>
                            <?php echo create_breadcrumb(); ?>
                        </ol>
                        <h1 class="text-info">News Headlines</h1>
                        <p class="text-justify text-info font-schools">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>
                        <div class="row">
                            <div class="col-sm-12">
                                <h3 class="text-success">Progress Review and Media Brief on Green University Town</h3>
                                <img class="thumbnail pull-left" style=" width: 100px; height: 90px; margin-right:10px;" src="http://www.nsbm.lk/4togallery/Progress_Review/1.jpg">
                                <p class="text-justify underline p-news">A media briefing and a progress review of the construction of NSBM Green University town, a venture inspired by the vision of Mahinda Chintanaya to develop Sri Lanka to be the ‘Knowledge Hub’....<a href="<?php echo base_url();?>news/read/1">Read More</a></p>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-sm-12">
                                <h3 class="text-success">Introduction to Android Development (NSBM DevOps).</h3>
                                <img class="thumbnail pull-left" style=" width: 100px; height: 90px; margin-right:10px;" src="http://www.nsbm.lk/icludes/news/images/Introduction_to_Android/1.jpg">
                                <p class="text-justify underline p-news">Mobile development can be considered as a trending area within developers around the globe. Learning about mobile development in relation to computing by any undergraduate can be considered a timely necessity..... <a href="target_home.php?eid=Progress_Review" target="_self">Read More</a></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <h3 class="text-success">NSBM Software Competition 2014</h3>
                                <img class="thumbnail pull-left" style=" width: 100px; height: 90px; margin-right:10px;" src="http://www.nsbm.lk/icludes/news/images/NSBM_Software_Competition/1.jpg">
                                <p class="text-justify underline p-news">Believing that “Competition is the driving force for innovation”, National School of Business Management launched the NSBM Software Competition - NSC 2014<a href="target_home.php?eid=Progress_Review" target="_self">Read More</a></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <h3 class="text-success">The Inception Convocation Ceremony of NSBM</h3>
                                <img class="thumbnail pull-left" style=" width: 100px; height: 90px; margin-right:10px;" src="http://www.nsbm.lk/icludes/news/images/UGC-Convocation-2014/1.jpg">
                                <p class="text-justify underline p-news">The The Inception Convocation Ceremony of the National School of Business Management was held on the 29th January 2014 at BMICH, Main Assembly Hall under the distinguished patronage of His Excellency Mahinda Rajapaksa,....<a href="target_home.php?eid=Progress_Review" target="_self">Read More</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
