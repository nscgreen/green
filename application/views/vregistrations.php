<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jul 17, 2014, 12:54:26 PM
 */

?>
<body class="pages-body">
    <?php $this->load->view('main_all/vmain_all_top_menu'); ?>  <!--include menu and header-->
    <div class="container pages-container">
        <div id="content" class="row-fluid">
            <div class="well col-md-3 col-sm-4 sidebar-main">
                <?php $this->load->view('main_all/vmain_side_menu.php') ?>
            </div>

            <div class="col-md-9 col-sm-8 right-content">
                <!--School of Computing main page content-->
                <div class="row">
                    <div class="col-sm-12">
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url();?>"><span class="glyphicon glyphicon-home"></span></a></li>
                            <?php echo create_breadcrumb(); ?>
                        </ol>
                        <h1 class="text-info">Online Registrations <small>Fill the registration form</small></h1>
                        <p class="text-justify text-info font-schools">
                            01. This registration form can be used to register courses conducted at National School of Business Management only.
                        </p>
                        <p class="text-justify text-info font-schools">
                            02. This registration form can only be used in order to keep you informed about the training programme you want to follow. Certain courses require prospective student to submit a application followed by a interview or aptitude test procedure before they may be selected.
                        </p>
                        <p class="text-justify text-info font-schools">
                            03. When a course receives the maximum number of registrations allowed, the system will close the entry.
                        </p>
                        <!--<h1><small>Offered by National School of Business Management</small></h1>-->
                        <div class="well well-sm">
                        <form class="form" role="form" action="" method="">
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="font-label">Select the School</label>
                                    <div class="input-group">
                                        <select class="form-control" required="true">
                                            <option>Undergraduate Courses</option>
                                            <option>Postgraduate Courses</option>
                                            <option>Foundation Courses</option>
                                        </select>
                                        <span class="input-group-addon primary"><span class="glyphicon glyphicon-star"></span></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="font-label">Select the Course Level</label>
                                    <div class="input-group">
                                        <select class="form-control" required="true">
                                            <option>Undergraduate Courses</option>
                                            <option>Postgraduate Courses</option>
                                            <option>Foundation Courses</option>
                                        </select>
                                        <span class="input-group-addon primary"><span class="glyphicon glyphicon-list-alt"></span></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="font-label">Select the Course</label>
                                    <div class="input-group">
                                        <select class="form-control" required="true">
                                            <option>Undergraduate Courses</option>
                                            <option>Postgraduate Courses</option>
                                            <option>Foundation Courses</option>
                                        </select>
                                        <span class="input-group-addon primary"><span class="glyphicon glyphicon-list-alt"></span></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="font-label">Name</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" required="true" placeholder="Enter your name (*required)"/>
                                        <span class="input-group-addon danger"><span class="glyphicon glyphicon-star"></span></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="font-label">Address</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" required="true" placeholder="Enter your address (*required)"/>
                                        <span class="input-group-addon danger"><span class="glyphicon glyphicon-star"></span></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="font-label">E-mail Address</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" required="true" placeholder="Enter your email address (*required)"/>
                                        <span class="input-group-addon danger"><span class="glyphicon glyphicon-envelope"></span></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="font-label">Telephone Number (Mobile)</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" required="true" placeholder="Enter your mobile number (*required)"/>
                                        <span class="input-group-addon danger"><span class="glyphicon glyphicon-earphone"></span></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="font-label">Telephone Number (Home/Office)</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Optional"/>
                                        <span class="input-group-addon default"><span class="glyphicon glyphicon-phone-alt"></span></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="font-label">Educational Qualification</label>
                                    <textarea class="form-control counted" name="message" required="true" placeholder="Enter your Highest Educational Qualification (*required)" rows="2" style="margin-bottom:10px;"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-info btn-lg center-block" style="width: 30%">Register</button>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
