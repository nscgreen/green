<!DOCTYPE html>
<html lang="en">
  <head>
    <title><?php echo $title?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo $mDescription?>"/>
    <meta name="keywords" content="<?php echo $mKeywords?>">
    <link rel="icon" href="../../images/favicon.ico" type="image/x-icon"/>
    
    <!--include all header files-->
    <?php include 'includes/header_cnt.php';?>
    
  </head>
  <body>
      <header class="row">
          <div class="col-md-6 col-md-offset-3 col-xs-6 col-xs-offset-3">
              <!--Create site footer here--> 
          </div>
      </header>