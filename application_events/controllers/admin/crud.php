<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class crud extends CI_Controller {

	 public function index() {
       
            $this->load->view('crud');
        }
        
        public function viewD(){
            $data= array();
            $this->load->view('crud', $data);
            
        }

    function main(){
        $this->load->library('table');
        
        $this->load->helper('html');    
        $this->load->model('cruddb');
        
        $data = $this->cruddb->general();
        
        $data['query'] = $this->cruddb->events_getall();
        
        $this->load->view('crud_main',$data);
    }

    function input($event_id = 0){

        $this->load->helper('form');  
        $this->load->helper('html');    
        $this->load->model('cruddb');

        if($this->input->post('mysubmit')){
        if($this->input->post('event_id')){
            $this->cruddb->entry_update();   
        }else{
            $this->cruddb->entry_insert();
        }
    }
        $data = $this->cruddb->general();

        if((int)$event_id > 0){
          $query = $this->cruddb->get($event_id);
          $data['fevent_id']['value'] = $query['event_id'];
          $data['ftitle']['value'] = $query['title'];
          $data['fdescription']['value'] = $query['description'];
          $data['fmax_attendance']['value'] = $query['max_attendance'];

          if($query['is_visible']=='1'){
            $data['fis_visible']['checked'] = TRUE;
              }else{
            $data['fis_visible']['checked'] = FALSE;   
              }

          $data['fcreated_at']['value'] = $query['created_at'];
          $data['fupdated_at']['value'] = $query['updated_at'];
          $data['fvenue']['value'] = $query['venue'];
          $data['fdate']['value'] = $query['date'];
          $data['fcontact_number']['value'] = $query['contact_number'];
          $data['fcontact_email']['value'] = $query['contact_email'];
          $data['faudience']['value'] = $query['audience'];
          $data['fimg_path']['value'] = $query['img_path'];

          }

            
          $this->load->view('crud_input',$data); 
          }

    function del($event_id){
        $this->load->library('table');
        $this->load->helper('html');    
        $this->load->model('cruddb');

        if((int)$event_id > 0){
             $this->cruddb->delete($event_id);
        }

        $data = $this->cruddb->general();
        $data['query'] = $this->cruddb->events_getall();
    
        $this->load->view('crud_main',$data);    
        }

    function edi($event_id){
        $this->load->library('table');
        $this->load->helper('html');    
        $this->load->model('cruddb');

        if((int)$event_id > 0){
             $this->cruddb->entry_update();
        }

        $data = $this->cruddb->general();
        $data['query'] = $this->cruddb->events_getall();
    
        $this->load->view('crud_input',$data); 

    }

}

