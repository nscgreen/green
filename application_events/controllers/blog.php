<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends CI_Controller {

	public function index() {
            $data = array(
                'title' => 'Events-Blog',
                'mDescription' => 'NSBM Event management system blog',
                'mKeywords' => 'Meta Keywords goes hera',
            );
            $this->load->helper('html');
            $this->load->helper('url');
            $this->load->view('vheader', $data);
                        
            // load Logged menu
            $this->load->model('muser_components');
            $this->muser_components->generate_usermenu();
            // End Logged menu
            
            $this->load->view('vmenu');
            $this->load->view('vblog');
            $this->load->view('vfooter');
        }
        
        public function page() {
            $data = array(
                'title' => 'Blog',
                'mDescription' => 'Meta desctiptions goes hera',
                'mKeywords' => 'Meta Keywords goes hera',
            );
            $this->load->view('vheader', $data);
                        
            // load Logged menu
            $this->load->model('muser_components');
            $this->muser_components->generate_usermenu();
            // End Logged menu
            
            $this->load->view('vmenu');
            $this->load->view('/users/vblog_post');
            $this->load->view('vfooter');
        }

}

/* End of file blog.php */
/* Location: ./application/controllers/blog.php */