<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Calendar extends CI_Controller {
            
    // Calander Function
    public function displaycal($year = NULL, $month = NULL, $firsttime = NULL) {
            
        $prefs = array(
            'start_day' => 'monday',
            'show_next_prev' => TRUE,
            'next_prev_url' => base_url('calendar/displaycal/')
        );

        $prefs['template'] = '

           {table_open}<table border="0" cellpadding="0" cellspacing="0" class="table-condensed table-bordered table-striped table ">{/table_open}

           {heading_row_start}<tr>{/heading_row_start}
                   {heading_previous_cell}<th style="text-align:center"><a href="{previous_url}" onclick="calajax(this.href); return false;"><span class="btn btn-lg"><img class="img-responsive" width="64px" height="64px" src="' . base_url() . 'assets_events/img/leftarrow.png"></span></a></th>{/heading_previous_cell}
                   {heading_title_cell}<th colspan="{colspan}" style="text-align:center; color: #55b4ff"><h2>{heading}</h2></th>{/heading_title_cell}
                   {heading_next_cell}<th style="text-align:center"><a href="{next_url}" onclick="calajax(this.href); return false;"><span class="btn btn-lg"><img class="img-responsive" width="64px" height="64px" src="' . base_url() . 'assets_events/img/rightarrow.png"></span></a></th>{/heading_next_cell}

           {heading_row_end}</tr>{/heading_row_end}

           {week_row_start}<tr>{/week_row_start}
           {week_day_cell}<td height="50" width="50" style="text-align:center"><span class="btn btn-lg">{week_day}</span></td>{/week_day_cell}
           {week_row_end}</tr>{/week_row_end}

           {cal_row_start}<tr>{/cal_row_start}
           {cal_cell_start}<td height="50" width="50" style="text-align:center;">{/cal_cell_start}

           {cal_cell_content}<div><a class="btn" href="{content}" >{day}</a></div>{/cal_cell_content}
           {cal_cell_content_today}<div class="highlight"><a class="btn" href="{content}">{day}</a></div>{/cal_cell_content_today}

           {cal_cell_no_content}<div><a class="btn" href="">{day}</a></div>{/cal_cell_no_content}
           {cal_cell_no_content_today}<div class="btn btn-info">{day}</div>{/cal_cell_no_content_today}

           {cal_cell_blank}&nbsp;{/cal_cell_blank}

           {cal_cell_end}</td>{/cal_cell_end}
           {cal_row_end}</tr>{/cal_row_end}

           {table_close}</table>{/table_close}
            ';

        $this->load->library('calendar', $prefs);

        if ($firsttime == TRUE) {
            $data2['cal'] = $this->calendar->generate($year, $month);
            return $data2;
        } else {
            echo $this->calendar->generate($year, $month);
        }
        
    }
    
}
