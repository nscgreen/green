<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Event extends CI_Controller {

	public function index() { // for events timeline
            $data = array(
                'title' => 'Put Page title here',
                'mDescription' => 'Meta desctiptions goes hera',
                'mKeywords' => 'Meta Keywords goes hera',
            );
            $this->load->view('vheader', $data);
            
            // load Logged menu
            $this->load->model('muser_components');
            $this->muser_components->generate_usermenu();
            // End Logged menu
            
            $this->load->view('vmenu');
            
            $this->load->model('mevent');
            $data['events']= $this->mevent->get_events_data_timeline();
            $this->load->view('event/vevents_timeline',$data);
            $this->load->view('vfooter');
        }
        
        public function show() { // for logged user's events
            $this->load->helper('html');
            $this->load->helper('url');
            $this->load->view('includes/tmp_header_cnt');
            
            $userid=$this->session->userdata('user_id');       
            $this->load->model('mevent');
            
            $data['created_events']= $this->mevent->get_events_data_show($userid);
            $this->load->view('event/vshow_events',$data);
            $this->load->view('includes/tmp_footer_cnt');
        }

        public function create() { // for event create
            $this->load->helper('html');
            $this->load->helper('url');
            $this->load->view('includes/tmp_header_cnt');
            
            $this->load->model('mevent_bar');
            $data['upcoming_events']= $this->mevent_bar->get_upcoming_events_data();
            
            $this->load->view('event/vcreate_event',$data);
            $this->load->view('includes/tmp_footer_cnt');
        }
        
        public function edit($event_id) { // for event edit
            
            $this->load->helper('html');
            $this->load->helper('url');
            $this->load->model('mevent');
            $data['records']= $this->mevent->get_events_data($event_id);
            
            $this->load->model('mevent_bar');
            $data['upcoming_events']= $this->mevent_bar->get_upcoming_events_data();
            
            $this->load->view('includes/tmp_header_cnt');
            $this->load->view('event/vedit_event',$data);
            $this->load->view('includes/tmp_footer_cnt');
        }
        
        public function save() { 

                $this->load->helper('html');
                $this->load->helper('url');

                $this->load->database();
                
                // Count existing events
                $count=$this->db->count_all('events');
                $id=$count+1;
                
                $faculty=$this->input->post('faculty',TRUE);
                $eventid=$id.$faculty;
                
                $organizer_id=1;
                
                $this->load->model('mevent');
                $image1path= $this->mevent->event_create_upload_image('image1',$eventid);
                
                $this->mevent->create_event_save($id,$eventid,$image1path,$organizer_id);
                
                redirect(base_url().'event/view/'.$eventid);
            
        }
        
        public function update() { 

                $this->load->helper('html');
                $this->load->helper('url');

                $this->load->database();
                
                $faculty=$this->input->post('faculty',TRUE);
                
                $eventid=$this->input->post('event_id',TRUE);
                
                $this->load->model('mevent');
                $row= $this->mevent->get_events_data_update($eventid);
                
                $organizer_id=1;
                
            //---------------For Image 1-------------
                $img1_path=$row->img1_path;
                $image1path=$this->mevent->event_update_upload_image('image1',$eventid,$img1_path);
            //---------------End For Image 1---------
                
                $this->mevent->edit_event_update($image1path,$organizer_id,$eventid);
                
                redirect(base_url());
            
        }
        
        public function view($eventid) { // for view single event
            $data = array(
                'title' => 'Put Page title here',
                'mDescription' => 'Meta desctiptions goes hera',
                'mKeywords' => 'Meta Keywords goes hera',
            );
            $this->load->helper('html');
            $this->load->helper('url');
            $this->load->view('vheader', $data);
                        
            // load Logged menu
            $this->load->model('muser_components');
            $this->muser_components->generate_usermenu();
            // End Logged menu
            
            $this->load->model('mevent');
//            $data['records']= $this->mevent->get_events_data_row($event_id);
            $data['records']= $this->mevent->get_events_data($eventid);
            
            $this->load->model('User_model');
            
            if ($this->User_model->is_log()) {
                $data['logged']=TRUE;
                $userid=$this->session->userdata('user_id');
                $data['joined']=$this->mevent->join_event_check($userid,$eventid);
                
                $data['userid']=$userid;
                $data['eventid']=$eventid;
                
            }else{
                $data['logged']=FALSE;
            }
            
            $this->load->view('vmenu');
            $this->load->view('event/vsingle_event',$data);
            $this->load->view('vfooter');
        }
        
        
        
        public function join($userid,$eventid) { 
            $this->load->model('mevent');
            $this->mevent->join_event($userid,$eventid);
            
            
        }
        
}

/* End of file events.php */
/* Location: ./application/controllers/events.php */