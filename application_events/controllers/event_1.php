<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Event extends CI_Controller {

	public function index() {
            $data = array(
                'title' => 'Put Page title here',
                'mDescription' => 'Meta desctiptions goes hera',
                'mKeywords' => 'Meta Keywords goes hera',
            );
            $this->load->helper('html');
            $this->load->helper('url');
            $this->load->view('vheader', $data);
            $this->load->view('vmenu');
            $this->load->view('event/vevents');
            $this->load->view('vfooter');
        }
        
        public function show() {
            $this->load->helper('html');
            $this->load->helper('url');
            $this->load->view('includes/tmp_header_cnt');
            $this->load->view('event/vshow_events');
            $this->load->view('includes/tmp_footer_cnt');
        }

        public function create() {
            $this->load->helper('html');
            $this->load->helper('url');
            $this->load->view('includes/tmp_header_cnt');
            $this->load->view('event/vcreate_event');
            $this->load->view('includes/tmp_footer_cnt');
        }
        
        public function edit($event_id) {
            $this->load->helper('html');
            $this->load->helper('url');
            $this->load->model('mevent');
            $data['records']= $this->mevent->get_events_data($event_id);
//            echo $data;
//            $this->mevent->get_events_data($event_id);
//            
//            $row=$data['records'];
//            echo $row->title;
            
            $this->load->view('includes/tmp_header_cnt');
            $this->load->view('event/vedit_event',$data);
            $this->load->view('includes/tmp_footer_cnt');
        }
        
        public function save() { 

                $this->load->helper('html');
                $this->load->helper('url');

                $this->load->database();
                
                // Count existing events
                $count=$this->db->count_all('events');
                $id=$count+1;
                
                $faculty=$this->input->post('faculty',TRUE);
                
                //Create the folder for the new event
                mkdir('./uploads/events/'.$id);
                
                $eventid=$id.$faculty;
                $organizer_id=1;
                $image1name='event'.$id.$faculty.'_image1_cover';
                //Real path for upload the image
                $uploadImage1realpath=realpath(APPPATH . '../uploads/events/'.$id);
                
                $this->load->model('upload');
                $image1name=$this->upload->do_upload('image1',$uploadImage1realpath,$image1name);
                $image1path= base_url(). 'uploads/events/'.$id.'/'.$image1name;
                
                //Data set to insert into the Database
                $insertdata=array(
                    'id'=>$id,
                    'event_id'=>$eventid,
                    'title'=>$this->input->post('title',TRUE),
                    'description'=>$this->input->post('description',TRUE),
                    'themecolor'=>$this->input->post('color',TRUE),
                    'faculty'=>$this->input->post('faculty',TRUE),
                    'date'=>$this->input->post('date',TRUE),
                    'time'=>$this->input->post('time',TRUE),
                    'seats'=>$this->input->post('seats',TRUE),
                    'venue'=>$this->input->post('venue',TRUE),
                    'img1_path'=>$image1path,
                    'organizer_id'=>$organizer_id,
                    'contact_name'=>$this->input->post('name',TRUE),
                    'contact_email'=>$this->input->post('email',TRUE),
                    'contact_number'=>$this->input->post('phone',TRUE),
                    'created_at'=>date('Y/m/d H:i:s A'),
                );
                
                //Insert the event details into the database
                $this->db->insert('events', $insertdata);
                
                redirect('home');
            
        }
        
        
        
}

/* End of file events.php */
/* Location: ./application/controllers/events.php */