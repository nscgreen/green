<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

    public function index($year = NULL, $month = NULL) { // tParameters for calender
        $data = array(
            'title' => 'NSBM-EMS',
            'mDescription' => 'Event Management System of NSBM',
            'mKeywords' => 'Meta Keywords goes hera',
        );
        $this->load->view('vheader', $data);

        // load Logged menu
        $this->load->model('muser_components');
        $this->muser_components->generate_usermenu();
        // End Logged menu

        $this->load->view('vmenu');
        $this->load->view('vhome');
        $this->load->view('vfooter');
    }

    public function features() {
        $data = array(
            'title' => 'Features',
            'mDescription' => 'Event Management System of NSBM',
            'mKeywords' => 'Meta Keywords goes hera',
        );
        $this->load->view('vheader', $data);

        // load Logged menu
        $this->load->model('muser_components');
        $this->muser_components->generate_usermenu();
        // End Logged menu

        $this->load->view('vmenu');
        $this->load->view('vfeatures');
        $this->load->view('vfooter');
    }

    public function about() {
        $data = array(
            'title' => 'About us',
            'mDescription' => 'Event Management System of NSBM',
            'mKeywords' => 'Meta Keywords goes hera',
        );
        $this->load->view('vheader', $data);

        // load Logged menu
        $this->load->model('muser_components');
        $this->muser_components->generate_usermenu();
        // End Logged menu

        $this->load->view('vmenu');
        $this->load->view('vabout');
        $this->load->view('vfooter');
    }

    public function contact() {
        $data = array(
            'title' => 'Contact Us',
            'mDescription' => 'Event Management System of NSBM',
            'mKeywords' => 'Meta Keywords goes hera',
        );
        $this->load->view('vheader', $data);

        // load Logged menu
        $this->load->model('muser_components');
        $this->muser_components->generate_usermenu();
        // End Logged menu

        $this->load->view('vmenu');
        $this->load->view('vcontact');
        $this->load->view('vfooter');
    }

    public function team() {
        $data = array(
            'title' => 'The Team',
            'mDescription' => 'Event Management System of NSBM',
            'mKeywords' => 'Meta Keywords goes hera',
        );
        $this->load->view('vheader', $data);

        // load Logged menu
        $this->load->model('muser_components');
        $this->muser_components->generate_usermenu();
        // End Logged menu

        $this->load->view('vmenu');
        $this->load->view('vteam');
        $this->load->view('vfooter');
    }

    /**
     * Log In Functions
     */
    function validate_credentials() {
        $this->load->model('User_model');
        $query = $this->User_model->validate();

        $email = $this->input->post('login_username');
        $row = $this->User_model->get_userss_data($email);

        $user_id = $row->id_users;

        if ($query) { // if the user's credentials validated...
            $data = array(
                'user_id' => $user_id,
                'is_logged_in' => true
            );

            $this->session->set_userdata($data);
            // echo $this->session->userdata('user_id');
            redirect('user');
        } else {
            //$this->index();
            echo $this->input->post('login_username');
        }
    }

    /**
     * log Out Function
     */
    function logout() {
        $this->session->set_userdata('is_logged_in', false);
        redirect(base_url());
    }

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */