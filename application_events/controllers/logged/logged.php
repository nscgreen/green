<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Logged extends CI_Controller {

    public function index() { // to Common pages
        $this->load->model('User_model');

        if ($this->User_model->is_log()) {
            $data = array(
                'title' => 'Put Page title here',
                'mDescription' => 'Meta desctiptions goes hera',
                'mKeywords' => 'Meta Keywords goes hera',
            );
            $this->load->helper('html');
            $this->load->helper('url');
            $this->load->view('vheader', $data);
            
            // load Logged menu
            $this->load->model('muser_components');
            $this->muser_components->generate_usermenu();
            // End Logged menu
            
            $this->load->view('vmenu');

//            if ($para1=="create") {
//                $this->load->view('admin/create_event');
//            }else if ($para1=="test") {
//                $this->load->view('admin/test');
//            }else {
//                $this->load->view('admin/adhome');
//            }
            $this->load->view('logged/lghome'); // Load this files to only for Logged users
            $this->load->view('vfooter');
        } else {

            redirect(base_url());
        }
    }

    public function calendar() {
        $this->load->helper('html');
        $this->load->helper('url');
        //$this->load->model('Calendar');
        //$caldata= $this->Calendar->displaycal('','',TRUE);
//            $dat = $this->datacal->displaycal('','',TRUE);
        $this->load->view('vcalendar');
    }

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */