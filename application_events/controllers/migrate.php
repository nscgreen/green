<?php defined("BASEPATH") or exit("No direct script access allowed");

 class Migrate extends CI_Controller {
 
    public function index($name){
        $this->load->dbforge();
        if ($this->dbforge->create_database($name)) {
            echo 'Database created!';
        }   
    }
    
    public function dbdrop($name) {
        $this->load->dbforge();
        if ($this->dbforge->drop_database($name)) {
            echo 'Database deleted!';
        }
    }

    public function runmigration($version) {
        $this->load->library('migration');
        if (!$this->migration->version($version)) {
            show_error($this->migration->error_string());
        }else {
            echo 'Table Successfully Creatred !!';
        }
    }
    
    public function droptable($tablename) {
        $this->dbforge->drop_table($tablename);
    }

}
