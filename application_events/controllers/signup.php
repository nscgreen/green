<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Signup extends CI_Controller {

    public function index() {
        // load signup only for new users
        $this->load->model('User_model');
        if (!$this->User_model->is_log()) {

            $data = array(
                'title' => 'Put Page title here',
                'mDescription' => 'Meta desctiptions goes hera',
                'mKeywords' => 'Meta Keywords goes hera',
            );
            $this->load->helper('html');
            $this->load->helper('url');
            $this->load->view('vheader', $data);
            $this->load->view('vmenu');
            
            $this->load->model('mevent_bar');
            
            $data['upcoming_events']= $this->mevent_bar->get_upcoming_events_data();
            $this->load->view('vsignup',$data);
            $this->load->view('vfooter');
        } else {

            redirect('user');
        }
    }

    function create_member(){
        
        /*$this->load->library('form_validation');
        //field name, error message, validation rules
        
        $this->form_validation->set_rules('f_name', 'Name', 'trim|required');
        $this->form_validation->set_rules('index_no', 'Index Number', 'trim|required');
        $this->form_validation->set_rules('email', 'Email address', 'trim|required|valid_email');
        
        //$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[4]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]|max_length[32]');
        $this->form_validation->set_rules('password2', 'Password Confirmation', 'trim|required|matches[password]');
        
        if($this->form_validation->run() == FALSE){

        	
            $this->index();
            
        }  else {  */
            
       $this->load->model('User_model');

        if ($query = $this->User_model->create_user()) {

            $data = array(
                'username' => $this->input->post('login_username'),
                'is_logged_in' => true
            );

            $this->session->set_userdata($data);
            redirect('user');
        } else {

            redirect('home/team');
        }
        //}
        
        
    }

}

/* End of file login.php */
/* Location: ./application/controllers/login.php */