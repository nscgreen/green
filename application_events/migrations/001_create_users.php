<?php

class Migration_create_users extends CI_Migration {

    public function up() {
        $this->dbforge->add_field(array(
            'id_users' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'email' => array(
                'type' => 'VARCHAR',
                'constraint' => '100'
            ),
            'password' => array(
                'type' => 'VARCHAR',
                'constraint' => '128'
            ),
            'f_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '50'
            ),
            'l_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '50'
            ),
            'index_no' => array(
                'type' => 'VARCHAR',
                'constraint' => '25',
                
            ),
            'batch' => array(
                'type' => 'VARCHAR',
                'constraint' => '30',
            ),
            'uni' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
            ),
            'contact_no' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
            ),
            'pro_img' => array(
                'type' => 'TEXT'
            ),
            'address' => array(
                'type' => 'VARCHAR',
                'constraint' => '200'
            ),
        ));
        $this->dbforge->add_key('id_users');
        $this->dbforge->create_table('users');
    }

    public function down() {
        $this->dbforge->drop_table('users');
    }

}
