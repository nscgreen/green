<?php

class Migration_participants extends CI_Migration {

    public function up() {
        $fields = array(
            'attend_no INT NOT NULL',
            'joined_at VARCHAR(45) NULL',
            'user_id INT NOT NULL',
            'event_id VARCHAR(11) NOT NULL',
            'attendence VARCHAR(10) NULL DEFAULT "FALSE" ',
        );
        
        $this->dbforge->add_field($fields);      
        $this->dbforge->create_table('participants');
        $this->dbforge->add_key('attend_no');
    }

    public function down() {
        $this->dbforge->drop_table('participants');
    }

}
