<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_events extends CI_Migration {

    public function up() {
        $fields = array(
            'id INT (5) NULL',
            'event_id VARCHAR(10) NULL',
            'title  VARCHAR(45) NULL',
            'description  TEXT NULL',
            'organized_by VARCHAR(50) NULL',
            'themecolor  VARCHAR(255) NULL',
            'faculty VARCHAR(20) NULL',
            'date  VARCHAR(15) NULL',
            'time  VARCHAR(15) NULL',
            'seats  INT(4) NULL',
            'venue  VARCHAR(45) NULL',
            'img1_path TEXT NULL',
            'organizer_id  VARCHAR(30) NULL',
            'contact_name  VARCHAR(50) NULL',
            'contact_number  INT NULL',
            'contact_email  VARCHAR(100) NULL',
            'created_at  VARCHAR(45) NULL',
            'updated_at  VARCHAR(45) NULL',
            'approval  TINYINT(1)  NULL'
            
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->create_table('events');


        $this->dbforge->add_key('event_id');
    }

    public function down() {
        $this->dbforge->drop_table('events');
    }

}
