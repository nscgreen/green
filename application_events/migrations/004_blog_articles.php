<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Apr 27, 2014, 11:52:44 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_blog_articles extends CI_Migration {

    public function up() {
        $this->dbforge->add_field(array(
            'article_id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'zerofill' => TRUE,
                'auto_increment' => TRUE,
                'null' => FALSE
            ),
            'article_title' => array(
                'type' => 'VARCHAR',
                'constraint' => '30',
                'null' => FALSE,
            ),
            'article_description' => array(
                'type' => 'TEXT',
                'null' => FALSE,
            ),
            'article_timestamp' => array(
                'type' => 'INT',
                'constraint' => 10,
                'unsigned' => TRUE,
                'null' => FALSE,
            ),
            'article_author' => array(
                'type' => 'VARCHAR',
                'constraint' => '20',
                'null' => FALSE,
            ),
        ));

        $this->dbforge->add_key('article_id');
        $this->dbforge->create_table('articles');
    }

    public function down() {
        $this->dbforge->drop_table('articles');
    }

}
