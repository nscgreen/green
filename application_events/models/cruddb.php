<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class cruddb extends CI_Model{

  function cruddb(){
      $this->load->helper('url');       
  }

function entry_insert(){

    $this->load->database();

    $data = array(

              'event_id'=>$this->input->post('event_id'),

              'title'=>$this->input->post('title'),

              'description'=>$this->input->post('description'),

              'max_attendance'=>$this->input->post('max_attendance'),

              'is_visible'=>$this->input->post('is_visible'),

              'created_at'=>$this->input->post('created_at'),

              'updated_at'=>$this->input->post('updated_at'),

              'venue'=>$this->input->post('venue'),

              'date'=>$this->input->post('date'),

              'contact_number'=>$this->input->post('contact_number'),

              'contact_email'=>$this->input->post('contact_email'),

              'audience'=>$this->input->post('audience'),

              'img_path'=>$this->input->post('img_path'),

            );

    $this->db->insert('events',$data);

  }

  function get($event_id){
      $this->load->database();
      //have to change get or getwhere
      $query = $this->db->get('events',array('event_id'=>$event_id));
   return $query->row_array();      
    }

    function events_getall()
  {
  $this->load->database();
  $query = $this->db->get('events');
  return $query->result();
  }

  function getall(){
  $this->load->database();
  $this->load->library('table');
  
  $query = $this->db->query('SELECT * FROM events'); 
  $table = $this->table->generate($query);
  return $table;  
  }



  function general(){
  $this->load->library('MyMenu');
  $menu = new MyMenu;
  $data['base']   = $this->config->item('base_url');
  $data['menu']     = $menu->show_menu();
  //$data['webtitle'] = 'Book Collection';
  //$data['websubtitle']= 'We collect all title of books on the world';
  //$data['webfooter']  = '© copyright by step by step php tutorial';
  

               
  $data['event_id']    = 'event_id';
  $data['title']   = 'title';
  $data['description']  = 'description';        
  $data['max_attendance']   = 'max_attendance';
  $data['is_visible']   = 'is_visible';
  $data['is_visibles']   = array('0'=>'0','1'=>'1');  
  $data['created_at']   = 'created_at';
  $data['updated_at']   = 'updated_at';
  $data['venue']   = 'venue';
  $data['date']   = 'date';
  $data['contact_number']   = 'contact_number';
  $data['contact_email']   = 'contact_email';
  $data['audience']    = 'audience';  
  $data['img_path']  = 'img_path';  

  $data['forminput']  = 'Form Input';
  $data['fevent_id']       = array('name'=>'event_id','size'=>30);
  $data['ftitle']          = array('name'=>'title','size'=>45);
  $data['fdescription']    = array('name'=>'description','size'=>255);
  $data['fmax_attendance']   = array('name'=>'max_attendance','size'=>11);
  $data['fis_visible']   = array('name'=>'is_visible','size'=>1);
  $data['fcreated_at']  = array('name'=>'created_at','size'=>45);
  $data['fupdated_at']   = array('name'=>'updated_at','size'=>45);
  $data['fvenue']   = array('name'=>'venue','size'=>45);
  $data['fdate']  = array('name'=>'date','size'=>45);
  $data['fcontact_number']   = array('name'=>'contact_number','size'=>45);
  $data['fcontact_email']   = array('name'=>'contact_email','size'=>50);
  $data['faudience']  = array('name'=>'audience','size'=>50);
  $data['fimg_path']  = array('name'=>'img_path','size'=>100);

    return $data; 
  }


  function entry_update(){
   $this->load->database();
   $data = array(

              'event_id'=>$this->input->post('event_id'),

              'title'=>$this->input->post('title'),

              'description'=>$this->input->post('description'),

              'max_attendance'=>$this->input->post('max_attendance'),

              'is_visible'=>$this->input->post('is_visible'),

              'created_at'=>$this->input->post('created_at'),

              'updated_at'=>$this->input->post('updated_at'),

              'venue'=>$this->input->post('venue'),

              'date'=>$this->input->post('date'),

              'contact_number'=>$this->input->post('contact_number'),

              'contact_email'=>$this->input->post('contact_email'),

              'audience'=>$this->input->post('audience'),

              'img_path'=>$this->input->post('img_path'),

            );
$this->db->where('event_id',$this->input->post('event_id'));
$this->db->update('events',$data);  
 }

 function delete($event_id){
  $this->load->database();
  $this->db->delete('events', array('event_id' => $event_id)); 
}
  
}