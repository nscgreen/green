<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mevent extends CI_Model {
        public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
//                $this->load->library('database');
	}
        
        public function create_event_save($id,$eventid,$image1path,$organizer_id){
                //Data set to insert into the Database
                $insertdata=array(
                    'id'=>$id,
                    'event_id'=>$eventid,
                    'title'=>$this->input->post('title',TRUE),
                    'description'=>$this->input->post('description',TRUE),
                    'themecolor'=>$this->input->post('color',TRUE),
                    'faculty'=>$this->input->post('faculty',TRUE),
                    'date'=>$this->input->post('date',TRUE),
                    'time'=>$this->input->post('time',TRUE),
                    'seats'=>$this->input->post('seats',TRUE),
                    'venue'=>$this->input->post('venue',TRUE),
                    'img1_path'=>$image1path,
                    'organizer_id'=>$organizer_id,
                    'contact_name'=>$this->input->post('name',TRUE),
                    'contact_email'=>$this->input->post('email',TRUE),
                    'contact_number'=>$this->input->post('phone',TRUE),
                    'created_at'=>date('Y/m/d h:i:s A'),
                );
                
                //Insert the event details into the database
                $this->db->insert('events', $insertdata);
        }
        
        public function edit_event_update($image1path,$organizer_id,$eventid){
 //Data set to insert into the Database
                $insertdata=array(
//                    'id'=>$id,
//                    'event_id'=>$eventid,
                    'title'=>$this->input->post('title',TRUE),
                    'description'=>$this->input->post('description',TRUE),
                    'themecolor'=>$this->input->post('color',TRUE),
                    'faculty'=>$this->input->post('faculty',TRUE),
                    'date'=>$this->input->post('date',TRUE),
                    'time'=>$this->input->post('time',TRUE),
                    'seats'=>$this->input->post('seats',TRUE),
                    'venue'=>$this->input->post('venue',TRUE),
                    'img1_path'=>$image1path,
                    'organizer_id'=>$organizer_id,
                    'contact_name'=>$this->input->post('name',TRUE),
                    'contact_email'=>$this->input->post('email',TRUE),
                    'contact_number'=>$this->input->post('phone',TRUE),
                    'created_at'=>date('Y/m/d H:i:s A'),
                );
                
                //Insert the event details into the database
                
                $this->db->where('event_id', $eventid);
                $this->db->update('events', $insertdata); 
//                $this->db->insert('events', $insertdata);
        }
        
        public function get_events_data($event_id){
           $query = $this->db->get_where('events', array('event_id' => $event_id));
           $result=$query->result();
           return $result;
        }        
        
        public function get_events_data_update($event_id){
           $query = $this->db->get_where('events', array('event_id' => $event_id));
           $result=$query->row();
           echo array($result);
           return $result;
        }
        
        public function get_events_data_show($userid){
           $query = $this->db->get_where('events', array('organizer_id' => $userid));
           $result=$query->result();
           return $result;
        }
        
        public function get_events_data_timeline(){
           $this->db->order_by("id", "desc"); 
           $query = $this->db->get('events');
           $result=$query->result();
           return $result;
        }
        
        public function checkimagepath($imageColumnName,$event_id){
           //$query = $this->db->get_where('events', array('event_id' => $event_id, "$image"=>NULL));
           
           $query = $this->db->query("SELECT * FROM events WHERE event_id='$event_id' AND $imageColumnName IS NULL");
           if($query->num_rows()==1){
               return TRUE;
               $details['nofile']=TRUE;
               $details['filepath']="";
           }else{
               $details['nofile']=TRUE;
               $details['filepath']="";
           }
        }

        public function event_create_upload_image($image_field_name,$eventid){

            if (isset($_FILES[$image_field_name]['name']) && !empty($_FILES[$image_field_name]['name'])) {
                // do_upload
            //Create the folder for the new event
                if (!is_dir('./uploads/events/'.$eventid)) {
                //    mkdir('./uploads/' . $date, 0777, TRUE);
                    mkdir('./uploads/events/'.$eventid);

                }

                $image1name='event'.$eventid.'_image1_cover';
                //Real path for upload the image
                $uploadImage1realpath=realpath(APPPATH . '../uploads/events/'.$eventid);

                $this->load->model('upload');
                $image1name=$this->upload->do_upload('image1',$uploadImage1realpath,$image1name);

//                    $image1path= base_url(). 'uploads/events/'.$id.'/'.$image1name;
                $image1path= 'uploads/events/'.$eventid.'/'.$image1name;
                return $image1path;
            }else{
                $image1path= NULL;
                return $image1path;
            }
            
        }
        
        public function event_update_upload_image($image_field_name,$eventid,$img1path){

            if (isset($_FILES[$image_field_name]['name']) && !empty($_FILES[$image_field_name]['name'])) {
                //Create the folder for the new event
                if (!is_dir('./uploads/events/'.$eventid)) {
                //    mkdir('./uploads/' . $date, 0777, TRUE);
                    mkdir('./uploads/events/'.$eventid);

                }                    
//                    $this->load->model('mevent');
                if(!($this->mevent->checkimagepath('img1_path',$eventid)==TRUE)){
//                    $img1path=$row->img1_path;
                    unlink($img1path);
                    }

                $image1name='event'.$eventid.'_image1_cover';
                //Real path for upload the image
                $uploadImage1realpath=realpath(APPPATH . '../uploads/events/'.$eventid);

                $this->load->model('upload');
                $image1name=$this->upload->do_upload('image1',$uploadImage1realpath,$image1name);
                $image1path= base_url(). 'uploads/events/'.$eventid.'/'.$image1name;
                return $image1path;
            }else{
                $image1path= NULL;
                return $image1path;
            }
            
        }
        
        public function join_event_check($userid,$eventid){
//            $this->load->model('User_model');
//            if ($this->User_model->is_log()) {
//                $joined_details['logged']=TRUE;
                
                $query =$this->db->get_where('participants', array('event_id' => $eventid,'user_id'=>$userid,'attendence'=>'Joined'));
                if($query->num_rows()>0){
                    $joined=TRUE;
                }else{
                    $joined=FALSE;
                }
//            }
//            else{
//                $joined_details['logged']=FALSE;
//            }
            return $joined;
        }
        
        public function join_event($userid,$eventid){
           
                    $query1 = $this->db->get_where('participants', array('event_id' => $eventid));
                    $num=$query1->num_rows();
                    $attend_no = ++$num;
                    //Data set to insert into the Database
                    $insertdata=array(
                        'attend_no'=>$attend_no,
                        'joined_at'=>date('Y/m/d h:i:s A'),
                        'user_id'=>$userid,
                        'event_id'=>$eventid,
                        'attendence'=>'Joined'
                    );
                    
                    //Insert the participant details into the database
                    $this->db->insert('participants', $insertdata);
                    
                    redirect(base_url().'event/view/'.$eventid);

        }
        
        
        
        
}
?>

