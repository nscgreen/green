<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mevent extends CI_Model {
        public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
//                $this->load->library('database');
	}
        
        public function get_events_data($event_id){
           $query = $this->db->get_where('events', array('event_id' => $event_id));
           $result=$query->result();
           return $result;
        }
        
}
?>

