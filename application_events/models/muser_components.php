<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Muser_components extends CI_Model {

        //  Loading User Logged Menu
        public function generate_usermenu(){
           $this->load->model('User_model');
            if ($this->User_model->is_log()) {
                $userid=$this->session->userdata('user_id');  
               
                $this->load->model('mevent');

                $data['events']= $this->mevent->get_events_data_show($userid);
                
                $this->load->view('logged/lgmenu',$data);
                $this->load->view('logged/space');
            }
        }
}
?>

