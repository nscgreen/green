<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Upload extends CI_Model {
        public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}
        
        public function do_upload($imageField,$path,$imagename){
            
                $config['upload_path'] = $path;
		$config['allowed_types'] = 'gif|jpg|png';
                $config['file_name']	= $imagename;
		$config['max_size']	= '2048';
//		$config['max_width']  = '1024';
//		$config['max_height']  = '768';

                $config['overwrite']=FALSE;
                
		$this->load->library('upload', $config);
                
                if ( ! $this->upload->do_upload($imageField))
		{
			$error = array('error' => $this->upload->display_errors());
                        echo $error.'<br>';
                        echo 'error'.'<br>';
		}
		else
		{
                        $uploadedData=$this->upload->data();
                        $image1Name=$uploadedData['file_name'];
                        
		}
                return $image1Name;

        }
    
}
?>

