<html>
<head></head>
<body>

<div id="menu">
<?php 	$this->load->view('crud_menu'); ?>
</div>
<?php echo heading('List of Events',3); ?>
<table border="1">
  <tr>
    <th>event_id</th>
    <th>title</th>    
    <th>description</th>    
    <th>max_attendance</th>  
    <th>is_visible</th>
    <th>created_at</th>    
    <th>updated_at</th>    
    <th>venue</th>  
    <th>date</th>  
    <th>contact_number</th>
    <th>contact_email</th>    
    <th>audience</th>    
    <th>img_path</th>      
  </tr>
<?php 
foreach($query as $row){
  echo "<tr>";
	echo "<td>". $row->event_id ."</td>";
	echo "<td>". $row->title ."</td>";
	echo "<td>". $row->description ."</td>";
	echo "<td>". $row->max_attendance ."</td>";
  echo "<td>". $row->is_visible ."</td>";
  echo "<td>". $row->created_at ."</td>";
  echo "<td>". $row->updated_at ."</td>";
  echo "<td>". $row->venue ."</td>";
  echo "<td>". $row->date ."</td>";
  echo "<td>". $row->contact_number ."</td>";
  echo "<td>". $row->contact_email."</td>";
  echo "<td>". $row->audience ."</td>";
  echo "<td>". $row->img_path."</td>";

	echo "<td>". anchor('crud/input/'.$row->event_id,'Edit') ."</td>";
    echo "<td>". anchor('crud/del/'.$row->event_id,'Delete') ."</td>";
  echo "</tr>";	
}
?>
</table>

</body>
</html>