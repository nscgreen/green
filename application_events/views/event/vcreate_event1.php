<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Apr 21, 2014, 1:41:46 PM
 */
?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-sm-8 col-xm-8">
            <div class="page-header text-center" style="border-bottom: 5px solid #BD30B7;">
                <h1 id="timeline" class="animated bounceInUp" style="color: #009999;">Create a Event</h1>
            </div>
            <div class="">
            <form>
                <h1>Fill the Event Details</h1>
                <div class="well">
                    <h3>Event Title</h3>
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon">T</span>
                        <input type="text" name="title" class="form-control" placeholder="Event Title">
                    </div>
                    
                    <h3>Description</h3>
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon">D</span>
                        <textarea  style="height: 100px"  name="title" class="form-control" placeholder="Enter text ..."></textarea>
                    </div>
                    
                    <h3>Select a event theme colour</h3>
                    <div class="bfh-colorpicker" data-name="color" data-color="#FF0000"></div>

                    <h3>Select a faculty</h3>
                    <div class="bfh-selectbox" data-name="faculty" data-value="al" data-filter="true">
                        <div data-value="al">All</div>
                        <div data-value="cm">Computing</div>
                        <div data-value="mn">Management</div>
                        <div data-value="mu">Multimedia</div>
                    </div>
                    
                    <h3>Select Date</h3>
                    <div class="bfh-datepicker" data-name="date" data-min="01/15/2013" data-close="true></div>
                    <h3>Time</h3>
                    <div class="bfh-timepicker" data-name="time" data-mode="12h"></div>
                    <h3>Seats</h3>
                    <div class="bfh-slider"data-min="10" data-max="800" data-name="seats"></div>
                    <h3>Venue</h3>
                    <div class="input-group">
                        <span class="input-group-addon">V</span>
                        <input type="text" name="venue" class="form-control" placeholder="Venue e.g: NSBM Aauditoriam">
                    </div>
                    
                    <h3>Event Cover</h3>
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-new thumbnail" style="width: 200px; height: 200px;">
                            <img data-src="holder.js/100%x100%" src="http://placehold.it/900x300" alt="image1">
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                        <div>
                            <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="..."></span>
                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                        </div>
                    </div>
                </div>
                
                <h1>Contact details</h1>
                <div class="well">
                    <h3>Name</h3>
                    <div class="input-group">
                        <span class="input-group-addon glyphicon glyphicon-user"></span>
                        <input type="text" name="venue" class="form-control" placeholder="Name">
                    </div>
                    <h3>Email</h3>
                    <div class="input-group">
                        <span class="input-group-addon">@</span>
                        <input type="text" name="venue" class="form-control" placeholder="Email">
                    </div>
                    <h3>Tel</h3>
                    <div class="input-group">
                        <span class="input-group-addon">+94</span>
                        <input type="text" name="venue" class="form-control" placeholder="Phone Number">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5 col-md-offset-7 col-sm-5 col-sm-offset-6 col-xs-12">
                        <button type="submit" class="btn btn-success btn-lg">Submit Event Details</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
        
        <div class="col-md-3 col-sm-3 hidden-xs col-md-offset-1 col-sm-offset-1">
            <br /><br /><br /><br />
            <h1 id="responsive_headline" style="color: #ff0099"> Upcoming Events</h1>
            <p><small>The Inception Convocation Ceremony of the National School of Business Management was held on the 29th January 2014 at BMICH Business Management was held on the 29th January 2014 at BMICH</small></p>
            <div class="row">
                <div class="thumbnail">
                    <img src="<?php echo base_url(); ?>assets/img/hh.jpg" alt="...">
                    <div class="caption" style="background: #33ccff">
                        <h3>Event Name</h3>
                        <p>Date :</p>
                        <p>Time :</p>
                        <p>Venue :</p>
                        <p><a href="#" class="btn btn-primary" role="button">View More</a></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="thumbnail">
                    <img src="<?php echo base_url(); ?>assets/img/hh.jpg" alt="...">
                    <div class="caption" style="background: #00ccff">
                        <h3>Event Name</h3>
                        <p>Date :</p>
                        <p>Time :</p>
                        <p>Venue :</p>
                        <p><a href="#" class="btn btn-primary" role="button">View More</a></p>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>
<br /><br /><br />