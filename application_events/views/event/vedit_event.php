<?php
//$row=$records;
//$title=$row->title;
//echo $title;

foreach ($records as $row) {
    $event_id=$row->event_id;
    $title=$row->title;
    $descrition=$row->description;
    $organized_by=$row->organized_by;    
    $themecolor=$row->themecolor;
    $faculty=$row->faculty;
    $date=$row->date;
    $time=$row->time;
    $seats=$row->seats;
    $venue=$row->venue;
//    $organizer_id'=>$organizer_id,
    $contact_name=$row->contact_name;
    $contact_email=$row->contact_email;
    $contact_number=$row->contact_number;
}
?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-sm-8 col-xm-8">
            <div class="page-header text-center" style="border-bottom: 5px solid #ff9900;">
                <h1 id="timeline" class="animated bounceInUp" style="color: #009999;">Modify Event Details</h1>
            </div>
            <div class="">
            <form action="<?php echo base_url();?>event/update" method="post" enctype="multipart/form-data">
                <input type="hidden" value="<?php echo $event_id ?>" name="event_id">
                <h1>Agile workshop Details</h1>
                <div class="well">
                    <h3>Event Title</h3>
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon">T</span>
                        <input type="text" name="title" class="form-control" value="<?php echo $title ?>">
                    </div>
                    
                    <h3>Description</h3>
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon">D</span>
                        <textarea  style="height: 100px"  name="description" class="form-control"><?php echo $descrition ?></textarea>
                    </div>
                    
                    <h3>Organized by</h3>
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon">O</span>
                        <input type="text" name="organized_by" class="form-control" value="<?php echo $organized_by ?>">
                    </div>
                    
                    <h3>Select a event theme colour</h3>
                    <div class="bfh-colorpicker" data-name="color" data-color="<?php echo $themecolor ?>"></div>

                    <h3>Select a faculty</h3>
                    <div class="bfh-selectbox" data-name="faculty" data-value="<?php echo $faculty ?>" data-filter="true">
                        <div data-value="al">All</div>
                        <div data-value="cm">Computing</div>
                        <div data-value="mn">Management</div>
                        <div data-value="mu">Multimedia</div>
                    </div>
                    
                    <h3>Select Date</h3>
                    <div class="bfh-datepicker" data-name="date" data-min="01/15/2013" data-close="true" data-date="<?php echo $date ?>"></div>
                    <h3>Time</h3>
                    <div class="bfh-timepicker" data-name="time" data-mode="12h" data-time="<?php echo $time ?>"></div>
                    <h3>Seats</h3>
                    <div class="bfh-slider"data-min="10" data-max="800" data-name="seats" data-value="<?php echo $seats ?>"></div>
                    <h3>Venue</h3>
                    <div class="input-group">
                        <span class="input-group-addon">V</span>
                        <input type="text" name="venue" class="form-control" value="<?php echo $venue ?>">
                    </div>
                    
                    <h3>Event Cover</h3>
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-new thumbnail" style="width: 200px; height: 200px;">
                            <img data-src="holder.js/100%x100%" src="http://placehold.it/900x300" alt="image1">
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                        <div>
                            <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="image1"></span>
                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                        </div>
                    </div>
                </div>
                
                <h1>Contact details</h1>
                <div class="well">
                    <h3>Name</h3>
                    <div class="input-group">
                        <span class="input-group-addon glyphicon glyphicon-user"></span>
                        <input type="text" name="name" class="form-control" value="<?php echo $contact_name ?>">
                    </div>
                    <h3>Email</h3>
                    <div class="input-group">
                        <span class="input-group-addon">@</span>
                        <input type="text" name="name" class="form-control" value="<?php echo $contact_email ?>">
                    </div>
                    <h3>Tel</h3>
                    <div class="input-group">
                        <span class="input-group-addon">+94</span>
                        <input type="text" name="phone" class="form-control" value="<?php echo $contact_number ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-md-offset-8 col-sm-3 col-sm-offset-8 col-xs-12">
                        <button type="submit" class="btn btn-primary btn-lg">Save</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
        
        <div class="col-md-3 col-sm-3 hidden-xs col-md-offset-1 col-sm-offset-1">
            <br /><br /><br /><br />
            <h1 id="responsive_headline" style="color: #ff0099"> Upcoming Events</h1>
            <p><small>The Inception Convocation Ceremony of the National School of Business Management was held on the 29th January 2014 at BMICH Business Management was held on the 29th January 2014 at BMICH</small></p>
            <?php
            foreach ($upcoming_events as $row){
            ?>
            <div class="row">
                <div class="thumbnail">
                    <img src="<?php echo base_url().$row->img1_path; ?>" alt="...">
                    <div class="caption" style="background: #33ccff">
                        <h3><?php echo $row->title ?></h3>
                        <p>Date : <?php echo $row->date ?></p>
                        <p>Time : <?php echo $row->time ?></p>
                        <p>Venue : <?php echo $row->venue ?></p>
                        <p><a href="<?php echo base_url().'event/view/'.$row->event_id ?>" class="btn btn-primary" role="button">View More</a></p>
                    </div>
                </div>
            </div>
            <?php
            } 
            ?>
        </div>
        
    </div>
</div>
<br /><br /><br />