<div id="wrapper">
<div class="container">
    <div class="page-header text-center">
        <h1 id="timeline">Upcoming Events @ NSBM</h1>
    </div>
    
<!--    Start Timeline of Events    -->
    <ul class="timeline">
<?php
$numevents=count($events);
$inverted=FALSE;
foreach ($events as $row){
    If($inverted==FALSE){
?>
    <!--        Start Event with Normal Class     -->
        <li>
          <div class="timeline-badge primary"><a><i class="glyphicon glyphicon-record" rel="tooltip" title="<?php echo 'On '.$row->date.'  at '.$row->time ?>" id=""></i></a></div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <img class="img-responsive" src="<?php echo base_url().$row->img1_path; ?>" />
              
            </div>
            <div class="timeline-body">
              <p><?php echo $row->description ?></p>
              
            </div>
            
            <div class="timeline-footer">
                <a><i class="glyphicon glyphicon-thumbs-up"></i></a>
                <a href="<?php echo base_url().'event/view/'.$row->event_id ?>"><i class="glyphicon glyphicon-share"></i></a>
                <a href="<?php echo base_url().'event/view/'.$row->event_id ?>" class="pull-right">Want more info..?</a>
            </div>
          </div>
        </li>
    <!--        End Event with Normal Class     -->
<?php 
    $inverted=TRUE;
    }else {
?>
    <!--        Start Event with Inverted Class     -->
        <li  class="timeline-inverted">
          <div class="timeline-badge primary"><a><i class="glyphicon glyphicon-record invert" rel="tooltip" title="<?php echo 'On '.$row->date.'  at '.$row->time ?>" id=""></i></a></div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <img class="img-responsive" src="<?php echo base_url().$row->img1_path; ?>" />
              
            </div>
            <div class="timeline-body">
              <p><?php echo $row->description ?></p>
             
            </div>
            
            <div class="timeline-footer">
                <a><i class="glyphicon glyphicon-thumbs-up"></i></a>
                <a href="<?php echo base_url().'event/view/'.$row->event_id ?>"><i class="glyphicon glyphicon-share"></i></a>
                <a href="<?php echo base_url().'event/view/'.$row->event_id ?>" class="pull-right">Want more info..?</a>
            </div>
          </div>
        </li>
    <!--        End Event with Inverted Class     -->
<?php
    $inverted=FALSE;
    }
}
?>    
<!--    End Timeline of Events    -->        
        

        
        <li class="clearfix" style="float: none;"></li>
    </ul>
</div>
</div>