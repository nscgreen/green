<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Apr 21, 2014, 1:41:46 PM
 */
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">

            <h1 class="alert alert-info">Events you Joined</h1>
            <hr>
            <div class="media">
                <a class="pull-left" href="<?php echo base_url(); ?>event/view">
                    <img class="media-object img-circle img-responsive" style="width: 100px; height: 100px; padding: 0;"  src="<?php echo base_url(); ?>assets/img/nsbmbuilding1.jpg">
                </a>
                <div class="media-body">
                    <h4 class="media-heading"><a href="<?php echo base_url(); ?>event/view">Event Title</a></h4>
                    <h5>Date : 12-12-2323</h5>
                    <h5>Time : 12.12 PM <a href="<?php echo base_url(); ?>event/view"><span class="glyphicon glyphicon-new-window"></span></a></h5>
                    <p align="justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus, sapien nec dignissim porta. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus, sapien nec dignissim porta. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
            </div><hr>
            
            <div class="media">
                <a class="pull-left" href="#">
                    <img class="media-object img-circle img-responsive" style="width: 100px; height: 100px; padding: 0;"  src="<?php echo base_url(); ?>assets/img/nsbmbuilding1.jpg">
                </a>
                <div class="media-body">
                    <h4 class="media-heading"><a href="">Event Title</a></h4>
                    <h5>Date : 12-12-2323</h5>
                    <h5>Time : 12.12 PM <a href="<?php echo base_url(); ?>event/view"><span class="glyphicon glyphicon-new-window"></span></a></h5>
                    <p align="justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus, sapien nec dignissim porta. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus, sapien nec dignissim porta. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
            </div><hr>
            
            <div class="media">
                <a class="pull-left" href="#">
                    <img class="media-object img-circle img-responsive" style="width: 100px; height: 100px; padding: 0;"  src="<?php echo base_url(); ?>assets/img/nsbmbuilding1.jpg">
                </a>
                <div class="media-body">
                    <h4 class="media-heading"><a href="">Event Title</a></h4>
                    <h5>Date : 12-12-2323</h5>
                    <h5>Time : 12.12 PM <a href="<?php echo base_url(); ?>event/view"><span class="glyphicon glyphicon-new-window"></span></a></h5>
                    <p align="justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus, sapien nec dignissim porta. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus, sapien nec dignissim porta. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </div>
            </div>
        </div>
    </div><!--First row -->

    <div class="row" style="margin: 20px auto;">
        <div class="col-md-12">
            <h1 class="alert alert-success">Events you Created</h1>
            <hr>
    <!--        Start Created Events     -->
            <?php
            foreach ($created_events as $row){
            ?>
            
            <div class="media">
                <a class="pull-left" href="<?php echo base_url().'event/view/'.$row->event_id ?>">
                    <img class="media-object img-circle img-responsive" style="width: 100px; height: 100px; padding: 0;"  src="<?php echo base_url().$row->img1_path; ?>">
                </a>
                <div class="media-body">
                    <h4 class="media-heading"><a href="<?php echo base_url().'event/view/'.$row->event_id ?>"><?php echo $row->title ?></a><a href="#" onclick='loadwrap("#wrapper","<?php echo base_url(); ?>event/edit/<?php echo $row->event_id ?>");'><span class="glyphicon glyphicon-edit"></span></a></h4>
                    <h5>Date : <?php echo $row->date ?></h5>
                    <h5>Time : <?php echo $row->time ?></h5>
                    <p align="justify"><?php echo $row->description ?></p>
                </div>
            </div><hr>
            <?php
            } 
            ?>
    <!--        End Created Events     -->
        </div>
    </div> <!--Second row -->
</div>