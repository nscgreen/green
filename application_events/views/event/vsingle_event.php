<?php
function get_month_short($mnum){
    switch ($mnum) {
    case "01":
        return "Jan";
    case "02":
        return "Feb";
    case "03":
        return "Mar";
    case "04":
        return "Apr";
    case "05":
        return "May";
    case "06":
        return "Jun";
    case "07":
        return "Jul";
    case "08":
        return "Aug";
    case "09":
        return "Sep";
    case "10":
        return "Oct";
    case "11":
        return "Nov";
    case "12":
        return "Dec";
    }
}
foreach ($records as $row) {
    $event_id=$row->event_id;
    $title=$row->title;
    $descrition=$row->description;
    $organized_by=$row->organized_by;
    
//////// Theme color////////////
    $themecolor=$row->themecolor;
//////// Theme color////////////
    
    $faculty=$row->faculty;
    $date=$row->date;
    $time=$row->time;
    $seats=$row->seats;
    $venue=$row->venue;
    $img1_path=$row->img1_path;
//    $organizer_id'=>$organizer_id,
    $contact_name=$row->contact_name;
    $contact_email=$row->contact_email;
    $contact_number=$row->contact_number;
  
    
//    Break the date into pieces as month number,day number;
$dateParts1 = explode("/", $date);
$mnum=$dateParts1[0];
$month=get_month_short($mnum);
$day=$dateParts1[1];

$dateParts2 = explode(" ", $time);
$timeShort=$dateParts2[0];
}

?>

<div id="wrapper">
<div class="jumbotron cus-jumbo2" style="padding: 0"> <!-- Cover image -->
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
<!--                <center><img src="http://placehold.it/900x300" class="img-responsive" style="height:300px"></center>-->
                <center><img src="<?php echo base_url().$img1_path ?>" class="img-responsive" style="height:300px"></center>
                <span class="img-circle img-polaroid" style=" width: 100px; height: 100px; background-color: #000; position: absolute; top: 240px; left: 200px;text-align: center; padding: 0">
                    <h3 style="padding: 0;"><?php echo $month ?><br />
                        <?php echo $day ?><br />
                        <p style="padding: 0; margin: 0"><small><?php echo $timeShort ?></small></p></h3>
                </span>
            </div>
            
        </div>
    </div>
</div>

<div class="container" style="margin: 50px auto;">
    <div class="row">
        <div class="col-md-7 col-sm-7 col-xs-12" style="">
            <h1 class="alert alert-info"><?php echo $title ?></h1>
            <div class="jumbotron" style="padding: 30px; margin: 0 10px">
                <h2>Description:</h2>
                <div class="row">
                    <span class="col-md-2 col-sm-1 hidden-xs"></span>
                    <span class="col-md-10 col-sm-11 col-xs-12">
                        <p style="text-align: justify"><?php echo $descrition ?></p>
                    </span>
                </div>

                <div>
                    <h2>To : </h2>
                    <p style="display: inline; text-align: justify; "><?php echo $faculty ?></p>
                </div>

                <h2>Date : </h2>
                <div class="row">
                    <span class="col-md-2 col-sm-1 hidden-xs"></span>
                    <span class="col-md-10 col-sm-11 col-xs-12">
                        <p style="text-align: justify"><?php echo $date ?></p>
                    </span>
                </div>

                <h2>Time : </h2>
                <div class="row">
                    <span class="col-md-2 col-sm-1 hidden-xs"></span>
                    <span class="col-md-10 col-sm-11 col-xs-12">
                        <p style="text-align: justify"><?php echo $time ?></p>
                    </span>
                </div>
                
                <!--Newly added-->
                <h2>Venue : </h2>
                <div class="row">
                    <span class="col-md-2 col-sm-1 hidden-xs"></span>
                    <span class="col-md-10 col-sm-11 col-xs-12">
                        <p style="text-align: justify"><?php echo $venue ?></p>
                    </span>
                </div>
                
                <h2>Seats : </h2>
                <div class="row">
                    <span class="col-md-2 col-sm-1 hidden-xs"></span>
                    <span class="col-md-10 col-sm-11 col-xs-12">
                        <p style="text-align: justify"><?php echo $seats ?></p>
                    </span>
                </div>
                
                <!--End Newly added-->
                
                <h2>Organized By : </h2>
                <div class="row">
                    <span class="col-md-2 col-sm-1 hidden-xs"></span>
                    <span class="col-md-10 col-sm-11 col-xs-12">
                        <p style="text-align: justify"><?php echo $organized_by ?></p>
                    </span>
                </div>
            </div>
        </div>
        
<?php 
if(isset($logged) && $logged===TRUE){
?>
        <div class="col-md-4 col-sm-4 hidden-xs well">
            <h2>Join the event</h2>
            
            <div class="row">
                <span class="col-md-2 col-sm-1 hidden-xs"></span>
                <span class="col-md-10 col-sm-11 col-xs-12">
                    <?php 
                    if(isset($joined) && $joined===FALSE){
                    ?>
                    <a class="btn btn-info" href="<?php echo base_url().'event/join/'.$userid.'/'.$eventid; ?>">Join</a>
                    <br>
                    <?php
                    }
                    else{
                    ?>
                    Already joined
                    <?php
                    }
                    ?>
<!--                    <p style="text-align: justify">School of Computing</p>-->
                </span>
            </div>
        </div>
<?php
}
?>

    </div>
</div>
</div>