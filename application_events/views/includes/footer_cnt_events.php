<!-- Menu Script START-->
    <script>
        //  The function to change the class
        var changeClass = function(r, className1, className2) {
            var regex = new RegExp("(?:^|\\s+)" + className1 + "(?:\\s+|$)");
            if (regex.test(r.className)) {
                r.className = r.className.replace(regex, ' ' + className2 + ' ');
            }
            else {
                r.className = r.className.replace(new RegExp("(?:^|\\s+)" + className2 + "(?:\\s+|$)"), ' ' + className1 + ' ');
            }
            return r.className;
        };

        //  Creating our button in JS for smaller screens
        var menuElements = document.getElementById('menu');
        menuElements.insertAdjacentHTML('afterBegin', '<button type="button" id="menutoggle" class="menu_navtoogle" aria-hidden="true"><i aria-hidden="true" class="icon-menu"> </i> Menu</button>');

        //  Toggle the class on click to show / hide the menu
        document.getElementById('menutoggle').onclick = function() {
            changeClass(this, 'menu_navtoogle active', 'menu_navtoogle');
        }

        // responsive-retina
        document.onclick = function(e) {
            var mobileButton = document.getElementById('menutoggle'),
                    buttonStyle = mobileButton.currentStyle ? mobileButton.currentStyle.display : getComputedStyle(mobileButton, null).display;

            if (buttonStyle === 'block' && e.target !== mobileButton && new RegExp(' ' + 'active' + ' ').test(' ' + mobileButton.className + ' ')) {
                changeClass(mobileButton, 'menu_navtoogle active', 'menu_navtoogle');
            }
        };
    </script> <!-- Menu Script END-->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bst_formhelp.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bst_fileinput.min.js"></script>
    
    <script src="<?php echo base_url(); ?>assets_events/js/bst_jasny.min.js"></script>
    <script src="<?php echo base_url(); ?>assets_events/js/menu_modernizr.custom.js"></script>
    <script src="<?php echo base_url(); ?>assets_events/js/parallax.js"></script>
    <script src="<?php echo base_url(); ?>assets_events/js/init.js"></script>
    <script type="text/javascript" src="assets_events/js/site.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script> <!-- For admin menu-->
    <!--<script src="<?php echo base_url(); ?>assets/js/form_validations.js"></script>-->
    <!--<script src="<?php echo base_url(); ?>assets/js/signup_runfuntionswithtime.js"></script>-->

<!-- FitText load START-->
    <script src="<?php echo base_url(); ?>assets_events/js/fittext.js"></script>
    <script>
        window.fitText( document.getElementById("responsive_headline") );
        window.fitText( document.getElementById("responsive_headline"), 1.2 ); // turn the compressor up (font will shrink a bit more aggressively)
        window.fitText( document.getElementById("responsive_headline"), 0.8 ); // turn the compressor down (font will shrink less aggressively)
         window.fitText( document.getElementById("responsive_headline"), 1.2, { minFontSize: '30px', maxFontSize: '80px' } );
    </script> <!-- FitText load END-->

<!-- Timeline load START-->
    <script>
    $(document).ready(function(){
        var my_posts = $("[rel=tooltip]");

        var size = $(window).width();
        for(i=0;i<my_posts.length;i++){
                the_post = $(my_posts[i]);

                if(the_post.hasClass('invert') && size >=767 ){
                        the_post.tooltip({ placement: 'left'});
                        the_post.css("cursor","pointer");
                }else{
                        the_post.tooltip({ placement: 'rigth'});
                    the_post.css("cursor", "pointer");
                }
            }
        });
    </script> <!-- Timeline load END-->
    
    <!-- Ajax Wrapper loader -->
    <script>
    function loadwrap($div, $page) {
        $($div).load($page);
    }
    function calajax(page) { // for calendar
        $('#cal').load(page);
    }
    </script>