<!-- Fonts -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Lustria' rel='stylesheet' type='text/css'> <!-- For headers-->
    <link href='http://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'> <!-- p -->
    <link href="//fonts.googleapis.com/css?family=Nova+Slim" rel="stylesheet" type="text/css"> <!-- Quato -->

<!-- CSS -->
    <?php echo link_tag('assets/css/normalize.css'); ?>
    <?php echo link_tag('assets/css/cstm_reset.css'); ?>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
    <?php echo link_tag('assets/css/anime.css'); ?>
    <?php echo link_tag('assets/css/bst_formhelp.min.css');?>

    <?php echo link_tag('assets_events/css/menu.css');?>
    <?php echo link_tag('assets_events/css/site.css');?>
    <?php echo link_tag('assets_events/css/bst_jasny.min.css');?>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->