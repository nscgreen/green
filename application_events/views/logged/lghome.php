<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Apr 21, 2014, 1:48:25 PM
 */
?>

<div id="wrapper">
    <div class="container well" style="margin: 30px auto;">
        <div class="row">
            <div class="col-md-5 col-sm-5 col-xs-4">
            <div class="span2">
                <img src="<?php echo base_url();?>assets/img/user.jpg"  alt="" class="img-rounded img-responsive">
            </div>
            </div>
            
            <div class="col-md-6 col-sm-6 col-xs-6 col-md-offset-1 col-sm-offset-1">
                <div class="span4">
                    <h2>User Profile</h2><br /><br />
                    <span class="col-md-2 col-sm-3 col-xs-4"><img class="img-responsive" src="<?php echo base_url(); ?>assets/img/profile/user.png"></span>
                    <blockquote>
                        <p>Bruce Wayne</p>
                        <small><cite title="Source Title">Gotham, United Kingdom</cite></small>
                        <p style="margin-left: 68px; text-align: justify;">Bio<small>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. </small></p>
                    </blockquote>

                    <div class="row">
                        <span class="col-md-2 col-sm-3 col-xs-4"><img class="img-responsive" src="<?php echo base_url(); ?>assets/img/profile/email.png"></span>
                        <p style="margin-top: 15px ">masterwayne@batman.com</p>
                    </div>
                    <div class="row">
                        <span class="col-md-2 col-sm-3 col-xs-4"><img class="img-responsive" src="<?php echo base_url(); ?>assets/img/profile/www.png"></span>
                        <p style="margin-top: 15px "><a href="#">www.UnitedKingdom.com</a></p>
                    </div>
                    <div class="row">
                        <span class="col-md-2 col-sm-3 col-xs-4"><img class="img-responsive" src="<?php echo base_url(); ?>assets/img/profile/phone.png"></span>
                        <p style="margin-top: 15px ">19744859544</p>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>