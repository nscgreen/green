<!-- Mobile Header START -->
<div class="mobile-header">
    <ul class="clearfloat">
        <li class="m-menu">
            <a><span class="menu-open glyphicon glyphicon-th"> Priyanga</span><span class="menu-close glyphicon glyphicon-chevron-left"> Close</span></a>
        </li>
        <li class="m-menu-normal">
            <a onclick='loadwrap("#wrapper", "<?php echo base_url(); ?>event/show");'><span class="glyphicon glyphicon-th-list"> Events</span></a>
        </li>
        <li class="m-menu-normal">
            <a onclick='loadwrap("#wrapper", "<?php echo base_url(); ?>logged/logged/calendar");
                return false'><span class="glyphicon glyphicon-calendar"> Calendar</span></a>
        </li>
        <li class="m-menu-normal">
             <a href="<?php echo base_url() ?>home/logout"><span class="glyphicon glyphicon-off"></span></a>
        </li>
    </ul>
</div>
<!-- Mobile Header END -->

<!-- Mobile Menu START -->
<div id="container-outer">
    <div class="mobile-menu">
        <div class="list-group" style="background: #222e2f;">
            <!--<div class="dropdown">-->
            <a href="<?php echo base_url(); ?>user" class="list-group-item">
                <span class="glyphicon glyphicon-user"> Profile</a>
            <a href="#" onclick='loadwrap("#wrapper","<?php echo base_url(); ?>event/create");' class="list-group-item">
                <span class="glyphicon glyphicon-pencil"></span> Create Event</a>
            <div class="dropdown">
                <a class="list-group-item" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-edit glyphicon"></span> Modify event</a>
                <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel" style="left: 115px; top: 30px">
                <?php
                if( !empty($events) ) {
                    foreach ($events as $row1){
                ?>
                    <li><a href="#" onclick='loadwrap("#wrapper","<?php echo base_url(); ?>event/edit/<?php echo $row1->event_id ?>");'><?php echo $row1->title ?><span style="margin-left: 10px;"class="glyphicon glyphicon-chevron-right"></span></a></li>
                    <?php
                    }
                }
                    ?>
                </ul>
            </div>
            <a href="#" onclick='loadwrap("#wrapper","<?php echo base_url(); ?>admin/admin/test");' class="list-group-item">
                <span class="glyphicon glyphicon-cog"> Account</a>
            <a href="<?php echo base_url(); ?>help/index.html" target="_blank" class="list-group-item">
                <span class="glyphicon glyphicon-info-sign"> Help</a>
        </div>
    </div>
    <div class="mobile-menu-bg"></div>
</div>
<!-- Mobile Menu END -->