<body>
    <br />
    <div id="wrapper">
        <div class="container img-rounded" style="background-color: #EBEBEB;">

            <div class="row">
                <div class="col-lg-8">

                    <!-- the actual blog post: title/author/date/content -->
                    <h1 id="responsive_headline">The Apprentice</h1>
                    <h4 class="lead"><span class="text-muted">by</span> Chamodh hettiarachchi. <small>(13.1 computer science batch)</small></h4>
                    <p style="margin-top: -20px"><small><span class="glyphicon glyphicon-time"></span> Posted on April 01, 2014 at 8:00 PM</small></p>
                    <img src="../../uploads_events/events/2/apprentice.jpg" class="img-responsive" style="width: 900px; height:300px;">
                    <br />
                    <p class="text-justify">“YOU ARE FIRED” the world famous quote said by the Mr. Donald Trump (Chairman of The Trump organization) in one of the BBC`s reality television shows is a good example to show how the majority of interns end their careers. The “DevOps” team of National School of Business Management thought to address this matter in a professional approach in order to give undergraduates to get answers for the unsolved questions and myths about internship. After their inaugural workshop named “Towards agile” which was very successful, their second workshop “The apprentice” also turned out to be an even bigger successor than its predecessor with more than 300 participants which made NSBM auditorium houseful</p>
                    <p class="text-justify">Companies briefed the participants about their internship programs and the processed of how they select a candidate as their intern. Hence majority of the participants were 2nd and 3rd year undergraduates who are willing to find internships they found the discussion to be very rich with all the facts that they had to know about the internships. It’s always a good practice to have a clear idea of what's expected from you and what you should expect from your employer before you start your internship so that you could hear the words “YOU ARE HIRED”! .</p>
                    
                    <br /><br />
                    <!-- the comment box -->
                    <div class="well">
                        <h4>Leave a Comment:</h4>
                        <form role="form">
                            <div class="form-group">
                                <textarea class="form-control" rows="3"></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                    <br /><br />
                    
                    <!-- the comments -->
                    <!--            <h3>Comment 01
                                    <small>9:41 PM on August 24, 2013</small>
                                </h3>
                                <p>This has to be the worst blog post I have ever read. It simply makes no sense. You start off by talking about space or something, then you randomly start babbling about cupcakes, and you end off with random fish names.</p>-->

                </div>

                <div class="col-lg-4 hidden-xs">
                    <br /><br /><br /><br />
                    <div class="alert alert-info">
                        <h4>Blog Search</h4>
                        <div class="input-group">
                            <input type="text" class="form-control">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <span class="glyphicon glyphicon-search"></span>
                                </button>
                            </span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div id="fb-root"></div>
                            <script>(function(d, s, id) {
                                    var js, fjs = d.getElementsByTagName(s)[0];
                                    if (d.getElementById(id))
                                        return;
                                    js = d.createElement(s);
                                    js.id = id;
                                    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
                                    fjs.parentNode.insertBefore(js, fjs);
                                }(document, 'script', 'facebook-jssdk'));</script>

                            <div class="fb-like-box" data-href="https://www.facebook.com/nsbmDevOps" data-width="360px" data-height="500px" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="true" data-show-border="true"></div>
                        </div>
                    </div>
                    <br /><br />
                    <div class="row">
                        <div class=" alert alert-info center-block" style="border-radius: 0; margin: auto 15px">
                            <a class="twitter-timeline" href="https://twitter.com/NSBMDevOps" data-widget-id="487955245624881152">Tweets by @NSBMDevOps</a>
                            <script>
                                !function(d, s, id) {
                                    var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
                                    if (!d.getElementById(id)) {
                                        js = d.createElement(s);
                                        js.id = id;
                                        js.src = p + "://platform.twitter.com/widgets.js";
                                        fjs.parentNode.insertBefore(js, fjs);
                                    }
                                }(document, "script", "twitter-wjs");
                            </script>
                        </div>
                    </div>
                </div>
            </div>

            <footer>
                <div class="row">
                    <div class="col-lg-12">
                        <p>Copyright &copy; DevOps 2013</p>
                    </div>
                </div>
            </footer>

        </div>
    </div>