<div id="wrapper">
<div class="container">
    <div class="row">
        <div class="col-md-12 well" style="margin:20px auto;">
            <!--Edit Main Content Area here-->
            <div class="row">
                <div class="col-md-8">
                <h1 class="text-info">About Us</h1>
                <p class="text-justify"><strong class="text-error">Content on this page is for presentation purposes only.</strong>  Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                    Curabitur ac tortor elit, non hendrerit felis. Nam adipiscing gravida magna, ac pretium neque volutpat a. Integer gravida 
                    lorem lorem. Nunc tempor, eros in sagittis iaculis, ligula mi semper neque, ut mattis dui nulla at quam. Suspendisse eleifend 
                    nulla blandit lorem venenatis sed elementum massa commodo. Morbi semper rhoncus lacus, vitae accumsan nibh hendrerit semper. 
                    Suspendisse quis nisl mauris, eget varius leo. Vestibulum auctor lectus vel tellus semper molestie tempus nibh tincidunt. 
                    Nunc posuere consequat consequat. Aenean ullamcorper pharetra libero sed suscipit. Fusce sit amet gravida neque. 
                    Cras nisi sem, eleifend at adipiscing ornare, dictum eu risus. Praesent non erat enim.
                </p>
                </div>
                <div class="col-md-4">
                    <img style="margin:0; width: 70%" src="<?php echo base_url(); ?>assets_events/img/nsbmbuilding1.jpg" class="img-responsive pull-right hidden-xs">
                </div>
            </div>
        </div>
    </div>
    
    <div class="row" style="margin:20px auto;">
        <h2 class="text-info text-center">Connect with NSBM Network</h2>
        <hr />
        <div class="col-md-3">					
            <div class="box" style="text-align: center">						
                <center><img width="84px" height="84px" src="<?php echo base_url(); ?>assets_events/img/profile/user.png" class="img-responsive"></center>
                <h3 class="title">My-NSBM</h3> <hr/>
                <p>My NSBM is a place where all of you will be able to access NSBM LMS, NSBM EMS and NSBM Forum. From My NSBM you can create the University Profile where all of your information is saved. This feature will help outside personals to gain your educational ...</p>
                <p><a class="btn btn-info" style="margin:5px 0px 15px;">Learn more</a></p>				
            </div>						
        </div>
        
        <div class="col-md-3">					
            <div class="box" style="text-align: center">						
                <center><img width="81px" height="82px" src="<?php echo base_url(); ?>logos/lms.png" class="img-responsive"></center>
                <h3 class="title">L &middot; M &middot; S</h3> <hr/>
                <p>NSBM learning management system . This provides self learning facilities to all the students in 
                    NSBM. All of the assignments, projects and important news events will be displayed here.
                    Academic staff members will facilitate you through this system 24/7.</p>
                <p><a class="btn btn-info" style="margin:5px 0px 15px;">Learn more</a></p>				
            </div>						
        </div>
        
        <div class="col-md-3">					
            <div class="box" style="text-align: center">						
                <center><img style="margin-top: 44px" width="150px" height="" src="logos/lgdevops.png" class="img-responsive"></center>
                <h3 class="title">DevOps</h3> <hr/>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus, sapien nec dignissim porta.
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus, sapien nec dignissim porta.
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                <p><a class="btn btn-info" style="margin:5px 0px 15px;">Learn more</a></p>				
            </div>						
        </div>
        
        <div class="col-md-3">					
            <div class="box" style="text-align: center">						
                <center><img width="84px" height="84px" src="<?php echo base_url(); ?>assets_events/img/profile/user.png" class="img-responsive"></center>
                <h3 class="title">Forum</h3> <hr/>
                <p>NSBM Forum is a new feature that we are going to introduce the network. This is to 
                    increase the communication between Nsbimian. From this forum students and lecturers will reap many uses
                    Our developing team will hope to launch the NSBM forum soon enough.</p>
                <p><a class="btn btn-info" style="margin:5px 0px 15px;">Learn more</a></p>				
            </div>						
        </div>

    </div>
    
    <div class="row well" >
        <div class="col-md-2 col-sm-2 col-xs-2">
            <a href="http://www.youthskillsmin.gov.lk/" target="_blank">
                <center><img style="height: 100px; width: 70px" src="logos/gov.png" class="img-responsive"></center>
            </a>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-2">
            <a href="http://www.ugc.ac.lk" target="_blank">
                <center><img style="margin-top: 5px" src="logos/ugc.png" class="img-responsive"></center>
            </a>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-2">
            <a href="http://www.nibm.lk" target="_blank">
                <center><img style="margin-top: 20px" src="logos/nibm.png" class="img-responsive"></center>
            </a>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-2">
            <a href="http://www.d2real.com" target="_blank">
                <center><img style="margin-top: 15px" src="logos/lgnsbm.png" class="img-responsive"></center>
            </a>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-2">
            <a href="http://www.ucd.ie" target="_blank">
                <center><img style="margin-top: 5px" src="logos/ucd.png" class="img-responsive"></center>
            </a>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-2">
            <a href="http://www5.plymouth.ac.uk" target="_blank">
                <center><img style="margin-top: 20px" src="logos/plymouth.png" class="img-responsive"></center>
            </a>
        </div>
    </div>
</div>
</div>