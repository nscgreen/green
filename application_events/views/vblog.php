<body>
    <div id="wrapper">
        <br />
        <div class="container img-rounded" style="background-color: #EBEBEB;" >
            <div class="row">
                <div class="col-lg-8">
                    <div class="page-header text-center">
                        <h1 id="timeline" class="text-info">NSBM Events Blog</h1>
                    </div>
                    <!-- blog entry -->
                    <h2 class="font-lustria"><a href="#" class=" text-success">The Apprentice</a></h2>
                    <h4 class="lead"><span class="text-muted">by</span> Chamodh hettiarachchi. <small>(13.1 computer science batch)</small></h4>
                    <p style="margin-top: -20px"><small><span class="glyphicon glyphicon-time"></span> Posted on April 01, 2014 at 8:00 PM</small></p>
                    <img src="../../uploads_events/events/2/apprentice.jpg" class="img-responsive" style="width: 900px; height:300px;">
                    <br />
                    <p class="text-justify">“YOU ARE FIRED” the world famous quote said by the Mr. Donald Trump (Chairman of The Trump organization) in one of the BBC`s reality television shows is a good example to show how the majority of interns end their careers ....</p>
                    <a class="btn btn-primary" href="<?php echo base_url(); ?>blog/page">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>
                    <hr />

                    <!-- second blog entry -->
                    <h2 class="font-lustria"><a href="#" class=" text-success">Future of software development with the “Agile” </a></h2>
                    <h4 class="lead"><span class="text-muted">by</span> Chamodh hettiarachchi. <small>(13.1 computer science batch)</small></h4>
                    <p style="margin-top: -20px"><small><span class="glyphicon glyphicon-time"></span> Posted on March 28, 2014 at 10:00 PM</small></p>
                    <img src="../../uploads_events/events/1/agile.jpg" class="img-responsive" style="width: 900px; height:300px;">
                    <br />
                    <p class="text-justify">The workshop “Towards Agile” organized by the “DevOpe” team of national school of business management in collaborating with “<a href="#">99X technology</a>” was held in the NSBM auditorium on 6th of March. Agile software development methodology as one of the most used developing concepts in modern world and ....</p>
                    <a class="btn btn-primary" href="<?php echo base_url(); ?>blog/page">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>
                    <hr />

                    <!-- pager -->
                    <ul class="pager">
                        <li class="previous"><a href="#">&larr; Previous</a>
                        </li>
                        <li class="next"><a href="#">Next &rarr;</a>
                        </li>
                    </ul>
                </div>
                
                <div class="col-lg-4 hidden-xs">
                    <br /><br /><br /><br />
                    <div class="alert alert-info">
                        <h4>Blog Search</h4>
                        <div class="input-group">
                            <input type="text" class="form-control">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <span class="glyphicon glyphicon-search"></span>
                                </button>
                            </span>
                        </div>
                    </div>

<!--                    <div class="well">
                        <h4>Popular Blog Categories</h4>
                        <div class="row">
                            <div class="col-lg-6">
                                <ul class="list-unstyled">
                                    <li><a href="#dinosaurs">Dinosaurs</a>
                                    </li>
                                    <li><a href="#spaceships">Spaceships</a>
                                    </li>
                                    <li><a href="#fried-foods">Fried Foods</a>
                                    </li>
                                    <li><a href="#wild-animals">Wild Animals</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-lg-6">
                                <ul class="list-unstyled">
                                    <li><a href="#alien-abductions">Alien Abductions</a>
                                    </li>
                                    <li><a href="#business-casual">Business Casual</a>
                                    </li>
                                    <li><a href="#robots">Robots</a>
                                    </li>
                                    <li><a href="#fireworks">Fireworks</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>-->
                    <div class="row">
                        <div class="col-sm-12">
                            <div id="fb-root"></div>
                            <script>(function(d, s, id) {
                                    var js, fjs = d.getElementsByTagName(s)[0];
                                    if (d.getElementById(id))
                                        return;
                                    js = d.createElement(s);
                                    js.id = id;
                                    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
                                    fjs.parentNode.insertBefore(js, fjs);
                                }(document, 'script', 'facebook-jssdk'));</script>

                            <div class="fb-like-box" data-href="https://www.facebook.com/nsbmDevOps" data-width="360px" data-height="500px" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="true" data-show-border="true"></div>
                        </div>
                    </div>
                    <br /><br />
                    <div class="row">
                        <div class=" alert alert-info center-block" style="border-radius: 0; margin: auto 15px">
                            <a class="twitter-timeline" href="https://twitter.com/NSBMDevOps" data-widget-id="487955245624881152">Tweets by @NSBMDevOps</a>
                            <script>
                                !function(d, s, id) {
                                    var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
                                    if (!d.getElementById(id)) {
                                        js = d.createElement(s);
                                        js.id = id;
                                        js.src = p + "://platform.twitter.com/widgets.js";
                                        fjs.parentNode.insertBefore(js, fjs);
                                    }
                                }(document, "script", "twitter-wjs");
                            </script>
                        </div>
                    </div>
                </div>
            </div>

            <footer>
                <div class="row">
                    <div class="col-lg-12">
                        <p>Copyright &copy; DevOps 2014</p>
                    </div>
                </div>
            </footer>
        </div>    
    </div>

<!--<script src="//embed.flowplayer.org/5.4.3/embed.min.js"><div class="flowplayer" data-origin="https://course.bighistoryproject.com/bhplive?WT.mc_id=03_26_2014_BG-Join_fb&amp;WT.tsrc=Facebook" data-key="$661193221874914,$468743015507892" style="width: 640px; height: 360px;"><video><source type="video/mp4" src="//////"><source type="video/ogg" src="https://course.bighistoryproject.com/media/bhp3video/Bh_Anthem.ogv"></video></div></script>-->