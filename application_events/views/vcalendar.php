<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Apr 20, 2014, 11:57:24 PM
 */
?>
<script>
    $('#cal').load('http://localhost/eventmagtsys/calendar/displaycal/');
</script>
<!--<body onload="loadwrap('#cal','http://localhost/eventmagtsys/calendar/displaycal/');">-->
<div class="container">
    <div class="page-header text-center">
        <h1 id="timeline">Events Calendar @ NSBM</h1>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <div id="cal" class="datepicker-days table-responsive">
            </div>
        </div>
    </div>
</div>
