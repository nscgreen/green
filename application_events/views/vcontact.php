<body>
    <br />
    <div id="wrapper">
        <div class="container" style="padding: 0">
        <div class="jumbotron cus-jumbo1">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <h1>
                            Contact us <small>Feel free to contact us</small></h1>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-7 jumbotron" >
                    <div class="row jumbotron" style="padding: 0px;">
                        <div class="col-md-5 col-sm-5">
                            <br /><br /><br />
                            <img src="<?php echo base_url(); ?>assets_events/img/lgems.png" img class="img-responsive">                   
                        </div>
                        <div class="col-md-7 col-sm-7">
                            <h2 class="text-info">NSBM Event Managment</h2>
                            <p class="text-justify">Interested about coding or thinking to expand you knowledge about coding? Then you have all the qualifications to join with us. Just fill and send the below form to join with us. If you need further details about the registering process or any other problem that need to be clarified by us please feel free to contact us at any time.</p>
                        </div>

                    </div>

                    <div class="row hidden-xs">
                        <div class="col-md-12 col-sm-12">
                            <div id="wrap">
                                <!--<h1>Send a message</h1>-->
                                <div id='form_wrap'>
                                    <form class="cus_form1">
                                        <p class="cus_p1">Hello User..</p>
                                        <!--<p class="cus_p1">Best,</p>-->	
                                        <label class="cus_label1" for="name">Name: </label>
                                        <input class="cus_texta1" type="text" name="name" value="" id="name" />
                                        <label class="cus_label1" for="name">NSBM Student or not: </label>
                                        <select class="cus_texta1">
                                            <option>No</option>
                                            <option>Yes</option>
                                        </select>
                                        <label class="cus_label1" for="email">If NSBM student enter batch ID: </label>
                                        <input class="cus_texta1" type="text" name="email" value="" id="email" />
                                        <label class="cus_label1" for="email">Email: </label>
                                        <input class="cus_texta1" type="text" name="email" value="" id="email" />
                                        <label class="cus_label1" for="email">Telephone: </label>
                                        <input class="cus_texta1" type="text" name="email" value="" id="email" />
                                        <label class="cus_label1" for="email">Your massage or problem in brief: </label>
                                        <textarea class="cus_texta1 cus_texta2" name="message" value="Your Message" id="message" ></textarea>
                                        <input class="cus_submit1" type="submit" name ="submit" value="Send, thanks!" />
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="row jumbotron visible-xs">
                        <div class="col-md-12 col-sm-12">
                            <div class="well well-sm">
                                <form>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label for="name">
                                                    Name</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span>
                                                    </span>
                                                    <input type="text" class="form-control" id="name" placeholder="Enter name" required="required" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="name">NSBM Student or not</label>
                                                <select class="form-control">
                                                    <option>No</option>
                                                    <option>Yes</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="name">
                                                    If NSBM student enter batch ID</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-bookmark"></span>
                                                    </span>
                                                    <input type="text" class="form-control" id="name" placeholder="Enter batch id" required="required" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="email">
                                                    Email Address</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span>
                                                    </span>
                                                    <input type="email" class="form-control" id="email" placeholder="Enter email" required="required" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="email">
                                                    Telephone</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-earphone"></span>
                                                    </span>
                                                    <input type="email" class="form-control" id="email" placeholder="Enter phone number" required="required" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label for="name">
                                                    Your massage or problem in brief</label>
                                                <textarea name="message" id="message" class="form-control" rows="9" cols="25" required="required"
                                                          placeholder="Message"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <button type="submit" class="btn btn-info pull-right" id="btnContactUs">Send Message</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-md-4 col-sm-4 col-md-offset-1 col-sm-offset-1">
                    <div class="row">
                        <form style="margin: 15px;">
                            <legend><span class="glyphicon glyphicon-envelope"></span>    Address</legend>
                            <address>
                                <strong>NSBM,</strong><br>
                                No.105,<br>
                                Highlevel Road,<br>
                                Colombo 05.<br>
                                <abbr title="Phone">
                                    Dial:</abbr>
                                (123) 456-7890
                            </address>
                            <address>
                                <strong>Web Address</strong><br>
                                <a href="http://www.d2real.com" targety="_blank">www.d2real.com</a>
                            </address>
                        </form>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-10 col-xs-10">
                            <form>
                                <legend><span class="glyphicon glyphicon-heart"></span> Keep in Touch with us..</legend>
                                <address>
                                    <i class="fa fa-facebook"><a href="http://www.d2real.com" targety="_blank"> &nbsp;&nbsp;Like us on Facebook and join the conversation</a></i><br />
                                    <i class="fa fa-twitter"><a href="http://www.d2real.com" targety="_blank"> Follow us on Twitter</a></i><br />
                                    <i class="fa fa-google-plus"><a href="http://www.d2real.com" targety="_blank"> Connect us on Google+</a></i><br />
                                </address>
                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-10 col-xs-10">
                            <form>
                                <legend><span class="glyphicon glyphicon-globe"></span> We are here..</legend>
                                <iframe width="100%" height="400px" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" 
                                        src="https://maps.google.co.uk/maps?f=q&source=s_q&hl=en&geocode=&q=15+Springfield+Way,+Hythe,+CT21+5SH&aq=t&sll=52.8382,-2.327815&sspn=8.047465,13.666992&ie=UTF8&hq=&hnear=15+Springfield+Way,+Hythe+CT21+5SH,+United+Kingdom&t=m&z=14&ll=51.077429,1.121722&output=embed">
                                </iframe>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>