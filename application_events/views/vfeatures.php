<body>
    <div id="wrapper" style="margin-bottom: -30px">
        <!-- Section #1 -->
        <section id="section1" data-speed="0" data-type="background">
            <div class="container">
                <div class="raw">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <h1 id="responsive_headline" style="color: #009999">Welcome To....</h1>
                        <h2 id="responsive_headline" style="color: #0066cc">Event Management System of NSBM</h2>
                        <p style="text-align: justify">Interested in joining with us? Then down and see the opportunities that are available for you. If you have any problems please feel free to <a href="<?php echo base_url(); ?>contact">contact</a> us any time.</p>
                        <br /><br />
                    </div>
                </div>

                <div class="row-fluid">
                    <center>
                        <a class="down">
                            <div class="hidden-xs">
                                <button  type="button" href="#home" class="btn btn-default btn-circle btn-xl" style="opacity: 0.4; border-color: background;">
                                    <img src="assets_events/img/features/down.ico">
                                </button>
                            </div>
                        </a>
                    </center>
                </div>
                <br /><br />
            </div>
        </section>

        <!-- Section #2 -->
        <section id="section2" data-speed="4" data-type="background">
            <div class="container">
                <div class="row">
                    <article class="col-md-5 col-sm-5 col-xs-10 col-xs-offset-1">
                        <h2 id="responsive_headline" class="h2-fea">Organizer</h2>
                        <blockquote class="blockquote-reverse">
                            <p class="pquote-fea">“Out of clutter, find simplicity. From discord, find harmony. In the middle of difficulty lies opportunity.”</p>
                            <footer>- Albert Einstein.</footer>
                        </blockquote>
                        <p id="responsive_headline">If you think you have what it takes to be a good organizer this is your chance. Join with us an event organizer to help us with our organizing processes.  Currently this opportunity is open to NSBM students as well as to students from the other universities. This will definitely look good and become an extra advantage on your curriculum vitiates.</p>
                    </article>
                    <div class="col-md-5 col-md-offset-1 col-sm-5 col-sm-offset-1 col-xs-10 col-xs-offset-1">
                        <img src="assets_events/img/features/or.png" class="img-responsive">
                    </div>
                </div>

                <div class="row-fluid">
                    <center>
                        <a class="down1">
                            <div class="hidden-xs">
                                <button  type="button" href="#home" class="btn btn-default btn-circle btn-xl" style="opacity: 0.4;">
                                    <img src="assets_events/img/features/down.ico">
                                </button>
                            </div>
                        </a>
                    </center>
                </div>
            </div>
        </section>

        <!-- Section #3 -->
        <section id="section3" data-speed="4" data-type="background">
            <div class="container">
                <div class="row">
                    <div class="col-md-5 col-sm-5 col-xs-10 col-xs-offset-1">
                        <img src="assets_events/img/features/st.png" class="img-responsive">
                    </div>
                    <article class="col-md-5 col-md-offset-1 col-sm-5 col-sm-offset-1 col-xs-10 col-xs-offset-1">
                        <h2 id="responsive_headline" class="h2-fea" style="color: #ff3333">Student</h2>
                        <blockquote class="blockquote-reverse">
                            <p class="pquote-fea">“To those of you who received awards and distinctions, I say well done. And to the ‘C’ students, I say you, too, can be president of the United States.”</p>
                            <footer>- George W. Bush.</footer>
                        </blockquote>
                        <p id="responsive_headline">Anyone can learn to code it simple as that. No matter about your knowledge in programming, our team will help you to learn from the basics. If you think you have the energy to code until you get a program without any errors that all it takes, join with us today so we could assist you through our projects and to help you to learn in order to become a good programmer to suite the industry level. Currently this category is opened only for NSBM students.</p>
                    </article>
                </div>

                <div class="row-fluid">
                    <center>
                        <a class="down2">
                            <div class="hidden-xs">
                                <button  type="button" href="#home" class="btn btn-default btn-circle btn-xl" style="opacity: 0.4;">
                                    <img src="assets_events/img/features/down.ico">
                                </button>
                            </div>
                        </a>
                    </center>
                </div>
            </div>
        </section>

        <!-- Section #4 -->
        <section id="section5" data-speed="4" data-type="background">
            <div class="container">
                <div class="row">
                    <article class="col-md-5 col-sm-5 col-xs-10 col-xs-offset-1">
                        <h2 id="responsive_headline" class="h2-fea">Users</h2>
                        <p id="responsive_headline">If you share a passion for coding like we do, spending hours to get a program without errors that is the spirit we seek. Join with us and see what are the projects and events we have done and what we are willing to do in future. By registering as normal user will send you newsletter of our upcoming workshops and keep you updated about us. This is an open category to anyone interested in coding.</p>
                    </article>
                    <div class="col-md-5 col-md-offset-1 col-sm-5 col-sm-offset-1 col-xs-10 col-xs-offset-1">
                        <img src="assets_events/img/features/us.png" class="img-responsive">
                    </div>
                </div>
                <br /><br /><br /><br /><br /><br /><br />
                <div class="row-fluid">
                    <center>
                        <a class="scroll-top">
                            <div class="hidden-xs">
                                <button type="button"  href="#about" class="btn btn-default btn-circle btn-xl" button type="button" href="#home" class="btn btn-default btn-circle btn-xl" style="opacity: 0.4;"> 
                                    <img src="assets_events/img/features/up.ico">
                                </button>
                            </div>
                        </a>
                    </center>
                </div>
                <br />
            </div>
        </section>

    </div>
<!-- Section #5 -->
<!--<section id="section5" data-speed="4" data-type="background">
    <div class="container">
        
        <div class="row">
            <div class="col-md-6 col-md-offset-0 col-sm-6 col-sm-offset-0 col-xs-8 col-xs-offset-2">
                <img src="<?php //echo base_url(); ?>assets/img/features/1.png" class="img-responsive">
            </div>
            <article class="col-md-6 col-md-offset-0 col-sm-6 col-sm-offset-0 col-xs-10 col-xs-offset-1 jumbotron">
                <h2 id="responsive_headline">Services</h2>
                <p id="responsive_headline">Wisdom Pet Medicine is a state-of-the-art veterinary hospital and a staff   of seasoned veterinary specialists in the areas of general veterinary   medicine and surgery, oncology, dermatology, orthopedics, radiology,   ultrasound, and much more. We also have a 24-hour emergency clinic in   the event your pet needs urgent medical care after regular business hours.<a href="#">[<font color="red">More</font>]</a></p>
            </article>
        </div>

        <div class="row-fluid">
            <center>
                <a class="scroll-top">
                    <div class="hidden-xs">
                        <button type="button"  href="#about" class="btn btn-default btn-circle btn-xl" button type="button" href="#home" class="btn btn-default btn-circle btn-xl" style="opacity: 0.4;"> 
                            <img src="<?php //echo base_url(); ?>assets/img/features/up.ico">
                        </button>
                    </div>
                </a>
            </center>
        </div>
        <br />
    </div>
</section>-->