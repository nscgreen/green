<body onload="loadwrap('#cal', 'http://events.d2real.com/calendar/displaycal/');">
    <div id="wrapper">
        <br />
        <div class="container">
            <div class="row">
                <div class="col-md-6 jumbotron" style="padding: 20px;">
                    <div class="row">
                        <div class="col-md-5 col-sm-5 col-xs-5">
                            <br /><br /><br />
                            <img src="<?php echo base_url(); ?>assets_events/img/emslogo.png" class="img-responsive">                         
                        </div>
                        <div class="col-md-7 col-sm-7 col-xs-7 animated bounceIn">
                            <h1 id="responsive_headline" style="color: #006dcc;">NSBM</h1>
                            <h1 id="" class="animated bounceInUps" style="color: #22b757;">Event Managment System</h1>                   
                        </div>    
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <img src="logos/lgnsbm.png" class="img-responsive">  
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <br />
                            <img src="logos/lgdevops.png" class="img-responsive">
                        </div>
                        <div class="col-md-4">
                            <span></span>
                        </div>
                    </div>
                    
                    <br />
                    <div class="row ">
                        <div class="col-md-12">
                            <p align="justify" class="">Living in a society where things are getting computerized in each and every second, knowing how to code is becoming an essential fact. As the NSBM <a href="">DevOps</a> we are a group and a community that is joined together to sharpen the programming skills. So if you too share the passion for coding like we do join with us to master programming skills to the industry’s required level.
                                <br /><br />Coding isn’t just about writing a computer program, it’s about writing program for millions of humans in order to make their busy lives less complex and easier by helping to solve their problems by a single click.
                                <br />
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-4 col-xs-5">
                            <h1 id="responsive_headline" style="color: #cc00cc">Featured</h1>
                        </div>
                        <div class="col-md-6 col-sm-8 col-xs-7">
                            <br />
                            <a href="<?php echo base_url(); ?>features">
                                <button type="button" class="btn btn-default btn-circle btn-xl"><img class="img-responsive" src="assets_events/img/home/2.ico"></button>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="row">
                        <div id="loginbox" style="margin-top:0px;" class="mainbox col-md-9 col-md-offset-1 col-sm-10 col-sm-offset-2">                    
                            <div class="panel panel-info" style="background-color: #d8ecf4">
                                <div class="panel-heading">
                                    <div class="panel-title"><p>Sign In</p></div>
                                    <div style="float:right; font-size: 85%; position: relative; top:-10px"><a href="#">Forgot password ?</a></div>
                                </div>     

                                <div style="padding-top:30px" class="panel-body">

                                    <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>

                                    <form method="post" action="http://connect.d2real.com" id="loginform" name="submit_form" class="form-horizontal form-signin" role="form" >
                                        <div style="margin-bottom: 25px" class="input-group">
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                            <input id="login-username" type="text" class="form-control" name="login_username" value="" placeholder="username or email">                                        
                                        </div>
                                        <div style="margin-bottom: 25px" class="input-group">
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                            <input id="login-password" type="password" class="form-control" name="login_password" placeholder="password">
                                        </div>
                                        <div style="margin:10px" class="form-group">
                                            <!-- Button -->
                                            <div class="col-sm-12 controls">
                                                <input id="btn-login" type="submit" class="btn-lg btn-success btn-group" value="Login" >
                                                <!--<input id="btn-fblogin" type="button" class="btn btn-primary btn-group" value="Login with Facebook" >-->
                                                <a id="btn-fblogin" href="http://connect.d2real.com"" class="btn-lg btn-info btn-group">Sign Up</a> 
                                            </div>
                                        </div>

                                    <!--<div class="form-group">
                                        <div class="col-md-12 control">
                                            <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%" >
                                                Don't have an account! 
                                                <a href="<?php echo base_url(); ?>signup">
                                                    <p style=" display: inline; font-size: 16px;">Sign Up Here</p>
                                                </a>
                                            </div>
                                        </div>
                                    </div>-->
                                    </form>
                                </div>                     
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <script>
                            function calajax(url) {
                                $('#cal').load(url);
                            }
                        </script>
                        <div class="col-md-11 col-sm-11 col-md-offset-1 col-sm-offset-1">
                            <h2 style="color: #666666">Event Calander</h2>
                            <p align="justify">See what we have done and what is coming up for more details keep in touch with us..!</p>
                            <br />
                            <div id="cal" class="datepicker-days"></div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="row">
                <div class="well wellhome" style="background-color: #3ae7ec">
                    <blockquote class="blockquote-reverse">
                        <p class="pquote">“ Most good programmers do programming not because they expect to get paid or get adulation by the public, but because it is fun to program. ”</p>
                        <footer>- Linus Torvalds<cite title="Source Title">(software engineer, developed the Linux kernel)</cite></footer>
                    </blockquote>
                </div>
            </div>
        </div>
    </div>