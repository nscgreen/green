<div id="main_menu" class="menu_container">	
    <div class="main clearfix">
        <nav id="menu" class="menu_nav">					
            <ul>
                <li>
                    <a href="<?php echo base_url();?>">
                        <span class="icon">
                            <i aria-hidden="true" class="icon-home"></i>
                        </span>
                        <span>Home</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>features">
                        <span class="icon"> 
                            <i aria-hidden="true" class="icon-services"></i>
                        </span>
                        <span>Features</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="icon">
                            <i aria-hidden="true" class="icon-portfolio"></i>
                        </span>
                        <span>Events</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>blog">
                        <span class="icon">
                            <i aria-hidden="true" class="icon-blog"></i>
                        </span>
                        <span>Blog</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>about">
                        <span class="icon">
                            <i aria-hidden="true" class="icon-team"></i>
                        </span>
                        <span>About</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>contact">
                        <span class="icon">
                            <i aria-hidden="true" class="icon-contact"></i>
                        </span>
                        <span>Contact</span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</div><!-- /container -->