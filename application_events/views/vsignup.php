<div id="wrapper">
<div class="container cus-container">
    <div class="row" >
        
        <div class="col-md-5 col-sm-5 col-xm-12">
            <h1 id="responsive_headline">Fill the form to Register</h1>
            <p style="text-align: justify;"><small>The Inception Convocation Ceremony of the National School of Business Management was held on the 29th January 2014 at BMICH, Main Assembly Hall under the distinguished patronage of His Excellency Mahinda Rajapaksa, President of the Democratic Socialist Republic of Sri Lanka on the invitation of Hon. Dullas Alahapperuma, Minister of Youth Affairs & Skills Developme</small></p>
            <!--<div class="row">-->
            <div class="col-sm-8 col-md-8 signupWrapper">
                
                <form method="post" action="<?php echo base_url(); ?>signup/create_member" class="form-horizontal" role="form" name="submit_form" onsubmit="return validateSignUpForm();" enctype="multipart/form-data" > 
                    <div class="formgroup">
                        <label class="form-label" for="fname">First Name:</label>
                        <input  id="fname" name="fname" class="form-control" type="text" size="50"  onkeyup="onchangefname()" placeholder="First Name" />
                        <span id="fnamemsg" class="no-display ">Enter the First Name</span>
                    </div>

                    <div class="formgroup Apform">
                        <label class="form-label" for="lname">Last Name:</label>
                        <input id="lname" name="lname" class="form-control" type="text" size="50"  onkeyup="onchangelname()" placeholder="Last Name" />
                        <span id="lnamemsg" class="no-display ">Enter the Last Name</span>
                    </div>

                    <div class="formgroup Apform">
                        <label class="form-label" for="email">Email:</label>
                        <input id="email" name="email" class="form-control" type="text" size="50"  onkeyup="onchangeemail()" placeholder="Email" />
                        <span id="emailmsg" class="no-display ">Enter Email correctly</span>
                    </div>

                    <div class="formgroup Apform">
                        <label class="form-label" for="emailcon">Confirm Email:</label>
                        <input id="emailcon" name="emailconfirme" class="form-control" type="text" size="50"  onkeyup="onchangeemailcon()" placeholder="Confirme Email" />
                        <span id="emailconmsg" class="no-display ">Email not matched</span>
                    </div>

                    <div class="formgroup Apform">
                        <label class="form-label" for="password">Password:</label>
                        <input id="password" name="password" class="form-control" type="password" size="50"  onkeyup="onchangepass()" placeholder="Password" />
                        <span id="passmsg" class="no-display ">Enter the Password</span>
                    </div>

                    <div class="formgroup Apform">
                        <label class="form-label" for="passwordcon">Confirm Password:</label>
                        <input id="passwordcon" name="passwordcon" class="form-control" type="password" size="50"  onkeyup="onchangepasscon()" placeholder="Re-enter Password" />
                        <span id="passconmsg" class="no-display ">Password not Matched</span>
                    </div>

<!--                    <div class="formgroup Apform">
                        <label class="form-label" for="Phone number">Phone:</label>
                        <input id="phone" name="phone" class="form-control" type="text" size="50"  onkeyup="onchangephone()" placeholder="Enter the Phone Number" />
                        <span id="phonemsg" class="no-display ">Enter the Number correctly</span>
                    </div>-->
                    
                                    <!-- ================================================  -->
                    <!-- have to validate from here -->
                    <div class="formgroup Apform">
                        <label class="form-label" for="indexNo">Index Number:</label>
                        <input id="indexNo" name="indexNo" class="form-control" type="text" size="50"  placeholder="Your Index Number" />
                        <!-- <span id="phonemsg" class="no-display ">Enter the Number correctly</span> -->
                    </div>
                    
                    <!-- have to validate from here -->
<!--                    <div class="formgroup Apform">
                        <label class="form-label" for="batch">Batch:</label>
                        <input id="batch" name="batch" class="form-control" type="text" size="50"  placeholder="Enter the Batch name" />
                         <span id="phonemsg" class="no-display ">Enter the Number correctly</span> 
                    </div>-->
                    
                    <!-- have to validate from here -->
<!--                    <div class="formgroup Apform">
                        <label class="form-label" for="uni">University:</label>
                        <input id="uni" name="uni" class="form-control" type="text" size="50"  placeholder="Enter the Uni name" />
                         <span id="phonemsg" class="no-display ">Enter the Number correctly</span> 
                    </div>-->
                    
                    <!-- have to validate from here -->
<!--                    <div class="formgroup Apform">
                        <label class="form-label" for="address">Address:</label>
                        <input id="address" name="address" class="form-control" type="text" size="50"  placeholder="Address" />
                         <span id="phonemsg" class="no-display ">Enter the Number correctly</span> 
                    </div>-->
                    
                    
                    
                    <!-- Upload pic -->
<!--                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-new thumbnail" style="width: 200px; height: 200px;">
                            <img data-src="holder.js/100%x100%" src="http://placehold.it/900x300" alt="image1">
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                        <div>
                            <span class="btn btn-default btn-file">
                                <span class="fileinput-new">Select image</span>
                                <span class="fileinput-exists">Change</span>
                                <input type="file" name="pro_img">
                            </span>
                            <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                        </div>
                    </div>-->
                    <!-- Upload pic -->
                    
                    <br /><br />
                    <center><input class="btn btn-info" type="submit" value="Sign Up" name="signup" id="signup"/></center>
                    <br /><br />
                    <center><input class="btn btn-default" type="reset" value="Reset" name="signup_reset" id="reset" onclick="signupreset()"/></center>
                </form>
                 <br /><br />
            </div>

        
        </div>
        
        <div class="col-md-3 col-sm-3 hidden-xs">
            <h1 id="responsive_headline"> Past Events</h1>
            <p><small>The Inception Convocation Ceremony of the National School of Business Management was held on the 29th January 2014 at BMICH Business Management was held on the 29th January 2014 at BMICH</small></p>
            <div class="row">
                <div class="thumbnail">
                    <img src="<?php echo base_url(); ?>assets/img/1.jpg" alt="...">
                    <div class="caption">
                        <h3>Event Name</h3>
                        <p>Date :</p>
                        <p>Time :</p>
                        <p>Venue :</p>
                        <p><a href="#" class="btn btn-primary" role="button">View More</a></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="thumbnail">
                    <img src="<?php echo base_url(); ?>assets/img/1.jpg" alt="...">
                    <div class="caption">
                        <h3>Event Name</h3>
                        <p>Date :</p>
                        <p>Time :</p>
                        <p>Venue :</p>
                        <p><a href="#" class="btn btn-primary" role="button">View More</a></p>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-md-3 col-sm-3 hidden-xs col-md-offset-1 col-sm-offset-1">
            <h1 id="responsive_headline" style="color: #ff0099"> Upcoming Events</h1>
            <p><small>The Inception Convocation Ceremony of the National School of Business Management was held on the 29th January 2014 at BMICH Business Management was held on the 29th January 2014 at BMICH</small></p>
            
            <!--    Loading Upcoming Events     -->
            <?php
            foreach ($upcoming_events as $row){
            ?>
            <div class="row">
                <div class="thumbnail">
                    <img src="<?php echo base_url().$row->img1_path; ?>" alt="...">
                    <div class="caption" style="background: #33ccff">
                        <h3><?php echo $row->title ?></h3>
                        <p>Date : <?php echo $row->date ?></p>
                        <p>Time : <?php echo $row->time ?></p>
                        <p>Venue : <?php echo $row->venue ?></p>
                        <p><a href="<?php echo base_url().'event/view/'.$row->event_id ?>" class="btn btn-primary" role="button">View More</a></p>
                    </div>
                </div>
            </div>
            <?php
            } 
            ?>
            <!--    End Loading Upcoming Events     -->
            
            
        </div>
        
    </div>
</div>
    
</div>