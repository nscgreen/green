
<!-- JavaScript -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script> <!-- Boostrap JS -->

<!--script src="https://code.jquery.com/jquery.js"></script>-->
    <script src="<?php echo base_url(); ?>assets/js/parallax.js"></script>  To Parallax Designs 
    <script src="<?php echo base_url(); ?>assets/js/site.js"></script>  To whole site custom JS 

<!-- Vhome JS -->
<script src="<?php echo base_url(); ?>assets/js/vhome/bootshape.js"></script>
<script src="<?php echo base_url(); ?>assets/js/vhome/jcarousel.responsive.js"></script>
<script src="<?php echo base_url(); ?>assets/js/vhome/jquery-loader.js"></script>
<script src="<?php echo base_url(); ?>assets/js/vhome/jquery.jcarousel.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/vhome/jquery.onepage-scroll.js"></script>
<script src="<?php echo base_url(); ?>assets/js/vhome/jquery.smartmenus.js"></script>
<script src="<?php echo base_url(); ?>assets/js/vhome/modernizr.custom.79639.js"></script>
<script src="<?php echo base_url(); ?>assets/js/vhome/script.js"></script>
<script src="<?php echo base_url(); ?>assets/js/vhome/js.js"></script>
<!-- Vhome JS end -->

<!-- FitText load START-->
    <script src="<?php echo base_url(); ?>assets/js/fittext.js"></script>  To Responsive Text 
    <script>
        window.fitText( document.getElementById("responsive_headline") );
        window.fitText( document.getElementById("responsive_headline"), 1.2 ); // turn the compressor up (font will shrink a bit more aggressively)
        window.fitText( document.getElementById("responsive_headline"), 0.8 ); // turn the compressor down (font will shrink less aggressively)
        window.fitText( document.getElementById("responsive_headline"), 1.2, { minFontSize: '30px', maxFontSize: '80px' } );
    </script>
<!-- FitText load END-->
<script>
$(document).ready(function() {
    $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
    });
});
</script>

<script>
    $('.flip').click(function(){
        $(this).find('.card').addClass('flipped').mouseleave(function(){
            $(this).removeClass('flipped');
        });
        return false;
    });
</script>