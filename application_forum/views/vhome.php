<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Free coming soon template with jQuery countdown">
    <meta name="author" content="#">
    <link rel="shortcut icon" href="assets/img/favicon.png">

    <title>NSBM-Forum</title>

    <!-- Bootstrap -->
   <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">



    <!-- siimple style -->
	<style>
    /* ==== Google font ==== */
@import url('http://fonts.googleapis.com/css?family=Lato:400,300,700,900');

body {
	background: #394864;
    font-family: 'Lato', sans-serif;
    font-weight: 300;
    font-size: 16px;
    color: #555;
    line-height: 1.6em;
    -webkit-font-smoothing: antialiased;
    -webkit-overflow-scrolling: touch;
}

h1, h2, h3, h4, h5, h6 {
    font-family: 'Lato', sans-serif;
    font-weight: 300;
    color: #444;
}

h1 {
	font-size: 40px;
}

h3 {
	font-weight: 400;
}

h4 {
	font-weight: 400;
	font-size: 20px;
}

p {
    margin-bottom: 20px;
    font-size: 16px;
}


a {
    color:#000000;
    word-wrap: break-word;
    -webkit-transition: color 0.1s ease-in, background 0.1s ease-in;
    -moz-transition: color 0.1s ease-in, background 0.1s ease-in;
    -ms-transition: color 0.1s ease-in, background 0.1s ease-in;
    -o-transition: color 0.1s ease-in, background 0.1s ease-in;
    transition: color 0.1s ease-in, background 0.1s ease-in;
}

a:hover,
a:focus {
    color: #4F92AF;
    text-decoration: none;
    outline: 0;
}

a:before,
a:after {
    -webkit-transition: color 0.1s ease-in, background 0.1s ease-in;
    -moz-transition: color 0.1s ease-in, background 0.1s ease-in;
    -ms-transition: color 0.1s ease-in, background 0.1s ease-in;
    -o-transition: color 0.1s ease-in, background 0.1s ease-in;
    transition: color 0.1s ease-in, background 0.1s ease-in;
}

.alignleft {
    text-align: left;
}
.alignright {
    text-align: right;
}

.aligncenter {
    text-align: center;
}

.btn {
  display: inline-block;
  padding: 10px 20px;
  margin-bottom: 0;
  font-size: 14px;
  font-weight: normal;
  line-height: 1.428571429;
  text-align: center;
  white-space: nowrap;
  vertical-align: middle;
  cursor: pointer;
  -webkit-user-select: none;
     -moz-user-select: none;
      -ms-user-select: none;
       -o-user-select: none;
          user-select: none;
  background-image: none;
  border: 1px solid transparent;
  border-radius: 0;
}

.btn-theme  {
  color: #fff;
  background-color: #4F92AF;
  border-color: #4F92AF;
}
.btn-theme:hover  {
  color: #fff;
  background-color: #444;
  border-color: #444;
}
form.signup input  {
	height: 42px;
	width: 200px;
	border-radius: 0;
	/*border: none;*/
}
form.signup button.btn {
	font-weight: 700;
}
form.signup input.form-control:focus {
	border-color: #fd680e;
}


/* wrapper */

#wrapper {
	text-align: center;
	padding: 50px 0;
        background: url(../../assets_forum/img/main-bg-for.jpg) no-repeat center top;
	background-attachment: relative;
	background-position: center center;
	min-height: 650px;
	width: 100%;	
    -webkit-background-size: 100%;
    -moz-background-size: 100%;
    -o-background-size: 100%;
    background-size: 100%;

    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
}



#wrapper h1 {
	margin-top: 60px;
	margin-bottom: 40px;
	color: #C2C2C2;
	font-size: 45px;
	font-weight: 900;
	letter-spacing: -1px;
}

h2.subtitle {
	color: #C2C2C2;
	font-size: 24px;
}


/* footer */
p.copyright {
	margin-top: 20px;
	color:#C2C2C2;
	text-align: center;
}	

.nsbmlogo{
                margin: 15px auto 20px auto; 
                width: 240px; 
                height: auto;
                box-shadow:
                    0 2px 2px rgba(0,0,0,0.2),        
                    0 1px 5px rgba(0,0,0,0.2),        
                    0 0 0 8px rgba(255,255,255,0.4);
            }
            #lms-home-social-menu2 > li{
                font-family: Cambria, Georgia, serif;
                display: inline;
                margin-right: 20px;
                text-align: center;
            }
            #lms-home-social-menu2 > li > a{
                color:#33ccff;
                text-decoration: none;
            }
            #lms-home-social-menu2 > li > a:hover{
                color: #ffffff;
                text-decoration: none;
            }
</style>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

	<div id="wrapper">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12 col-lg-12">
                                    <a href="http://d2real.com" target="_blank"><img class="image-responsive center-block nsbmlogo" src="<?php echo base_url(); ?>logos/nsbmwhite.gif"></a>
                                    <h1><span style="color: #0099cc">NSBM</span> Forum</h1>
                                    <h2 class="subtitle" style="color: #0099cc">NSBM Forum is a new feature that we are going to intorduce the main website. This is to 
increase the communication between NSBM students and the communication between
students and lecturers. From this forum both students and lecturers will reap many uses,
like exchange of ideas, couse materials, discussions or even use as a bulletin board. 
Our developing team will hope to launch the NSBM forum soon enough.</h2>
					<br><br>
					<form class="form-inline signup" role="form">
					  <div class="form-group">
					    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter your email address">
					  </div>
					  <button type="submit" class="btn btn-theme">Get notified!</button>
					</form>		
					
				</div>
				
			</div>
                    <div class="container" style="margin-bottom: 0px; margin-top: 65px;">
                    <div class="row" style="margin: 0 auto; padding: 0">
                        <div class="col-xs-12" style="margin: 0 auto; padding: 0">
                            <div class="center-block" style="margin: 0 auto; padding: 0">
                                <center>
                                    <ul id="lms-home-social-menu2"  style="margin: 0 auto; padding: 0">
                                        <li><a href="http://d2real.com"  target="_blank">NSBM</a></li>
                                        <li><a href="http://my.d2real.com"  target="_blank">My NSBM</a></li>
                                        <li><a href="http://lms.d2real.com"  target="_blank">L.M.S</a></li>
                                        <li><a href="http://forum.d2real.com"  target="_blank">Forum</a></li>
                                        <li><a href="http://events.d2real.com"  target="_blank">Events</a></li>
                                        <li><a href="http://communities.d2real.com"  target="_blank">Communities</a></li>
                                        <li><a href="http://help.d2real.com"  target="_blank">Help</a></li>
                                    </ul>
                                
                                <p class="copyright">Copyright &copy; 2014 - <a href="http://d2real.com">National School of Business Management</a></p></center>
                            </div>
                        </div>
                    </div>
                </div>	
		</div>
	</div>
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
  </body>
</html>
