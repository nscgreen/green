<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Forum extends CI_Controller {
    
        public function index() { 
	
         $data = array(
            'title' => 'Forum',
            'mDescription' => 'Learning Management System of National School of business Management',
            'mKeywords' => 'Forum',
            'pagetitle' => '/ with Forum',
        );
		
        $this->load->model('help_dbproccess');
        $data_All['slide'] = $this->help_dbproccess->slidermenu();
        $data_All['fullArticle'] = $this->help_dbproccess->fullArticle(); 

        $this->load->view('vheader', $data);
        $this->load->view('vforum', $data_All);
        $this->load->view('vfooter');    
    }
    
}

?>