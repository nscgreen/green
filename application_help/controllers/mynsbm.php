<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Mynsbm extends CI_Controller{

	public function index(){
        
        $data = array(
            'title' => 'My-NSBM',
            'mDescription' => 'Learning Management System of National School of business Management',
            'mKeywords' => 'My-NSBM',
            'pagetitle' => '/ with my-NSBM',
        );
		
        $this->load->model('help_dbproccess');
        $data_All['slide'] = $this->help_dbproccess->slidermenu();
        $data_All['fullArticle'] = $this->help_dbproccess->fullArticle(); 

        $this->load->view('vheader', $data);
        $this->load->view('vmynsbm', $data_All);
        $this->load->view('vfooter');  
    }
}

?>