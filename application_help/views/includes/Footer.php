  <footer class="footer">
      <div class="container">
        <p>Copyright &COPY; National School Of Business Management</p>
        <ul class="footer-links">
          <li><a href="#"  style="text-decoration: none;">NIBM</a></li>
          <li class="muted">&middot;</li>
          <li><a href="#"  style="text-decoration: none;">NSBM</a></li>
          <li class="muted">&middot;</li>
          <li><a href="#"  style="text-decoration: none;">LMS</a></li>
          <li class="muted">&middot;</li>
          <li><a href="#"  style="text-decoration: none;">EMS</a></li>
          <li class="muted">&middot;</li>
          <li><a href="#"  style="text-decoration: none;">Forum</a></li>
        </ul>
      </div>
    </footer>
