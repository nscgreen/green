
    <div class="navbar navbar-inverse navbar-fixed-top" style="position: fixed">
        <div class="navbar-inner">
            <div class="container" style="padding: 0 15px;">
                <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="brand" href="http://www.d2real.com">NSBM</a>
                <a class="brand" href="#up" style="color: #0088cc"><i class="icon-chevron-up"></i></a>
                <div class="nav-collapse collapse">
                    <ul class="nav">
                        <li class="active">
                            <a href="index.html">Get started</a>
                        </li>
                        <li class="">
                            <a href="mynsbm.php">My-NSBM</a>
                        </li>
                        <li class="">
                            <a href="lms.php">E-Learning</a>
                        </li>
                        <li class="">
                            <a href="ems.php">EMS</a>
                        </li>
                        <li class="">
                            <a href="community.php">Communities</a>
                        </li>
                        <li class="">
                            <a href="forum.php">Forum</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
