<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jul 3, 2014, 3:40:02 PM
 */

?>

 <!-- Footer
    ================================================== -->
    <footer class="footer">
      <div class="container">
        <p>Copyright &COPY; National School of Business Management</p>
        <ul class="footer-links">
            <li><a href="http://main.d2real.com" target="_blank" style="text-decoration: none;">NSBM</a></li>
          <li class="muted">&middot;</li>
          <li><a href="http://lms.d2real.com" target="_blank" style="text-decoration: none;">LMS</a></li>
          <li class="muted">&middot;</li>
          <li><a href="http://forum.d2real.com" target="_blank" style="text-decoration: none;">Forum</a></li>
          <li class="muted">&middot;</li>
          <li><a href="http://events.d2real.com" target="_blank" style="text-decoration: none;">EMS</a></li>
          <li class="muted">&middot;</li>
          <li><a href="http://communities.d2real.com" target="_blank" style="text-decoration: none;">Communities</a></li>
        </ul>
      </div>
    </footer>



    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
<!--    <script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-transition.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-alert.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-modal.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-dropdown.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-scrollspy.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-tab.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-tooltip.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-popover.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-button.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-collapse.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-carousel.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-typeahead.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-affix.js"></script>

    <script src="<?php echo base_url(); ?>assets/js/holder/holder.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/google-code-prettify/prettify.js"></script>

    <script src="<?php echo base_url(); ?>assets/js/application.js"></script>-->
    
    
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
    <script src="<?php echo base_url(); ?>assets_help/js/jquery.js"></script>
    <script src="<?php echo base_url(); ?>assets_help/js/bootstrap-transition.js"></script>
    <script src="<?php echo base_url(); ?>assets_help/js/bootstrap-alert.js"></script>
    <script src="<?php echo base_url(); ?>assets_help/js/bootstrap-modal.js"></script>
    <script src="<?php echo base_url(); ?>assets_help/js/bootstrap-dropdown.js"></script>
    <script src="<?php echo base_url(); ?>assets_help/js/bootstrap-scrollspy.js"></script>
    <script src="<?php echo base_url(); ?>assets_help/js/bootstrap-tab.js"></script>
    <script src="<?php echo base_url(); ?>assets_help/js/bootstrap-tooltip.js"></script>
    <script src="<?php echo base_url(); ?>assets_help/js/bootstrap-popover.js"></script>
    <script src="<?php echo base_url(); ?>assets_help/js/bootstrap-button.js"></script>
    <script src="<?php echo base_url(); ?>assets_help/js/bootstrap-collapse.js"></script>
    <script src="<?php echo base_url(); ?>assets_help/js/bootstrap-carousel.js"></script>
    <script src="<?php echo base_url(); ?>assets_help/js/bootstrap-typeahead.js"></script>
    <script src="<?php echo base_url(); ?>assets_help/js/bootstrap-affix.js"></script>

    <script src="<?php echo base_url(); ?>assets_help/js/holder/holder.js"></script>
    <script src="<?php echo base_url(); ?>assets_help/js/google-code-prettify/prettify.js"></script>
    <script src="<?php echo base_url(); ?>assets_help/js/application.js"></script>

  </body>
</html>