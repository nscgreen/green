<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jul 3, 2014, 3:26:00 PM
 */

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo $title?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php echo $mDescription?>"/>
    <meta name="keywords" content="<?php echo $mKeywords?>">
    <meta name="author" content="Html5TemplatesDreamweaver">

    <!-- Le styles -->
<!--    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
    <link href="assets/css/docs.css" rel="stylesheet">
    <link href="assets/js/google-code-prettify/prettify.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/font-awesome.css" type="text/css" />-->
    
    <link href="<?php echo base_url(); ?>assets_help/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets_help/css/bootstrap-responsive.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets_help/css/docs.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets_help/js/google-code-prettify/prettify.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets_help/css/font-awesome.css" rel="stylesheet" />

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

  <body data-spy="scroll" data-target=".bs-docs-sidebar">

    <!-- Navbar
    ================================================== -->
    <div class="navbar navbar-inverse navbar-fixed-top" style="position: fixed">
        <div class="navbar-inner">
            <div class="container" style="padding: 0 15px;">
                <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="brand" href="<?php echo base_url();?>">NSBM</a>
                <a class="brand" href="#up" style="color: #0088cc"><i class="icon-chevron-up"></i></a>
                <div class="nav-collapse collapse">
                    <ul class="nav">
                        <li class="<?php if($pagetitle === ''){echo 'active' ;}?>">
                            <a href="<?php echo base_url(); ?>home">Get started</a>
                        </li>
                        <li class="<?php if($pagetitle === '/ with my-NSBM'){echo 'active' ;}?>">
                            <a href="<?php echo base_url(); ?>mynsbm">My-NSBM</a>
                        </li>
                        <li class="<?php if($pagetitle === '/ with Learning Management System'){echo 'active' ;}?>">
                            <a href="<?php echo base_url(); ?>lms">LMS</a>
                        </li>
                        <li class="<?php if($pagetitle === '/ with Event Management System'){echo 'active' ;}?>">
                            <a href="<?php echo base_url(); ?>ems">EMS</a>
                        </li>
                        <li class="<?php if($pagetitle === '/ with Communities'){echo 'active' ;}?>">
                            <a href="<?php echo base_url(); ?>communities">Communities</a>
                        </li>
                        <li class="<?php if($pagetitle === '/ with Forum'){echo 'active' ;}?>">
                            <a href="<?php echo base_url(); ?>forum">Forum</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div id="up" ></div>
    <!-- Subhead
    ================================================== -->
    <header class="jumbotron subhead" id="overview">
        <div class="container">
            <h1 style="margin-top: 35px"><a href="<?php echo base_url();?>" style="text-decoration: none;">National School of Business Management</a><br /><small>Getting Started <span style="color: #ffffff"><?php echo $pagetitle?></span></small></h1>
            <p class="lead">This Page will help you getting started with the NSBM Network</p>
        </div>
    </header>
    