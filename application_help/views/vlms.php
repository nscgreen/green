<div class="container">
    <!-- Docs nav
    ================================================== -->
    <div class="row">
        <div class="span3 bs-docs-sidebar">         
            <ul class="nav nav-list bs-docs-sidenav">
                <?php foreach ($slide as $slider) { ?>
                    <?php if ($slider['category'] == 'lms') { ?>
                        <li>
                            <a href="#<?php echo $slider['slide_title']; ?>" >
                                <i class="icon-chevron-right"></i>
                                <?php echo $slider['slide_title']; ?> 
                            </a>
                        </li>
                    <?php }
                }
                ?>   
            </ul>
        </div>  

        <div class="span9">
            <ul style="list-style: none">
                <?php foreach ($fullArticle as $article) { ?>
                    <?php if ($article['category'] == 'lms') { ?>
                        <li>
                            <section id="<?php echo $article['slide_title'] ?>">
                                <h1 style="color: #0099cc"> <?php echo $article['title']; ?> </h1></br>
                                <p style="text-align: justify"> <?php echo $article['content']; ?> </p>
                            </section>
                            <hr class="bs-docs-separator"> 
                        </li>
                    <?php
                    }
                }
                ?>          
            </ul>
        </div>
    </div>
</div>