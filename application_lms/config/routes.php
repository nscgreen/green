<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home";
$route['404_override'] = '';

/*
||| Routes for the student selecting stuff section
*/
$route['student/select/tutes'] = 'student/select/index/tutes';
$route['student/select/assignments'] = 'student/select/index/assignments';
$route['student/select/references'] = 'student/select/index/references';
/*
||| End of Routes for the student selecting stuff section
*/


/*
||| Routes for the lecturers uploading stuff section
*/
$route['lecturer/select/tutes'] = 'lecturer/select/index/tutes';
$route['lecturer/select/assignments'] = 'lecturer/select/index/assignments';
$route['lecturer/select/references'] = 'lecturer/select/index/references';
/*
||| End of Routes for the lecturers uploading stuff section
*/


/*
||| Routes for the coordinators uploading stuff section
*/
$route['coordinator/select/tutes'] = 'coordinator/select/index/tutes';
$route['coordinator/select/assignments'] = 'coordinator/select/index/assignments';
$route['coordinator/select/references'] = 'coordinator/select/index/references';
/*
||| End of Routes for the coordinators uploading stuff section
*/


/*
||| Routes for download stuff section
*/
$route['download/(:any)/(:any)'] = 'download/index/$1/$2';
/*
||| End of Routes for download stuff section
*/


/* End of file routes.php */
/* Location: ./application/config/routes.php */