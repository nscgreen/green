<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comments extends CI_Controller {
    
        public function add_comment() {
            $this->load->library('nativesession');
            $commentdetails['batch']=$this->input->post('batch', TRUE);
            $commentdetails['lecturer_id']=$this->nativesession->get('mynsbm_id');
            
            $commentdetails['comment']=$this->input->post('comment', TRUE);
            $commentdetails['start_date']=$this->input->post('start_date', TRUE);
            $commentdetails['start_time']=$this->input->post('start_time', TRUE);
            $commentdetails['end_date']=$this->input->post('end_date', TRUE);
            $commentdetails['end_time']=$this->input->post('end_time', TRUE);

            $this->load->model('mcomments');
            $this->mcomments->save_comment($commentdetails);
            
            redirect($this->input->post('nexturl', TRUE));
            
        }
        
}

/* End of file comments.php */
/* Location: ./application/controllers/lecturer/comments.php */