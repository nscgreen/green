<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Assignments extends CI_Controller {
	
        public function index()
	{
            $this->load->helper(array('form', 'url'));
            $this->load->library('nativesession');
            $this->load->model('mdetails');
            if(isset($_POST['batch']) && isset($_POST['acyear']) && isset($_POST['semester']) && isset($_POST['module'])){
                $assignmentdetails['batch']=$this->input->post('batch', TRUE);
                
                $assignmentdetails['batch']=$this->input->post('batch', TRUE);
                $assignmentdetails['batch_name']=$this->mdetails->get_batch_name($assignmentdetails);
                
                $assignmentdetails['acyear']=$this->input->post('acyear', TRUE); 
                $assignmentdetails['semester']=$this->input->post('semester', TRUE);
                $assignmentdetails['module']=$this->input->post('module', TRUE); 
                
                $assignmentdetails['module_name']=$this->mdetails->get_module_name_by_batch($assignmentdetails);
                
                $this->nativesession->set('batch',$assignmentdetails['batch']);
                $this->nativesession->set('batch_name',$assignmentdetails['batch_name']);
                $this->nativesession->set('acyear',$assignmentdetails['acyear']);
                $this->nativesession->set('semester',$assignmentdetails['semester']);
                $this->nativesession->set('module',$assignmentdetails['module']);
                $this->nativesession->set('module_name',$assignmentdetails['module_name']);

            }elseif($this->nativesession->sessionIsset('batch')==TRUE){
                $assignmentdetails['batch']=$this->nativesession->get('batch');
                $assignmentdetails['batch_name']=$this->nativesession->get('batch_name');
                $assignmentdetails['acyear']=$this->nativesession->get('acyear');
                $assignmentdetails['semester']=$this->nativesession->get('semester');
                $assignmentdetails['module']=$this->nativesession->get('module');
                $assignmentdetails['module_name']=$this->nativesession->get('module_name');
            }else{
                redirect('coordinator/select/assignments');
            }
                        
            $data = array(
            'title' => 'NSBM-LMS',
            'mDescription' => 'Learning Management System of National School of business Management',
            'mKeywords' => 'LMS');
                
            $dataall = array(
            'batch'=>$assignmentdetails['batch'],
            'batch_name'=>$assignmentdetails['batch_name'],
            'acyear'=>$assignmentdetails['acyear'],
            'semester'=>$assignmentdetails['semester'],
            'module'=>$assignmentdetails['module'],
            'module_name'=>$assignmentdetails['module_name'],

            );
            
            $this->load->model('coordinator/massignments','massignments');
            $dataall['assignments']= $this->massignments->get_assignments($assignmentdetails);
            
            $this->load->helper('html');
            $this->load->helper('url');
            
            if($this->nativesession->sessionIsset('readyforcomment')==TRUE){
                $dataall['ready_for_comment']=$this->nativesession->get('readyforcomment');
                $dataall['assignment_id']=$this->nativesession->get('assignment_id');
                
                $this->nativesession->delete('readyforcomment');
                $this->nativesession->delete('assignment_id');
                $dataall['view_edit_modal']=FALSE;
            }else{
                $data['ready_for_comment']=FALSE;
                if($this->nativesession->get('uploadfailed')==TRUE){
                    $dataall['view_edit_modal']=FALSE;
                    $dataall['upload_failed']=TRUE;
                    $this->nativesession->delete('uploadfailed');
                }else{
                    $dataall['upload_failed']=FALSE;
                    if(isset($_POST['id'])){
                        $data['view_edit_modal']=TRUE;
                    }else{
                        $data['view_edit_modal']=FALSE;
                    }
                }
            }
            
            $this->load->model('logged/mlg_user','mlg_user');
            $user_details=$this->mlg_user->load_menu_user_details();
            
            $this->load->model('mcomments');      
            $data['comments_lecturers']=TRUE;
            $data['comments_for_users']= $this->mcomments->get_comments_for_lecturer();
            
            $this->load->model('mtimetable');
            $data['time_table']= $this->mtimetable->get_coordinator_timetable($this->nativesession->get('mynsbm_id'));
            $data['time_table_coordinator']=TRUE;
            
            $this->load->view('vheader', $data);
            // Header Panel for Logged users
            if(!$user_details===FALSE){
                $this->load->view('vlg_menu',$user_details);
            }else{
                 redirect (base_url()); // If not log
            }
            $this->load->view('lg_coordinator/vassignments',$dataall);
            $this->load->view('vright_panel');
            $this->load->view('vfooter');
	}
        
        public function add_assignment() { 
            $this->load->helper('html');
            $this->load->helper('url');

            $this->load->database();

            if (isset($_FILES['assignmentfile']['name']) && !empty($_FILES['assignmentfile']['name'])) {
                $assignmentdetails['batch']=$this->input->post('batch', TRUE);
                $assignmentdetails['acyear']=$this->input->post('acyear', TRUE);
                $assignmentdetails['semester']=$this->input->post('semester', TRUE);
                $assignmentdetails['module']=$this->input->post('module', TRUE);

                $assignmentdetails['submit_date']=$this->input->post('submit_date', TRUE);
                $assignmentdetails['submit_time']=$this->input->post('submit_time', TRUE);

                $assignmentdetails['assignment_name']=$this->input->post('assignment_name', TRUE);
                $assignmentdetails['assignment_description']=$this->input->post('assignment_description', TRUE);

                $assignmentdetails['date']=date("Y-m-d");
                $assignmentidprefix='Assignment_';
                $assignmentdetails['assignment_id']=uniqid($assignmentidprefix);

                $assignmentfilename='Assignment_'.$assignmentdetails['assignment_name'];

                $this->load->model('mfile_handler');

                // Check and create the folders for the module
                $uploadertype='coordinators';
                $uploadtype='assignments';
                $folders=array(
                            1=>$assignmentdetails['batch'],
                            2=>$assignmentdetails['acyear'],
                            3=>$assignmentdetails['semester'],
                            4=>$assignmentdetails['module']
                );           
                $uploadpaths=$this->mfile_handler->create_folders($uploadertype,$uploadtype,$folders);

                $folderupload=$uploadpaths['folderupload'];
                $folderdb=$uploadpaths['folderdb'];

                $this->load->model('coordinator/massignments','massignments');

                $uploaddetails=$this->massignments->upload_assignment($assignmentfilename,$folderupload,$folderdb,'assignmentfile');

                $this->load->library('nativesession');
                if ($this->nativesession->get('uploadfailed')==TRUE) {
                    redirect('coordinator/assignments');  
                }

                $assignmentdetails['assignmentpath']= $uploaddetails['assignmentpath'];
                $assignmentdetails['filename']= $uploaddetails['filename'];

                $fileinfo = new SplFileInfo($uploaddetails['filename']);
                $assignmentdetails['file_extension']= $fileinfo->getExtension();

                $this->massignments->save_assignment($assignmentdetails);

                $this->nativesession->set('readyforcomment',TRUE);
                $this->nativesession->set('assignment_id',$assignmentdetails['assignment_id']);
                redirect('coordinator/assignments');                    
            }
        }

        
        public function update_assignment() { 
            $this->load->helper('html');
            $this->load->helper('url');

            $this->load->database();
            $assignmentdetails['id']=$this->input->post('id', TRUE);
            $assignmentdetails['assignment_id']=$this->input->post('assignment_id', TRUE);

            $assignmentdetails['batch']=$this->input->post('batch', TRUE);
            $assignmentdetails['acyear']=$this->input->post('acyear', TRUE);
            $assignmentdetails['semester']=$this->input->post('semester', TRUE);
            $assignmentdetails['module']=$this->input->post('module', TRUE);

            $assignmentdetails['submit_date']=$this->input->post('submit_date', TRUE);
            $assignmentdetails['submit_time']=$this->input->post('submit_time', TRUE);

            $assignmentdetails['assignment_name']=$this->input->post('assignment_name', TRUE);
            $assignmentdetails['assignment_description']=$this->input->post('assignment_description', TRUE);
            $assignmentdetails['date']=$this->input->post('date', TRUE);

            $assignmentdetails['file_path_old']=$this->input->post('file_path_old', TRUE);
            $assignmentdetails['file_extension_old']=$this->input->post('file_extension_old', TRUE);

            $this->load->model('coordinator/massignments','massignments');

            if(isset($_FILES['assignmentfile']['name']) && !empty($_FILES['assignmentfile']['name'])) {

                $this->load->model('mfile_handler');
                $this->mfile_handler->delete_file($assignmentdetails['file_path_old']);

                $assignmentfilename='Assignment_'.$assignmentdetails['assignment_name'];

                $this->load->model('mfile_handler');

                // Check and create the folders for the module
                $uploadertype='coordinators';
                $uploadtype='assignments';
                $folders=array(
                            1=>$assignmentdetails['batch'],
                            2=>$assignmentdetails['acyear'],
                            3=>$assignmentdetails['semester'],
                            4=>$assignmentdetails['module']
                );
                $uploadpaths=$this->mfile_handler->create_folders($uploadertype,$uploadtype,$folders);

                $folderupload=$uploadpaths['folderupload'];
                $folderdb=$uploadpaths['folderdb'];

                $uploaddetails=$this->massignments->upload_assignment($assignmentfilename,$folderupload,$folderdb,'assignmentfile');

                $this->load->library('nativesession');
                if ($this->nativesession->get('uploadfailed')==TRUE) {
                    redirect('coordinator/assignments');  
                }

                $assignmentdetails['assignmentpath']= $uploaddetails['assignmentpath'];
                $fileinfo = new SplFileInfo($uploaddetails['filename']);
                $assignmentdetails['file_extension']= $fileinfo->getExtension();

            }elseif(!isset($_FILES['assignmentfile']['name']) || empty($_FILES['assignmentfile']['name'])) {
                $assignmentdetails['assignmentpath']= $this->input->post('file_path_old', TRUE);
                $assignmentdetails['file_extension']= $this->input->post('file_extension_old', TRUE);
            }
                $this->massignments->update_assignment($assignmentdetails);
                $this->load->library('nativesession');
                $this->nativesession->set('readyforcomment',TRUE);
                $this->nativesession->set('assignment_id',$assignmentdetails['assignment_id']);
                redirect('coordinator/assignments'); 
        }
        
        public function delete_assignment($assignment_id) {
            $this->load->helper('html');
            $this->load->helper('url');

            $this->load->database();

            $this->load->model('coordinator/massignments','massignments');
            $this->massignments->delete_assignment($assignment_id);

            redirect('coordinator/assignments');
        }
}

/* End of file assignments.php */
/* Location: ./application/controllers/coordinator/assignments.php */
