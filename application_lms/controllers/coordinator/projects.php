<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Projects extends CI_Controller {
	
public function index()
	{
            $this->load->helper(array('form', 'url'));
            $this->load->library('nativesession');
            $this->load->model('mdetails');
            if(isset($_POST['batch']) && isset($_POST['acyear'])){
                $projectdetails['batch']=$this->input->post('batch', TRUE);
                $projectdetails['batch_name']=$this->mdetails->get_batch_name($projectdetails);
                
                $projectdetails['acyear']=$this->input->post('acyear', TRUE); 
                
                $this->nativesession->set('batch',$projectdetails['batch']);
                $this->nativesession->set('batch_name',$projectdetails['batch_name']);
                $this->nativesession->set('acyear',$projectdetails['acyear']);

            }elseif($this->nativesession->sessionIsset('batch')==TRUE){
                $projectdetails['batch']=$this->nativesession->get('batch');
                $projectdetails['batch_name']=$this->nativesession->get('batch_name');
                $projectdetails['acyear']=$this->nativesession->get('acyear');
            }else{
                redirect('coordinator/select/projects');
            }
            
            $data = array(
            'title' => 'NSBM-LMS',
            'mDescription' => 'Learning Management System of National School of business Management',
            'mKeywords' => 'LMS');
                
            $dataall = array(
            'batch'=>$projectdetails['batch'],
            'batch_name'=>$projectdetails['batch_name'],
            'acyear'=>$projectdetails['acyear']
            );
            
            $this->load->model('coordinator/mprojects','mprojects');
            $dataall['projects']= $this->mprojects->get_projects($projectdetails);
            
            if($this->nativesession->sessionIsset('readyforcomment')==TRUE){
                $dataall['ready_for_comment']=$this->nativesession->get('readyforcomment');
                $dataall['project_id']=$this->nativesession->get('project_id');
                
                $this->nativesession->delete('readyforcomment');
                $this->nativesession->delete('project_id');
                $dataall['view_edit_modal']=FALSE;
            }else{
                $dataall['ready_for_comment']=FALSE;
                if($this->nativesession->get('uploadfailed')==TRUE){
                    $dataall['view_edit_modal']=FALSE;
                    $dataall['upload_failed']=TRUE;
                    $this->nativesession->delete('uploadfailed');
                }else{
                    $dataall['upload_failed']=FALSE;
                    if(isset($_POST['id'])){
                        $dataall['view_edit_modal']=TRUE;
                    }else{
                        $dataall['view_edit_modal']=FALSE;
                    }
                }
            }
            
            $this->load->model('logged/mlg_user','mlg_user');
            $user_details=$this->mlg_user->load_menu_user_details();
            
            $this->load->model('mcomments');      
            $data['comments_lecturers']=TRUE;
            $data['comments_for_users']= $this->mcomments->get_comments_for_lecturer();
            
            $this->load->model('mtimetable');
            $data['time_table']= $this->mtimetable->get_coordinator_timetable($this->nativesession->get('mynsbm_id'));
            $data['time_table_coordinator']=TRUE;
            
            $this->load->view('vheader', $data);
            // Header Panel for Logged users
            if(!$user_details===FALSE){
                $this->load->view('vlg_menu',$user_details);
            }else{
                 redirect (base_url()); // If not log
            }
            $this->load->view('lg_coordinator/vprojects',$dataall);
            $this->load->view('vright_panel');
            $this->load->view('vfooter');
	}
        
        public function add_project() { 
            $this->load->helper('html');
            $this->load->helper('url');

            $this->load->database();

            if (isset($_FILES['projectfile']['name']) && !empty($_FILES['projectfile']['name'])) {
                $projectdetails['batch']=$this->input->post('batch', TRUE);
                $projectdetails['acyear']=$this->input->post('acyear', TRUE);

                $projectdetails['submit_date']=$this->input->post('submit_date', TRUE);
                $projectdetails['submit_time']=$this->input->post('submit_time', TRUE);

                $projectdetails['project_name']=$this->input->post('project_name', TRUE);
                $projectdetails['project_description']=$this->input->post('project_description', TRUE);

                $projectdetails['date']=date("Y-m-d");

                $projectfilename='Project_'.$projectdetails['project_name'];

                $this->load->model('mfile_handler');

                // Check and create the folders for the module
                $uploadertype='coordinators';
                $uploadtype='projects';
                $folders=array(
                            1=>$projectdetails['batch'],
                            2=>$projectdetails['acyear']
                );           
                $uploadpaths=$this->mfile_handler->create_folders($uploadertype,$uploadtype,$folders);

                $folderupload=$uploadpaths['folderupload'];
                $folderdb=$uploadpaths['folderdb'];

                $this->load->model('coordinator/mprojects','mprojects');

                $uploaddetails=$this->mprojects->upload_project($projectfilename,$folderupload,$folderdb,'projectfile');

                $this->load->library('nativesession');
                if ($this->nativesession->get('uploadfailed')==TRUE) {
                    redirect('coordinator/projects');  
                }

                $projectdetails['projectpath']= $uploaddetails['projectpath'];
                $projectdetails['filename']= $uploaddetails['filename'];

                $fileinfo = new SplFileInfo($uploaddetails['filename']);
                $projectdetails['file_extension']= $fileinfo->getExtension();

                $this->mprojects->save_project($projectdetails);

                $this->nativesession->set('readyforcomment',TRUE);
                $this->nativesession->set('project_id',$projectdetails['project_id']);
                redirect('coordinator/projects');                    
            }
        }

        
        public function update_project() { 
            $this->load->helper('html');
            $this->load->helper('url');

            $this->load->database();
            $projectdetails['id']=$this->input->post('id', TRUE);

            $projectdetails['batch']=$this->input->post('batch', TRUE);
            $projectdetails['acyear']=$this->input->post('acyear', TRUE);

            $projectdetails['submit_date']=$this->input->post('submit_date', TRUE);
            $projectdetails['submit_time']=$this->input->post('submit_time', TRUE);

            $projectdetails['project_name']=$this->input->post('project_name', TRUE);
            $projectdetails['project_description']=$this->input->post('project_description', TRUE);
            $projectdetails['date']=$this->input->post('date', TRUE);

            $projectdetails['file_path_old']=$this->input->post('file_path_old',TRUE);
            var_dump($projectdetails['file_path_old']);
            $projectdetails['file_extension_old']=$this->input->post('file_extension_old', TRUE);

            $this->load->model('coordinator/mprojects','mprojects');

            if(isset($_FILES['projectfile']['name']) && !empty($_FILES['projectfile']['name'])) {

                $this->load->model('mfile_handler');
                $this->mfile_handler->delete_file($projectdetails['file_path_old']);

                $projectfilename='Project_'.$projectdetails['project_name'];

                $this->load->model('mfile_handler');

                // Check and create the folders for the module
                $uploadertype='coordinators';
                $uploadtype='projects';
                $folders=array(
                            1=>$projectdetails['batch'],
                            2=>$projectdetails['acyear']
                );
                $uploadpaths=$this->mfile_handler->create_folders($uploadertype,$uploadtype,$folders);

                $folderupload=$uploadpaths['folderupload'];
                $folderdb=$uploadpaths['folderdb'];

                $uploaddetails=$this->mprojects->upload_project($projectfilename,$folderupload,$folderdb,'projectfile');

                $projectdetails['projectpath']= $uploaddetails['projectpath'];
                $fileinfo = new SplFileInfo($uploaddetails['filename']);
                $projectdetails['file_extension']= $fileinfo->getExtension();

            }elseif(!isset($_FILES['projectfile']['name']) || empty($_FILES['projectfile']['name'])) {
                $projectdetails['projectpath']= $this->input->post('file_path_old', TRUE);
                $projectdetails['file_extension']= $this->input->post('file_extension_old', TRUE);
            }
                $this->mprojects->update_project($projectdetails);
                $this->load->library('nativesession');
                $this->nativesession->set('readyforcomment',TRUE);
                $this->nativesession->set('project_id',$projectdetails['project_id']);
                redirect('coordinator/projects'); 
        }
        
        public function delete_project($project_id) {
                $this->load->helper('html');
                $this->load->helper('url');

                $this->load->database();
                $this->load->model('coordinator/mprojects','mprojects');
                $this->mprojects->delete_project($project_id);
                
                redirect('coordinator/projects');
        }
        
        

}

/* End of file projects.php */
/* Location: ./application/controllers/coordinator/projects.php */