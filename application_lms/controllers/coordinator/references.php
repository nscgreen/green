<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class References extends CI_Controller {
	
        public function index()
	{
            $this->load->helper(array('form', 'url'));
            $this->load->library('nativesession');
            $this->load->model('mdetails');
            if(isset($_POST['batch']) && isset($_POST['acyear']) && isset($_POST['semester']) && isset($_POST['module'])){        
                $referencedetails['batch']=$this->input->post('batch', TRUE);
                $referencedetails['batch_name']=$this->mdetails->get_batch_name($referencedetails);
                $referencedetails['degree_id']=$this->mdetails->get_degreeid_from_batch($referencedetails);
                $referencedetails['acyear']=$this->input->post('acyear', TRUE); 
                $referencedetails['semester']=$this->input->post('semester', TRUE);
                $referencedetails['module']=$this->input->post('module', TRUE);                
                $referencedetails['module_name']=$this->mdetails->get_module_name_by_batch($referencedetails);
                
                $this->nativesession->set('batch',$referencedetails['batch']);
                $this->nativesession->set('batch_name',$referencedetails['batch_name']);
                $this->nativesession->set('acyear',$referencedetails['acyear']);
                $this->nativesession->set('semester',$referencedetails['semester']);
                $this->nativesession->set('module',$referencedetails['module']);
                $this->nativesession->set('module_name',$referencedetails['module_name']);

            }elseif($this->nativesession->sessionIsset('batch')==TRUE){
                $referencedetails['batch']=$this->nativesession->get('batch');
                $referencedetails['batch_name']=$this->nativesession->get('batch_name');
                $referencedetails['degree_id']=$this->mdetails->get_degreeid_from_batch($referencedetails);
                $referencedetails['acyear']=$this->nativesession->get('acyear');
                $referencedetails['semester']=$this->nativesession->get('semester');
                $referencedetails['module']=$this->nativesession->get('module');
                $referencedetails['module_name']=$this->nativesession->get('module_name');
            }else{
                redirect('coordinator/select/references');
            }
                        
            $data = array(
            'title' => 'NSBM-LMS',
            'mDescription' => 'Learning Management System of National School of business Management',
            'mKeywords' => 'LMS');
                
            $dataall = array(
            'degree_id'=>$referencedetails['degree_id'],
            'batch'=>$referencedetails['batch'],
            'batch_name'=>$referencedetails['batch_name'],
            'acyear'=>$referencedetails['acyear'],
            'semester'=>$referencedetails['semester'],
            'module'=>$referencedetails['module'],
            'module_name'=>$referencedetails['module_name'],

            );
            
            $this->load->model('coordinator/mreferences','mreferences');
            $dataall['books']= $this->mreferences->get_references($referencedetails,'ref_books');
            $dataall['articles']= $this->mreferences->get_references($referencedetails,'ref_articles');
            $dataall['videos']= $this->mreferences->get_references($referencedetails,'ref_videos');
            $dataall['websites']= $this->mreferences->get_references($referencedetails,'ref_websites');
            
            $this->load->helper('html');
            
            if($this->nativesession->sessionIsset('readyforcomment')==TRUE){
                $dataall['ready_for_comment']=$this->nativesession->get('readyforcomment');
                
                $dataall['reference_id_field']=$this->nativesession->get('reference_id_field');
                $dataall['reference_id']=$this->nativesession->get('reference_id');    
                $dataall['ref_type']=$this->nativesession->get('ref_type');
                
                $this->nativesession->delete('readyforcomment');
                $this->nativesession->delete('reference_id');
                $dataall['view_edit_modal']=FALSE;
                $dataall['upload_failed']=FALSE;
            }else{
                $dataall['ready_for_comment']=FALSE;
                if($this->nativesession->get('uploadfailed')==TRUE){
                    $dataall['view_edit_modal']=FALSE;
                    $dataall['upload_failed']=TRUE;
                    $this->nativesession->delete('uploadfailed');
                }else{
                    $dataall['upload_failed']=FALSE;
                    if(isset($_POST['ref_type']) && !isset($_POST['edit_modal_close'])){
                        $dataall['view_edit_modal']=TRUE;
                        if($this->input->post('ref_type')==="ref_books"){
                            $referenceidfield="id";
                            $referenceid=$this->input->post('id');
                            $tablename=$this->input->post('ref_type');
                            $dataall['update_book_details']= $this->mreferences->get_reference_for_edit($referenceidfield,$referenceid,$tablename);
                        }elseif($this->input->post('ref_type')==="ref_articles"){
                            $referenceidfield="id";
                            $referenceid=$this->input->post('id');
                            $tablename=$this->input->post('ref_type');
                            $dataall['update_article_details']= $this->mreferences->get_reference_for_edit($referenceidfield,$referenceid,$tablename);
                        }elseif($this->input->post('ref_type')==="ref_videos"){
                            $referenceidfield="id";
                            $referenceid=$this->input->post('id');
                            $tablename=$this->input->post('ref_type');
                            $dataall['update_video_details']= $this->mreferences->get_reference_for_edit($referenceidfield,$referenceid,$tablename);
                        }elseif($this->input->post('ref_type')==="ref_websites"){
                            $referenceidfield="id";
                            $referenceid=$this->input->post('id');
                            $tablename=$this->input->post('ref_type');
                            $dataall['update_website_details']= $this->mreferences->get_reference_for_edit($referenceidfield,$referenceid,$tablename);
                        }
                    }else{
                        $dataall['view_edit_modal']=FALSE;
                    }
                }
            }
            
            $this->load->model('logged/mlg_user','mlg_user');
            $user_details=$this->mlg_user->load_menu_user_details();
            
            $this->load->model('mcomments');      
            $data['comments_lecturers']=TRUE;
            $data['comments_for_users']= $this->mcomments->get_comments_for_lecturer();
            
            $this->load->model('mtimetable');
            $data['time_table']= $this->mtimetable->get_coordinator_timetable($this->nativesession->get('mynsbm_id'));
            $data['time_table_coordinator']=TRUE;
            
            $this->load->view('vheader', $data);
            // Header Panel for Logged users
            if(!$user_details===FALSE){
                $this->load->view('vlg_menu',$user_details);
            }else{
                redirect (base_url()); // If not log
            }
            $this->load->view('lg_coordinator/vreferences',$dataall);
            $this->load->view('vright_panel');
            $this->load->view('vfooter');
	}
        
        public function add_reference() { 
            $this->load->helper('html');
            $this->load->helper('url');

            $this->load->database();

            if($this->input->post('ref_type')==="ref_books"){
                $referencetype="Books";
                $referencedetails['author']=$this->input->post('author', TRUE);
            }elseif($this->input->post('ref_type')==="ref_articles"){
                $referencetype="Articles";
                $referencedetails['author']=$this->input->post('author', TRUE);
            }elseif($this->input->post('ref_type')==="ref_videos"){
                $referencetype="Videos";
            }elseif($this->input->post('ref_type')==="ref_websites"){
                $referencetype="Websites";
            }

            $referenceidprefix='Reference_'.$referencetype.'_';
            $referencedetails['reference_id']=uniqid($referenceidprefix);
            
            $referencedetails['title']=$this->input->post('title', TRUE);
            $referencedetails['description']=$this->input->post('description', TRUE);
            $referencedetails['added_date']=date("Y-m-d");
            $referencedetails['url']=$this->input->post('url', TRUE);

            $referencefilename='Reference_'.$referencetype.'_'.$referencedetails['title'];

            $referencedetails['batch']=$this->input->post('batch', TRUE);
            $referencedetails['degree_id']=$this->input->post('degree_id', TRUE);
            $referencedetails['acyear']=$this->input->post('acyear', TRUE);
            $referencedetails['semester']=$this->input->post('semester', TRUE);
            $referencedetails['module']=$this->input->post('module', TRUE);

            $this->load->model('coordinator/mreferences','mreferences');
            $this->load->library('nativesession');
            
            if(isset($_FILES['reference_file']['name']) && !empty($_FILES['reference_file']['name'])) {
                $this->load->model('mfile_handler');
                // Check and create the folders for the module
                $uploadertype='coordinators';
                $uploadtype='references';
                $folders=array(
                            1=>$referencedetails['batch'],
                            2=>$referencedetails['acyear'],
                            3=>$referencedetails['semester'],
                            4=>$referencedetails['module'],
                            5=>$referencetype
                );           
                $uploadpaths=$this->mfile_handler->create_folders($uploadertype,$uploadtype,$folders);

                $folderupload=$uploadpaths['folderupload'];
                $folderdb=$uploadpaths['folderdb'];

                $uploaddetails=$this->mreferences->upload_reference($referencefilename,$folderupload,$folderdb,'reference_file');

                if ($this->nativesession->get('uploadfailed')==TRUE) {
                    redirect('coordinator/references');  
                }

                $referencedetails['reference_path']= $uploaddetails['reference_path'];
                $referencedetails['file_name']= $uploaddetails['file_name'];

                $fileinfo = new SplFileInfo($uploaddetails['file_name']);
                $referencedetails['file_extension']= $fileinfo->getExtension();
            }

            $this->mreferences->save_reference($referencedetails,$referencetype);

            $this->nativesession->set('readyforcomment',TRUE);
            $this->nativesession->set('ref_type',$this->input->post('ref_type'));
            $this->nativesession->set('reference_id',$referencedetails['reference_id']);

            if($this->input->post('ref_type')==="ref_books"){
                $this->nativesession->set('reference_id_field','book_ref_id');
            }elseif($this->input->post('ref_type')==="ref_articles"){
                $this->nativesession->set('reference_id_field','article_ref_id');
            }elseif($this->input->post('ref_type')==="ref_videos"){
                $this->nativesession->set('reference_id_field','video_ref_id');
            }elseif($this->input->post('ref_type')==="ref_websites"){
                $this->nativesession->set('reference_id_field','website_ref_id');
            }
                
            redirect('coordinator/references');                    
                
        }

        
        public function update_reference() { 
            $this->load->helper('html');
            $this->load->helper('url');

            $this->load->database();

            if($this->input->post('ref_type')==="ref_books"){
                $referencetype="Books";
                $referencedetails['author']=$this->input->post('author', TRUE);
            }elseif($this->input->post('ref_type')==="ref_articles"){
                $referencetype="Articles";
                $referencedetails['author']=$this->input->post('author', TRUE);
            }elseif($this->input->post('ref_type')==="ref_videos"){
                $referencetype="Videos";
            }elseif($this->input->post('ref_type')==="ref_websites"){
                $referencetype="Websites";
            }

            $referencedetails['reference_id']=$this->input->post('book_ref_id', TRUE);

            $referencedetails['id']=$this->input->post('id', TRUE);
            $referencedetails['title']=$this->input->post('title', TRUE);
            $referencedetails['description']=$this->input->post('description', TRUE);
            $referencedetails['added_date']=date("Y-m-d");
            $referencedetails['url']=$this->input->post('url', TRUE);

            $referencefilename='Reference_'.$referencetype.'_'.$referencedetails['title'];

            $referencedetails['batch']=$this->input->post('batch', TRUE);
            $referencedetails['degree_id']=$this->input->post('degree_id', TRUE);
            $referencedetails['acyear']=$this->input->post('acyear', TRUE);
            $referencedetails['semester']=$this->input->post('semester', TRUE);
            $referencedetails['module']=$this->input->post('module', TRUE);
            
            $referencedetails['file_path_old']=$this->input->post('file_path_old', TRUE);
            $referencedetails['file_extension_old']=$this->input->post('file_extension_old', TRUE);

            $this->load->model('coordinator/mreferences','mreferences');
            $this->load->library('nativesession'); 
            if(isset($_FILES['reference_file']['name']) && !empty($_FILES['reference_file']['name'])) {
                $this->load->model('mfile_handler');
                $this->mfile_handler->delete_file($referencedetails['file_path_old']);
                // Check and create the folders for the module
                $uploadertype='coordinators';
                $uploadtype='references';
                $folders=array(
                            1=>$referencedetails['batch'],
                            2=>$referencedetails['acyear'],
                            3=>$referencedetails['semester'],
                            4=>$referencedetails['module'],
                            5=>$referencetype
                );           
                $uploadpaths=$this->mfile_handler->create_folders($uploadertype,$uploadtype,$folders);

                $folderupload=$uploadpaths['folderupload'];
                $folderdb=$uploadpaths['folderdb'];

                $uploaddetails=$this->mreferences->upload_reference($referencefilename,$folderupload,$folderdb,'reference_file');

                if ($this->nativesession->get('uploadfailed')==TRUE) {
                    redirect('coordinator/references');
                }

                $referencedetails['reference_path']= $uploaddetails['reference_path'];
                $referencedetails['file_name']= $uploaddetails['file_name'];

                $fileinfo = new SplFileInfo($uploaddetails['file_name']);
                $referencedetails['file_extension']= $fileinfo->getExtension();
            }else{
                $referencedetails['reference_path']= $this->input->post('file_path_old', TRUE);
                $referencedetails['file_extension']= $this->input->post('file_extension_old', TRUE);
            }

            $this->mreferences->update_reference($referencedetails,$referencetype);

            $this->nativesession->set('readyforcomment',TRUE);
            $this->nativesession->set('ref_type',$this->input->post('ref_type'));
            $this->nativesession->set('reference_id',$referencedetails['reference_id']);

            if($this->input->post('ref_type')==="ref_books"){
                $this->nativesession->set('reference_id_field','book_ref_id');
            }elseif($this->input->post('ref_type')==="ref_articles"){
                $this->nativesession->set('reference_id_field','article_ref_id');
            }elseif($this->input->post('ref_type')==="ref_videos"){
                $this->nativesession->set('reference_id_field','video_ref_id');
            }elseif($this->input->post('ref_type')==="ref_websites"){
                $this->nativesession->set('reference_id_field','website_ref_id');
            }

            redirect('coordinator/references');
        }
        
        public function delete_reference($referenceidfield,$referenceid,$tablename) { 
            $this->load->helper('html');
            $this->load->helper('url');

            $this->load->database();                

            $this->load->model('coordinator/mreferences','mreferences');
            $this->mreferences->delete_reference($referenceidfield,$referenceid,$tablename);

            redirect('coordinator/references');
        }
}

/* End of file references.php */
/* Location: ./application/controllers/coordinator/references.php */
