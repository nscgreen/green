<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/***************** LMS Site Lecturer Home Page********************/
class Timetable extends CI_Controller {
	
    public function update_timetable()
    {
        $this->load->helper('html');
        $this->load->helper('url');

        $timetabledetails['id']=$this->input->post('id', TRUE);        
        $timetabledetails['added_date']=date("Y-m-d");
        $timetabledetails['tt_batch_id']=$this->input->post('tt_batch_id', TRUE);   
        
        $this->load->model('mfile_handler');        
        $this->load->model('mtimetable');
        
        if(isset($_FILES['timetablefile']['name']) && !empty($_FILES['timetablefile']['name'])) {
            $this->load->model('mfile_handler');
            if($timetabledetails['file_path_old']!=="uploads_lms/coordinators/time_tables/Time_Table.pdf"){
                $this->mfile_handler->delete_file($timetabledetails['file_path_old']);
            }
            
            // Check and create the folders for the module
            $uploadertype='coordinators';
            $uploadtype='time_tables';
            $folders=array(
                        1=>$timetabledetails['tt_batch_id']
            );           

            $uploadpaths=$this->mfile_handler->create_folders($uploadertype,$uploadtype,$folders);

            $folderupload=$uploadpaths['folderupload'];
            $folderdb=$uploadpaths['folderdb'];
            
            $uploaddetails=$this->mtimetable->upload_timetable($timetabledetails['tt_batch_id'],$folderupload,$folderdb,'timetablefile');

            if ($this->nativesession->get('uploadfailed')==TRUE){                
                redirect(base_url());  
            }
           
            $timetabledetails['file_path']= $uploaddetails['timetablepath'];
        }else{
            $timetabledetails['file_path']=$this->input->post('file_path_old', TRUE);
        }

        $this->mtimetable->update_timetable($timetabledetails);
        redirect(base_url());  
    

    }

}

/* End of file home.php */
/* Location: ./application/controllers/coordinator/home.php */