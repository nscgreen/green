<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Download extends CI_Controller {
        public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}
        public function index($dbtable,$idfield,$value) {
            $query = $this->db->get_where($dbtable, array($idfield => $value));
            $row=$query->row();

            //$this->load->helper('file');
            $filepath=  base_url().'./'.$row->file_path;
            $filename= basename($filepath);
            header("Content-disposition: attachment; filename=$filename");
            header("Content-type: application-x/force-download");
            readfile("$filepath");
        }
    
}
	


/* End of file download.php */
/* Location: ./application/controllers/download.php */