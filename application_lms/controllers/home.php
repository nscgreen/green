<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/***************** LMS Site Main Home Page********************/
class Home extends CI_Controller {
	
        public function index()
	{
            $data = array(
                'title' => 'NSBM-LMS',
                'mDescription' => 'Learning Management System of National School of business Management',
                'mKeywords' => 'LMS',
            );
            $this->load->helper('html');
            $this->load->helper('url');
            $this->load->library('nativesession');
//            echo $this->input->get('user_type');
            
            $this->load->model('logged/mlg_user','mlg_user');
            $lg_data=$this->mlg_user->islogged_user();
            if(!$lg_data['islogged']===FALSE){
                if($lg_data['user_type']==="ST"){
                    redirect('student');
                }elseif($lg_data['user_type']==="CO"){
                    redirect('coordinator');
                }elseif($lg_data['user_type']==="PL" || $lg_data['user_type']==="VL"){
                    redirect('lecturer');
                }
                
            }else{
                //echo 'not logged';
                $this->load->view('vheader', $data);
                $this->load->view('vhome');
                $this->load->view('vfooter');
                
                // Unset all sesions
                $this->nativesession->delete('islogged');
                $this->nativesession->delete('mynsbm_id');
                $this->nativesession->delete('user_type');
                $this->nativesession->delete('st_index');
                $this->nativesession->delete('nick_name');
        };
          
	}
        
        public function logout(){
//            $lg_data['islogged'] === FALSE;

            // Unset all sesions
            $this->nativesession->delete('islogged');
            $this->nativesession->delete('mynsbm_id');
            $this->nativesession->delete('user_type');
            $this->nativesession->delete('st_index');
            $this->nativesession->delete('nick_name');
            redirect (base_url());
    }
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */