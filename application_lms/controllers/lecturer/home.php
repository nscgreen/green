<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/***************** LMS Site Lecturer Home Page********************/
class home extends CI_Controller {
	
        public function index()
	{
            $data = array(
                'title' => 'NSBM-LMS',
                'mDescription' => 'Learning Management System of National School of business Management',
                'mKeywords' => 'LMS',
            );
            $this->load->helper('html');
            $this->load->helper('url');
            $this->load->library('nativesession');
            
            $this->load->model('logged/mlg_user','mlg_user');
            $user_details=$this->mlg_user->load_menu_user_details();
            
            $this->load->model('mcomments');      
            $data['comments_lecturers']=TRUE;
            $data['comments_for_users']= $this->mcomments->get_comments_for_lecturer();
            
            $this->load->model('mtimetable');
            $data['time_table']= $this->mtimetable->get_coordinator_timetable($this->nativesession->get('mynsbm_id'));
            $data['time_table_coordinator']=TRUE;
            
            $this->load->view('vheader', $data);
            // Header Panel for Logged users
            if(!$user_details===FALSE){
                $this->load->view('vlg_menu',$user_details);
            }else{
                redirect (base_url()); //If not log
            }
            $this->load->view('lg_lecturer/vpl_home');
            $this->load->view('vright_panel');
            $this->load->view('vfooter');
            
	}

}

/* End of file home.php */
/* Location: ./application/controllers/lecturer/home.php */