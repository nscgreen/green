<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Select extends CI_Controller {
	
        public function index($selectedid)
	{
            $data = array(
                'title' => 'NSBM-LMS',
                'mDescription' => 'Learning Management System of National School of business Management',
                'mKeywords' => 'LMS',
                'selectedid'=>$selectedid
            );
            $this->load->helper('html');
            $this->load->helper('url');            
            
            $this->load->model('logged/mlg_user','mlg_user');
            $user_details=$this->mlg_user->load_menu_user_details();
            
            $this->load->model('mcomments');      
            $data['comments_lecturers']=TRUE;
            $data['comments_for_users']= $this->mcomments->get_comments_for_lecturer();
            
            $this->load->model('mtimetable');
            $data['time_table']= $this->mtimetable->get_coordinator_timetable($this->nativesession->get('mynsbm_id'));
            $data['time_table_coordinator']=TRUE;
            
            $this->load->view('vheader', $data);
            // Header Panel for Logged users
            if(!$user_details===FALSE){
                $this->load->model('lecturer/mselect','mselect');
                $batches=$this->mselect->get_lec_batches($this->nativesession->get('mynsbm_id'));
                $user_details['batches']=$batches;
                $user_details['lec_id']=$this->nativesession->get('mynsbm_id');
                
                $this->load->view('vlg_menu',$user_details);
            }else{
                redirect (base_url()); //If not log
            }
            if($selectedid=='assignments'){
                $this->load->view('lg_lecturer/vselect_asg_prj');
            }else{
                $this->load->view('lg_lecturer/vselect');
            }
            $this->load->view('vright_panel');
            $this->load->view('vfooter');
	}

        
        public function select_acyears_from_batch($batch){
            $this->db->select('ac_year');
            $this->db->from('batch');
            $this->db->where(array('batch_id'=>$batch));
            $query = $this->db->get();            
            $result=$query->row();
            
            echo '<select class="form-control" id="acyear" name="acyear" >';
            echo'<option value="-" >Select an Academic Year</option>';
            
            for($i=1;$i<=$result->ac_year;$i++) {
                echo'<option value="'.$i.'" >Academic Year ' .$i. '</option>';
            }
            echo '</select>';
        }
        
        public function select_lec_modules_from_b_a_s($batch,$acyear,$semester){
            
            if($this->nativesession->get('user_type')==="PL"){
                $this->db->select('m.name,m.module_id');
                $this->db->from('module as m');
                $this->db->join('plec_has_batch lb', 'm.module_id = lb.plec_batch_module_id');  
                $this->db->where(array('academic_year' => $acyear,'m.semester' => $semester,'plec_batch_id'=>$batch,'plec_id'=>$this->nativesession->get('mynsbm_id')));
                $query = $this->db->get();            
                $result=$query->result();
            }elseif($this->nativesession->get('user_type')==="PL"){
                $this->db->select('m.name,m.module_id');
                $this->db->from('module as m');
                $this->db->join('vlec_has_batch lb', 'm.module_id = lb.vlec_batch_module_id');  
                $this->db->where(array('academic_year' => $acyear,'m.semester' => $semester,'vlec_batch_id'=>$batch,'vlec_id'=>$this->nativesession->get('mynsbm_id')));
                $query = $this->db->get();             
                $result=$query->result();
            }
            echo '<select class="form-control" id="module" name="module" >';
            echo'<option value="-" >Select a Module</option>';
            
            foreach ($result as $module) {
                echo'<option value="'.$module->module_id.'" >' . $module->name . '</option>';
            }
            echo '</select>';
        }
        
}

/* End of file select.php */
/* Location: ./application/controllers/lecturer/select.php */