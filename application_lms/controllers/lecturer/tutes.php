<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tutes extends CI_Controller {
	
        public function index()
	{
            $this->load->helper(array('form', 'url'));
            $this->load->library('nativesession');
            $this->load->model('mdetails');
            if(isset($_POST['batch']) && isset($_POST['acyear']) && isset($_POST['semester']) && isset($_POST['module'])){
                $tutedetails['batch']=$this->input->post('batch', TRUE);
                $tutedetails['batch_name']=$this->mdetails->get_batch_name($tutedetails);
                
                $tutedetails['acyear']=$this->input->post('acyear', TRUE); 
                $tutedetails['semester']=$this->input->post('semester', TRUE);
                
                $tutedetails['module']=$this->input->post('module', TRUE); 
                $tutedetails['module_name']=$this->mdetails->get_module_name_by_batch($tutedetails);
                
                $this->nativesession->set('batch',$tutedetails['batch']);
                $this->nativesession->set('batch_name',$tutedetails['batch_name']);
                $this->nativesession->set('acyear',$tutedetails['acyear']);
                $this->nativesession->set('semester',$tutedetails['semester']);
                $this->nativesession->set('module',$tutedetails['module']);
                $this->nativesession->set('module_name',$tutedetails['module_name']);

            }elseif($this->nativesession->sessionIsset('batch')==TRUE){
                $tutedetails['batch']=$this->nativesession->get('batch');
                $tutedetails['batch_name']=$this->nativesession->get('batch_name');
                $tutedetails['acyear']=$this->nativesession->get('acyear');
                $tutedetails['semester']=$this->nativesession->get('semester');
                $tutedetails['module']=$this->nativesession->get('module');
                $tutedetails['module_name']=$this->nativesession->get('module_name');
            }else{
                redirect('lecturer/select/tutes');
            }
            
            $data = array(
            'title' => 'NSBM-LMS',
            'mDescription' => 'Learning Management System of National School of business Management',
            'mKeywords' => 'LMS');
                
            $dataall = array(
            'batch'=>$tutedetails['batch'],
            'batch_name'=>$tutedetails['batch_name'],
            'acyear'=>$tutedetails['acyear'],
            'semester'=>$tutedetails['semester'],
            'module'=>$tutedetails['module'],
            'module_name'=>$tutedetails['module_name'],

            );
            
            $this->load->model('lecturer/mtutes','mtutes');
            $dataall['tutes']= $this->mtutes->get_tutes($tutedetails);
            
            $this->load->helper('html');
            $this->load->helper('url');
            
            if($this->nativesession->sessionIsset('readyforcomment')==TRUE){
                $dataall['ready_for_comment']=$this->nativesession->get('readyforcomment');
                $dataall['tute_id']=$this->nativesession->get('tute_id');
                
                $this->nativesession->delete('readyforcomment');
                $this->nativesession->delete('tute_id');
                $dataall['view_edit_modal']=FALSE;
            }else{
                $dataall['ready_for_comment']=FALSE;
                if($this->nativesession->get('uploadfailed')==TRUE){
                    $dataall['view_edit_modal']=FALSE;
                    $dataall['upload_failed']=TRUE;
                    $this->nativesession->delete('uploadfailed');
                }else{
                    $dataall['upload_failed']=FALSE;
                    if(isset($_POST['id'])){
                        $dataall['view_edit_modal']=TRUE;
                    }else{
                        $dataall['view_edit_modal']=FALSE;
                    }
                }
            }
            
            $this->load->model('logged/mlg_user','mlg_user');
            $user_details=$this->mlg_user->load_menu_user_details();
            
            $this->load->model('mcomments');      
            $data['comments_lecturers']=TRUE;
            $data['comments_for_users']= $this->mcomments->get_comments_for_lecturer();
            
            $this->load->model('mtimetable');
            $data['time_table']= $this->mtimetable->get_coordinator_timetable($this->nativesession->get('mynsbm_id'));
            $data['time_table_coordinator']=TRUE;
            
            $this->load->view('vheader', $data);
            // Header Panel for Logged users
            if(!$user_details===FALSE){
                $this->load->view('vlg_menu',$user_details);
            }else{
                redirect (base_url()); //If not log
            }
            $this->load->view('lg_lecturer/vtutes',$dataall);
            $this->load->view('vright_panel');
            $this->load->view('vfooter');
	}
        
        public function add_tute() { 
            $this->load->helper('html');
            $this->load->helper('url');

            $this->load->database();

            if (isset($_FILES['tutefile']['name']) && !empty($_FILES['tutefile']['name'])) {
                $tutedetails['batch']=$this->input->post('batch', TRUE);
                $tutedetails['acyear']=$this->input->post('acyear', TRUE);
                $tutedetails['semester']=$this->input->post('semester', TRUE);
                $tutedetails['module']=$this->input->post('module', TRUE);

                $tutedetails['tute_name']=$this->input->post('tute_name', TRUE);
                $tutedetails['tute_description']=$this->input->post('tute_description', TRUE);

                $tutedetails['date']=date("Y-m-d");
                $tuteidprefix='Tute_';
                $tutedetails['tute_id']=uniqid($tuteidprefix);

                $tutefilename='Tute '.$tutedetails['tute_name'];                   

                $this->load->model('mfile_handler');

                // Check and create the folders for the module
                $uploadertype='lecturers';
                $uploadtype='tutes';
                $folders=array(
                            1=>$tutedetails['batch'],
                            2=>$tutedetails['acyear'],
                            3=>$tutedetails['semester'],
                            4=>$tutedetails['module']
                );
                $uploadpaths=$this->mfile_handler->create_folders($uploadertype,$uploadtype,$folders);

                $folderupload=$uploadpaths['folderupload'];
                $folderdb=$uploadpaths['folderdb'];

                $this->load->model('lecturer/mtutes','mtutes');

                $uploaddetails=$this->mtutes->upload_tute($tutefilename,$folderupload,$folderdb,'tutefile');

                $this->load->library('nativesession');
                if ($this->nativesession->get('uploadfailed')==TRUE) {
                    redirect('lecturer/tutes');  
                }

                $tutedetails['tutepath']= $uploaddetails['tutepath'];
                $tutedetails['filename']= $uploaddetails['filename'];

                $fileinfo = new SplFileInfo($uploaddetails['filename']);
                $tutedetails['file_extension']= $fileinfo->getExtension();

                $this->mtutes->save_tute($tutedetails);

                $this->nativesession->set('readyforcomment',TRUE);
                $this->nativesession->set('tute_id',$tutedetails['tute_id']);
                redirect('lecturer/tutes');                    
            }
        }

        
        public function update_tute() { 
            $this->load->helper('html');
            $this->load->helper('url');

            $this->load->database();
            $tutedetails=array();
            $tutedetails['id']=$this->input->post('id', TRUE);
            $tutedetails['tute_id']=$this->input->post('tute_id', TRUE);

            $tutedetails['batch']=$this->input->post('batch', TRUE);
            $tutedetails['acyear']=$this->input->post('acyear', TRUE);
            $tutedetails['semester']=$this->input->post('semester', TRUE);
            $tutedetails['module']=$this->input->post('module', TRUE);

            $tutedetails['tute_name']=$this->input->post('tute_name', TRUE);
            $tutedetails['tute_description']=$this->input->post('tute_description', TRUE);
            $tutedetails['date']=$this->input->post('date', TRUE);

            $tutedetails['file_path_old']=$this->input->post('file_path_old', TRUE);
            $tutedetails['file_extension_old']=$this->input->post('file_extension_old', TRUE);

            $this->load->model('lecturer/mtutes','mtutes');

            if (isset($_FILES['tutefile']['name']) && !empty($_FILES['tutefile']['name'])) {

                $this->load->model('mfile_handler');
                $this->mfile_handler->delete_file($tutedetails['file_path_old']);

                $tutefilename='Tute '.$tutedetails['tute_name'];

                //$this->load->model('mfile_handler');

                // Check and create the folders for the module
                $uploadertype='lecturers';
                $uploadtype='tutes';
                $folders=array(
                            1=>$tutedetails['batch'],
                            2=>$tutedetails['acyear'],
                            3=>$tutedetails['semester'],
                            4=>$tutedetails['module']
                );
                $uploadpaths=$this->mfile_handler->create_folders($uploadertype,$uploadtype,$folders);

                $folderupload=$uploadpaths['folderupload'];
                $folderdb=$uploadpaths['folderdb'];

                $uploaddetails=$this->mtutes->upload_tute($tutefilename,$folderupload,$folderdb,'tutefile');

                $tutedetails['tutepath']= $uploaddetails['tutepath'];
                $fileinfo = new SplFileInfo($uploaddetails['filename']);
                $tutedetails['file_extension']= $fileinfo->getExtension();

            }elseif (!isset($_FILES['tutefile']['name']) || empty($_FILES['tutefile']['name'])) {

                $tutedetails['tutepath']= $this->input->post('file_path_old', TRUE);
                $tutedetails['file_extension']= $this->input->post('file_extension_old', TRUE);
                echo $tutedetails['tutepath'];
            }
                $this->mtutes->update_tute($tutedetails);

                $this->load->library('nativesession');
                $this->nativesession->set('readyforcomment',TRUE);
                $this->nativesession->set('tute_id',$tutedetails['tute_id']);
                redirect('lecturer/tutes');
        }
        
        public function delete_tute($tute_id) { 
                $this->load->helper('html');
                $this->load->helper('url');

                $this->load->database();                
                $this->load->model('lecturer/mtutes','mtutes');
                $this->mtutes->delete_tute($tute_id);
                
                redirect('lecturer/tutes');
        }
}

/* End of file tutes.php */
/* Location: ./application/controllers/lecturer/tutes.php */