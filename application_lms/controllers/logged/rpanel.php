<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rpanel extends CI_Controller {
	
        public function index()
	{
            $this->load->helper('html');
            $this->load->helper('url');
            $this->load->view('vright_panel');
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */