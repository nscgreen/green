<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Assignments extends CI_Controller {
	
        public function index()
	{
            $this->load->helper(array('form', 'url'));
            $this->load->library('nativesession');
            $this->load->model('mdetails');
            if(isset($_POST['acyear']) && isset($_POST['semester']) && isset($_POST['module'])){
                
                $assignmentdetails['batch']=$this->nativesession->get('batch');
                $assignmentdetails['batch_name']=$this->nativesession->get('batch_name');
                
                $assignmentdetails['acyear']=$this->input->post('acyear', TRUE); 
                $assignmentdetails['semester']=$this->input->post('semester', TRUE);
                $assignmentdetails['module']=$this->input->post('module', TRUE);
                
                $assignmentdetails['module_name']=$this->mdetails->get_module_name_by_batch($assignmentdetails);
                
                $this->nativesession->set('batch',$assignmentdetails['batch']);
                $this->nativesession->set('batch_name',$assignmentdetails['batch_name']);
                $this->nativesession->set('acyear',$assignmentdetails['acyear']);
                $this->nativesession->set('semester',$assignmentdetails['semester']);
                $this->nativesession->set('module',$assignmentdetails['module']);
                $this->nativesession->set('module_name',$assignmentdetails['module_name']);

            }elseif($this->nativesession->sessionIsset('batch')==TRUE){
                $assignmentdetails['batch']=$this->nativesession->get('batch');
                $assignmentdetails['batch_name']=$this->nativesession->get('batch_name');
                $assignmentdetails['acyear']=$this->nativesession->get('acyear');
                $assignmentdetails['semester']=$this->nativesession->get('semester');
                $assignmentdetails['module']=$this->nativesession->get('module');
                $assignmentdetails['module_name']=$this->nativesession->get('module_name');
            }else{
                redirect('student/select/assignments');
            }
            
            
                        
            $data = array(
            'title' => 'NSBM-LMS',
            'mDescription' => 'Learning Management System of National School of business Management',
            'mKeywords' => 'LMS');
                
            $dataall = array(
            'batch'=>$assignmentdetails['batch'],
            'batch_name'=>$assignmentdetails['batch_name'],
            'acyear'=>$assignmentdetails['acyear'],
            'semester'=>$assignmentdetails['semester'],
            'module'=>$assignmentdetails['module'],
            'module_name'=>$assignmentdetails['module_name'],
            );
            
            $this->load->model('student/massignments','massignments');            
            $dataall['assignments']= $this->massignments->get_assignments($assignmentdetails);
            
            $dataall['submited_assignments']= $this->massignments->get_submitted_assignments($assignmentdetails);
            
            
            $this->load->model('mcomments');            
            $batch['batch_name']=$this->nativesession->get('batch_name');
            $batch['batch']=$this->nativesession->get('batch');   
            
            $data['comments_for_users']= $this->mcomments->get_comments_for_students_by_batch($batch);
            
            $this->load->model('mtimetable');
            $data['time_table_student']=TRUE;
            $data['time_table']= $this->mtimetable->get_student_timetable($batch['batch']);
            if($this->nativesession->sessionIsset('readyforcomment')==TRUE){
                $dataall['ready_for_comment']=$this->nativesession->get('readyforcomment');
                $dataall['assignment_id']=$this->nativesession->get('assignment_id');
                
                $this->nativesession->delete('readyforcomment');
                $this->nativesession->delete('assignment_id');
                $dataall['upload_assignment']=FALSE;
                $dataall['re_upload_assignment']=FALSE;
                $dataall['upload_failed']=FALSE;
            }else{
                $data['ready_for_comment']=FALSE;
                if(isset($_POST['assignment_id']) && isset($_POST['id'])){
                    $dataall['re_upload_assignment']=TRUE;
                    $dataall['upload_assignment']=FALSE;
                    $dataall['upload_failed']=FALSE;
                }elseif(isset($_POST['assignment_id'])){
                    $dataall['upload_assignment']=TRUE;
                    $dataall['re_upload_assignment']=FALSE;
                    $dataall['upload_failed']=FALSE;
                }elseif($this->nativesession->get('uploadfailed')==TRUE){
                    $dataall['upload_assignment']=FALSE;
                    $dataall['re_upload_assignment']=FALSE;
                    $dataall['upload_failed']=TRUE;
                    $this->nativesession->delete('uploadfailed');
                }else{
                    $dataall['upload_assignment']=FALSE;
                    $dataall['upload_failed']=FALSE;
                    $dataall['re_upload_assignment']=FALSE;

                }
            }
            
            $this->load->model('logged/mlg_user','mlg_user');
            $user_details=$this->mlg_user->load_menu_user_details();
            
            $this->load->view('vheader', $data);
            // Header Panel for Logged users
            if(!$user_details===FALSE){
                $this->load->view('vlg_menu',$user_details);
            }else{
                redirect (base_url());                
            }
            $this->load->view('lg_student/vassignments',$dataall);
            $this->load->view('vright_panel');
            $this->load->view('vfooter');
	}
        
        public function submit_assignment() { 
            $this->load->helper('html');
            $this->load->helper('url');

            $this->load->database();

            if (isset($_FILES['assignmentfile']['name']) && !empty($_FILES['assignmentfile']['name'])) {
                $assignmentdetails['batch']=$this->input->post('batch', TRUE);
                $assignmentdetails['acyear']=$this->input->post('acyear', TRUE);
                $assignmentdetails['semester']=$this->input->post('semester', TRUE);
                $assignmentdetails['module']=$this->input->post('module', TRUE);

                $assignmentdetails['assignment_id']=$this->input->post('assignment_id', TRUE);
                $assignmentdetails['assignment_name']=$this->input->post('assignment_name', TRUE);
                $assignmentdetails['assignment_description']=$this->input->post('assignment_description', TRUE);

                $assignmentdetails['submit_date']=$this->input->post('submit_date', TRUE);
                $assignmentdetails['submit_time']=$this->input->post('submit_time', TRUE);

                $assignmentdetails['student_submited_date']=date("Y-m-d");
                $assignmentdetails['student_submited_time']=date("h:i:s A");
                $assignmentdetails['student_note']=$this->input->post('student_note', TRUE);

                $assignmentidprefix='Assignment_Submission_';
                $assignmentdetails['student_submition_id']=uniqid($assignmentidprefix);

                $name = $_FILES["assignmentfile"]["name"];
                $ext = end((explode(".", $name))); 
                $assignmentfilename='BSC-UCD-CSC-13.1-028'.'.'.$ext;

                $assignmentdetails['student_id']="0001";
                
                $assignmentfileinfo = new SplFileInfo($this->input->post('assignment_file_path', TRUE));
                $assignmentdetails['aasignment_folder']=$assignmentfileinfo->getBasename('.'.$this->input->post('assignment_file_extension', TRUE));

                $this->load->model('mfile_handler');

                // Check and create the folders for the module
                $uploadertype='students';
                $uploadtype='assignments';
                $folders=array(
                            1=>$assignmentdetails['batch'],
                            2=>$assignmentdetails['acyear'],
                            3=>$assignmentdetails['semester'],
                            4=>$assignmentdetails['module']
                );           
                $uploadpaths=$this->mfile_handler->create_folders($uploadertype,$uploadtype,$folders);

                $folderupload=$uploadpaths['folderupload'];
                $folderdb=$uploadpaths['folderdb'];

                $this->load->model('student/massignments','massignments');

                $uploaddetails=$this->massignments->upload_assignment($assignmentfilename,$folderupload,$folderdb,'assignmentfile');

                $this->load->library('nativesession');
                if ($this->nativesession->get('uploadfailed')==TRUE) {
                    redirect('student/assignments');  
                }
                $assignmentdetails['assignment_path']= $uploaddetails['assignment_path'];
                $assignmentdetails['file_name']= $uploaddetails['file_name'];

                $fileinfo = new SplFileInfo($uploaddetails['file_name']);
                $assignmentdetails['file_extension']= $fileinfo->getExtension();

                $this->massignments->save_submited_assignment($assignmentdetails);

                $this->nativesession->set('readyforcomment',TRUE);
                $this->nativesession->set('assignment_id',$assignmentdetails['assignment_id']);
                redirect('student/assignments');     
            }
        }
        
        public function re_submit_assignment() { 
            $this->load->helper('html');
            $this->load->helper('url');

            $this->load->database();

            if (isset($_FILES['assignmentfile']['name']) && !empty($_FILES['assignmentfile']['name'])) {
                $assignmentdetails['batch']=$this->input->post('batch', TRUE);
                $assignmentdetails['acyear']=$this->input->post('acyear', TRUE);
                $assignmentdetails['semester']=$this->input->post('semester', TRUE);
                $assignmentdetails['module']=$this->input->post('module', TRUE);

                $assignmentdetails['id']=$this->input->post('id', TRUE);
                $assignmentdetails['assignment_id']=$this->input->post('assignment_id', TRUE);
                $assignmentdetails['assignment_name']=$this->input->post('assignment_name', TRUE);
                $assignmentdetails['assignment_description']=$this->input->post('assignment_description', TRUE);

                $assignmentdetails['submit_date']=$this->input->post('submit_date', TRUE);
                $assignmentdetails['submit_time']=$this->input->post('submit_time', TRUE);

                $assignmentdetails['file_path_old']=$this->input->post('file_path_old', TRUE);
                $assignmentdetails['file_extension_old']=$this->input->post('file_extension_old', TRUE);
                
                $assignmentdetails['student_submited_date']=date("Y-m-d");
                $assignmentdetails['student_submited_time']=date("h:i:s A");
                echo 'stu_note:'.$this->input->post('student_note', TRUE);
                $assignmentdetails['student_note']=$this->input->post('student_note', TRUE);

                $assignmentidprefix='Assignment_Submission_';
                $assignmentdetails['student_submition_id']=uniqid($assignmentidprefix);

                $name = $_FILES["assignmentfile"]["name"];
                $ext = end((explode(".", $name))); 
                $assignmentfilename='BSC-UCD-CSC-13.1-028'.'.'.$ext;

                $assignmentdetails['student_id']="0001";
                
                $assignmentfileinfo = new SplFileInfo($this->input->post('assignment_file_path', TRUE));
                $assignmentdetails['aasignment_folder']=$assignmentfileinfo->getBasename('.'.$this->input->post('assignment_file_extension', TRUE));

                $this->load->model('mfile_handler');
                
                if (isset($_FILES['assignmentfile']['name']) && !empty($_FILES['assignmentfile']['name'])) {
                    $this->mfile_handler->delete_file($assignmentdetails['file_path_old']);
                    // Check and create the folders for the module
                    $uploadertype='students';
                    $uploadtype='assignments';
                    $folders=array(
                                1=>$assignmentdetails['batch'],
                                2=>$assignmentdetails['acyear'],
                                3=>$assignmentdetails['semester'],
                                4=>$assignmentdetails['module']
                    );           
                    $uploadpaths=$this->mfile_handler->create_folders($uploadertype,$uploadtype,$folders);

                    $folderupload=$uploadpaths['folderupload'];
                    $folderdb=$uploadpaths['folderdb'];

                    $this->load->model('student/massignments','massignments');

                    $uploaddetails=$this->massignments->upload_assignment($assignmentfilename,$folderupload,$folderdb,'assignmentfile');

                    $this->load->library('nativesession');
                        if ($this->nativesession->get('uploadfailed')==TRUE) {
                            redirect('student/assignments');  
                        }
                    $assignmentdetails['assignment_path']= $uploaddetails['assignment_path'];
                    $assignmentdetails['file_name']= $uploaddetails['file_name'];

                    $fileinfo = new SplFileInfo($uploaddetails['file_name']);
                    $assignmentdetails['file_extension']= $fileinfo->getExtension();
                }elseif (!isset($_FILES['assignmentfile']['name']) || empty($_FILES['assignmentfile']['name'])) {
                    
                    $assignmentdetails['assignment_path']= $this->input->post('file_path_old', TRUE);
                    $assignmentdetails['file_name']= $this->input->post('file_extension_old', TRUE);
                }
                
                $this->massignments->update_submited_assignment($assignmentdetails);

                $this->nativesession->set('readyforcomment',TRUE);
                $this->nativesession->set('assignment_id',$assignmentdetails['assignment_id']);
                redirect('student/assignments');     
            }
        }        
        
}

/* End of file assignments.php */
/* Location: ./application/controllers/student/assignments.php */
