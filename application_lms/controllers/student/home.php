<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/***************** LMS Site Student Home Page********************/
class home extends CI_Controller {
	
        public function index()
	{
            $data = array(
                'title' => 'NSBM-LMS',
                'mDescription' => 'Learning Management System of National School of business Management',
                'mKeywords' => 'LMS',
            );
            
            $this->load->model('logged/mlg_user','mlg_user');
            $user_details=$this->mlg_user->load_menu_user_details();
            
            $this->load->model('mcomments');            
            $batch['batch_name']=$this->nativesession->get('batch_name');
            $batch['batch']=$this->nativesession->get('batch');   
            
            $data['comments_for_users']= $this->mcomments->get_comments_for_students_by_batch($batch);
            
            $this->load->model('mtimetable');
            $data['time_table_student']=TRUE;
            $data['time_table']= $this->mtimetable->get_student_timetable($batch['batch']);
            
            $this->load->view('vheader', $data);
            // Header Panel for Logged users
            if(!$user_details===FALSE){
                $this->load->view('vlg_menu',$user_details);
            }else{
                redirect (base_url());
            }
            $this->load->view('lg_student/vst_home');
            $this->load->view('vright_panel');
            $this->load->view('vfooter');
	}

}

/* End of file home.php */
/* Location: ./application/controllers/student/home.php */