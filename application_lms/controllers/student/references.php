<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class References extends CI_Controller {
	
        public function index()
	{
            $this->load->helper(array('form', 'url'));
            $this->load->model('mdetails');
            if(isset($_POST['acyear']) && isset($_POST['semester']) && isset($_POST['module'])){
                
                $referencedetails['batch']=$this->nativesession->get('batch');
                $referencedetails['batch_name']=$this->nativesession->get('batch_name');
                $referencedetails['degree_id']=$this->mdetails->get_degreeid_from_batch($referencedetails); 
                $referencedetails['acyear']=$this->input->post('acyear', TRUE); 
                $referencedetails['semester']=$this->input->post('semester', TRUE);
                $referencedetails['module']=$this->input->post('module', TRUE);
                $referencedetails['module_name']=$this->mdetails->get_module_name_by_batch($referencedetails);
                
                $this->nativesession->set('batch',$referencedetails['batch']);
                $this->nativesession->set('batch_name',$referencedetails['batch_name']);
                $this->nativesession->set('acyear',$referencedetails['acyear']);
                $this->nativesession->set('semester',$referencedetails['semester']);
                $this->nativesession->set('module',$referencedetails['module']);
                $this->nativesession->set('module_name',$referencedetails['module_name']);

            }elseif($this->nativesession->sessionIsset('batch')==TRUE){
                $referencedetails['batch']=$this->nativesession->get('batch');
                $referencedetails['batch_name']=$this->nativesession->get('batch_name');
                $referencedetails['degree_id']=$this->mdetails->get_degreeid_from_batch($referencedetails); 
                $referencedetails['acyear']=$this->nativesession->get('acyear');
                $referencedetails['semester']=$this->nativesession->get('semester');
                $referencedetails['module']=$this->nativesession->get('module');
                $referencedetails['module_name']=$this->nativesession->get('module_name');
            }else{
                redirect('student/select/references');
            }
            
            $data = array(
            'title' => 'NSBM-LMS',
            'mDescription' => 'Learning Management System of National School of business Management',
            'mKeywords' => 'LMS');
                
            $dataall = array(
            'degree_id'=>$referencedetails['degree_id'],
            'batch'=>$referencedetails['batch'],
            'batch_name'=>$referencedetails['batch_name'],
            'acyear'=>$referencedetails['acyear'],
            'semester'=>$referencedetails['semester'],
            'module'=>$referencedetails['module'],
            'module_name'=>$referencedetails['module_name'],
            );
            
            $this->load->model('student/mreferences','mreferences');
            $dataall['books']= $this->mreferences->get_references($referencedetails,'ref_books');
            $dataall['articles']= $this->mreferences->get_references($referencedetails,'ref_articles');
            $dataall['videos']= $this->mreferences->get_references($referencedetails,'ref_videos');
            $dataall['websites']= $this->mreferences->get_references($referencedetails,'ref_websites');
            
            $this->load->helper('html');
            
            $this->load->model('logged/mlg_user','mlg_user');
            $user_details=$this->mlg_user->load_menu_user_details();            
            
            $this->load->model('mcomments');
            
            $batch['batch_name']=$this->nativesession->get('batch_name');
            $batch['batch']=$this->nativesession->get('batch');   
            
            $data['comments_for_users']= $this->mcomments->get_comments_for_students_by_batch($batch);
            
            $this->load->model('mtimetable');
            $data['time_table_student']=TRUE;
            $data['time_table']= $this->mtimetable->get_student_timetable($batch['batch']);
            
            $this->load->view('vheader', $data);
            // Header Panel for Logged users
            if(!$user_details===FALSE){                
                $this->load->view('vlg_menu',$user_details);
            }else{
                redirect (base_url());               
            }
            $this->load->view('lg_student/vreferences',$dataall);
            $this->load->view('vright_panel');
            $this->load->view('vfooter');
	}
}

/* End of file references.php */
/* Location: ./application/controllers/student/references.php */