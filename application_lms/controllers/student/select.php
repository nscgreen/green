<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Select extends CI_Controller {
	
        public function index($selectedid)
	{
            $data = array(
                'title' => 'NSBM-LMS',
                'mDescription' => 'Learning Management System of National School of business Management',
                'mKeywords' => 'LMS',
                'selectedid'=>$selectedid
            );
            
            $this->load->library('nativesession');
            
            $this->load->model('logged/mlg_user','mlg_user');
            $user_details=$this->mlg_user->load_menu_user_details(); 
            
            $this->load->model('mcomments');
            
            $batch['batch_name']=$this->nativesession->get('batch_name');
            $batch['batch']=$this->nativesession->get('batch');   
            
            $data['comments_for_users']= $this->mcomments->get_comments_for_students_by_batch($batch);
            
            $this->load->model('mtimetable');
            $data['time_table_student']=TRUE;
            $data['time_table']= $this->mtimetable->get_student_timetable($batch['batch']);
            
            $this->load->view('vheader', $data);
            
            // Header Panel for Logged users
            if(!$user_details===FALSE){                
                $this->load->model('student/mselect','mselect');
                $details=$this->mselect->get_acyear_and_bacth($user_details['st_index']);
                $user_details['acyear']=$details['acyear'];
                $user_details['batch']=$details['batch'];
                
                $this->load->view('vlg_menu',$user_details);
            }else{
                redirect (base_url()); // If not log
            }
            if($selectedid=='assignments'){
                $this->load->view('lg_student/vselect_asg_prj');
            }else{
                $this->load->view('lg_student/vselect');
            }
            $this->load->view('vright_panel');
            $this->load->view('vfooter');
	}

        public function select_modules_from_b_a_s($batch,$acyear,$semester){
            $this->db->select('m.name,m.module_id');
            $this->db->from('module as m');
            $this->db->join('degree as d', 'm.module_degree_id = d.degree_id');
            $this->db->join('batch as b', 'd.degree_id = b.batch_degree_id');
            $this->db->where(array('academic_year' => $acyear,'m.semester' => $semester,'batch_id'=>$batch));
            $query = $this->db->get();            
            $result=$query->result();
            
            echo '<select class="form-control" id="module" name="module" >';
            foreach ($result as $module) {
                echo'<option value="'.$module->module_id.'" >' . $module->name . '</option>';
            }
            echo '</select>';
        }
        
}

/* End of file select.php */
/* Location: ./application/controllers/student/select.php */