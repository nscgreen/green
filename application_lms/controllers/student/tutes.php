<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tutes extends CI_Controller {
	
        public function index()
	{
            $this->load->helper(array('form', 'url'));
            $this->load->library('nativesession');
            $this->load->model('mdetails');
            if(isset($_POST['acyear']) && isset($_POST['semester']) && isset($_POST['module'])){
                
                $tutedetails['batch']=$this->nativesession->get('batch');
                $tutedetails['batch_name']=$this->nativesession->get('batch_name');
                
                $tutedetails['acyear']=$this->input->post('acyear', TRUE); 
                $tutedetails['semester']=$this->input->post('semester', TRUE);
                
                $tutedetails['module']=$this->input->post('module', TRUE); 
                $tutedetails['module_name']=$this->mdetails->get_module_name_by_batch($tutedetails); 
                
                $this->nativesession->set('batch',$tutedetails['batch']);
                $this->nativesession->set('batch_name',$tutedetails['batch_name']);
                $this->nativesession->set('acyear',$tutedetails['acyear']);
                $this->nativesession->set('semester',$tutedetails['semester']);
                $this->nativesession->set('module',$tutedetails['module']);
                $this->nativesession->set('module_name',$tutedetails['module_name']);

            }elseif($this->nativesession->sessionIsset('batch')==TRUE){
                $tutedetails['batch']=$this->nativesession->get('batch');
                $tutedetails['batch_name']=$this->nativesession->get('batch_name');
                $tutedetails['acyear']=$this->nativesession->get('acyear');
                $tutedetails['semester']=$this->nativesession->get('semester');
                $tutedetails['module']=$this->nativesession->get('module');
                $tutedetails['module_name']=$this->nativesession->get('module_name');
            }else{
                redirect('student/select/tutes');
            }
            
            $data = array(
            'title' => 'NSBM-LMS',
            'mDescription' => 'Learning Management System of National School of business Management',
            'mKeywords' => 'LMS');
                
            $dataall = array(
            'batch'=>$tutedetails['batch'],
            'batch_name'=>$tutedetails['batch_name'],
            'acyear'=>$tutedetails['acyear'],
            'semester'=>$tutedetails['semester'],
            'module'=>$tutedetails['module'],
            'module_name'=>$tutedetails['module_name'],

            );
            
            $this->load->model('student/mtutes','mtutes');
            $dataall['tutes']= $this->mtutes->get_tutes($tutedetails);
            
            $this->load->helper('html');
            $this->load->helper('url');
            
            $this->load->model('logged/mlg_user','mlg_user');
            $user_details=$this->mlg_user->load_menu_user_details(); 
            
            $this->load->model('mcomments');
            
            $batch['batch_name']=$this->nativesession->get('batch_name');
            $batch['batch']=$this->nativesession->get('batch');   
            
            $data['comments_for_users']= $this->mcomments->get_comments_for_students_by_batch($batch);
            
            $this->load->model('mtimetable');
            $data['time_table_student']=TRUE;
            $data['time_table']= $this->mtimetable->get_student_timetable($batch['batch']);
            
            $this->load->view('vheader', $data);
            // Header Panel for Logged users
            if(!$user_details===FALSE){                
                $this->load->view('vlg_menu',$user_details);
            }else{
                redirect (base_url()); // If not log
            }
            $this->load->view('lg_student/vtutes',$dataall);
            $this->load->view('vright_panel');
            $this->load->view('vfooter');
	}
        
}

/* End of file tutes.php */
/* Location: ./application/controllers/student/tutes.php */