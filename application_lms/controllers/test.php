<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Test extends CI_Controller {

	public function json_test(){            
            $array=array(
                1=>
                array(
                'module_code'=>'BCS201',
                'module_name'=>'Descrete Maths',
                'date'=>'2014-02-13',
                'start_time'=>'09.00 AM',
                'end_time'=>'12.00 PM'),
                2=>
                array(
                'module_code'=>'BCS202',
                'module_name'=>'Foundation Maths',
                'date'=>'2014-02-15',
                'start_time'=>'09.00 AM',
                'end_time'=>'12.00 PM',)
            );
            $encarray=  json_encode($array);
            
            $insert=array('array_content'=>$encarray);
            
            //$this->db->insert('testdb_store_array', $insert);
            
            $this->db->select('array_content');
            $query = $this->db->get('testdb_store_array');
            $result=$query->result();
            
            foreach ($result as $exam) {
            $dec=json_decode($exam->array_content);
            
                foreach ($dec as $value) {
                    echo $value->module_code;
                    echo '<br>';
                    echo $value->module_name;
                    echo '<br><br>';

                }
                    
            }
            
            //By array method
//            $result=$query->result_array();
//            
//            foreach ($result as $exam) {
//            $dec=json_decode($exam['array_content']);
//            
//                foreach ($dec as $value) {
//                    echo $value['module_code'];
//                    echo '<br>';
//                    echo $value['module_name'];
//                    echo '<br><br>';
//
//                }
//                    
//            }
            
        }
        
        
        public function create_view() { // for events timeline
            $this->load->helper('url');
            
            $viewname=$this->input->post('vname');
            $querypart=$this->input->post('query');
            
            $query='CREATE VIEW '.$viewname.' AS '.$querypart;
            $this->db->query($query);
            redirect(base_url().'test/test_q');
        }
        
        public function test_q() {//Test and output queries
            $this->load->helper('url');
            $this->load->helper('html');
            
            $this->db->order_by("a.id", "desc"); 
            $this->db->select('a.*,m.academic_year,b.batch_id,m.semester,a.module_id as a_module_id');
            $this->db->from('assignments as a');
            $this->db->join('batch as b', 'a.asg_batch_id = b.batch_id');
            $this->db->join('module as m', 'a.module_id = m.module_id');
            $query = $this->db->get();
            
            echo 'Query<br>';
            print_r($this->db->last_query());
            
            echo '
                <form action="'.base_url().'test/create_view" method="POST">
                    <input type="text" name="vname" />
                    <br/>
                    <textarea name="query" rows="10" cols="30"></textarea>
                    <br/>
                    <input type="submit" value="Create View" name="submit"/>
                </form>     ' ;
        }
                
        public function index() { // for events timeline
//            echo 'test';
//            $array=array(
//                'tet' => "bar",
//                2 => "foo",
//            );
//            
//            echo $array['tet'];
            
                   $query = $this->db->get('school');
                   $result=$query->result();
                   print_r($result);
                   echo '<br>';
//                   $newr=array_merge( $result);
//                   var_dump($newr);
//                   
//                   echo $newr->lec_name;
                   $result[0]->lec_name="Lec1";
                   
                   //$result['lec_name']="Lec1";
                   
                   var_dump($result);
                  // echo $result['lec_name'];
                   $size=  sizeof($result);
                  for($i=0;$i<$size;$i++){
                      //$array=$result;
                      $result[$i]->lec_name="Lec1";
                  }
                  var_dump($result);
//                  
//                  echo $result['lec_name'];
                  
//                  echo $result['school_id'];
                  
//                  echo $array[0]->school_id;
                  //echo $result->lec_name;
                   
        }
        
        public function replace() {
            $realfolder_part="abcdefghijk/";
            $realfolder_part=substr_replace($realfolder_part ,"",-1);
            echo $realfolder_part;
            

        }       
        
        
}


/* End of file test.php */
/* Location: ./application/controllers/test.php */
?>

<!--<form action="'.base_url().'test/create_view" method="POST">
    <input type="text" name="vname" />
    <input type="hidden" value="'$query'"/>
    <input type="submit" value="Create View" name="submit"/>
</form>-->