<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mreferences extends CI_Model {
        public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}
        
        public function get_reference_for_edit($referenceidfield,$referenceid,$tablename){
           $selectdata=array(
                    $referenceidfield=>$referenceid
                   );
           $query = $this->db->get_where($tablename, $selectdata);
           $result=$query->result();
           return $result;
        }
        
        public function get_references($referencedetails,$tablename){
           $this->db->order_by("id", "desc");
           $selectdata=array(
                    'degree_id'=>$referencedetails['degree_id'],
                    'ac_year'=>$referencedetails['acyear'],
                    'semester'=>$referencedetails['semester'],
                    'module'=>$referencedetails['module']
                   );
           $query = $this->db->get_where($tablename, $selectdata);
           $result=$query->result();
           
           if(empty($result)){
               return FALSE;
           }else{
               return $result;
           }
        }
        
        public function upload_reference($referencename,$folderupload,$folderdb,$inputfieldname){
                $this->load->model('mfile_handler');
                
                $config['upload_path'] = $folderupload;
		$config['allowed_types'] = 'gif|jpg|png|pdf|zip|doc|docx|ppt|pptx|sql';
                $config['file_name']	= $referencename;
//		$config['max_size']	= '2048';
                $config['overwrite']=TRUE;
                $this->load->model('mfile_handler');
                $referencefilename=$this->mfile_handler->do_upload($inputfieldname,$config);

                $uploaddetails['reference_path']= $folderdb.$referencefilename;
                $uploaddetails['file_name']= $referencefilename;
                
                return $uploaddetails;
        }
        
        public function save_reference($referencedetails,$referencetype){
            if($referencetype==="Books"){
                //Data set to insert into the Database
                $insertdata=array(
                    'id'=>null,
                    'degree_id'=>$referencedetails['degree_id'],
                    'ac_year'=>$referencedetails['acyear'],
                    'semester'=>$referencedetails['semester'],
                    'module'=>$referencedetails['module'],
                    'title'=>$referencedetails['title'],
                    'author'=>$referencedetails['author'],
                    'description'=>$referencedetails['description'],
                    'added_date'=>$referencedetails['added_date'],
                    'url'=>$referencedetails['url'],
                    'file_path'=>$referencedetails['reference_path'],
                    'file_extension'=>$referencedetails['file_extension'],
                    
                    );
                //Insert the event details into the database
                $this->db->insert('ref_books', $insertdata);
                
            }elseif($referencetype==="Articles"){
                //Data set to insert into the Database
                $insertdata=array(
                    'id'=>null,
                    'degree_id'=>$referencedetails['degree_id'],
                    'ac_year'=>$referencedetails['acyear'],
                    'semester'=>$referencedetails['semester'],
                    'module'=>$referencedetails['module'],
                    'title'=>$referencedetails['title'],
                    'author'=>$referencedetails['author'],
                    'description'=>$referencedetails['description'],
                    'added_date'=>$referencedetails['added_date'],
                    'url'=>$referencedetails['url'],
                    'file_path'=>$referencedetails['reference_path'],
                    'file_extension'=>$referencedetails['file_extension'],
                    
                    );
                //Insert the event details into the database
                $this->db->insert('ref_articles', $insertdata);
                
            }elseif($referencetype==="Videos"){
                //Data set to insert into the Database
                $insertdata=array(
                    'id'=>null,
                    'degree_id'=>$referencedetails['degree_id'],
                    'ac_year'=>$referencedetails['acyear'],
                    'semester'=>$referencedetails['semester'],
                    'module'=>$referencedetails['module'],
                    'title'=>$referencedetails['title'],
                    'description'=>$referencedetails['description'],
                    'added_date'=>$referencedetails['added_date'],
                    'url'=>$referencedetails['url'],
                    'file_path'=>$referencedetails['reference_path'],
                    'file_extension'=>$referencedetails['file_extension'],
                    
                    );
                //Insert the event details into the database
                $this->db->insert('ref_videos', $insertdata);
                
            }elseif($referencetype==="Websites"){
                //Data set to insert into the Database
                $insertdata=array(
                    'id'=>null,
                    'degree_id'=>$referencedetails['degree_id'],
                    'ac_year'=>$referencedetails['acyear'],
                    'semester'=>$referencedetails['semester'],
                    'module'=>$referencedetails['module'],
                    'title'=>$referencedetails['title'],
                    'description'=>$referencedetails['description'],
                    'added_date'=>$referencedetails['added_date'],
                    'url'=>$referencedetails['url'],
                    
                    );
                //Insert the event details into the database
                $this->db->insert('ref_websites', $insertdata);
                
            }
            
        }
        
        public function update_reference($referencedetails,$referencetype){
             if($referencetype==="Books"){
                //Data set to insert into the Database
                $updatedata=array(
                    'id'=>$referencedetails['id'],
                    'degree_id'=>$referencedetails['degree_id'],
                    'ac_year'=>$referencedetails['acyear'],
                    'semester'=>$referencedetails['semester'],
                    'module'=>$referencedetails['module'],
                    'title'=>$referencedetails['title'],
                    'author'=>$referencedetails['author'],
                    'description'=>$referencedetails['description'],
                    'added_date'=>$referencedetails['added_date'],
                    'url'=>$referencedetails['url'],
                    'file_path'=>$referencedetails['reference_path'],
                    'file_extension'=>$referencedetails['file_extension'],
                    
                    );
                //Update the Tute details in the database
                $this->db->where('id', $referencedetails['id']);
                $this->db->update('ref_books', $updatedata);
                
            }elseif($referencetype==="Articles"){
                //Data set to insert into the Database
                $updatedata=array(
                    'id'=>$referencedetails['id'],
                    'degree_id'=>$referencedetails['degree_id'],
                    'ac_year'=>$referencedetails['acyear'],
                    'semester'=>$referencedetails['semester'],
                    'module'=>$referencedetails['module'],
                    'title'=>$referencedetails['title'],
                    'author'=>$referencedetails['author'],
                    'description'=>$referencedetails['description'],
                    'added_date'=>$referencedetails['added_date'],
                    'url'=>$referencedetails['url'],
                    'file_path'=>$referencedetails['reference_path'],
                    'file_extension'=>$referencedetails['file_extension'],
                    
                    );
                //Insert the event details into the database
                $this->db->where('id', $referencedetails['id']);
                $this->db->update('ref_articles', $updatedata);
                
            }elseif($referencetype==="Videos"){
                //Data set to insert into the Database
                $updatedata=array(
                    'id'=>$referencedetails['id'],
                    'degree_id'=>$referencedetails['degree_id'],
                    'ac_year'=>$referencedetails['acyear'],
                    'semester'=>$referencedetails['semester'],
                    'module'=>$referencedetails['module'],
                    'title'=>$referencedetails['title'],
                    'description'=>$referencedetails['description'],
                    'added_date'=>$referencedetails['added_date'],
                    'url'=>$referencedetails['url'],
                    'file_path'=>$referencedetails['reference_path'],
                    'file_extension'=>$referencedetails['file_extension'],
                    
                    );
                //Insert the event details into the database
                $this->db->where('id', $referencedetails['id']);
                $this->db->update('ref_videos', $updatedata);
                
            }elseif($referencetype==="Websites"){
                //Data set to insert into the Database
                $updatedata=array(
                    'id'=>$referencedetails['id'],
                    'degree_id'=>$referencedetails['degree_id'],
                    'ac_year'=>$referencedetails['acyear'],
                    'semester'=>$referencedetails['semester'],
                    'module'=>$referencedetails['module'],
                    'title'=>$referencedetails['title'],
                    'description'=>$referencedetails['description'],
                    'added_date'=>$referencedetails['added_date'],
                    'url'=>$referencedetails['url'],
                    
                    );
                //Insert the event details into the database
                $this->db->where('id', $referencedetails['id']);
                $this->db->update('ref_websites', $updatedata);
            }
        }
        
        public function delete_reference($referenceidfield,$referenceid,$tablename){
           $query = $this->db->get_where($tablename, array(
                    $referenceidfield=>$referenceid
                   ));
           $row=$query->row();
           $filepath=$row->file_path;
           
           $this->load->model('mfile_handler');
           $this->mfile_handler->delete_file($filepath);
           
           $this->db->where($referenceidfield, $referenceid);
           $this->db->delete($tablename);
        }
        
        
}
