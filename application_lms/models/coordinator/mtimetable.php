<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mtimetable extends CI_Model {
        public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}
        
        public function get_coordinator_timetable($lec_id) {
            if($this->nativesession->get('user_type')==="CO"){
                $this->db->select('name,batch_id');
                $this->db->from('batch');              
                $this->db->where(array('batch_coordinator_id' => $lec_id));
                $query1 = $this->db->get();            
                $row1=$query1->row();
                
                $this->db->select('*');
                $this->db->from('time_table');              
                $this->db->where(array('tt_batch_id' => $row1->batch_id));
                $query = $this->db->get();            
                $result=$query->row();
                return $result;
                
                
            }
                    
        }
        
        public function get_acyear_and_bacth($st_index){
            $this->db->select('m.st_index,b.ac_year,b.batch_id');
            $this->db->from('my_nsbm_stu as m');
            $this->db->join('student as s', 'm.st_index = s.index_nm');
            $this->db->join('batch as b', 's.stu_batch_id = b.batch_id');
            $this->db->where(array('st_index' => $st_index));
            $query = $this->db->get();            
            $row=$query->row();
            
            $data['batch']=$row->batch_id;
            $data['acyear']=$row->ac_year;
            return $data;
        }
              
}
