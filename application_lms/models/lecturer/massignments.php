<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Massignments extends CI_Model {
        public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}
        
        public function get_assignments($assignmentdetails){
            $this->db->order_by("a.id", "desc"); 
            $this->db->select('a.*,m.academic_year,m.semester,m.name as module_name');
            $this->db->from('assignments as a');
            $this->db->join('batch as b', 'a.asg_batch_id = b.batch_id');
            $this->db->join('module as m', 'a.module_id = m.module_id');
            $this->db->where(array('asg_batch_id'=>$assignmentdetails['batch'],'academic_year' => $assignmentdetails['acyear'],'m.semester' => $assignmentdetails['semester'],'a.module_id'=>$assignmentdetails['module']));

            $query = $this->db->get();            
            $result=$query->result();
            $size=  sizeof($result);
            for($i=0;$i<$size;$i++){
                if($this->nativesession->get('user_type')==="PL" || $this->nativesession->get('user_type')==="CO"){
                    $this->db->select('m.pe_mynsbm_id,m.nick_name as lec_name');
                    $this->db->from('my_nsbm_pe as m');               
                    $this->db->where(array('m.pe_mynsbm_id' => $this->nativesession->get('mynsbm_id')));
                    $query = $this->db->get();            
                    $row=$query->row();

                    $result[$i]->lec_name=$row->lec_name;
                }elseif($this->nativesession->get('user_type')==="VL"){
                    $this->db->select('m.vl_mynsbm_id,m.nick_name as lec_name');
                    $this->db->from('my_nsbm_vl as m');               
                    $this->db->where(array('m.vl_mynsbm_id' => $this->nativesession->get('mynsbm_id')));
                    $query = $this->db->get();            
                    $row=$query->row();

                    $result[$i]->lec_name=$row->lec_name;
                }
            
            }
            return $result;
        }
        
        public function upload_assignment($assignmentname,$folderupload,$folderdb,$inputfieldname){
                $this->load->model('mfile_handler');
                
                $config['upload_path'] = $folderupload;
		$config['allowed_types'] = 'gif|jpg|png|pdf|zip|doc|docx|ppt|pptx|sql';
                $config['file_name']	= $assignmentname;
//		$config['max_size']	= '2048';
                $config['overwrite']=TRUE;
                $this->load->model('mfile_handler');
                $assignmentfilename=$this->mfile_handler->do_upload($inputfieldname,$config);

                $uploaddetails['assignmentpath']= $folderdb.$assignmentfilename;
                $uploaddetails['filename']= $assignmentfilename;
                
                return $uploaddetails;
        }
        
        public function save_assignment($assignmentdetails){
            //Data set to insert into the Database
            $insertdata=array(
//                'assignment_id'=>$assignmentdetails['assignment_id'],
                'asg_batch_id'=>$assignmentdetails['batch'],
                'module_id'=>$assignmentdetails['module'],
                'lecturer_id'=>$this->nativesession->get('mynsbm_id'),
                'assignment_name'=>$assignmentdetails['assignment_name'],
                'assignment_description'=>$assignmentdetails['assignment_description'],
                'submit_date'=>$assignmentdetails['submit_date'],
                'submit_time'=>$assignmentdetails['submit_time'],
                'add_date'=>$assignmentdetails['date'],
                'file_path'=>$assignmentdetails['assignmentpath'],
                'file_extension'=>$assignmentdetails['file_extension'],
                

            );
            //Insert the event details into the database
            $this->db->insert('assignments', $insertdata);
        }
        
        public function update_assignment($assignmentdetails){
            //Data set to insert into the Database
            $updatedata=array(
                'id'=>$assignmentdetails['id'],
                'asg_batch_id'=>$assignmentdetails['batch'],
                'module_id'=>$assignmentdetails['module'],
                'lecturer_id'=>$this->nativesession->get('mynsbm_id'),
                'assignment_name'=>$assignmentdetails['assignment_name'],
                'assignment_description'=>$assignmentdetails['assignment_description'],
                'submit_date'=>$assignmentdetails['submit_date'],
                'submit_time'=>$assignmentdetails['submit_time'],
                'add_date'=>$assignmentdetails['date'],
                'file_path'=>$assignmentdetails['assignmentpath'],
                'file_extension'=>$assignmentdetails['file_extension']
            );
            //Update the Tute details in the database
            $this->db->where('id', $assignmentdetails['id']);
            $this->db->update('assignments', $updatedata);
        }
        
        public function delete_assignment($assignment_id){
           $query = $this->db->get_where('assignments', array(
                    'id'=>$assignment_id
                   ));
           $row=$query->row();
           $filepath=$row->file_path;
           
           $this->load->model('mfile_handler');
           $this->mfile_handler->delete_file($filepath);
           
           $this->db->where('id', $assignment_id);
           $this->db->delete('assignments');
        }
        
        
}
