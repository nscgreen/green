<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mprojects extends CI_Model {
        public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}
        
        public function get_projects($projectdetails){
           $this->db->order_by("id", "desc");
           $selectdata=array(
                    'pr_batch_id'=>$projectdetails['batch'],
                    'ac_year'=>$projectdetails['acyear']
                   );
           $query = $this->db->get_where('projects', $selectdata);
           $result=$query->result();
           $size=  sizeof($result);
            for($i=0;$i<$size;$i++){
                if($this->nativesession->get('user_type')==="PL"){
                    $this->db->select('m.pe_mynsbm_id,m.nick_name as lec_name');
                    $this->db->from('my_nsbm_pe as m');               
                    $this->db->where(array('m.pe_mynsbm_id' => $this->nativesession->get('mynsbm_id')));
                    $query = $this->db->get();            
                    $row=$query->row();

                    $result[$i]->lec_name=$row->lec_name;
                }elseif($this->nativesession->get('user_type')==="VL"){
                    $this->db->select('m.vl_mynsbm_id,m.nick_name as lec_name');
                    $this->db->from('my_nsbm_vl as m');               
                    $this->db->where(array('m.vl_mynsbm_id' => $this->nativesession->get('mynsbm_id')));
                    $query = $this->db->get();            
                    $row=$query->row();

                    $result[$i]->lec_name=$row->lec_name;
                }
            
            }
            return $result;
        }
        
        public function upload_project($projectname,$folderupload,$folderdb,$inputfieldname){
                $this->load->model('mfile_handler');
                
                $config['upload_path'] = $folderupload;
		$config['allowed_types'] = 'gif|jpg|png|pdf|zip|doc|docx|ppt|pptx|sql|txt';
                $config['file_name']	= $projectname;
//		$config['max_size']	= '2048';
                $config['overwrite']=TRUE;
                $this->load->model('mfile_handler');
                $projectfilename=$this->mfile_handler->do_upload($inputfieldname,$config);

                $uploaddetails['projectpath']= $folderdb.$projectfilename;
                $uploaddetails['filename']= $projectfilename;
                
                return $uploaddetails;
        }
        
        public function save_project($projectdetails){
            //Data set to insert into the Database
            $insertdata=array(
                'pr_batch_id'=>$projectdetails['batch'],
                'ac_year'=>$projectdetails['acyear'],
                'project_name'=>$projectdetails['project_name'],
                'project_description'=>$projectdetails['project_description'],
                'submit_date'=>$projectdetails['submit_date'],
                'submit_time'=>$projectdetails['submit_time'],
                'add_date'=>$projectdetails['date'],
                'file_path'=>$projectdetails['projectpath'],
                'file_extension'=>$projectdetails['file_extension'],
                

            );
            //Insert the event details into the database
            $this->db->insert('projects', $insertdata);
        }
        
        public function update_project($projectdetails){
            //Data set to insert into the Database
            $updatedata=array(
                'id'=>$projectdetails['id'],
                'pr_batch_id'=>$projectdetails['batch'],
                'ac_year'=>$projectdetails['acyear'],
                'project_name'=>$projectdetails['project_name'],
                'project_description'=>$projectdetails['project_description'],
                'submit_date'=>$projectdetails['submit_date'],
                'submit_time'=>$projectdetails['submit_time'],
                'add_date'=>$projectdetails['date'],
                'file_path'=>$projectdetails['projectpath'],
                'file_extension'=>$projectdetails['file_extension'],
            );
            //Update the Tute details in the database
            $this->db->where('id', $projectdetails['id']);
            $this->db->update('projects', $updatedata);
        }
        
        public function delete_project($project_id){
           //$this->load->library('database'); 
           $query = $this->db->get_where('projects', array(
                    'id'=>$project_id
                   ));
           $row=$query->row();
           $filepath=$row->file_path;
           
           $this->load->model('mfile_handler');
           $this->mfile_handler->delete_file($filepath);
           
           $this->db->where('id', $project_id);
           $this->db->delete('projects');
        }
        
        
}
