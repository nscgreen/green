<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mselect extends CI_Model {
        public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}
        
        public function get_lec_batches($lec_id) {
            if($this->nativesession->get('user_type')==="PL"){
                $this->db->select('lb.plec_id,b.name as batch_name,b.batch_id');
                $this->db->from('plec_has_batch as lb');
                $this->db->join('batch as b', 'lb.plec_batch_id = b.batch_id');                
                $this->db->where(array('plec_id' => $lec_id));
                $query = $this->db->get();            
                $results=$query->result();
                return $results;
            }elseif($this->nativesession->get('user_type')==="VL"){
                $this->db->select('lb.plec_id,b.name as batch_name,b.batch_id');
                $this->db->from('plec_has_batch as lb');
                $this->db->join('batch as b', 'lb.plec_batch_id = b.batch_id');                
                $this->db->where(array('vlec_id' => $lec_id));
                $query = $this->db->get();            
                $results=$query->result();
                return $results;
                
            }
                    
        }
        
        public function get_acyear_and_bacth($st_index){
            $this->db->select('m.st_index,b.ac_year,b.batch_id');
            $this->db->from('my_nsbm_stu as m');
            $this->db->join('student as s', 'm.st_index = s.index_nm');
            $this->db->join('batch as b', 's.stu_batch_id = b.batch_id');
            $this->db->where(array('st_index' => $st_index));
            $query = $this->db->get();            
            $row=$query->row();
            
            $data['batch']=$row->batch_id;
            $data['acyear']=$row->ac_year;
            return $data;
        }
              
}
