<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mtutes extends CI_Model {
        public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}        
        
        public function get_tutes($tutedetails){           
            $this->db->order_by("t.id", "desc"); 
            $this->db->select('t.*,m.academic_year,m.semester,m.name as module_name');
            $this->db->from('tutes as t');
            $this->db->join('batch as b', 't.tut_batch_id = b.batch_id');
            $this->db->join('module as m', 't.tut_module_id = m.module_id');
            $this->db->where(array('tut_batch_id'=>$tutedetails['batch'],'academic_year' => $tutedetails['acyear'],'m.semester' => $tutedetails['semester'],'t.tut_module_id'=>$tutedetails['module']));
                        
            $query = $this->db->get();            
            $result=$query->result();
            return $result;
        }
        
        public function upload_tute($tutename,$folderupload,$folderdb,$inputfieldname){
                $this->load->model('mfile_handler');
                
                $config['upload_path'] = $folderupload;
		$config['allowed_types'] = 'gif|jpg|png|pdf|zip|doc|docx|ppt|pptx|sql';
                $config['file_name']	= $tutename;
//		$config['max_size']	= '2048';
                $config['overwrite']=TRUE;
                $this->load->model('mfile_handler');
                $tutefilename=$this->mfile_handler->do_upload($inputfieldname,$config);
                
                $uploaddetails['tutepath']= $folderdb.$tutefilename;
                $uploaddetails['filename']= $tutefilename;
                
                return $uploaddetails;
        }
        
        public function save_tute($tutedetails){
            //Data set to insert into the Database
            $insertdata=array(
                'tut_batch_id'=>$tutedetails['batch'],
                'tut_module_id'=>$tutedetails['module'],
                'tute_id'=>$tutedetails['tute_id'],
                'tute_name'=>$tutedetails['tute_name'],
                'tute_description'=>$tutedetails['tute_description'],
                'date'=>$tutedetails['date'],
                'file_path'=>$tutedetails['tutepath'],
                'file_extension'=>$tutedetails['file_extension'],               
            );
            //Insert the event details into the database
            $this->db->insert('tutes', $insertdata);
        }
        
        public function update_tute($tutedetails){
            //Data set to insert into the Database
            $updatedata=array(
                'id'=>$tutedetails['id'],
                'tut_batch_id'=>$tutedetails['batch'],
                'tut_module_id'=>$tutedetails['module'],
                'tute_id'=>$tutedetails['tute_id'],
                'tute_name'=>$tutedetails['tute_name'],
                'tute_description'=>$tutedetails['tute_description'],
                'date'=>$tutedetails['date'],
                'file_path'=>$tutedetails['tutepath'],
                'file_extension'=>$tutedetails['file_extension'],
            );
            //Update the Tute details in the database
            $this->db->where('id', $tutedetails['id']);
            $this->db->update('tutes', $updatedata);
        }
        
        public function delete_tute($tuteid){
           $query = $this->db->get_where('tutes', array(
                    'id'=>$tuteid
                   ));
           $row=$query->row();
           $filepath=$row->file_path;
           
           $this->load->model('mfile_handler');
           $this->mfile_handler->delete_file($filepath);
           
           $this->db->where('id', $tuteid);
           $this->db->delete('tutes');
        }
        
        
}
