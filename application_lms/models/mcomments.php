<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mcomments extends CI_Model {
        public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
//                $this->load->library('database');
	}
        
        public function save_comment($commentdetails){
                //Data set to insert into the Database
                $insertdata=array(
                    'batch_id'=>$commentdetails['batch'],
                    'lecturer_id'=>$commentdetails['lecturer_id'],
                    'comment'=>$commentdetails['comment'],
                    'start_date'=>$commentdetails['start_date'],
                    'start_time'=>$commentdetails['start_time'],
                    'end_date'=>$commentdetails['end_date'],
                    'end_time'=>$commentdetails['end_time'],                    
                );
                
                //Insert the event details into the database
                $this->db->insert('comments', $insertdata);
        }
        
        public function get_comments_for_students_by_batch($details){
            $this->db->order_by("c.id", "desc"); 
            $this->db->limit(10);
            $this->db->select('c.*');
            $this->db->from('comments as c');
//            $this->db->join('batch as b', 'b.batch_id = c.batch_id');
            $this->db->where(array('c.batch_id'=>$details['batch']));
//            $this->db->limit(10);
            $query = $this->db->get();            
            $result=$query->result();                      
            
            $size=  sizeof($result);
            for($i=0;$i<$size;$i++){
                    
                    $this->db->select('m.pe_mynsbm_id,m.nick_name as lec_name,profile_pic');
                    $this->db->from('my_nsbm_pe as m');               
                    $this->db->where(array('m.pe_mynsbm_id' => $result[$i]->lecturer_id));
                    $query = $this->db->get();            
                    $row1=$query->row();
                    
                    If($query->num_rows == 1){
//                        $result2=$query->row();
                        $result[$i]->lec_name=$row1->lec_name;
                        $result[$i]->lec_profile_pic=$row1->profile_pic;
                    }else {
                        $this->db->select('m.vl_mynsbm_id,m.nick_name as lec_name,profile_pic');
                        $this->db->from('my_nsbm_vl as m');               
                        $this->db->where(array('m.vl_mynsbm_id' => $result[$i]->lecturer_id));
                        $query2 = $this->db->get();  
                        $row2=$query2->row();
                        
                        If($query2->num_rows == 1){
                            $result[$i]->lec_name=$row2->lec_name;
                            $result[$i]->lec_profile_pic=$row2->profile_pic;
                        }
                    }               
            
            }
           
           return $result;
        }
        
        public function get_comments_for_lecturer(){
            $this->db->order_by("c.id", "desc"); 
            $this->db->limit(10);
            $this->db->select('c.*,b.name as batch_name');
            $this->db->from('comments as c');
            $this->db->join('batch as b', 'b.batch_id = c.batch_id');
//            $this->db->where(array('c.batch_id'=>$details['batch']));
//            $this->db->limit(10);
            $query = $this->db->get();            
            $result=$query->result();                      
            
            $size=  sizeof($result);
            for($i=0;$i<$size;$i++){
                    
                    $this->db->select('m.pe_mynsbm_id,m.nick_name as lec_name,profile_pic');
                    $this->db->from('my_nsbm_pe as m');               
                    $this->db->where(array('m.pe_mynsbm_id' => $result[$i]->lecturer_id));
                    $query = $this->db->get();            
                    $row1=$query->row();
                    
                    If($query->num_rows == 1){
//                        $result2=$query->row();
                        $result[$i]->lec_name=$row1->lec_name;
                        $result[$i]->lec_profile_pic=$row1->profile_pic;
                    }else {
                        $this->db->select('m.vl_mynsbm_id,m.nick_name as lec_name,profile_pic');
                        $this->db->from('my_nsbm_vl as m');               
                        $this->db->where(array('m.vl_mynsbm_id' => $result[$i]->lecturer_id));
                        $query2 = $this->db->get();  
                        $row2=$query2->row();
                        
                        If($query2->num_rows == 1){
                            $result[$i]->lec_name=$row2->lec_name;
                            $result[$i]->lec_profile_pic=$row2->profile_pic;
                        }
                    }               
            
            }
           
           return $result;
        }
        
        
        
        
}