<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdetails extends CI_Model {
        public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
//                $this->load->library('database');
	}
        
        public function get_batch_name($details) {
            //Get the batch name
            if(isset($details['batch'])){
            $this->db->select('name');
            $this->db->from('batch');
            $this->db->where(array('batch_id'=>$details['batch']));
            $query = $this->db->get();
            $row1=$query->row();
            $batch_name=$row1->name;
            return $batch_name;
            }
        }
        
        public function get_module_name_by_batch($details) {
            //Get the module name
            if(isset($details['module'])){
            $this->db->select('name');
            $this->db->from('module');
            $this->db->where(array('module_id'=>$details['module']));
            $query2 = $this->db->get();
            $row2=$query2->row();
            $module_name=$row2->name;
            return $module_name;
            }
        }
        
        public function get_batch_module_names($details) {
            //Get the batch name
            if(isset($details['batch'])){
            $this->db->select('name');
            $this->db->from('batch');
            $this->db->where(array('batch_id'=>$details['batch']));
            $query = $this->db->get();
            $row1=$query->row();
            $tutedetails2['batch_name']=$row1->name;
            }
            
            //Get the module name
            if(isset($details['module'])){
            $this->db->select('name');
            $this->db->from('module');
            $this->db->where(array('module_id'=>$details['module']));
            $query2 = $this->db->get();
            $row2=$query2->row();
            $tutedetails2['module_name']=$row2->name;
            }
            
            return $tutedetails2;
        }
        
        public function get_degreeid_from_batch($details) {
            //Get the degree id from batch id
            $this->db->select('d.degree_id,b.batch_id');
            $this->db->from('degree as d');
            $this->db->join('batch as b', 'd.degree_id = b.batch_degree_id');
            $this->db->where(array('batch_id'=>$details['batch']));
            $query = $this->db->get();
            $row1=$query->row();
            $degree_id=$row1->degree_id;
            
            return $degree_id;
        }
        
        
}
