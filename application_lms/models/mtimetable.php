<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mtimetable extends CI_Model {
        public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
//                $this->load->library('database');
	}
        
        public function get_coordinator_timetable($lec_id) {
            //echo $this->nativesession->get('user_type');
            if($this->nativesession->get('user_type')==="CO"){
                $this->db->select('name,batch_id');
                $this->db->from('batch');              
                $this->db->where(array('batch_coordinator_id' => $lec_id));
                $query1 = $this->db->get();            
                $row1=$query1->row();
                
                $this->db->select('t.*,b.name as batch_name');
                $this->db->from('time_table as t');  
                $this->db->join('batch as b','b.batch_id=t.tt_batch_id');
                $this->db->where(array('tt_batch_id' => $row1->batch_id));
                $query = $this->db->get();            
                $result=$query->row();
                return $result;
                
                
            }
                    
        }
        
        public function get_student_timetable($batch) {                
                $this->db->select('*');
                $this->db->from('time_table');  
                $this->db->where(array('tt_batch_id' => $batch));
                $query = $this->db->get();            
                $result=$query->row();
                return $result;
        }
        
        public function upload_timetable($assignmentname,$folderupload,$folderdb,$inputfieldname){
                $this->load->model('mfile_handler');
                
                $config['upload_path'] = $folderupload;
		$config['allowed_types'] = 'gif|jpg|png|pdf|zip|doc|docx|ppt|pptx|xls|xlsx|xl';
                $config['file_name']	= $assignmentname;
//		$config['max_size']	= '2048';
                $config['overwrite']=TRUE;
                $this->load->model('mfile_handler');
                $assignmentfilename=$this->mfile_handler->do_upload($inputfieldname,$config);

                $uploaddetails['timetablepath']= $folderdb.$assignmentfilename;
                $uploaddetails['filename']= $assignmentfilename;
                
                return $uploaddetails;
        }
        
        public function update_timetable($timetabledetails){
            //Data set to insert into the Database
            $updatedata=array(
//                'id'=>$timetabledetails['id'],
                'added_date'=>$timetabledetails['added_date'],
                'file_path'=>$timetabledetails['file_path'],
            );
            //Update the Tute details in the database
            $this->db->where(array('id'=>$timetabledetails['id'],'tt_batch_id'=>$timetabledetails['tt_batch_id']));
            $this->db->update('time_table', $updatedata);
        }
        
              
}
