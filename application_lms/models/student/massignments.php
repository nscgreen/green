<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Massignments extends CI_Model {
        public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}
        
        public function get_assignments($assignmentdetails){
            $this->db->order_by("a.id", "desc"); 
            $this->db->select('a.*,m.academic_year,b.batch_id,m.semester,m.module_id as m_module_id,m.name as module_name');
            $this->db->from('assignments as a');
            $this->db->join('batch as b', 'a.asg_batch_id = b.batch_id');
            $this->db->join('module as m', 'a.module_id = m.module_id');
            $this->db->where(array('asg_batch_id'=>$assignmentdetails['batch'],'a.module_id'=>$assignmentdetails['module']));
            $query = $this->db->get();            
            $result=$query->result();
           
           return $result;
        }
        
        public function get_submitted_assignments($assignmentdetails){  
            $st_index=$this->nativesession->get('mynsbm_id');
            //echo $st_index;
            $this->db->order_by("a_sub.id", "desc"); 
//            $this->db->select('a_sub.*,a.module_id as a_module_id,a.assignment_name,a.assignment_description,a.submit_date,stm.fk_student_id');
            $this->db->select('a_sub.*,a.id as a_assignment_id,a.asg_batch_id,a.module_id,a.lecturer_id,a.assignment_name,a.assignment_description,a.submit_date,a.submit_time,m.name as module_name,m.academic_year,b.name as batch_name,b.batch_id as batch,m.semester');
            $this->db->from('assignments_submitted as a_sub');
            $this->db->join('assignments as a', 'a_sub.assignment_id = a.id');
            $this->db->join('batch as b', 'a.asg_batch_id = b.batch_id');
            $this->db->join('module as m', 'a.module_id = m.module_id');
            $this->db->join('student_take_modules as stm', 'a_sub.id = stm.fk_assignment_id');
            $this->db->where(array('asg_batch_id'=>$assignmentdetails['batch'],'stm.fk_module_id'=>$assignmentdetails['module'],'stm.fk_student_id'=>$st_index));
            $query = $this->db->get();            
            $result=$query->result();
           
//            print_r($this->db->last_query());
//            //echo 'result';
//            var_dump($result);
           return $result;
        }
        
        public function upload_submited_assignment($assignmentname,$folderupload,$folderdb,$inputfieldname){
                $this->load->model('mfile_handler');
                echo '<br>';
                echo $folderupload;
                $config['upload_path'] = $folderupload;
		$config['allowed_types'] = 'gif|jpg|png|pdf|zip|doc|docx|ppt|pptx|sql|txt';
                $config['file_name']	= $assignmentname;
//		$config['max_size']	= '2048';
                $config['overwrite']=TRUE;
                $this->load->model('mfile_handler');
                $assignmentfilename=$this->mfile_handler->do_upload($inputfieldname,$config);

                $uploaddetails['assignmentpath']= $folderdb.$assignmentfilename;
                $uploaddetails['filename']= $assignmentfilename;
                
                return $uploaddetails;
        }
        
        public function upload_assignment($assignmentname,$folderupload,$folderdb,$inputfieldname){
                $this->load->model('mfile_handler');
                echo $inputfieldname;
                $config['upload_path'] = $folderupload;
//		$config['allowed_types'] = 'gif|jpg|png|pdf|zip|doc|docx|ppt|pptx|sql|txt';
                $config['allowed_types'] = '*';
                $config['file_name']	= $assignmentname;
//		$config['max_size']	= '2048';
                $config['overwrite']=TRUE;
                $assignmentfilename=$this->mfile_handler->do_upload($inputfieldname,$config);

                $uploaddetails['assignment_path']= $folderdb.$assignmentfilename;
                $uploaddetails['file_name']= $assignmentfilename;
                
                return $uploaddetails;
        }
        
        public function save_submited_assignment($assignmentdetails){
            //Data set to insert into the Database
            $insertdata=array(
                'assignment_id'=>$assignmentdetails['assignment_id'],
                'stu_submited_date'=>$assignmentdetails['student_submited_date'],
                'stu_submited_time'=>$assignmentdetails['student_submited_time'],
                'stu_note'=>$assignmentdetails['student_note'],
                'file_path'=>$assignmentdetails['assignment_path'],
                'file_extension'=>$assignmentdetails['file_extension'],
                

            );
            //Insert the submitted assignment details into the database
            $this->db->insert('assignments_submitted', $insertdata);
            
            //Insert data to student_take_modules table
            $st_index=$this->nativesession->get('st_index');
            $insertdata2=array(
                'fk_assignment_id'=>$assignmentdetails['assignment_id'],
                'fk_student_id'=>$st_index,
                'fk_module_id'=>$assignmentdetails['module']
            );
            //Insert the submitted assignment details into the database
            $this->db->insert('student_take_modules', $insertdata2);
            
        }
        
        public function update_submited_assignment($assignmentdetails){
            //Data set to insert into the Database
            $insertdata=array(
                'id'=>$assignmentdetails['id'],
                'assignment_id'=>$assignmentdetails['assignment_id'],
                'stu_submited_date'=>$assignmentdetails['student_submited_date'],
                'stu_submited_time'=>$assignmentdetails['student_submited_time'],
                'stu_note'=>$assignmentdetails['student_note'],
                'file_path'=>$assignmentdetails['assignment_path'],
                'file_extension'=>$assignmentdetails['file_extension'],
                

            );
            //Update the submitted assignment details into the database
            $this->db->where('id', $assignmentdetails['id']);
            $this->db->update('assignments_submitted', $insertdata);
            
            //Insert data to student_take_modules table
//            $st_index=$this->nativesession->get('st_index');
//            $insertdata2=array(
//                'fk_assignment_id'=>$assignmentdetails['id'],
//                'fk_student_id'=>$st_index,
//                'fk_module_id'=>$assignmentdetails['module'],
//                
//
//            );
//            //Insert the submitted assignment details into the database
//            $this->db->where('fk_assignment_id', $assignmentdetails['assignment_id']);
//            $this->db->update('student_take_modules', $insertdata2);
        }        
        
}
