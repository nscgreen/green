<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mexams extends CI_Model {
        public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}
        
        public function get_tutes($tutedetails){
           $this->db->order_by("id", "desc");
           $selectdata=array(
                    'batch'=>$tutedetails['batch'],
                    'acyear'=>$tutedetails['acyear'],
                    'semester'=>$tutedetails['semester'],
                    'module'=>$tutedetails['module']
                   );
           $query = $this->db->get_where('tutes', $selectdata);
           $result=$query->result();
           return $result;
        }
        
        
}
