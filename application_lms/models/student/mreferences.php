<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mreferences extends CI_Model {
        public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}
        
        public function get_reference_for_edit($referenceidfield,$referenceid,$tablename){
           $selectdata=array(
                    $referenceidfield=>$referenceid
                   );
           $query = $this->db->get_where($tablename, $selectdata);
           $result=$query->result();
           return $result;
        }
        
        public function get_references($referencedetails,$tablename){
           $this->db->order_by("id", "desc");
           $selectdata=array(
                    'degree_id'=>$referencedetails['degree_id'],
                    'ac_year'=>$referencedetails['acyear'],
                    'semester'=>$referencedetails['semester'],
                    'module'=>$referencedetails['module']
                   );
           $query = $this->db->get_where($tablename, $selectdata);
           $result=$query->result();
           if(empty($result)){
               return FALSE;
           }else{
               return $result;
           }
        }        
        
}
