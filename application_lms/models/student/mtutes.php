<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mtutes extends CI_Model {
        public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}
        
        public function get_tutes($tutedetails){
           
            $selectdata=array(
                    'tut_batch_id'=>$tutedetails['batch'],
                    'tut_module_id'=>$tutedetails['module']
                   );
            $this->db->order_by("id", "desc");
            $query = $this->db->get_where('tutes', $selectdata);
            $result=$query->result();
            return $result;
        }
        
        
}
