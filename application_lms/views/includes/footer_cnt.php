<!-- JavaScript -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script> <!-- Boostrap JS -->

    <script src="<?php echo base_url(); ?>assets/js/bst_formhelp.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bst_fileinput.min.js"></script>
    <script src="<?php echo base_url(); ?>assets_lms/js/newsTicker.min.js"></script>
    <script src="<?php echo base_url(); ?>assets_lms/js/site.js"></script>

<!-- FitText load START-->
    <script src="<?php echo base_url(); ?>assets/js/fittext.js"></script>
    <script>
        window.fitText( document.getElementById("responsive_headline") );
        window.fitText( document.getElementById("responsive_headline"), 1.2 ); // turn the compressor up (font will shrink a bit more aggressively)
        window.fitText( document.getElementById("responsive_headline"), 0.8 ); // turn the compressor down (font will shrink less aggressively)
        window.fitText( document.getElementById("responsive_headline"), 1.2, { minFontSize: '30px', maxFontSize: '80px' } );
    </script>
<!-- FitText load END-->

<!-- Ajax Wrapper loader -->
    <script>
        function loadwrap($div, $page) {
            $($div).load($page);
        }
        function calajax(page) { // for calendar
            $('#cal').load(page);
        }
    </script>
<!-- Ajax Wrapper loader End -->