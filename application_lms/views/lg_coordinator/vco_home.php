<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >, Anton Perera < antondilshanperera@gmail.com >
 * Description : 
 * Created on : Jun 13, 2014, 3:44:18 PM
 */

?>
<body onload='loadwrap("#cal", "http://lms.d2real.com/calendar/displaycal/");'>

    <div class="container" id="main-container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-md-offset-1">
                <img class="center-block" style="margin: 10px auto auto auto; width: 160px" src="<?php echo base_url();?>logos/lms.png">
                <h1 style="text-align: center">Welcome to NSBM LMS</h1>
                <hr class="hr-style-two" style="margin: auto auto 40px auto">
                
                <div class="blockquote-box clearfix">
                    <div class="square pull-left">
                        <span class="box-perpal glyphicon text-center"><b>T</b></span>
                    </div>
                    <h4 style="color: #AFAFAF">Upload Tutes</h4>
                    <p>Create a description about Modules tutes downloade Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a
                        ante.
                    </p>
                    <a href="<?php echo base_url();?>coordinator/select/tutes" class="btn btn-default pull-right" style="margin: auto 20px auto auto" name="select_id=1">Find more.. <i class="fa fa-angle-double-right"></i></a>
                </div>
                <div class="blockquote-box blockquote-primary clearfix">
                    <div class="square pull-left">
                        <span class="box-perpal glyphicon text-center" style="padding-top: 0px;"><b>A</b> <small><i>&</i></small> <b>P</b></span>
                    </div>
                    <h4 style="color: #357EBD">Upload Assignments & Projects</h4>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a
                        ante. 
                    </p>
                    <a href="<?php echo base_url();?>coordinator/select/assignments" class="btn btn-default pull-right" style="margin: auto 20px auto auto">Find more.. <i class="fa fa-angle-double-right"></i></a>
                </div>
                <div class="blockquote-box blockquote-success clearfix">
                    <div class="square pull-left">
                        <span class="box-perpal glyphicon text-center"><b>R</b></span>
                    </div>
                    <h4 style="color: #4CAE4C">Add References</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a
                        ante.
                    </p>
                    <a href="<?php echo base_url();?>coordinator/select/references" class="btn btn-default pull-right" style="margin: auto 20px auto auto">Find more.. <i class="fa fa-angle-double-right"></i></a>
                </div>
                <div class="blockquote-box blockquote-info clearfix">
                    <div class="square pull-left">
                        <span class="box-perpal glyphicon text-center"><b>S</b></span>
                    </div>
                    <h4 style="color: #46B8DA">Manage Students</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a
                        ante.
                    </p>
                    <a href="<?php echo base_url();?>coordinator/select/batch" class="btn btn-default pull-right" style="margin: auto 20px auto auto">Find more.. <i class="fa fa-angle-double-right"></i></a>
                </div>
                
            </div>
            <div class="col-lg-1 col-md-1 col-sm-1 hidden-xs" style="height: 100%"></div>
