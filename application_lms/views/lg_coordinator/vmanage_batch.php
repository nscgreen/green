<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >, Anton Perera < antondilshanperera@gmail.com >
 * Description : 
 * Created on : Jun 9, 2014, 12:33:43 AM
 */

?>
<body onload='loadwrap("#ajaxload","<?php echo base_url(); ?>logged/rpanel");'>

<div class="container" id="main-container">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-md-offset-1">
            <h1 style="text-align: center">Manage a Batch Students</h1>
            <hr class="hr-style-two" style="margin: auto auto 40px auto">
            <div style="border-left: 10px solid #0099ff; padding-left: 20px; margin: 20px auto" class="">
                <h4 style="margin: 35px auto auto auto">School : {School of Computing}</h4>
                <h4>Batch Name : {13.1 Computer Science}</h4>
            </div>
            
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Students Details</h3>
                    <div class="pull-right" style="margin-top: -18px; font-size: 15px;">
                        <a href="" type="submit" class="" style="color: #ffffff" data-toggle="modal" data-target="#fullmodel">
                            <span class="glyphicon glyphicon-resize-full"  data-toggle="tooltip" data-placement="top" title="Pull Details"></span>
                        </a>
                        <span class="clickable filter" data-toggle="tooltip" title="Filter Students" data-container="body" style="margin-left:5px;">
                            <i class="glyphicon glyphicon-filter"></i>
                        </span>
                    </div>
                </div>
                <div class="panel-body" style="display: none;">
                    <input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-table" placeholder="Filter Students" />
                </div>
                <table class="table table-hover" id="dev-table">
                    <thead>
                        <tr class="alert alert-info">
                            <th>#</th>
                            <th>Index</th>
                            <th>Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        for ($i = 0; $i < 10; $i++) {
                            echo '
                            <tr>
                            <td>' . $i . '</td>
                            <td>BSC-UCD-CSC-13.1-008</td>
                            <td>H.W.S.P Fonseka</td>
                            <td>
                                <ul class="list-inline" style="margin: 0; padding: 0;">
                                    <li><a href="" class="" type="submit" data-toggle="modal" data-target="#edit"><span class="glyphicon glyphicon-pencil" data-toggle="tooltip" data-placement="top" title="Update Details"></span></a></li>
                                    <li><a href="" class="" type="submit" data-toggle="modal" data-target="#delete"><span class="glyphicon glyphicon-remove" data-toggle="tooltip" data-placement="top" title="Remove Student Details"></span></a></li>
                                    <li><a href="" class="" type="submit" data-toggle="modal" data-target="#message"><span class="glyphicon glyphicon-envelope" data-toggle="tooltip" data-placement="top" title="Send a Message"></span></a></li>
                                    <li><a href="" class="" type="submit" data-toggle="modal" data-target="#profile"><span class="glyphicon glyphicon-user" data-toggle="tooltip" data-placement="top" title="Profile"></span></a></li>
                                </ul>
                            </td>
                            </tr>';
                        }
                        ?>
                        <tr>
                            <td>##</td>
                            <td>BSC-UCD-CSC-13.1-008</td>
                            <td>H.W.S.P Fonseka</td>
                            <td>
                                <ul class="list-inline" style="margin: 0; padding: 0;">
                                    <li><a href="" class="" type="submit" data-toggle="modal" data-target="#edit"><span class="glyphicon glyphicon-pencil" data-toggle="tooltip" data-placement="top" title="Update Details"></span></a></li>
                                    <li><a href="" class="" type="submit" data-toggle="modal" data-target="#delete"><span class="glyphicon glyphicon-remove" data-toggle="tooltip" data-placement="top" title="Remove Student Details"></span></a></li>
                                    <li><a href="" class="" type="submit" data-toggle="modal" data-target="#message"><span class="glyphicon glyphicon-envelope" data-toggle="tooltip" data-placement="top" title="Send a Message"></span></a></li>
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <br />
    <!--Batch Notice Section-->
            <h2 class="sub-header">Notice for batch</h2>
            <div class="well well-sm">
                <form class="form" role="form" action="" method="">
                    <div class="panel-body">
                        <div class="form-group">
                            <h4>Title</h4>
                            <div class="input-group">
                                <input type="text" class="form-control"/>
                                <span class="input-group-addon danger"><span class="glyphicon glyphicon-star"></span></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <h4>To</h4>
                            <div class="input-group">
                                <input type="text" class="form-control" value="13.1 Computing - CS" disabled/>
                                <span class="input-group-addon info"><span class="glyphicon glyphicon-tag"></span></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control counted" name="message" placeholder="Type in your message" rows="5" style="margin-bottom:10px;"></textarea>
                            <h6 class="pull-right" id="counter">320 characters remaining</h6>
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-info center-block" style="width: 100%">Send</button>
                    </div>
                </form>
            </div>
            <br />
    <!--Student Register Section-->
            <h2 style="border-bottom: 2px solid #00cc66">Register a new Student</h2>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Fill the register form</h3>
                    <span class="pull-right clickable-panel" style="margin-top: -23px; font-size: 15px; margin-right: -9px;" data-toggle="tooltip" data-placement="right" title="Click">
                        <i class="glyphicon glyphicon-minus"></i></span>
                </div>
                <div class="panel-body">
                    <form class="" role="form" action="" method="">
                        <div class="form-group">
                            <label class="lms-label">Index no :</label>
                            <input type="text" class="form-control" placeholder="Index no : (ex: BSC-UCD-CSC-13.1-008)">
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="lms-label">First Name :</label>
                                    <input type="text" class="form-control" placeholder="Enter student's first name here">
                                </div>
                                <div class="col-sm-6">
                                    <label class="lms-label">Last Name :</label>
                                    <input type="text" class="form-control" placeholder="Enter student's last name here">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="lms-label">Initials :</label>
                                    <input type="text" class="form-control" placeholder="Enter student's initials here">
                                </div>
                                <div class="col-sm-6">
                                    <label for="fileName" class="lms-label">Date of Birth</label><br />
                                    <div class="bfh-datepicker" data-format="y-m-d" data-date="today" style="float: left"></div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="lms-label">Address line 1 :</label>
                            <input type="text" class="form-control" placeholder="Enter student's first name here">
                        </div>
                        <div class="form-group">
                            <label class="lms-label">Address line 2 :</label>
                            <input type="text" class="form-control" placeholder="Enter student's first name here">
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="lms-label" for="textinput">City</label>
                                    <input type="text" placeholder="City" class="form-control">
                                </div>
                                <div class="col-sm-6">
                                    <label class="lms-label" for="textinput">State</label>
                                    <input type="text" placeholder="State" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-group"> <!-- Text input-->
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="lms-label" for="textinput">Postcode</label>
                                    <input type="text" placeholder="Post Code" class="form-control">    
                                </div>
                                <div class="col-sm-6"> <!-- Text input-->
                                    <label class="lms-label" for="textinput">Country</label>
                                    <input type="text" placeholder="Country" class="form-control">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="panel-footer">
                    <div class="pull-right">
                    <button class="btn btn-default">Close</button>
                    <button class="btn btn-success">Register</button>
                    </div>
                </div>
            </div>
            <br />
        </div>
        
        <div class="col-lg-1 col-md-1 col-sm-1 hidden-xs"></div>
        <div id="ajaxload"></div>
    </div>
    
</div>
    
<!--Student full details model-->
<div class="modal fade" id="fullmodel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="width: 100%;">
    <div class="modal-dialog modal-lg" style="width: 100%">
        <div class="modal-content" style="width: 100%;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Students Full Details</h4>
            </div>
            <div class="modal-body">
                <div class="panel panel-primary" style="margin: 0">
                    <div class="panel-heading">
                        <h3 class="panel-title">Students Full Details</h3>
                    </div>
                    <table class="table table-hover table-responsive" id="dev-table">
                        <thead>
                            <tr class="alert alert-info">
                                <th>#</th>
                                <th>Index</th>
                                <th>Name</th>
                                <th>Name</th>
                                <th>Name</th>
                                <th>Name</th>
                                <th>E-mail</th>
                                <th>Phone</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            for ($i = 0; $i < 40; $i++) {
                                echo'
                                    <tr>
                                    <td>' . $i . '</td>
                                    <td>BSC-UCD-CSC-13.1-008</td>
                                    <td>H.W.S.P Fonseka</td>
                                    <td>s.priyanga22@gmail.com</td>
                                    <td>s.priyanga22@gmail.com</td>
                                    <td>s.priyanga22@gmail.com</td>
                                    <td>s.priyanga22@gmail.com</td>
                                    <td>0779093013</td>
                                    </tr>
                                    ';
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div> <!--Student full details model END-->

<!--Student details edit model-->
<div id="edit" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Update Student Details</h4>
            </div>
            <div class="modal-body">
                <div class="panel panel-primary" style="margin: 0 auto">
                    <div class="panel-heading">
                        <h3 class="panel-title">Edit Details</h3>
                    </div>
                    <div class="panel-body">
                        <form class="" role="form" action="" method="">
                            <div class="form-group">
                                <label class="lms-label">Index no :</label>
                                <input type="text" class="form-control" placeholder="Index no : (ex: BSC-UCD-CSC-13.1-008)">
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label class="lms-label">First Name :</label>
                                        <input type="text" class="form-control" placeholder="Enter student's first name here">
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="lms-label">Last Name :</label>
                                        <input type="text" class="form-control" placeholder="Enter student's last name here">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label class="lms-label">Initials :</label>
                                        <input type="text" class="form-control" placeholder="Enter student's initials here">
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="fileName" class="lms-label">Date of Birth</label><br />
                                        <div class="bfh-datepicker" data-format="y-m-d" data-date="today" style="float: left"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="lms-label">Address line 1 :</label>
                                <input type="text" class="form-control" placeholder="Enter student's first name here">
                            </div>
                            <div class="form-group">
                                <label class="lms-label">Address line 2 :</label>
                                <input type="text" class="form-control" placeholder="Enter student's first name here">
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label class="lms-label" for="textinput">City</label>
                                        <input type="text" placeholder="City" class="form-control">
                                    </div>
                                    <div class="col-sm-6">
                                        <label class="lms-label" for="textinput">State</label>
                                        <input type="text" placeholder="State" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group"> <!-- Text input-->
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label class="lms-label" for="textinput">Postcode</label>
                                        <input type="text" placeholder="Post Code" class="form-control">    
                                    </div>
                                    <div class="col-sm-6"> <!-- Text input-->
                                        <label class="lms-label" for="textinput">Country</label>
                                        <input type="text" placeholder="Country" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="pull-right">
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="true">Close</button>
                    <button class="btn btn-success">Register</button>
                </div>
            </div>
        </div>
    </div>
</div> <!--Student details edit model END-->

<!--Delete Student model-->
<div id="delete" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #ff0066">Do you want to delete this Student..?</h4>
            </div>
            <div class="modal-body">
                <h4><small>Index :</small> BSC-UCD-CSC-13.1-008</h4>
                <h4><small>Batch :</small> 13.1 Computing</h4>
                <h4><small>Name :</small> H.W.S.P Fonseka</h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-danger">Yes</button>
            </div>
        </div>
    </div>
</div> <!--Delete Student model END-->

<!--Student Message model-->
<div id="message" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #ff0066">Type a Message</h4>
            </div>
            <div class="modal-body">
                <form class="form" role="form" action="" method="">
                        <div class="form-group">
                            <h4>Title</h4>
                            <div class="input-group">
                                <input type="text" class="form-control"/>
                                <span class="input-group-addon danger"><span class="glyphicon glyphicon-star"></span></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <h4>To</h4>
                            <div class="input-group">
                                <input type="text" class="form-control" value="BSC-UCD-SCS-13.1-008 - Fname" disabled/>
                                <span class="input-group-addon info"><span class="glyphicon glyphicon-user"></span></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="message" placeholder="Type in your message" rows="5" style="margin-bottom:10px;"></textarea>
                        </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-info center-block" style="width: 100%">Send</button>
            </div>
        </div>
    </div>
</div> <!--Student message model END-->

<!--Student Profile model-->
<div id="profile" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #00ccff">Student Profile</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-8 col-xs-8">
                        <h4>Name : <span class="prf-name"> &nbsp; A.S.D Sajith</span></h4>
                        <div class="prf-contact">
                        <h5>Voice : <span class=""> &nbsp; 0779093013</span></h5>
                        <h5>Email : <span class=""> &nbsp; spriyanga22@gmail.com</span></h5>
                        <h5>FB : <span class=""> &nbsp; http://fb.com/spriyanga22</span></h5>
                        <h5>Linkedin : <span class=""> &nbsp; http://fb.com/spriyanga22</span></h5>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-4">
                        <img class="image-responsive img-circle" src="http://placehold.it/120x120">
                    </div>
                </div>
                <div class="row">
                    <hr class="hr-style-two">
                    <div class="col-sm-12 col-xs-12">
                        <h4>Guardian Details :</h4>
                        <h5>Name : <span class=""> &nbsp; jjjjjjj</span></h5>
                        <h5>Voice : <span class=""> &nbsp; 0779093013</span></h5>
                        <h5>Email : <span class=""> &nbsp; spriyanga22@gmail.com</span></h5>
                        <h5>Address : <span class=""> &nbsp; 0779093013</span></h5>
                        <h5>Relationship : <span class=""> &nbsp; spriyanga22@gmail.com</span></h5>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-info center-block" style="width: 100%" data-dismiss="modal" aria-hidden="true">Close</button>
            </div>
        </div>
    </div>
</div> <!--Student Profile model END-->