<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >, Anton Perera < antondilshanperera@gmail.com >
 * Description : 
 * Created on : Jun 9, 2014, 12:33:43 AM
 */

?>
<?php 
if($ready_for_comment==TRUE){

?>
    <body onload='loadwrap("#cal", "http://lms.d2real.com/calendar/displaycal/"); loadmodalwith_formaction("#comment","#mcommentform","<?php echo base_url().'comments/add_comment/'; ?>");'>

<?php
}elseif($upload_failed==TRUE){
   
?>
    <body onload='loadwrap("#cal", "http://lms.d2real.com/calendar/displaycal/"); loadmodal("#uploadfailed");'>
    
<?php
}else{
    if($view_edit_modal==TRUE){
?>
    <body onload='loadwrap("#cal", "http://lms.d2real.com/calendar/displaycal/"); loadmodal("#edit");'>

<?php
    }else{
?>
    <body onload='loadwrap("#cal", "http://lms.d2real.com/calendar/displaycal/");'>        
<?php
    }
}
?>

        <div class="container" id="main-container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-md-offset-1">
                    <div style="border-left: 10px solid #0099ff; padding-left: 20px; margin: 20px auto" class="">
                        <h4 style="margin: 35px auto auto auto">Batch Name : <?php echo $batch_name; ?></h4>
                        <h4>Academic Year : <?php echo $acyear; ?></h4>
                        <h4>Semester : <?php echo $semester; ?></h4>
                        <h4>Module : <?php echo $module_name; ?></h4>
                    </div>

                    <h3 style="text-align: center; margin: 35px auto auto auto">Add new References</h3>
                    <hr class="hr-style-two">

                    <div class="tabbable">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab1" data-toggle="tab">Books</a></li>
                            <li><a href="#tab2" data-toggle="tab">Articles</a></li>
                            <li><a href="#tab3" data-toggle="tab">Videos</a></li>
                            <li><a href="#tab4" data-toggle="tab">Web Sites</a></li>
                        </ul>
                        <div class="tab-content" style="margin-top: 20px;">

                            <!-- *************** Start Books tab *************** -->
                            <div class="tab-pane active" id="tab1">
                                <form class="" role="form" action="<?php echo base_url() . 'coordinator/references/add_reference/' ?>" method="POST" enctype="multipart/form-data" id="ref_books">
                                    
                                    <input type="hidden" name="degree_id" value="<?php echo $degree_id; ?>"/>
                                    <input type="hidden" name="acyear" value="<?php echo $acyear; ?>"/>
                                    <input type="hidden" name="semester" value="<?php echo $semester; ?>"/>
                                    <input type="hidden" name="module" value="<?php echo $module; ?>"/>
                                    <!--   End Hidden fields to store Batch,Academic Year,Semester and Module-->

                                    <input type="hidden" name="ref_type" value="ref_books"/>

                                    <div class="form-group">
                                        <label for="fileName" class="lms-label">Title of the Book</label>
                                        <input type="text" class="form-control" placeholder="Enter the Title of the Book here.." name="title">
                                    </div>
                                    <div class="form-group">
                                        <label for="fileName" class="lms-label">Author of the Book</label>
                                        <input type="text" class="form-control" placeholder="Enter the Author of the Book here.." name="author">
                                    </div>
                                    <div class="form-group">
                                        <label for="fileName" class="lms-label">Description</label>
                                        <textarea class="form-control" rows="4" placeholder="Enter the description of the Book here .." name="description"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="fileName" class="lms-label">Link<small> to Download or Refer</small></label>
                                        <input type="text" class="form-control" value="http://" name="url">
                                    </div>
                                    <div class="form-group">
                                        <label for="fileName" class="lms-label">Select a file</label>
                                        <input id="input-2" type="file" class="file" data-show-upload="false" data-show-caption="true" name="reference_file">
                                    </div>

                                    <button type="submit" class="btn btn-success btn-lg" >Upload</button>
                                </form>
                            </div>
                            <!-- *************** End Books tab *************** -->

                            <!-- *************** Start Articles tab *************** -->
                            <div class="tab-pane " id="tab2">
                                <form class="" role="form" action="<?php echo base_url() . 'coordinator/references/add_reference' ?>" method="POST" enctype="multipart/form-data" id="ref_articles">
                                    
                                    <input type="hidden" name="degree_id" value="<?php echo $degree_id; ?>"/>
                                    <input type="hidden" name="acyear" value="<?php echo $acyear; ?>"/>
                                    <input type="hidden" name="semester" value="<?php echo $semester; ?>"/>
                                    <input type="hidden" name="module" value="<?php echo $module; ?>"/>
                                    <!--   End Hidden fields to store Batch,Academic Year,Semester and Module-->

                                    <input type="hidden" name="ref_type" value="ref_articles"/>

                                    <div class="form-group">
                                        <label for="fileName" class="lms-label">Title of the Article</label>
                                        <input type="text" class="form-control" placeholder="Enter the Title of the Article here.." name="title">
                                    </div>
                                    <div class="form-group">
                                        <label for="fileName" class="lms-label">Author of the Article</label>
                                        <input type="text" class="form-control" placeholder="Enter the Author of the Article here.." name="author">
                                    </div>
                                    <div class="form-group">
                                        <label for="fileName" class="lms-label">Description</label>
                                        <textarea class="form-control" rows="4" placeholder="Enter the description of the Article here .." name="description"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="fileName" class="lms-label">Link<small> to Download or Refer</small></label>
                                        <input type="text" class="form-control" value="http://" name="url">
                                    </div>
                                    <div class="form-group">
                                        <label for="fileName" class="lms-label">Select a file</label>
                                        <input id="input-2" type="file" class="file" data-show-upload="false" data-show-caption="true" name="reference_file">
                                    </div>

                                    <button type="submit" class="btn btn-success btn-lg" >Upload</button>
                                </form>
                            </div>
                            <!-- *************** End Articles tab *************** -->

                            <!-- *************** Start Videos tab *************** -->
                            <div class="tab-pane " id="tab3">
                                <form class="" role="form" action="<?php echo base_url() . 'coordinator/references/add_reference' ?>" method="POST" enctype="multipart/form-data">
                                    
                                    <input type="hidden" name="degree_id" value="<?php echo $degree_id; ?>"/>
                                    <input type="hidden" name="acyear" value="<?php echo $acyear; ?>"/>
                                    <input type="hidden" name="semester" value="<?php echo $semester; ?>"/>
                                    <input type="hidden" name="module" value="<?php echo $module; ?>"/>
                                    <!--   End Hidden fields to store Batch,Academic Year,Semester and Module-->

                                    <input type="hidden" name="ref_type" value="ref_videos"/>

                                    <div class="form-group">
                                        <label for="fileName" class="lms-label">Title  of the Video</label>
                                        <input type="text" class="form-control" placeholder="Enter the Title of the Video here.." name="title">
                                    </div>
                                    <div class="form-group">
                                        <label for="fileName" class="lms-label">Description</label>
                                        <textarea class="form-control" rows="4" placeholder="Enter the description of the Video here .." name="description"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="fileName" class="lms-label">Link<small> to Download or Refer</small></label>
                                        <input type="text" class="form-control" value="http://" name="url">
                                    </div>
                                    <div class="form-group">
                                        <label for="fileName" class="lms-label">Select a file</label>
                                        <input id="input-2" type="file" class="file" data-show-upload="false" data-show-caption="true" name="reference_file">
                                    </div>

                                    <button type="submit" class="btn btn-success btn-lg" >Upload</button>
                                </form>
                            </div>
                            <!-- *************** End Videos tab *************** -->

                            <!-- *************** Start Websites tab *************** -->
                            <div class="tab-pane " id="tab4">
                                <form class="" role="form" action="<?php echo base_url() . 'coordinator/references/add_reference' ?>" method="POST" enctype="multipart/form-data" id="ref_websites">
                                    
                                    <input type="hidden" name="degree_id" value="<?php echo $degree_id; ?>"/>
                                    <input type="hidden" name="acyear" value="<?php echo $acyear; ?>"/>
                                    <input type="hidden" name="semester" value="<?php echo $semester; ?>"/>
                                    <input type="hidden" name="module" value="<?php echo $module; ?>"/>
                                    <!--   End Hidden fields to store Batch,Academic Year,Semester and Module-->

                                    <input type="hidden" name="ref_type" value="ref_websites"/>

                                    <div class="form-group">
                                        <label for="fileName" class="lms-label">Name of the Website</label>
                                        <input type="text" class="form-control" placeholder="Enter the Name of the Website here.." name="title">
                                    </div>
                                    <div class="form-group">
                                        <label for="fileName" class="lms-label">Description</label>
                                        <textarea class="form-control" rows="4" placeholder="Enter the description of the Website here .." name="description"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="fileName" class="lms-label">Link<small> of the Website</small></label>
                                        <input type="text" class="form-control" value="http://" name="url">
                                    </div>

                                    <button type="submit" class="btn btn-success btn-lg" >Upload</button>
                                </form>
                            </div>
                            <!-- *************** End Websites tab *************** -->

                        </div>
                    </div>




                    <h3 style="text-align: center; margin: 35px auto auto auto">Uploaded References for students</h3>
                    <hr class="hr-style-two">

                    <div class="tabbable">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab_b" data-toggle="tab">Books</a></li>
                            <li><a href="#tab_a" data-toggle="tab">Articles</a></li>
                            <li><a href="#tab_v" data-toggle="tab">Videos</a></li>
                            <li><a href="#tab_w" data-toggle="tab">Web Sites</a></li>
                        </ul>
                        <div class="tab-content" style="margin-top: 20px;">

                            <!-- *************** Start Books tab *************** -->
                            <div class="tab-pane active" id="tab_b">
                                <?php
                                if (!$books == FALSE) {
                                    foreach ($books as $row) {
                                        ?>
                                        <div class="media">
                                            <a class="pull-left" href="#">
                                                <img class="media-object" src="http://placehold.it/128x128" alt="...">
                                            </a>
                                            <div class="media-body">
                                                <form class="form-inline" action="" role="form" style="padding: 0; margin: 0px 15px 0 0px" method="POST">
                                                    <input type="hidden" name="ref_type" value="ref_books"/>
                                                    <input type="hidden" name="id" value="<?php echo $row->id; ?>"/>

                                                    <div class="row pull-right" style="padding: 0; margin-left: 5px">
                                                        <button type="submit" style="border: 0;background: none;outline: 0;"><a class="" style=""><span class="glyphicon glyphicon-pencil"></span></a></button>
                                                    </div>
                                                </form>
                                                <div class="row pull-right" style="padding: 0; padding-right: 20px">
                                                    <a class="" onclick='loadmodalwith_formaction("#delete","#mdeleteform","<?php echo base_url() . 'coordinator/references/delete_reference/id/' . $row->id . '/ref_books'; ?>");' style="cursor: pointer;"><span class="glyphicon glyphicon-remove"></span></a>
                                                </div>
                                                <h4 class="media-heading"><?php echo $row->title; ?><br /><small>Author : <?php echo $row->author; ?></small></h4>
                                                <h5>Download link : <a href="<?php echo $row->url; ?>">{<?php echo $row->url; ?>}</a>
                                                    <form action="<?php echo base_url() . 'download/ref_books/id/' . $row->id; ?>">
                                                        <small> or </small><button type="submit" style="border: 0;background: none;outline: 0;"><a><i class="fa fa-download"></i></a></button></h5>
                                                </form>
                                                <p class="bg-success"><?php echo $row->description; ?></p>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <h3 style="text-align: center; margin: 35px auto auto auto">No Book References on the selected Module</h3>
                                    <br/>
                                    <a href="<?php echo base_url() . 'coordinator/select/referennces' ?>" class="btn btn-default">Select a different Module</a>

                                    <?php
                                }
                                ?>
                            </div>

                            <!-- *************** End Books tab *************** -->

                            <!-- *************** Start Articles tab *************** -->
                            <div class="tab-pane " id="tab_a">
                                <?php
                                if (!$articles == FALSE) {
                                    foreach ($articles as $row2) {
                                        ?>
                                        <div class="media">
                                            <a class="pull-left" href="#">
                                                <img class="media-object" src="http://placehold.it/128x128" alt="...">
                                            </a>
                                            <div class="media-body">
                                                <form class="form-inline" action="" role="form" style="padding: 0; margin: 0px 15px 0 0px" method="POST">

                                                    <input type="hidden" name="ref_type" value="ref_articles"/>
                                                    <input type="hidden" name="id" value="<?php echo $row2->id; ?>"/>

                                                    <div class="row pull-right" style="padding: 0; margin-left: 5px">
                                                        <button type="submit" style="border: 0;background: none;outline: 0;"><a class="" style=""><span class="glyphicon glyphicon-pencil"></span></a></button>
                                                    </div>
                                                </form>
                                                
                                                <div class="row pull-right" style="padding: 0;padding-right: 20px">
                                                    
                                                    <a class="" onclick='loadmodalwith_formaction("#delete","#mdeleteform","<?php echo base_url() . 'coordinator/references/delete_reference/id/' . $row2->id . '/ref_articles'; ?>");' style="cursor: pointer;"><span class="glyphicon glyphicon-remove"></span></a>
                                                    
                                                </div>
                                                
                                                <h4 class="media-heading"><?php echo $row2->title; ?><br /><small>Author : <?php echo $row2->author; ?></small></h4>
                                                <h5>Download link : <a href="<?php echo $row2->url; ?>">{<?php echo $row2->url; ?>}</a>
                                                    <form action="<?php echo base_url() . 'download/ref_articles/id/' . $row2->id; ?>">
                                                        <small> or </small><button type="submit" style="border: 0;background: none;outline: 0;"><a><i class="fa fa-download"></i></a></button></h5>
                                                </form>
                                                <p class="bg-success"><?php echo $row2->description; ?></p>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <h3 style="text-align: center; margin: 35px auto auto auto">No Article References on the selected Module</h3>
                                    <br/>
                                    <a href="<?php echo base_url() . 'coordinator/select/referennces' ?>" class="btn btn-default">Select a different Module</a>

                                    <?php
                                }
                                ?>
                            </div>

                            <!-- *************** End Articles tab *************** -->

                            <!-- *************** Start Videos tab *************** -->
                            <div class="tab-pane " id="tab_v">
                                <?php
                                if (!$videos == FALSE) {
                                    foreach ($videos as $row3) {
                                        ?>
                                        <div class="media">
                                            <a class="pull-left" href="#">
                                                <img class="media-object" src="http://placehold.it/128x128" alt="...">
                                            </a>
                                            <div class="media-body">
                                                <form class="form-inline" action="" role="form" style="padding: 0; margin: 0px 15px 0 0px" method="POST">

                                                    <input type="hidden" name="ref_type" value="ref_videos"/>
                                                    <input type="hidden" name="id" value="<?php echo $row3->id; ?>"/>

                                                    <div class="row pull-right" style="padding: 0; margin-left: 5px">
                                                        <button type="submit" style="border: 0;background: none;outline: 0;"><a class="" style=""><span class="glyphicon glyphicon-pencil"></span></a></button>
                                                    </div>
                                                </form>
                                                
                                                <div class="row pull-right" style="padding: 0;padding-right: 20px">
                                                    
                                                    <a class="" onclick='loadmodalwith_formaction("#delete","#mdeleteform","<?php echo base_url() . 'coordinator/references/delete_reference/id/' . $row3->id . '/ref_videos'; ?>");' style="cursor: pointer;"><span class="glyphicon glyphicon-remove"></span></a>
                                                    
                                                </div>
                                                
                                                <h4 class="media-heading"><?php echo $row3->title; ?><br /><small></small></h4>
                                                <h5>Download link : <a href="<?php echo $row3->url; ?>">{<?php echo $row3->url; ?>}</a>
                                                    <form action="<?php echo base_url() . 'download/ref_videos/id/' . $row3->id; ?>">
                                                        <small> or </small><button type="submit" style="border: 0;background: none;outline: 0;"><a><i class="fa fa-download"></i></a></button></h5>
                                                </form>
                                                <p class="bg-success"><?php echo $row3->description; ?></p>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <h3 style="text-align: center; margin: 35px auto auto auto">No Video References on the selected Module</h3>
                                    <br/>
                                    <a href="<?php echo base_url() . 'coordinator/select/referennces' ?>" class="btn btn-default">Select a different Module</a>

                                    <?php
                                }
                                ?>
                            </div>

                            <!-- *************** End Videos tab *************** -->

                            <!-- *************** Start Websites tab *************** -->
                            <div class="tab-pane " id="tab_w">
                                <?php
                                if (!$websites == FALSE) {
                                    foreach ($websites as $row4) {
                                        ?>
                                        <div class="media">
                                            <a class="pull-left" href="#">
                                                <img class="media-object" src="http://placehold.it/128x128" alt="...">
                                            </a>
                                            <div class="media-body">
                                                <form class="form-inline" action="" role="form" style="padding: 0; margin: 0px 15px 0 0px" method="POST">

                                                    <input type="hidden" name="ref_type" value="ref_websites"/>
                                                    <input type="hidden" name="id" value="<?php echo $row4->id; ?>"/>

                                                    <div class="row pull-right" style="padding: 0; margin-left: 5px">
                                                        <button type="submit" style="border: 0;background: none;outline: 0;"><a class="" style=""><span class="glyphicon glyphicon-pencil"></span></a></button>
                                                    </div>
                                                </form>
                                                <div class="row pull-right" style="padding: 0;padding-right: 20px">
                                                    <a class="" onclick='loadmodalwith_formaction("#delete","#mdeleteform","<?php echo base_url() . 'coordinator/references/delete_reference/id/' . $row4->id . '/ref_websites'; ?>");' style="cursor: pointer;"><span class="glyphicon glyphicon-remove"></span></a>
                                                </div>
                                                <h4 class="media-heading"><?php echo $row4->title; ?><br /><small></small></h4>
                                                <h5>Website link : <a href="<?php echo $row4->url; ?>">{<?php echo $row4->url; ?>}</a>
                                                    
                                                    <p class="bg-success"><?php echo $row4->description; ?></p>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <h3 style="text-align: center; margin: 35px auto auto auto">No Website References on the selected Module</h3>
                                    <br/>
                                    <a href="<?php echo base_url() . 'coordinator/select/referennces' ?>" class="btn btn-default">Select a different Module</a>

                                    <?php
                                }
                                ?>
                            </div>

                            <!-- *************** End Websites tab *************** -->

                        </div>
                    </div>



                </div>

                <div class="col-lg-1 col-md-1 col-sm-1 hidden-xs"></div>
                
                
<!-- ******************************************** Modals Start ******************************************** -->

<!-- Comment Modal Start-->
<div class="modal fade" id="comment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #00cc00">Reference Upload Successfully .!
                    <small>Do you want to inform about this..?</small></h4>
            </div>
            <form class="" role="form" action="" id="mcommentform" name="mcommentform" method="post">
                <div class="modal-body">
                    <!--   Hidden fields to store Batch,Academic Year,Semester and Module-->
                    <input type="hidden" name="batch" value="<?php echo $batch; ?>"/>

                    <input type="hidden" name="nexturl" value="coordinator/references"/>
                    <!--   End Hidden fields to store Batch,Academic Year,Semester and Module-->
                    <div class="form-group">
                        <label for="tcomment" class="lms-label">Comment to inform</label>
                        <textarea class="form-control" rows="3" placeholder="Enter comment here .." name="comment"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="fileName" class="lms-label">Start Date & Time <small>to Display</small></label><br />
                        <div class="bfh-datepicker" data-format="y-m-d" data-date="today" style="width: 50%; float: left" data-name="start_date" ></div>
                        <div class="bfh-timepicker" data-mode="12h" style="width: 45%; float: right" data-name="start_time" ></div>
                    </div>
                    <br /><br />
                    <div class="form-group">
                        <label for="fileName" class="lms-label">End Date & Time <small>to Display</small></label><br />
                        <div class="bfh-datepicker" data-format="y-m-d" data-date="today" style="width: 50%; float: left" data-name="end_date" ></div>
                        <div class="bfh-timepicker" data-mode="12h" style="width: 45%; float: right" data-name="end_time" ></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save<small> / Publish</small> Comment</button>
                </div>
            </form>
        </div>
    </div>
</div> 
<!-- Comment Modal End-->

<!-- Upload Failed Modal Start-->
<div class="modal fade" id="uploadfailed" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #ff0066">Assignment Upload Failed..?</h4>
            </div>
            <div class="modal-body">
                <p>There was an error on uploading the Tute. Try again ..!!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div> 
<!-- Upload Failed Modal End-->


<!-- Edit Modal Start-->
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #ffcc00">References Edit</h4>
            </div>

            <?php
            if (isset($_POST['ref_type']) && $_POST['ref_type'] === "ref_books") {
                foreach ($update_book_details as $row) {
                    ?>
                    <!-- Edit reference details for a book -->

                    <form class="" role="form" action="<?php echo base_url() . 'coordinator/references/update_reference/' ?>" method="POST" enctype="multipart/form-data">
                        <div class="modal-body">
                            <!--   Hidden fields to store Batch,Academic Year,Semester and Module-->
<!--                            <input type="hidden" name="batch" value="<?php //echo $row->batch; ?>"/>-->
                            <input type="hidden" name="degree_id" value="<?php echo $row->degree_id; ?>"/>
                            <input type="hidden" name="acyear" value="<?php echo $row->acyear; ?>"/>
                            <input type="hidden" name="semester" value="<?php echo $row->semester; ?>"/>
                            <input type="hidden" name="module" value="<?php echo $row->module; ?>"/>
                            <!--   End Hidden fields to store Batch,Academic Year,Semester and Module-->

                            <input type="hidden" name="id" value="<?php echo $row->id; ?>"/>
                            <input type="hidden" name="ref_type" value="ref_books"/>
                            <input type="hidden" name="id" value="<?php echo $row->id; ?>"/>

                            <input type="hidden" name="file_path_old" value="<?php echo $row->file_path; ?>"/>
                            <input type="hidden" name="file_extension_old" value="<?php echo $row->file_extension; ?>"/>


                            <div class="form-group">
                                <label for="fileName" class="lms-label">Title of the Book</label>
                                <input type="text" class="form-control" placeholder="Enter the Title of the Book here.." name="title" value="<?php echo $row->title; ?>">
                            </div>
                            <div class="form-group">
                                <label for="fileName" class="lms-label">Author of the Book</label>
                                <input type="text" class="form-control" placeholder="Enter the Author of the Book here.." name="author" value="<?php echo $row->author; ?>">
                            </div>
                            <div class="form-group">
                                <label for="fileName" class="lms-label">Description</label>
                                <textarea class="form-control" rows="4" placeholder="Enter the description of the Book here .." name="description"><?php echo $row->description; ?></textarea>
                            </div>
                            <div class="form-group">
                                <label for="fileName" class="lms-label">Link<small> to Download or Refer</small></label>
                                <input type="text" class="form-control" value="<?php echo $row->url; ?>" name="url">
                            </div>
                            <div class="form-group">
                                <label for="fileName" class="lms-label">Select a file</label>
                                <input id="input-2" type="file" class="file" data-show-upload="false" data-show-caption="true" name="reference_file">
                            </div>


                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-warning" >Upload</button>
                            <!--                                        </form>
                                                                    <form action="" method="POST">-->
                            <button type="button" class="btn btn-default" data-dismiss="modal"name="edit_modal_close">Close</button>
                            <!--                                                    </form>-->
                        </div>
                    </form>
                    <?php
                }
            } elseif (isset($_POST['ref_type']) && $_POST['ref_type'] === "ref_articles") {
                foreach ($update_article_details as $row) {
                    ?>
                    <!-- Edit reference details for a article -->
                    <form class="" role="form" action="<?php echo base_url() . 'coordinator/references/update_reference/' ?>" method="POST" enctype="multipart/form-data">
                        <div class="modal-body">
                            <!--   Hidden fields to store Batch,Academic Year,Semester and Module-->
<!--                            <input type="hidden" name="batch" value="<?php //echo $row->batch; ?>"/>-->
                            <input type="hidden" name="degree_id" value="<?php echo $row->degree_id; ?>"/>
                            <input type="hidden" name="acyear" value="<?php echo $row->acyear; ?>"/>
                            <input type="hidden" name="semester" value="<?php echo $row->semester; ?>"/>
                            <input type="hidden" name="module" value="<?php echo $row->module; ?>"/>
                            <!--   End Hidden fields to store Batch,Academic Year,Semester and Module-->

                            <input type="hidden" name="id" value="<?php echo $row->id; ?>"/>
                            <input type="hidden" name="ref_type" value="ref_articles"/>
                            <input type="hidden" name="id" value="<?php echo $row->id; ?>"/>

                            <input type="hidden" name="file_path_old" value="<?php echo $row->file_path; ?>"/>
                            <input type="hidden" name="file_extension_old" value="<?php echo $row->file_extension; ?>"/>


                            <div class="form-group">
                                <label for="fileName" class="lms-label">Title of the Article</label>
                                <input type="text" class="form-control" placeholder="Enter the Title of the Article here.." name="title" value="<?php echo $row->title; ?>">
                            </div>
                            <div class="form-group">
                                <label for="fileName" class="lms-label">Author of the Article</label>
                                <input type="text" class="form-control" placeholder="Enter the Author of the Article here.." name="author" value="<?php echo $row->author; ?>">
                            </div>
                            <div class="form-group">
                                <label for="fileName" class="lms-label">Description</label>
                                <textarea class="form-control" rows="4" placeholder="Enter the description of the Article here .." name="description"><?php echo $row->description; ?></textarea>
                            </div>
                            <div class="form-group">
                                <label for="fileName" class="lms-label">Link<small> to Download or Refer</small></label>
                                <input type="text" class="form-control" value="<?php echo $row->url; ?>" name="url">
                            </div>
                            <div class="form-group">
                                <label for="fileName" class="lms-label">Select a file</label>
                                <input id="input-2" type="file" class="file" data-show-upload="false" data-show-caption="true" name="reference_file">
                            </div>


                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-warning" >Upload</button>
                        </div>
                    </form>
                    <?php
                }
            } elseif (isset($_POST['ref_type']) && $_POST['ref_type'] === "ref_videos") {
                foreach ($update_video_details as $row) {
                    ?>
                    <!-- Edit reference details for a video -->
                    <form class="" role="form" action="<?php echo base_url() . 'coordinator/references/update_reference/' ?>" method="POST" enctype="multipart/form-data">
                        <div class="modal-body">
                            <!--   Hidden fields to store Batch,Academic Year,Semester and Module-->
<!--                            <input type="hidden" name="batch" value="<?php //echo $row->batch; ?>"/>-->
                            <input type="hidden" name="degree_id" value="<?php echo $row->degree_id; ?>"/>
                            <input type="hidden" name="acyear" value="<?php echo $row->acyear; ?>"/>
                            <input type="hidden" name="semester" value="<?php echo $row->semester; ?>"/>
                            <input type="hidden" name="module" value="<?php echo $row->module; ?>"/>
                            <!--   End Hidden fields to store Batch,Academic Year,Semester and Module-->

                            <input type="hidden" name="id" value="<?php echo $row->id; ?>"/>
                            <input type="hidden" name="ref_type" value="ref_videos"/>
                            <input type="hidden" name="id" value="<?php echo $row->id; ?>"/>

                            <input type="hidden" name="file_path_old" value="<?php echo $row->file_path; ?>"/>
                            <input type="hidden" name="file_extension_old" value="<?php echo $row->file_extension; ?>"/>


                            <div class="form-group">
                                <label for="fileName" class="lms-label">Title of the Video</label>
                                <input type="text" class="form-control" placeholder="Enter the Title of the Video here.." name="title" value="<?php echo $row->title; ?>">
                            </div>
                            <div class="form-group">
                                <label for="fileName" class="lms-label">Description</label>
                                <textarea class="form-control" rows="4" placeholder="Enter the description of the Video here .." name="description"><?php echo $row->description; ?></textarea>
                            </div>
                            <div class="form-group">
                                <label for="fileName" class="lms-label">Link<small> to Download or Refer</small></label>
                                <input type="text" class="form-control" value="<?php echo $row->url; ?>" name="url">
                            </div>
                            <div class="form-group">
                                <label for="fileName" class="lms-label">Select a file</label>
                                <input id="input-2" type="file" class="file" data-show-upload="false" data-show-caption="true" name="reference_file">
                            </div>


                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-warning" >Upload</button>
                        </div>
                    </form>
                    <?php
                }
            } elseif (isset($_POST['ref_type']) && $_POST['ref_type'] === "ref_websites") {
                foreach ($update_website_details as $row) {
                    ?>
                    <!-- Edit reference details for a website -->
                    <form class="" role="form" action="<?php echo base_url() . 'coordinator/references/update_reference/' ?>" method="POST" enctype="multipart/form-data">
                        <div class="modal-body">
                            <!--   Hidden fields to store Batch,Academic Year,Semester and Module-->
<!--                            <input type="hidden" name="batch" value="<?php //echo $row->batch; ?>"/>-->
                            <input type="hidden" name="degree_id" value="<?php echo $row->degree_id; ?>"/>
                            <input type="hidden" name="acyear" value="<?php echo $row->acyear; ?>"/>
                            <input type="hidden" name="semester" value="<?php echo $row->semester; ?>"/>
                            <input type="hidden" name="module" value="<?php echo $row->module; ?>"/>
                            <!--   End Hidden fields to store Batch,Academic Year,Semester and Module-->

                            <input type="hidden" name="id" value="<?php echo $row->id; ?>"/>
                            <input type="hidden" name="ref_type" value="ref_websites"/>
                            <input type="hidden" name="id" value="<?php echo $row->id; ?>"/>


                            <div class="form-group">
                                <label for="fileName" class="lms-label">Title of the Website</label>
                                <input type="text" class="form-control" placeholder="Enter the Title of the Website here.." name="title" value="<?php echo $row->title; ?>">
                            </div>
                            <div class="form-group">
                                <label for="fileName" class="lms-label">Description</label>
                                <textarea class="form-control" rows="4" placeholder="Enter the description of the Website here .." name="description"><?php echo $row->description; ?></textarea>
                            </div>
                            <div class="form-group">
                                <label for="fileName" class="lms-label">Link<small> of the Website</small></label>
                                <input type="text" class="form-control" value="<?php echo $row->url; ?>" name="url">
                            </div>


                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-warning" >Upload</button>
                        </div>
                    </form>
                    <?php
                }
            }
            ?>  
        </div>
    </div>
</div> 
<!-- Edit Modal End-->

<!-- Delete Modal Start-->
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #ff0066">Do you want to delete this Assignment..?</h4>
            </div>
            <div class="modal-body">
                <p>If you delete this Assignment, Assignment will no longer display any more, and it will remove from whole database</p>
            </div>
            <div class="modal-footer">
                <form method="post" action="" id="mdeleteform">
                <!--<input type="hidden" value="" id="modal_assignment_id" name="modal_assignment_id"/>-->
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    <button type="submit" class="btn btn-danger">Yes</button>
                </form>
            </div>
        </div>
    </div>
</div> 
<!-- Delete Modal End-->
                
<!-- ******************************************** Modals End ******************************************** -->        