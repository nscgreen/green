<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constiproject a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >, Anton Perera < antondilshanperera@gmail.com >
 * Description : 
 * Created on : Jun 13, 2014, 3:44:18 PM
 */

?>

<?php
if ($ready_for_comment == TRUE) {
    ?>
    <body onload='loadwrap("#cal", "http://lms.d2real.com/calendar/displaycal/"); loadmodalwith_formaction("#comment","#mcommentform","<?php echo base_url() . 'comments/add_comment/'; ?>");'>

        <?php
    } elseif ($upload_failed == TRUE) {
        ?>
    <body onload='loadwrap("#cal", "http://lms.d2real.com/calendar/displaycal/"); loadmodal("#uploadfailed");'>

    <?php
} else {
    if ($view_edit_modal == TRUE) {
        ?>
        <body onload='loadwrap("#cal", "http://lms.d2real.com/calendar/displaycal/"); loadmodal("#edit");'>

        <?php
    } else {
        ?>
        <body onload='loadwrap("#cal", "http://lms.d2real.com/calendar/displaycal/");'>        
            <?php
        }
    }
    ?>

    <div class="container" id="main-container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-md-offset-1">
                <div style="border-left: 10px solid #0099ff; padding-left: 20px; margin: 20px auto" class="">
                <h4 style="margin: 35px auto auto auto">Batch Name : <?php echo $batch_name; ?></h4>
                <h4>Academic Year : <?php echo $acyear; ?></h4>
                </div>
                
                <h3 style="text-align: center; margin: 35px auto auto auto">Upload Project details</h3>
                <hr class="hr-style-two">
                
        <!--Project Upload form-->
            <form class="" role="form" action="<?php echo base_url().'lecturer/projects/add_project' ?>" enctype="multipart/form-data" method="POST">
                <!--   Hidden fields to store Batch,Academic Year-->
                <input type="hidden" name="batch" value="<?php echo $batch; ?>"/>
                <input type="hidden" name="acyear" value="<?php echo $acyear; ?>"/>
                <!--   End Hidden fields to store Batch,Academic Year-->
                    <div class="form-group">
                        <label for="fileName" class="lms-label">Project Name</label>
                        <input type="text" class="form-control" placeholder="Enter Project name here .." name="project_name" required>
                    </div>
                    <div class="form-group">
                        <label for="fileName" class="lms-label">Project Description</label>
                        <textarea class="form-control" rows="4" placeholder="Enter Project description here .." name="project_description" required></textarea>
                    </div>
                    <div class="form-group">
                        <label for="fileName" class="lms-label">Submit Date & Time</label><br />
                        <div class="bfh-datepicker" data-format="y-m-d" data-date="today" style="width: 50%; float: left" data-name="submit_date" required></div>
                        <div class="bfh-timepicker" data-mode="12h" style="width: 45%; float: right" data-name="submit_time" required></div>
                    </div><br /><br />
                    <div class="form-group">
                        <label for="fileName" class="lms-label">Select a file</label>
                        <input id="input-2" type="file" class="file" data-show-upload="false" data-show-caption="true" name="projectfile" required>
                    </div>
                    
                    <button type="submit" class="btn btn-success btn-lg" >Upload</button>
            </form>
        
                
                <h3 style="text-align: center; margin: 35px auto auto auto">Added Project details</h3>
                <hr class="hr-style-two">
                
                <div class="well">
                    <?php
                    $i=1;
                    if(!empty($projects)){
                    foreach ($projects as $row){
                    ?>
                    <div class="row user-row" style="margin-bottom: 15px">
                        <div class="col-sm-2 col-xs-2">
                                <?php
                                $ext=$row->file_extension;
                                if($ext==="jpg" || $ext==="png" || $ext==="gif" || $ext==="bmp"){
                                    $imagesrc=base_url().'assets_lms/img/uploadthumbs/image.png';
                                }elseif($ext==="zip" || $ext==="rar" || $ext==="tar" || $ext==="gz"){
                                    $imagesrc=base_url().'assets_lms/img/uploadthumbs/zip.png';
                                }elseif($ext==="pdf"){
                                    $imagesrc=base_url().'assets_lms/img/uploadthumbs/pdf.png';
                                }elseif($ext==="doc" || $ext==="docx"){
                                    $imagesrc=base_url().'assets_lms/img/uploadthumbs/docx.png';
                                }elseif($ext==="xlsx" || $ext==="xls"){
                                    $imagesrc=base_url().'assets_lms/img/uploadthumbs/xlsx.png';
                                }elseif($ext==="pptx" || $ext==="ppsx" || $ext==="ppt" || $ext==="pps"){
                                    $imagesrc=base_url().'assets_lms/img/uploadthumbs/pptx.png';
                                }elseif($ext==="sql"){
                                    $imagesrc=base_url().'assets_lms/img/uploadthumbs/db.png';
                                }elseif($ext==="txt"){
                                    $imagesrc=base_url().'assets_lms/img/uploadthumbs/text.png';
                                } 
                                ?>
                        <img class="img-rounded" alt="Thumbanails" src="<?php echo $imagesrc ?>" width="60" height="80">
                        </div>
                        <div class="col-sm-6 col-xs-6 alert alert-info">
                            <h4>Project Name : <?php echo $row->project_name ?></h4>
                            <span class="text-muted">Submit Date: <?php echo $row->submit_date ?></span>
                        </div>
                        <div class="col-sm-2 col-xs-2" style="margin: 15px auto auto 10px">
                                <div class="row" style="padding: 0; margin-bottom: 5px">
                                    <form action="" method="POST">
                                        <!--   Hidden fields to store Batch,Academic Year-->
                                        <input type="hidden" name="batch" value="<?php echo $row->pr_batch_id; ?>"/>
                                        <input type="hidden" name="acyear" value="<?php echo $row->ac_year; ?>"/>
                                        <!--   End Hidden fields to store Batch,Academic Year-->

                                        <!--   Hidden fields for Project details and Module-->
                                        <input type="hidden" name="id" value="<?php echo $row->id; ?>"/>
                                        <input type="hidden" name="project_name" value="<?php echo $row->project_name; ?>"/>
                                        <input type="hidden" name="project_description" value="<?php echo $row->project_description; ?>"/>
                                        <input type="hidden" name="submit_date" value="<?php echo $row->submit_date; ?>"/>
                                        <input type="hidden" name="submit_time" value="<?php echo $row->submit_time; ?>"/>
                                        <input type="hidden" name="date" value="<?php echo $row->add_date; ?>"/>                                                
                                        <input type="hidden" name="file_path_old" value="<?php echo $row->file_path; ?>"/>
                                        <input type="hidden" name="file_extension_old" value="<?php echo $row->file_extension; ?>"/>
                                        <!--   End Hidden fields for Project details and Module-->  

                                        <button type="submit" class="btn btn-warning btn-sm" >Edit</button>
                                    </form>   
                                </div>
                                <div class="row" style="padding: 0">
                                    <button type="button" class="btn btn-danger btn-sm" onclick='loadmodalwith_formaction("#delete","#mdeleteform","<?php echo base_url().'lecturer/projects/delete_project/'.$row->id; ?>");'>Delete</button>
                                    
                                </div>
                        </div>
                        <div class="col-sm-1 col-xs-1 dropdown-user" data-for=".<?php echo $i ?>" >
                            <i class="fa fa-angle-down fa-2x text-muted" style="margin-left: 10px;"></i>
                        </div>
                    </div>
                    
                    <div class="row-fluid user-infos <?php echo $i ?>" id="1">
                        <div class="col-sm-12 col-xs-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Project Details</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <table class="table table-condensed table-responsive table-user-information">
                                                <tbody>
                                                <tr>
                                                    <td>Academic year</td>
                                                    <td> : </td>
                                                    <td><?php echo $row->ac_year ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Lecturer</td>
                                                    <td> : </td>
                                                    <td><?php echo $row->lec_name ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Description</td>
                                                    <td> : </td>
                                                    <td><?php echo $row->project_description ?></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                
                                    <span class="pull-right">
                                        <form action="<?php echo base_url().'download/projects/id/'.$row->id; ?>">
                                
                                            <button type="submit" class="btn btn-default" type="button"
                                                data-toggle="tooltip"
                                                data-original-title="Download Project Details"><i class="fa fa-download"></i>
                                            </button>
                                        </form>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                        $i++;
                        }
                    }else{
                    ?>
                        <h3 style="text-align: center; margin: 35px auto auto auto">No Project Details for the selected Batch on the selected Academic Year. </h3>
                        <br/>
                        <a href="<?php echo base_url().'lecturer/select/assignments'?>" class="btn btn-default">Select a Batch or Academic Year</a>
                        
                    <?php
                    }
                    ?>
                    <span>&nbsp;</span>
                </div>             
                
            </div>
            <div class="col-lg-1 col-md-1 col-sm-1 hidden-xs" style="height: 100%"></div>
        
        
<!-- ******************************************** Modals Start ******************************************** -->

<!-- Comment Modal Start-->
<div class="modal fade" id="comment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #00cc00">Project Upload Successfully .!
                    <small>Do you want to inform about this..?</small></h4>
            </div>
            <form class="" role="form" action="" id="mcommentform" name="mcommentform" method="post">
                <div class="modal-body">
                    <!--   Hidden fields to store Batch,Academic Year-->
                    <input type="hidden" name="batch" value="<?php echo $batch; ?>"/>

                    <input type="hidden" name="nexturl" value="lecturer/projects"/>
                    <!--   End Hidden fields to store Batch,Academic Year-->
                    <div class="form-group">
                        <label for="tcomment" class="lms-label">Comment to inform</label>
                        <textarea class="form-control" rows="3" placeholder="Enter comment here .." name="comment"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="fileName" class="lms-label">Start Date & Time <small>to Display</small></label><br />
                        <div class="bfh-datepicker" data-format="y-m-d" data-date="today" style="width: 50%; float: left" data-name="start_date" ></div>
                        <div class="bfh-timepicker" data-mode="12h" style="width: 45%; float: right" data-name="start_time" ></div>
                    </div>
                    <br /><br />
                    <div class="form-group">
                        <label for="fileName" class="lms-label">End Date & Time <small>to Display</small></label><br />
                        <div class="bfh-datepicker" data-format="y-m-d" data-date="today" style="width: 50%; float: left" data-name="end_date" ></div>
                        <div class="bfh-timepicker" data-mode="12h" style="width: 45%; float: right" data-name="end_time" ></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save<small> / Publish</small> Comment</button>
                </div>
            </form>
        </div>
    </div>
</div> 
<!-- Comment Modal End-->

<!-- Upload Failed Modal Start-->
<div class="modal fade" id="uploadfailed" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #ff0066">Project Upload Failed..?</h4>
            </div>
            <div class="modal-body">
                <p>There was an error on uploading the Tute. Try again ..!!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div> 
<!-- Upload Failed Modal End-->

<!-- Edit Modal Start-->
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #ffcc00">Project Edit</h4>
            </div>
            <?php
                if($view_edit_modal==TRUE){
            ?>
            <form class="" role="form" action="<?php echo base_url().'lecturer/projects/update_project' ?>" name="meditform" method="POST" enctype="multipart/form-data" >
                <!--   Hidden fields to store Batch,Academic Year-->
                <input type="hidden" name="batch" value="<?php echo $_POST['batch']; ?>"/>
                <input type="hidden" name="acyear" value="<?php echo $_POST['acyear']; ?>"/>
                <!--   End Hidden fields to store Batch,Academic Year-->

                <!--   Hidden fields for Tute details and Module-->
                <input type="hidden" name="id" value="<?php echo $_POST['id']; ?>"/>
                <input type="hidden" name="date" value="<?php echo $_POST['date']; ?>"/>                                                
                <input type="hidden" name="file_path_old" value="<?php echo $_POST['file_path_old'];?>"/>
                <input type="hidden" name="file_extension_old" value="<?php echo $_POST['file_extension_old']; ?>"/>
                <!--   End Hidden fields for Tute details and Module-->

                <div class="modal-body">
                    <div class="form-group">
                        <label for="fileName" class="lms-label">Project Name</label>
                        <input type="text" class="form-control" placeholder="Enter Project name here .." name="project_name" value="<?php echo $_POST['project_name']; ?>" required >
                    </div>
                    <div class="form-group">
                        <label for="fileName" class="lms-label">Project Description</label>
                        <textarea class="form-control" rows="4" placeholder="Enter Project description here .." name="project_description" required ><?php echo $_POST['project_description']; ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="fileName" class="lms-label">Submit Date & Time</label><br />
                        <div class="bfh-datepicker" data-format="y-m-d" data-date="<?php echo $_POST['submit_date']; ?>" style="width: 50%; float: left" data-name="submit_date" required ></div>
                        <div class="bfh-timepicker" data-mode="12h" style="width: 45%; float: right" data-time="<?php echo $_POST['submit_time']; ?>" data-name="submit_time" required ></div>
                    </div><br /><br />
                    <div class="form-group">
                        <label for="fileName" class="lms-label">Select a file</label>
                        <input id="input-2" type="file" class="file" data-show-upload="false" data-show-caption="true" name="projectfile">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" >Close</button>
                    <button type="submit" class="btn btn-warning" >Upload</button>
                </div>
            </form>
            <?php
                }
            ?>
        </div>
    </div>
</div> 
<!-- Edit Modal End-->

<!-- Delete Modal Start-->
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #ff0066">Do you want to delete this Project..?</h4>
            </div>
            <div class="modal-body">
                <p>If you delete this Project, Project will no longer display any more, and it will remove from whole database</p>
            </div>
            <div class="modal-footer">
                <form method="post" action="" id="mdeleteform">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-danger">Yes</button>
                </form>
            </div>
        </div>
    </div>
</div> 
<!-- Delete Modal End-->

<!-- ******************************************** Modals End ******************************************** -->