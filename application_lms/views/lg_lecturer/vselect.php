<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >, Anton Perera < antondilshanperera@gmail.com >
 * Description : 
 * Created on : Jun 9, 2014, 12:33:43 AM
 */

?>
<body onload='loadwrap("#cal", "http://lms.d2real.com/calendar/displaycal/");'>

<div class="container" id="main-container">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-md-offset-1">
            <?php
            $id=$selectedid;
            switch($id){
                case 'tutes':
                    $sid=1;
                    break;
                case 'assignments':
                    $sid=2;
                    break;
                    
                case 'references':
                    $sid=3;
                    break;
            }
            if ($sid == 1) {
                echo '<div class="centered">
                        <div class="blockquote-box blockquote-default clearfix" style="border: 0; margin: 10px auto; width: 100px">
                            <div class="square">
                                <span class="box-perpal glyphicon text-center"><b>T</b></span>
                            </div>
                        </div>
                    </div>
                    <h1 style="text-align: center">Select a Module</h1>';
                $nextpage = 'lecturer/tutes';
            } elseif ($sid == 2) {
                echo '<h1 style="text-align: center">Assignments</h1>';
                $nextpage = 'lecturer/assignments';
            } elseif ($sid == 3) {
                echo '<div class="centered">
                        <div class="blockquote-box blockquote-success clearfix" style="border: 0; margin: 10px auto; width: 100px">
                            <div class="square">
                                <span class="box-perpal glyphicon text-center"><b>R</b></span>
                            </div>
                        </div>
                    </div>
                    <h1 style="text-align: center">Find References</h1>';
                $nextpage = 'lecturer/references';
            }
            ?>
            <hr class="hr-style-two" style="margin: auto auto 40px auto">
            
            <form role="form" action="<?php echo base_url().$nextpage; ?>" method="POST">
                
                <h4 style="text-align: center; margin: 35px auto 5px auto">Select a Batch</h4>
                <select class="form-control" id="batch" name="batch" onchange='loadacyear_with_batch("#acyeardiv","<?php echo base_url(); ?>lecturer/select/select_acyears_from_batch");'>
                    <option value="-">Select a Batch</option>
                    <?php                    
                    foreach($batches as $lec_batches) {
                        echo'<option value="'.$lec_batches->batch_id.'">' .$lec_batches->batch_name. '</option>';
                    }
                    ?>
                </select>
                
                <h4 style="text-align: center; margin: 35px auto 5px auto">Batch Academic year</h4>
                <div id="acyeardiv">
                <select class="form-control" name="acyear">
                    <option value="-">Select an Academic Year</option>
                    
                </select>
                </div>
                    
                <h4 style="text-align: center; margin: 35px auto 5px auto">Select Batch Semester</h4>
                
                <select class="form-control" id="semester" name="semester" onchange='loadlecmodule_with_b_a_s("#modulediv","<?php echo base_url(); ?>lecturer/select/select_lec_modules_from_b_a_s");'>
                    <option value="-">Select a Semester</option>
                    <?php
                    for ($i = 1; $i < 3; $i++) {
                        echo'<option value="'.$i.'">Semester ' . $i . '</option>';
                    }
                    ?>
                </select>

                <h4 style="text-align: center; margin: 35px auto 5px auto">Select a Module</h4>
                
                <div id="modulediv">
                <select class="form-control" name="module">
                    <option value="-">Select a Module</option>
                    
                </select>
                </div>
                    
                <button type="submit" class="btn btn-default btn-lg center-block" style="margin: 20px auto 20px auto; width: 100%">Find Details</button>
            </form>
            
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1 hidden-xs"></div>
