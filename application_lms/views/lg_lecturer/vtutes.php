<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >, Anton Perera < antondilshanperera@gmail.com >
 * Description : 
 * Created on : Jun 13, 2014, 3:44:18 PM
 */

?>

<?php
if ($ready_for_comment == TRUE) {
    ?>
    <body onload='loadwrap("#cal", "http://lms.d2real.com/calendar/displaycal/"); loadmodalwith_formaction("#comment","#mcommentform","<?php echo base_url() . 'comments/add_comment/'; ?>");'>

        <?php
    } elseif ($upload_failed == TRUE) {
        ?>
    <body onload='loadwrap("#cal", "http://lms.d2real.com/calendar/displaycal/"); loadmodal("#uploadfailed");'>

    <?php
} else {
    if ($view_edit_modal == TRUE) {
        ?>
        <body onload='loadwrap("#cal", "http://lms.d2real.com/calendar/displaycal/"); loadmodal("#edit");'>

        <?php
    } else {
        ?>
        <body onload='loadwrap("#cal", "http://lms.d2real.com/calendar/displaycal/");'>        
            <?php
        }
    }
    ?>
        
<div class="container" id="main-container">
        
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-md-offset-1">
                <div style="border-left: 10px solid #0099ff; padding-left: 20px; margin: 20px auto" class="">
                <h4 style="margin: 35px auto auto auto">Batch Name : <?php echo $batch_name; ?></h4>
                <h4>Academic Year : <?php echo $acyear; ?></h4>
                <h4>Semester : <?php echo $semester; ?></h4>
                <h4>Module : <?php echo $module_name; ?></h4>
                </div>
                
                <h3 style="text-align: center; margin: 35px auto auto auto">Upload a new Tute</h3>
                <hr class="hr-style-two">
                
                <form class="" role="form" action="<?php echo base_url().'lecturer/tutes/add_tute' ?>" enctype="multipart/form-data" method="POST">
                    <!--   Hidden fields to store Batch,Academic Year,Semester and Module-->
                    <input type="hidden" name="batch" value="<?php echo $batch; ?>"/>
                    <input type="hidden" name="acyear" value="<?php echo $acyear; ?>"/>
                    <input type="hidden" name="semester" value="<?php echo $semester; ?>"/>
                    <input type="hidden" name="module" value="<?php echo $module; ?>"/>
                    <!--   End Hidden fields to store Batch,Academic Year,Semester and Module-->

                        <div class="form-group">
                            <label for="tname" class="lms-label">Tute Name</label>
                            <input type="text" class="form-control" placeholder="Enter tute name here .." name="tute_name" required>
                        </div>
                        <div class="form-group">
                            <label for="tute_description" class="lms-label">Tute Description</label>
                            <textarea class="form-control counted" rows="3" placeholder="Enter tute description here .." name="tute_description" required></textarea>
                            <h6 class="pull-right" id="counter">320 characters remaining</h6>                    
                        </div>
                        <div class="form-group">
                            <label for="tutefile" class="lms-label">Select a file</label>
                            <input id="input-2" type="file" class="file" data-show-upload="false" data-show-caption="true" name="tutefile" required>
                        </div>

                        <button type="submit" class="btn btn-success btn-lg" >Upload</button>
                </form>
                                        
                <h3 style="text-align: center; margin: 35px auto auto auto">Uploaded Tutes</h3>
                <hr class="hr-style-two">
                
                
                <div class="list-group">
                    <?php
                    $i=1;
                    if(!empty($tutes)){
                    foreach ($tutes as $row){
                    ?>
                        <a id="tute-items" class="list-group-item active">
                            <div class="media col-md-3">
                                <figure class="pull-left">
                                <span class="btn alert-info ubuntu" style="margin-bottom : 10px"><?php echo $i;?></span>
                                <?php
                                $ext=$row->file_extension;
                                if($ext==="jpg" || $ext==="png" || $ext==="gif" || $ext==="bmp"){
                                    $imagesrc=base_url().'assets_lms/img/uploadthumbs/image.png';
                                }elseif($ext==="zip" || $ext==="rar" || $ext==="tar" || $ext==="gz"){
                                    $imagesrc=base_url().'assets_lms/img/uploadthumbs/zip.png';
                                }elseif($ext==="pdf"){
                                    $imagesrc=base_url().'assets_lms/img/uploadthumbs/pdf.png';
                                }elseif($ext==="doc" || $ext==="docx"){
                                    $imagesrc=base_url().'assets_lms/img/uploadthumbs/docx.png';
                                }elseif($ext==="xlsx" || $ext==="xls"){
                                    $imagesrc=base_url().'assets_lms/img/uploadthumbs/xlsx.png';
                                }elseif($ext==="pptx" || $ext==="ppsx" || $ext==="ppt" || $ext==="pps"){
                                    $imagesrc=base_url().'assets_lms/img/uploadthumbs/pptx.png';
                                }elseif($ext==="sql"){
                                    $imagesrc=base_url().'assets_lms/img/uploadthumbs/db.png';
                                }elseif($ext==="txt"){
                                    $imagesrc=base_url().'assets_lms/img/uploadthumbs/text.png';
                                } 
                                ?>
                                <img class="media-object img-rounded img-responsive" style="display: block;" src="<?php echo $imagesrc ?>"alt="placehold.it/350x450" >
                                </figure>
                            </div>
                            <div class="col-md-6">
                                <h4 class="list-group-item-heading"><?php echo $row->tute_name ?></h4>
                                <p class="list-group-item-text"> <?php echo $row->tute_description ?></p>
                            </div>
                            <div class="col-md-3 text-center">
                                <h5><small> Click to </small><br />Download</h5>
                                <form action="<?php echo base_url().'download/'.'tutes/id/'.$row->id; ?>">
                                
                                    <button type="submit" class="btn btn-default btn-lg btn-block"><i class="fa fa-download"></i>
                                </button>
                                </form>
                                <h6>Uploaded Date<br /><span style="color: #ff99ff"><?php echo $row->date ?></span></h6>
                                    <div class="row" style="padding: 0">
                                        <div class="col-sm-3 pull-left" style="padding: 0">
                                            <form action="" method="POST">
                                                <!--   Hidden fields to store Batch,Academic Year,Semester and Module-->
                                                <input type="hidden" name="batch" value="<?php echo $row->tut_batch_id; ?>"/>
                                                <input type="hidden" name="acyear" value="<?php echo $row->academic_year; ?>"/>
                                                <input type="hidden" name="semester" value="<?php echo $row->semester; ?>"/>
                                                <input type="hidden" name="module" value="<?php echo $row->tut_module_id; ?>"/>
                                                <!--   End Hidden fields to store Batch,Academic Year,Semester and Module-->
                                                
                                                <!--   Hidden fields for Tute details and Module-->
                                                <input type="hidden" name="id" value="<?php echo $row->id; ?>"/>          
                                                <input type="hidden" name="tute_id" value="<?php echo $row->tute_id; ?>"/>
                                                <input type="hidden" name="tute_name" value="<?php echo $row->tute_name; ?>"/>
                                                <input type="hidden" name="tute_description" value="<?php echo $row->tute_description; ?>"/>
                                                <input type="hidden" name="date" value="<?php echo $row->date; ?>"/>                                                
                                                <input type="hidden" name="file_path_old" value="<?php echo $row->file_path; ?>"/>
                                                <input type="hidden" name="file_extension_old" value="<?php echo $row->file_extension; ?>"/>
                                                <!--   End Hidden fields for Tute details and Module-->  
                                                
                                                <button type="submit" class="btn btn-warning btn-sm" >Edit</button>
                                            </form>
                                        </div>
                                        <div class="col-sm-6 pull-right" style="padding: 0;">
                                            <button type="button" class="btn btn-danger btn-sm" onclick='loadmodalwith_formaction("#delete","#mdeleteform","<?php echo base_url().'lecturer/tutes/delete_tute/'.$row->id; ?>");'>Delete</button>
                                        </div>
                                    </div>
                            </div>
                        </a>
                    <?php
                        $i++;
                        }
                    }else{
                    ?>
                        <h3 style="text-align: center; margin: 35px auto auto auto">No Tutes uploaded on the selected Module</h3>
                        <br/>
                        <a href="<?php echo base_url().'lecturer/select/tutes'?>" class="btn btn-default">Select a different Module</a>
                        
                    <?php
                    }
                    ?>

                </div>
                
            </div>
            <div class="col-lg-1 col-md-1 col-sm-1 hidden-xs" style="height: 100%"></div>


        
        
<!-- ******************************************** Modals Start ******************************************** -->

<!-- Comments Modal Start-->
<div class="modal fade" id="comment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #00cc00">Tute Upload Successfully .!
                    <small>Do you want to inform about this..?</small></h4>
            </div>
            <form class="" role="form" action="" id="mcommentform" name="mcommentform" method="post">
                <div class="modal-body">
                    <!--   Hidden fields to store Batch,Academic Year,Semester and Module-->
                    <input type="hidden" name="batch" value="<?php echo $batch; ?>"/>

                    <input type="hidden" name="nexturl" value="lecturer/tutes"/>
                    <!--   End Hidden fields to store Batch,Academic Year,Semester and Module-->
                    <div class="form-group">
                        <label for="tcomment" class="lms-label">Comment to inform</label>
                        <textarea class="form-control" rows="3" placeholder="Enter comment here .." name="comment"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="fileName" class="lms-label">Start Date & Time <small>to Display</small></label><br />
                        <div class="bfh-datepicker" data-format="y-m-d" data-date="today" style="width: 50%; float: left" data-name="start_date" ></div>
                        <div class="bfh-timepicker" data-mode="12h" style="width: 45%; float: right" data-name="start_time" ></div>
                    </div>
                    <br /><br />
                    <div class="form-group">
                        <label for="fileName" class="lms-label">End Date & Time <small>to Display</small></label><br />
                        <div class="bfh-datepicker" data-format="y-m-d" data-date="today" style="width: 50%; float: left" data-name="end_date" ></div>
                        <div class="bfh-timepicker" data-mode="12h" style="width: 45%; float: right" data-name="end_time" ></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save<small> / Publish</small> Comment</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Comments Modal End-->

<!-- Upload Failed Modal Start-->
<div class="modal fade" id="uploadfailed" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #ff0066">Assignment Upload Failed..?</h4>
            </div>
            <div class="modal-body">
                <p>There was an error on uploading the Tute. Try again ..!!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div> 
<!-- Upload Failed Modal End-->

<!-- Edit Modal Start-->
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #ffcc00">Tute Edit</h4>
            </div>
            <?php
                if($view_edit_modal==TRUE){
            ?>
            <form class="" role="form" action="<?php echo base_url().'lecturer/tutes/update_tute' ?>" name="meditform" method="POST" enctype="multipart/form-data" >
                <!--   Hidden fields to store Batch,Academic Year,Semester and Module-->
                <input type="hidden" name="batch" value="<?php echo $_POST['batch']; ?>"/>
                <input type="hidden" name="acyear" value="<?php echo $_POST['acyear']; ?>"/>
                <input type="hidden" name="semester" value="<?php echo $_POST['semester']; ?>"/>
                <input type="hidden" name="module" value="<?php echo $_POST['module']; ?>"/>
                <!--   End Hidden fields to store Batch,Academic Year,Semester and Module-->

                <!--   Hidden fields for Tute details and Module-->
                <input type="hidden" name="id" value="<?php echo $_POST['id']; ?>"/>          
                <input type="hidden" name="tute_id" value="<?php echo $_POST['tute_id']; ?>"/>
                <input type="hidden" name="date" value="<?php echo $_POST['date']; ?>"/>                                                
                <input type="hidden" name="file_path_old" value="<?php echo $_POST['file_path_old']; ?>"/>
                <input type="hidden" name="file_extension_old" value="<?php echo $_POST['file_extension_old']; ?>"/>
                <!--   End Hidden fields for Tute details and Module-->

                <div class="modal-body">
                    <div class="form-group">
                        <label for="tname" class="lms-label">Tute Name</label>
                        <input type="text" class="form-control" placeholder="Enter tute name here .." name="tute_name" value="<?php echo $_POST['tute_name']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="tute_description" class="lms-label">Tute Description</label>
                        <textarea class="form-control" rows="3" placeholder="Enter tute description here .." name="tute_description"><?php echo $_POST['tute_description']; ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="tutefile" class="lms-label">Select a file</label>
                        <input id="input-2" type="file" class="file" data-show-upload="false" data-show-caption="true" name="tutefile">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-warning" >Upload</button>
                </div>
            </form>
            <?php
                }
            ?>
        </div>
    </div>
</div> 
<!-- Edit Modal End-->

<!-- Delete Modal Start-->
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #ff0066">Do you want to delete this tute..?</h4>
            </div>
            <div class="modal-body">
                <p>If you delete this tute tute will no longer display any more, and it will remove from whole database</p>
            </div>
            <div class="modal-footer">
                <form method="post" action="" id="mdeleteform">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-danger">Yes</button>
                </form>
            </div>
        </div>
    </div>
</div> 
<!-- Delete Modal End-->

<!-- ******************************************** Modals End ******************************************** -->