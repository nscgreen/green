<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >, Anton Perera < antondilshanperera@gmail.com >
 * Description : 
 * Created on : Jun 13, 2014, 3:44:18 PM
 */

?>
<?php 
if($upload_assignment==TRUE){
?>
<body onload='loadwrap("#cal", "http://lms.d2real.com/calendar/displaycal/"); loadmodal("#uploadassignment");
      loadwrap("#cal", "http://lms.d2real.com/calendar/displaycal/");'>
        
<?php
}elseif($re_upload_assignment==TRUE){
?>
<body onload='loadwrap("#cal", "http://lms.d2real.com/calendar/displaycal/"); loadmodal("#reuploadassignment");
      loadwrap("#cal", "http://lms.d2real.com/calendar/displaycal/");'>
        
<?php
}elseif($upload_failed==TRUE){
   
?>
<body onload='loadwrap("#cal", "http://lms.d2real.com/calendar/displaycal/"); loadmodal("#uploadfailed");
      loadwrap("#cal", "http://lms.d2real.com/calendar/displaycal/");'>
    
<?php
}else{
?>
<body onload='loadwrap("#cal", "http://lms.d2real.com/calendar/displaycal/");
      loadwrap("#cal", "http://lms.d2real.com/calendar/displaycal/");'>
        
<?php
}
?>

    <div class="container" id="main-container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-md-offset-1">
                <div style="border-left: 10px solid #0099ff; padding-left: 20px; margin: 20px auto" class="">
                <h4>Academic Year : <?php echo $acyear; ?></h4>
                <h4>Semester : <?php echo $semester; ?></h4>
                <h4>Module : <?php echo $module_name; ?></h4>
                </div>
                
                <h3 style="text-align: center; margin: 35px auto auto auto">Assignments to Download</h3>
                <hr class="hr-style-two">
                
                <div class="well">
                    <?php
                    $i=1;
                    if(!empty($assignments)){
                    foreach ($assignments as $row){
                    ?>
                    <div class="row user-row" style="margin-bottom: 15px">
                        <div class="col-sm-2 col-xs-2">
                                <?php
                                $ext=$row->file_extension;
                                if($ext==="jpg" || $ext==="png" || $ext==="gif" || $ext==="bmp"){
                                    $imagesrc=base_url().'assets_lms/img/uploadthumbs/image.png';
                                }elseif($ext==="zip" || $ext==="rar" || $ext==="tar" || $ext==="gz"){
                                    $imagesrc=base_url().'assets_lms/img/uploadthumbs/zip.png';
                                }elseif($ext==="pdf"){
                                    $imagesrc=base_url().'assets_lms/img/uploadthumbs/pdf.png';
                                }elseif($ext==="doc" || $ext==="docx"){
                                    $imagesrc=base_url().'assets_lms/img/uploadthumbs/docx.png';
                                }elseif($ext==="xlsx" || $ext==="xls"){
                                    $imagesrc=base_url().'assets_lms/img/uploadthumbs/xlsx.png';
                                }elseif($ext==="pptx" || $ext==="ppsx" || $ext==="ppt" || $ext==="pps"){
                                    $imagesrc=base_url().'assets_lms/img/uploadthumbs/pptx.png';
                                }elseif($ext==="sql"){
                                    $imagesrc=base_url().'assets_lms/img/uploadthumbs/db.png';
                                }elseif($ext==="txt"){
                                    $imagesrc=base_url().'assets_lms/img/uploadthumbs/text.png';
                                } 
                                ?>
                        <img class="img-rounded" alt="Thumbanails" src="<?php echo $imagesrc ?>" width="60" height="80">
                        </div>
                        <div class="col-sm-6 col-xs-6 alert alert-info">
                            <h4>Assignment Name : <?php echo $row->assignment_name ?></h4>
                            <span class="text-muted">Submit Date: <?php echo $row->submit_date ?></span>
                        </div>
                        <div class="col-sm-2 col-xs-2">
                        </div>
                        <div class="col-sm-2 col-xs-2 dropdown-user" data-for=".<?php echo $i ?>" >
                            <i class="fa fa-angle-down fa-2x text-muted" style="margin-left: 10px;"></i>
                        </div>
                    </div>
                    
                    <div class="row-fluid user-infos <?php echo $i ?>">
                        <div class="col-sm-12 col-xs-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Assignment Details</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="row-fluid">
                                        <div class="span6">
                                            <table class="table table-condensed table-responsive table-user-information">
                                                <tbody>
                                                <tr>
                                                    <td>Academic year</td>
                                                    <td> : </td>
                                                    <td><?php echo $row->academic_year ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Semester</td>
                                                    <td> : </td>
                                                    <td><?php echo $row->semester ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Module</td>
                                                    <td> : </td>
                                                    <td><?php echo $row->module_name ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Description</td>
                                                    <td> : </td>
                                                    <td><?php echo $row->assignment_description ?></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer"> 
                                    <span class="pull-right">
                                        <form class="pull-left" action="<?php echo base_url().'download/assignments/id/'.$row->id; ?>">
                                            <button type="submit" class="btn btn-default" type="button"
                                                data-toggle="tooltip"
                                                data-original-title="Download Assignment"><i class="fa fa-download"></i>
                                            </button>
                                        </form>
                                        
                                        <form class="pull-right" action="" style="margin-left: 5px" method="POST">
                                                <!-- Hidden fields to store Batch,Academic Year,Semester and Module -->
                                                <input type="hidden" name="batch" value="<?php echo $row->batch_id; ?>"/>
                                                <input type="hidden" name="acyear" value="<?php echo $row->academic_year; ?>"/>
                                                <input type="hidden" name="semester" value="<?php echo $row->semester; ?>"/>
                                                <input type="hidden" name="module" value="<?php echo $row->module_id; ?>"/>
                                                <!-- End Hidden fields to store Batch,Academic Year,Semester and Module -->
    <!--                                            <input type="hidden" name="student_id" value="<?php //$student_id="0001"; echo $student_id;  ?>"/>-->

                                                <!-- Hidden fields for Assignment details and Module -->         
                                                <input type="hidden" name="assignment_id" value="<?php echo $row->id; ?>"/>
                                                <input type="hidden" name="assignment_name" value="<?php echo $row->assignment_name; ?>"/>
                                                <input type="hidden" name="assignment_description" value="<?php echo $row->assignment_description; ?>"/>
                                                <input type="hidden" name="submit_date" value="<?php echo $row->submit_date; ?>"/>
                                                <input type="hidden" name="submit_time" value="<?php echo $row->submit_time; ?>"/>

                                                <input type="hidden" name="assignment_file_path" value="<?php echo $row->file_path; ?>"/>
                                                <input type="hidden" name="assignment_file_extension" value="<?php echo $row->file_extension; ?>"/>
                                                <!-- End Hidden fields for Assignment details and Module -->  
                                                <button class="btn btn-success" type="submit"
                                                        data-toggle="tooltip"
                                                        data-original-title="Submit Assignment"><i class="fa fa-upload"></i>
                                                </button>
                                            </form>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                        $i++;
                        }
                    }else{
                    ?>
                        <h3 style="text-align: center; margin: 35px auto auto auto">No Assignments to download on the selected Module</h3>
                        <br/>
                        <a href="<?php echo base_url().'student/select/assignments'?>" class="btn btn-default">Select a different Module</a>
                        
                    <?php
                    }
                    ?>
                    <span>&nbsp;</span>
                </div>
                
                
                <h3 style="text-align: center; margin: 35px auto auto auto">Submited Assignments</h3>
                <hr class="hr-style-two">
                <div class="well">
                    <?php
                    $i=1;
                    if(!empty($submited_assignments)){
                        foreach ($submited_assignments as $row){
                    ?>
                        <div class="row user-row" style="margin-bottom: 15px">
                            <div class="col-sm-2 col-xs-2">
                                    <?php
                                    $ext=$row->file_extension;
                                    if($ext==="jpg" || $ext==="png" || $ext==="gif" || $ext==="bmp"){
                                        $imagesrc=base_url().'assets_lms/img/uploadthumbs/image.png';
                                    }elseif($ext==="zip" || $ext==="rar" || $ext==="tar" || $ext==="gz"){
                                        $imagesrc=base_url().'assets_lms/img/uploadthumbs/zip.png';
                                    }elseif($ext==="pdf"){
                                        $imagesrc=base_url().'assets_lms/img/uploadthumbs/pdf.png';
                                    }elseif($ext==="doc" || $ext==="docx"){
                                        $imagesrc=base_url().'assets_lms/img/uploadthumbs/docx.png';
                                    }elseif($ext==="xlsx" || $ext==="xls"){
                                        $imagesrc=base_url().'assets_lms/img/uploadthumbs/xlsx.png';
                                    }elseif($ext==="pptx" || $ext==="ppsx" || $ext==="ppt" || $ext==="pps"){
                                        $imagesrc=base_url().'assets_lms/img/uploadthumbs/pptx.png';
                                    }elseif($ext==="sql"){
                                        $imagesrc=base_url().'assets_lms/img/uploadthumbs/db.png';
                                    }elseif($ext==="txt"){
                                        $imagesrc=base_url().'assets_lms/img/uploadthumbs/text.png';
                                    } 
                                    ?>
                            <img class="img-rounded" alt="Thumbanails" src="<?php echo $imagesrc ?>" width="60" height="80">
                            </div>
                            <div class="col-sm-6 col-xs-6 alert alert-info">
                                <h4>Assignment Name : <?php echo $row->assignment_name ?></h4>
                                <span class="text-muted">Submited Date: <?php echo $row->stu_submited_date ?></span>
                            </div>
                            <div class="col-sm-2 col-xs-2">
                            </div>
                            <div class="col-sm-2 col-xs-2 dropdown-user" data-for=".s<?php echo $i ?>" >
                                <i class="fa fa-angle-down fa-2x text-muted" style="margin-left: 10px;"></i>
                            </div>
                        </div>

                        <div class="row-fluid user-infos s<?php echo $i ?>">
                            <div class="col-sm-12 col-xs-12">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Assignment Details</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row-fluid">
                                            <div class="span6">
                                                <table class="table table-condensed table-responsive table-user-information">
                                                    <tbody>
                                                    <tr>
                                                        <td>Academic year</td>
                                                        <td> : </td>
                                                        <td><?php echo $row->academic_year ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Semester</td>
                                                        <td> : </td>
                                                        <td><?php echo $row->semester ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Module</td>
                                                        <td> : </td>
                                                        <td><?php echo $row->module_name ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Description</td>
                                                        <td> : </td>
                                                        <td><?php echo $row->assignment_description ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Student Note:</td>
                                                        <td> : </td>
                                                        <td><?php echo $row->stu_note ?></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-footer">
                                        <span class="pull-right">
                                            <form action="" style="height: 0px;width: 0px" method="POST">
                                                <!-- Hidden fields to store Batch,Academic Year,Semester and Module -->
                                                <input type="hidden" name="batch" value="<?php echo $row->batch; ?>"/>
                                                <input type="hidden" name="acyear" value="<?php echo $row->academic_year; ?>"/>
                                                <input type="hidden" name="semester" value="<?php echo $row->semester; ?>"/>
                                                <input type="hidden" name="module" value="<?php echo $row->module_id; ?>"/>
                                                <!-- End Hidden fields to store Batch,Academic Year,Semester and Module -->

                                                <input type="hidden" name="student_id" value="<?php $student_id="0001"; echo $student_id; ?>"/>

                                                <!-- Hidden fields for Assignment details and Module -->         
                                                <input type="hidden" name="id" value="<?php echo $row->id; ?>"/>
                                                <input type="hidden" name="assignment_id" value="<?php echo $row->assignment_id; ?>"/>
                                                <input type="hidden" name="assignment_name" value="<?php echo $row->assignment_name; ?>"/>
                                                <input type="hidden" name="assignment_description" value="<?php echo $row->assignment_description; ?>"/>
                                                <input type="hidden" name="submit_date" value="<?php echo $row->submit_date; ?>"/>
                                                <input type="hidden" name="submit_time" value="<?php echo $row->submit_time; ?>"/>
                                                <input type="hidden" name="stu_note" value="<?php echo $row->stu_note; ?>"/>
                                                
                                                <input type="hidden" name="assignment_file_path" value="<?php echo $row->file_path; ?>"/>
                                                <input type="hidden" name="assignment_file_extension" value="<?php echo $row->file_extension; ?>"/>
                                                <!-- End Hidden fields for Assignment details and Module -->
                                                
                                                <button class="btn btn-success" type="submit" style="margin-left: -30px"
                                                    data-toggle="tooltip"
                                                    data-original-title="Re-Submit Assignment"><i class="fa fa-upload"></i>
                                                </button>
                                            </form>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                        $i++;
                        }
                    }else{
                    ?>
                        <h3 style="text-align: center; margin: 35px auto auto auto">No Assignments are submitted on the selected Module</h3>
                        <br/>
                        <a href="<?php echo base_url().'student/select/assignments'?>" class="btn btn-default">Select a different Module</a>
                        
                    <?php
                    }
                    ?>
                    <span>&nbsp;</span>
                </div>
                

                
            </div>
            <div class="col-lg-1 col-md-1 col-sm-1 hidden-xs" style="height: 100%"></div>

<!-- ******************************************** Modals Start ******************************************** -->

<!-- Upload Failed Modal Start-->
<div class="modal fade" id="uploadfailed" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #ff0066">Assignment Upload Failed..?</h4>
            </div>
            <div class="modal-body">
                <p>There was an error on uploading the Tute. Try again ..!!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div> 
<!-- Upload Failed Modal End-->

<!-- Upload Assignment Modal Start-->
<div class="modal fade" id="uploadassignment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #ffcc00">Submit Assignment</h4>
            </div>
            <?php
                if($upload_assignment==TRUE){
            ?>
            <form class="" role="form" action="<?php echo base_url().'student/assignments/submit_assignment' ?>" name="muploadform" method="POST" enctype="multipart/form-data" >
                <!--   Hidden fields to store Batch,Academic Year,Semester and Module-->
                <input type="hidden" name="batch" value="<?php echo $_POST['batch']; ?>"/>
                <input type="hidden" name="acyear" value="<?php echo $_POST['acyear']; ?>"/>
                <input type="hidden" name="semester" value="<?php echo $_POST['semester']; ?>"/>
                <input type="hidden" name="module" value="<?php echo $_POST['module']; ?>"/>
                <!--   End Hidden fields to store Batch,Academic Year,Semester and Module-->

                <!--   Hidden fields for Tute details and Module-->         
<!--                                <input type="hidden" name="id" value="<?php //echo $_POST['id']; ?>"/>-->
                <input type="hidden" name="assignment_id" value="<?php echo $_POST['assignment_id']; ?>"/>
                <input type="hidden" name="assignment_name" value="<?php echo $_POST['assignment_name']; ?>"/>
                <input type="hidden" name="assignment_description" value="<?php echo $_POST['assignment_description']; ?>"/>
                <input type="hidden" name="submit_date" value="<?php echo $_POST['submit_date']; ?>"/>
                <input type="hidden" name="submit_time" value="<?php echo $_POST['submit_time']; ?>"/>

                <input type="hidden" name="assignment_file_path" value="<?php echo $_POST['assignment_file_path']; ?>"/>
                <input type="hidden" name="assignment_file_extension" value="<?php echo $_POST['assignment_file_extension']; ?>"/>
                <!--   End Hidden fields for Tute details and Module-->

                <div class="modal-body">
                    <div class="form-group">
                        <label for="student_note" class="lms-label">Note: </label>
                        <textarea class="form-control" rows="4" placeholder="Enter notes, comments related with the Assignment that you want to enter .." name="student_note" required ><?php echo $_POST['assignment_description']; ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="fileName" class="lms-label">Select a file</label>
                        <input id="input-2" type="file" class="file" data-show-upload="false" data-show-caption="true" name="assignmentfile">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" >Close</button>
                    <button type="submit" class="btn btn-warning" >Upload</button>
                </div>
            </form>
            <?php
                }
            ?>
        </div>
    </div>
</div> 
<!-- Upload Assignment Modal End-->

<!-- Re-Upload Assignment Modal Start-->
<div class="modal fade" id="reuploadassignment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #ffcc00">Re-Submit Assignment</h4>
            </div>
            <?php
                if($re_upload_assignment==TRUE){
            ?>
            <form class="" role="form" action="<?php echo base_url().'student/assignments/re_submit_assignment' ?>" name="muploadform" method="POST" enctype="multipart/form-data" >
                <!--   Hidden fields to store Batch,Academic Year,Semester and Module-->
                <input type="hidden" name="batch" value="<?php echo $_POST['batch']; ?>"/>
                <input type="hidden" name="acyear" value="<?php echo $_POST['acyear']; ?>"/>
                <input type="hidden" name="semester" value="<?php echo $_POST['semester']; ?>"/>
                <input type="hidden" name="module" value="<?php echo $_POST['module']; ?>"/>
                <!--   End Hidden fields to store Batch,Academic Year,Semester and Module-->

                <!--   Hidden fields for Tute details and Module-->         
                <input type="hidden" name="id" value="<?php echo $_POST['id']; ?>"/>
                <input type="hidden" name="assignment_id" value="<?php echo $_POST['assignment_id']; ?>"/>
                <input type="hidden" name="assignment_name" value="<?php echo $_POST['assignment_name']; ?>"/>
                <input type="hidden" name="assignment_description" value="<?php echo $_POST['assignment_description']; ?>"/>
                <input type="hidden" name="submit_date" value="<?php echo $_POST['submit_date']; ?>"/>
                <input type="hidden" name="submit_time" value="<?php echo $_POST['submit_time']; ?>"/>

                <input type="hidden" name="assignment_file_path" value="<?php echo $_POST['assignment_file_path']; ?>"/>
                <input type="hidden" name="assignment_file_extension" value="<?php echo $_POST['assignment_file_extension']; ?>"/>
                <!--   End Hidden fields for Tute details and Module-->

                <div class="modal-body">
                    <div class="form-group">
                        <label for="student_note" class="lms-label">Note: </label>
                        <textarea class="form-control" rows="4" placeholder="Enter notes, comments related with the Assignment that you want to enter .." name="student_note" required ><?php echo $_POST['stu_note']; ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="fileName" class="lms-label">Select a file</label>
                        <input id="input-2" type="file" class="file" data-show-upload="false" data-show-caption="true" name="assignmentfile">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" >Close</button>
                    <button type="submit" class="btn btn-warning" >Upload</button>
                </div>
            </form>
            <?php
                }
            ?>
        </div>
    </div>
</div> 
<!-- Re-Upload Assignment Modal End-->        

<!-- Delete Modal Start-->
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #ff0066">Do you want to delete this Assignment..?</h4>
            </div>
            <div class="modal-body">
                <p>If you delete this Assignment, Assignment will no longer display any more, and it will remove from whole database</p>
            </div>
            <div class="modal-footer">
                <form method="post" action="" id="mdeleteform">
                <!--<input type="hidden" value="" id="modal_id" name="modal_id"/>-->
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="submit" class="btn btn-danger">Yes</button>
                </form>
            </div>
        </div>
    </div>
</div> 
<!-- Delete Modal End-->

<!-- ******************************************** Modals End ******************************************** -->