<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >, Anton Perera < antondilshanperera@gmail.com >
 * Description : 
 * Created on : Jun 13, 2014, 3:44:18 PM
 */

?>
<body onload='loadwrap("#cal", "http://lms.d2real.com/calendar/displaycal/");'>

    <div class="container" id="main-container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-md-offset-1">
                <h3 style="text-align: center; margin: 35px auto auto auto">Exam Dates</h3>
                <hr class="hr-style-two">
                
                <div style="border-left: 10px solid #0099ff; padding-left: 20px;" class="">
                <h4 style="margin: 35px auto auto auto">Academic Year : {1}</h4>
                <h4 style="">Semester : {1}<br /><small>Exam time table</small></h4>
                </div>
                
                <div class="panel panel-primary" style="margin-top: 30px">
                    <div class="panel-heading">Details</div>
                    <div class="panel-body">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                    </div>
                <!-- Table -->
                <div class="table-responsive">
                    <table class="table table-hover">
                        <tr class="info ubuntu" style="text-align: center; color: #0099ff; font-weight: 500">
                            <th>#</th>
                            <th>Module code</th>
                            <th>Module Name</th>
                            <th>Date</th>
                            <th>Time</th>
                        </tr>
                        <?php
                        for ($i = 0; $i < 7; $i++) {
                            echo '<tr>
                                    <td>'.($i+1).'</td>
                                    <td>BCS201</td>
                                    <td>Maths Descrete digital </td>
                                    <td>2014/02/13</td>
                                    <td>09.00 AM - 12.00 PM</td>
                                  </tr>';
                        }
                        ?>
                    </table>
                </div>
                </div>
                
                <div class="bg-info" style="border-left: 4px solid #0099ff; padding-left: 10px">
                    <h4 style="color: #ff3399">Note :<br /><span class="text-info">Exam Details and Conditions</span></h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.
                        <br /><a href="">Read more..</a></p>
                </div>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-1 hidden-xs" style="height: 100%"></div>
