<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jun 13, 2014, 3:44:18 PM
 */

?>
<body onload='loadwrap("#cal", "http://lms.d2real.com/calendar/displaycal/");'>

    <div class="container" id="main-container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-md-offset-1">
                <h3 style="text-align: center; margin: 35px auto auto auto">Exam Results</h3>
                <hr class="hr-style-two">
                
                <div class="pull-right">
                    <form class="form-inline" role="form">
                        <div class="form-group input-sm">
                            <select class="form-control">
                                <?php
                                for ($i = 1; $i < 5; $i++) {
                                    echo'<option>Academic year ' . $i . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group input-sm">
                            <select class="form-control">
                                <?php
                                if ($sid == 2) { // Student Home Selection ID
                                    echo '<option>All ...</option>';
                                }
                                for ($i = 1; $i < 3; $i++) {
                                    echo'<option>Semester ' . $i . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group input-sm">
                            <button type="submit" class="btn btn-default">Find <span class="glyphicon glyphicon-search"></span></button>
                        </div>
                    </form>
                </div>
                
                <br />
                <div style="border-left: 10px solid #0099ff; padding-left: 20px;" class="">
                <h4 style="margin: 35px auto auto auto">Academic Year : {1}</h4>
                <h4 style="">Semester : {1}<br /><small>Exam results</small></h4>
                </div>
                
                <div class="panel panel-primary" style="margin-top: 30px">
                    <div class="panel-heading">Results</div>
                    <div class="panel-body">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                    </div>
                <!-- Table -->
                <div class="table-responsive">
                    <table class="table table-hover">
                        <tr class="success ubuntu" style="text-align: center; color: #00cc66; font-weight: 500">
                            <th>#</th>
                            <th>Module code</th>
                            <th>Module Name</th>
                            <th>Grade</th>
                            <th>Pass/Fail</th>
                        </tr>
                        <?php
                        for ($i = 0; $i < 7; $i++) {
                            echo '<tr>
                                    <td>'.($i+1).'</td>
                                    <td>BCS201</td>
                                    <td>Maths Descrete digital </td>
                                    <td>A+</td>
                                    <td>Pass</td>
                                  </tr>';
                        }
                        ?>
                    </table>
                </div>
                </div>
                
            </div>
            <div class="col-lg-1 col-md-1 col-sm-1 hidden-xs" style="height: 100%"></div>
