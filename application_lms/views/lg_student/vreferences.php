<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jun 9, 2014, 12:33:43 AM
 */

?>
<body onload='loadwrap("#cal", "http://lms.d2real.com/calendar/displaycal/");'>        
        
    <div class="container" id="main-container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-md-offset-1">
                <div style="border-left: 10px solid #0099ff; padding-left: 20px; margin: 20px auto" class="">
                <h4 style="margin: 35px auto auto auto">Batch Name : <?php echo $batch_name; ?></h4>
                <h4>Academic Year : <?php echo $acyear; ?></h4>
                <h4>Semester : <?php echo $semester; ?></h4>
                <h4>Module : <?php echo $module_name; ?></h4>
                </div>
                
                <h3 style="text-align: center; margin: 35px auto auto auto">Uploaded References for students</h3>
                <hr class="hr-style-two">
                
                <div class="tabbable">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_b" data-toggle="tab">Books</a></li>
                        <li><a href="#tab_a" data-toggle="tab">Articles</a></li>
                        <li><a href="#tab_v" data-toggle="tab">Videos</a></li>
                        <li><a href="#tab_w" data-toggle="tab">Web Sites</a></li>
                    </ul>
                    <div class="tab-content" style="margin-top: 20px;">
                        
                        <!-- *************** Start Books tab *************** -->
                        <div class="tab-pane active" id="tab_b">
                            <?php
                            if(!$books==FALSE){
                                foreach ($books as $row){
                                
                            ?>
                                <div class="media">
                                        <a class="pull-left" href="#">
                                            <img class="media-object" src="http://placehold.it/128x128" alt="...">
                                        </a>
                                        <div class="media-body">
<!--                                            <form class="form-inline" action="" role="form" style="padding: 0; margin: 0px 15px 0 0px" method="POST">
                                                
                                                <input type="hidden" name="ref_type" value="ref_books"/>
                                                <input type="hidden" name="id" value="<?php //echo $row->id; ?>"/>
                                                
                                                <div class="row pull-right" style="padding: 0;">
                                                    <button type="submit" style="border: 0;background: none;outline: 0;"><a class="" style=""><span class="glyphicon glyphicon-pencil"></span></a></button>
                                                </div>
                                            </form>-->
<!--                                                <div class="row pull-right" style="padding: 0;padding-right: 20px">
                                                        <a class="" onclick='loadmodalwith_formaction("#delete","#mdeleteform","<?php //echo base_url().'lecturer/references/delete_reference/id/'.$row->id.'/ref_books'; ?>");' style="cursor: pointer;"><span class="glyphicon glyphicon-remove"></span></a>
                                                </div>-->
                                            <h4 class="media-heading"><?php echo $row->title; ?><br /><small>Author : <?php echo $row->author; ?></small></h4>
                                            <h5>Download link : <a href="<?php echo $row->url; ?>">{<?php echo $row->url; ?>}</a>
                                                <form action="<?php echo base_url().'download/ref_books/id/'.$row->id; ?>">
                                                    <small> or </small><button type="submit" style="border: 0;background: none;outline: 0;"><a><i class="fa fa-download"></i></a></button></h5>
                                                </form>
                                            <p class="bg-success"><?php echo $row->description; ?></p>
                                        </div>
                                   </div>
                            <?php
                                //$i++;
                                }
                            }else{
                            ?>
                                <h3 style="text-align: center; margin: 35px auto auto auto">No References as Books on the selected Module</h3>
                                <br/>
                                <a href="<?php echo base_url().'student/select/references'?>" class="btn btn-default">Select a different Module</a>

                            <?php
                            }
                            ?>
                        </div>

                        <!-- *************** End Books tab *************** -->
                            
                        <!-- *************** Start Articles tab *************** -->
                        <div class="tab-pane " id="tab_a">
                            <?php
                            if(!$articles==FALSE){
                                foreach ($articles as $row2){  
                            ?>
                                <div class="media">
                                        <a class="pull-left" href="#">
                                            <img class="media-object" src="http://placehold.it/128x128" alt="...">
                                        </a>
                                        <div class="media-body">
<!--                                            <form class="form-inline" action="" role="form" style="padding: 0; margin: 0px 15px 0 0px" method="POST">
                                                
                                                <input type="hidden" name="ref_type" value="ref_articles"/>
                                                <input type="hidden" name="id" value="<?php //echo $row2->id; ?>"/>
                                                
                                                <div class="row pull-right" style="padding: 0;">
                                                    <button type="submit" style="border: 0;background: none;outline: 0;"><a class="" style=""><span class="glyphicon glyphicon-pencil"></span></a></button>
                                                </div>
                                            </form>
                                                <div class="row pull-right" style="padding: 0;padding-right: 20px">
                                                        <a class="" onclick='loadmodalwith_formaction("#delete","#mdeleteform","<?php //echo base_url().'lecturer/references/delete_reference/id/'.$row2->id.'/ref_articles'; ?>");' style="cursor: pointer;"><span class="glyphicon glyphicon-remove"></span></a>
                                                </div>-->
                                            <h4 class="media-heading"><?php echo $row2->title; ?><br /><small>Author : <?php echo $row2->author; ?></small></h4>
                                            <h5>Download link : <a href="<?php echo $row2->url; ?>">{<?php echo $row2->url; ?>}</a>
                                                <form action="<?php echo base_url().'download/ref_articles/id/'.$row2->id; ?>">
                                                    <small> or </small><button type="submit" style="border: 0;background: none;outline: 0;"><a><i class="fa fa-download"></i></a></button></h5>
                                                </form>
                                            <p class="bg-success"><?php echo $row2->description; ?></p>
                                        </div>
                                   </div>
                            <?php
                                //$i++;
                                }
                            }else{
                            ?>
                                <h3 style="text-align: center; margin: 35px auto auto auto">No References as Articles on the selected Module</h3>
                                <br/>
                                <a href="<?php echo base_url().'student/select/references'?>" class="btn btn-default">Select a different Module</a>

                            <?php
                            }
                            ?>
                        </div>

                        <!-- *************** End Articles tab *************** -->
                        
                        <!-- *************** Start Videos tab *************** -->
                        <div class="tab-pane " id="tab_v">
                            <?php
                            if(!$videos==FALSE){
                                foreach ($videos as $row3){
                            ?>
                                <div class="media">
                                        <a class="pull-left" href="#">
                                            <img class="media-object" src="http://placehold.it/128x128" alt="...">
                                        </a>
                                        <div class="media-body">
<!--                                            <form class="form-inline" action="" role="form" style="padding: 0; margin: 0px 15px 0 0px" method="POST">
                                                
                                                <input type="hidden" name="ref_type" value="ref_videos"/>
                                                <input type="hidden" name="id" value="<?php //echo $row3->id; ?>"/>
                                                
                                                <div class="row pull-right" style="padding: 0;">
                                                    <button type="submit" style="border: 0;background: none;outline: 0;"><a class="" style=""><span class="glyphicon glyphicon-pencil"></span></a></button>
                                                </div>
                                            </form>-->
<!--                                                <div class="row pull-right" style="padding: 0;padding-right: 20px">
                                                        <a class="" onclick='loadmodalwith_formaction("#delete","#mdeleteform","<?php //echo base_url().'lecturer/references/delete_reference/id/'.$row3->id.'/ref_videos'; ?>");' style="cursor: pointer;"><span class="glyphicon glyphicon-remove"></span></a>
                                                </div>-->
                                            <h4 class="media-heading"><?php echo $row3->title; ?><br /><small></small></h4>
                                            <h5>Download link : <a href="<?php echo $row3->url; ?>">{<?php echo $row3->url; ?>}</a>
                                                <form action="<?php echo base_url().'download/ref_videos/id/'.$row3->id; ?>">
                                                    <small> or </small><button type="submit" style="border: 0;background: none;outline: 0;"><a><i class="fa fa-download"></i></a></button></h5>
                                                </form>
                                            <p class="bg-success"><?php echo $row3->description; ?></p>
                                        </div>
                                   </div>
                            <?php
                                //$i++;
                                }
                            }else{
                            ?>
                                <h3 style="text-align: center; margin: 35px auto auto auto">No References as Videos on the selected Module</h3>
                                <br/>
                                <a href="<?php echo base_url().'student/select/references'?>" class="btn btn-default">Select a different Module</a>

                            <?php
                            }
                            ?>
                        </div>

                        <!-- *************** End Videos tab *************** -->
                        
                        <!-- *************** Start Websites tab *************** -->
                        <div class="tab-pane " id="tab_w">
                            <?php
                            if(!$websites==FALSE){
                                foreach ($websites as $row4){
                            ?>
                                <div class="media">
                                        <a class="pull-left" href="#">
                                            <img class="media-object" src="http://placehold.it/128x128" alt="...">
                                        </a>
                                        <div class="media-body">
<!--                                            <form class="form-inline" action="" role="form" style="padding: 0; margin: 0px 15px 0 0px" method="POST">
                                                
                                                <input type="hidden" name="ref_type" value="ref_websites"/>
                                                <input type="hidden" name="id" value="<?php //echo $row4->id; ?>"/>
                                                
                                                <div class="row pull-right" style="padding: 0;">
                                                    <button type="submit" style="border: 0;background: none;outline: 0;"><a class="" style=""><span class="glyphicon glyphicon-pencil"></span></a></button>
                                                </div>
                                            </form>-->
<!--                                                <div class="row pull-right" style="padding: 0;padding-right: 20px">
                                                        <a class="" onclick='loadmodalwith_formaction("#delete","#mdeleteform","<?php // echo base_url().'lecturer/references/delete_reference/id/'.$row4->id.'/ref_websites'; ?>");' style="cursor: pointer;"><span class="glyphicon glyphicon-remove"></span></a>
                                                </div>-->
                                            <h4 class="media-heading"><?php echo $row4->title; ?><br /><small></small></h4>
                                            <h5>Website link : <a href="<?php echo $row4->url; ?>">{<?php echo $row4->url; ?>}</a>
                                            <p class="bg-success"><?php echo $row4->description; ?></p>
                                        </div>
                                   </div>
                            <?php
                                //$i++;
                                }
                            }else{
                            ?>
                                <h3 style="text-align: center; margin: 35px auto auto auto">No References as Websites on the selected Module</h3>
                                <br/>
                                <a href="<?php echo base_url().'student/select/references'?>" class="btn btn-default">Select a different Module</a>

                            <?php
                            }
                            ?>
                        </div>

                        <!-- *************** End Websites tab *************** -->
                            
                    </div>
                </div>

            </div>
            <div class="col-lg-1 col-md-1 col-sm-1 hidden-xs"></div>
