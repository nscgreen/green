<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >, Anton Perera < antondilshanperera@gmail.com >
 * Description : 
 * Created on : Jun 9, 2014, 12:33:43 AM
 */

?>
<body onload='loadwrap("#cal", "http://lms.d2real.com/calendar/displaycal/");'>

<div class="container" id="main-container">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-md-offset-1">
            <?php
            $id=$selectedid;
            switch($id){
                case 'tutes':
                    $sid=1;
                    break;
                case 'assignments':
                    $sid=2;
                    break;
                    
                case 'references':
                    $sid=3;
                    break;
            }
            
            if ($sid == 1) {
                echo '<div class="centered">
                        <div class="blockquote-box blockquote-default clearfix" style="border: 0; margin: 10px auto; width: 100px">
                            <div class="square">
                                <span class="box-perpal glyphicon text-center"><b>T</b></span>
                            </div>
                        </div>
                    </div> 
                    <h1 style="text-align: center">Tutes</h1>';
                $nextpage = 'student/tutes';
            } elseif ($sid == 2) {
                echo '<h1 style="text-align: center">Assignments</h1>';
                $nextpage = 'student/assignments';
            } elseif ($sid == 3) {
                echo '<div class="centered">
                        <div class="blockquote-box blockquote-success clearfix" style="border: 0; margin: 10px auto; width: 100px">
                            <div class="square">
                                <span class="box-perpal glyphicon text-center"><b>R</b></span>
                            </div>
                        </div>
                    </div>
                    <h1 style="text-align: center">Find References</h1>';
                $nextpage = 'student/references';
            }
            ?>
            <hr class="hr-style-two" style="margin: auto auto 40px auto">
                
            <form role="form" action="<?php echo base_url().$nextpage; ?>" method="POST">

                <input type="hidden" name="batch" value="<?php echo $batch; ?>"/>
                <h4 style="text-align: center; margin: 35px auto 5px auto">Select your Academic year</h4>
                <select class="form-control" id="acyear" name="acyear" onchange='loadmodule_with_b_a_s("#modulediv","<?php echo base_url(); ?>student/select/select_modules_from_b_a_s","<?php echo $batch; ?>");'>
                    <option value="-">Select Academic Year</option>
                    <?php
                    if(isset($acyear)){
                        for($i = 1; $i <= $acyear ; $i++){
                            echo'<option value="'.$i.'">Academic Year ' . $i . '</option>';
                        }
                    }
                    ?>
                </select>

                <h4 style="text-align: center; margin: 35px auto 5px auto">Select your Semester</h4>
                <select class="form-control" id="semester" name="semester" onchange='loadmodule_with_b_a_s("#modulediv","<?php echo base_url(); ?>student/select/select_modules_from_b_a_s","<?php echo $batch; ?>");'>
                    <option value="-">Select Semester</option>
                    <?php
                    for ($i = 1; $i < 3; $i++) {
                        echo'<option value="'.$i.'" >Semester ' . $i . '</option>';
                    }
                    ?>
                </select>

                <h4 style="text-align: center; margin: 35px auto 5px auto">Select Module</h4>
                <!--<hr class="hr-style-two">-->
                <div id="modulediv">
                <select class="form-control" id="module" name="module" >
                </select>
                </div>
                <button type="submit" class="btn btn-default btn-lg center-block" style="margin: 20px auto 20px auto; width: 100%">Find Details</button>
            </form>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1 hidden-xs"></div>
