<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >, Anton Perera < antondilshanperera@gmail.com >
 * Description : 
 * Created on : Jun 9, 2014, 12:33:43 AM
 */

?>
<body onload='loadwrap("#cal", "http://lms.d2real.com/calendar/displaycal/");'>

<div class="container" id="main-container">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-md-offset-1">
            <!--Exam Dates section-->
            <div class="centered">
                <div class="blockquote-box blockquote-info clearfix" style="border: 0; margin: 10px auto; width: 100px">
                    <div class="square">
                        <span class="box-perpal glyphicon text-center"><b>E<small>.d</small></b></span>
                    </div>
                </div>
            </div>
            <h1 style="text-align: center">Exam Dates</h1>
            <hr class="hr-style-two" style="margin: auto auto 40px auto">

            <h4 style="text-align: center; margin: 35px auto 5px auto">Select your Academic year</h4>
            <select class="form-control">
                <?php
                for ($i = 1; $i < 5; $i++) {
                    echo'<option>Academic year ' . $i . '</option>';
                }
                ?>
            </select>

            <h4 style="text-align: center; margin: 35px auto 5px auto">Select your Semester</h4>
            <select class="form-control">
                <?php
                if ($sid == 2) { // Student Home Selection ID
                    echo '<option>All ...</option>';
                }
                for ($i = 1; $i < 3; $i++) {
                    echo'<option>Semester ' . $i . '</option>';
                }
                ?>
            </select>
            <a class="btn btn-default btn-lg center-block" style="margin: 20px auto 20px auto" href="<?php echo base_url(); ?>student/exam/dates">Find Details</a>

            <!--Exam Results section-->
            <div class="centered">
                <div class="blockquote-box blockquote-info clearfix" style="border: 0; margin: 10px auto; width: 100px">
                    <div class="square">
                        <span class="box-perpal glyphicon text-center"><b>E<small>.r</small></b></span>
                    </div>
                </div>
            </div>
            <h1 style="text-align: center">Exam Results</h1>
            <hr class="hr-style-two" style="margin: auto auto 40px auto">

            <h4 style="text-align: center; margin: 35px auto 5px auto">Select your Academic year</h4>
            <select class="form-control">
                <?php
                for ($i = 1; $i < 5; $i++) {
                    echo'<option>Academic year ' . $i . '</option>';
                }
                ?>
            </select>

            <h4 style="text-align: center; margin: 35px auto 5px auto">Select your Semester</h4>
            <select class="form-control">
                <?php
                if ($sid == 2) { // Student Home Selection ID
                    echo '<option>All ...</option>';
                }
                for ($i = 1; $i < 3; $i++) {
                    echo'<option>Semester ' . $i . '</option>';
                }
                ?>
            </select>
            <a class="btn btn-default btn-lg center-block" style="margin: 20px auto 20px auto" href="<?php echo base_url(); ?>student/exam/results">Find Details</a>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1 hidden-xs"></div>
