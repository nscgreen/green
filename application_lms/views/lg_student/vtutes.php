<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >, Anton Perera < antondilshanperera@gmail.com >
 * Description : 
 * Created on : Jun 13, 2014, 3:44:18 PM
 */

?>
<body onload='loadwrap("#cal", "http://lms.d2real.com/calendar/displaycal/");'>

    <div class="container" id="main-container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-md-offset-1">
                <h3 style="text-align: center; margin: 35px auto auto auto">Tutes to Download</h3>
                <hr class="hr-style-two">
                
                <div style="border-left: 10px solid #0099ff; padding-left: 20px; margin: 20px auto" class="">
                <h4 style="margin: 35px auto auto auto">Academic Year : <?php echo $acyear; ?></h4>
                <h4>Semester : <?php echo $semester; ?></h4>
                <h4>Module : <?php echo $module_name; ?></h4>
                </div>
                
                <div class="list-group">
                    <div class="list-group">
                    <?php
                    $i=1;
                    if(!empty($tutes)){
                        foreach ($tutes as $row){
                    ?>
                            <a id="tute-items" class="list-group-item active">
                                <div class="media col-md-3">
                                    <figure class="pull-left">
                                    <span class="btn alert-info ubuntu" style="margin-bottom : 10px"><?php echo $i;?></span>
                                    <?php
                                    $ext=$row->file_extension;
                                    if($ext==="jpg" || $ext==="png" || $ext==="gif" || $ext==="bmp"){
                                        $imagesrc=base_url().'assets_lms/img/uploadthumbs/image.png';
                                    }elseif($ext==="zip" || $ext==="rar" || $ext==="tar" || $ext==="gz"){
                                        $imagesrc=base_url().'assets_lms/img/uploadthumbs/zip.png';
                                    }elseif($ext==="pdf"){
                                        $imagesrc=base_url().'assets_lms/img/uploadthumbs/pdf.png';
                                    }elseif($ext==="doc" || $ext==="docx"){
                                        $imagesrc=base_url().'assets_lms/img/uploadthumbs/docx.png';
                                    }elseif($ext==="xlsx" || $ext==="xls"){
                                        $imagesrc=base_url().'assets_lms/img/uploadthumbs/xlsx.png';
                                    }elseif($ext==="pptx" || $ext==="ppsx" || $ext==="ppt" || $ext==="pps"){
                                        $imagesrc=base_url().'assets_lms/img/uploadthumbs/pptx.png';
                                    }elseif($ext==="sql"){
                                        $imagesrc=base_url().'assets_lms/img/uploadthumbs/db.png';
                                    }elseif($ext==="txt"){
                                        $imagesrc=base_url().'assets_lms/img/uploadthumbs/text.png';
                                    } 
                                    ?>
                                    <img class="media-object img-rounded img-responsive" style="display: block;" src="<?php echo $imagesrc ?>"alt="placehold.it/350x450" >
                                    </figure>
                                </div>
                                <div class="col-md-6">
                                    <h4 class="list-group-item-heading"><?php echo $row->tute_name ?></h4>
                                    <p class="list-group-item-text"> <?php echo $row->tute_description ?></p>
                                </div>
                                <div class="col-md-3 text-center">
                                    <h5><small> Click to </small><br />Download</h5>
                                    <form action="<?php echo base_url().'download/'.'tutes/id/'.$row->id; ?>">

                                        <button type="submit" class="btn btn-default btn-lg btn-block"><i class="fa fa-download"></i>
                                    </button>
                                    </form>
                                    <h6>Uploaded Date<br /><span style="color: #ff99ff"><?php echo $row->date ?></span></h6>
                                </div>
                            </a>
                    <?php
                        $i++;
                        }
                    }else{
                    ?>
                        <h3 style="text-align: center; margin: 35px auto auto auto">No Tutes to Download on the selected Module</h3>
                        <br/>
                        <a href="<?php echo base_url().'student/select/tutes'?>" class="btn btn-default">Select a different Module</a>
                        
                    <?php
                    }
                    ?>

                </div>
                </div>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-1 hidden-xs" style="height: 100%"></div>
