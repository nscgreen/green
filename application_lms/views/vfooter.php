<footer class="row" style="margin: 30px auto auto auto">
    <div class="col-md-6 col-md-offset-3 col-xs-6 col-xs-offset-3">
        <hr id="shadow-footer" />
        <p class="text-center">Copyright &COPY; National School of Business Management</p>
        <ul class="list-inline text-center">
            <li><a href="http://main.d2real.com"  target="_blank">NSBM</a></li>
            <li><a href="http://mynsbm.d2real.com"  target="_blank">My NSBM</a></li>
            <li><a href="http://lms.d2real.com"  target="_blank">L.M.S</a></li>
            <li><a href="http://forum.d2real.com"  target="_blank">Forum</a></li>
            <li><a href="http://events.d2real.com"  target="_blank">Events</a></li>
            <li><a href="http://communities.d2real.com"  target="_blank">Communities</a></li>
            <li><a href="http://help.d2real.com"  target="_blank">Help</a></li>
        </ul> 
    </div>
</footer>

<?php include 'includes/footer_cnt.php'; ?>
</body>
</html>