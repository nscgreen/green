<?php
/*
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jun 9, 2014, 12:33:43 AM
 */
?>
<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-2 col-sm-offset-1 hidden-xs">
                <img style="margin: 10px auto auto auto; width: 246px; height: auto;" class="image-responsive" src="<?php echo base_url(); ?>logos/nsbm.gif">
            </div>
            <!--only for XS-->
            <div class="col-sm-2 col-sm-offset-1 visible-xs">
                <img style="margin: 10px auto auto auto; width: 246px; height: 150%;" class="image-responsive center-block" src="<?php echo base_url(); ?>logos/nsbm.gif">
            </div>
            <div class="col-sm-4 col-sm-offset-4 col-md-push-1 hidden-xs pull-left">
                <ul id="lms-home-social-menu1" class="animated fadeInDown">
                    <li><a href="#"><img style="width: 25px; height: 25px" src="<?php echo base_url(); ?>assets_lms/img/facebook.png"></a></li>
                    <li><a href="#"><img style="width: 25px; height: 25px" src="<?php echo base_url(); ?>assets_lms/img/twitter.png"></a></li>
                    <li><a href="#"><img style="width: 25px; height: 25px" src="<?php echo base_url(); ?>assets_lms/img/linkedin.png"></a></li>
                    <li><a href="#"><img style="width: 25px; height: 25px" src="<?php echo base_url(); ?>assets_lms/img/googleplusalt.png"></a></li>
                </ul>
                <ul id="lms-home-social-menu2">
                    <li><a class="ubuntu" href="http://mynsbm.d2real.com" target="_blank">My NSBM</a></li>
                    <li><a class="ubuntu" href="http://forum.d2real.com" target="_blank">Forum</a></li>
                    <li><a class="ubuntu" href="http://events.d2real.com" target="_blank">Events</a></li>
                    <li><a class="ubuntu" href="http://communities.d2real.com" target="_blank">Communities</a></li>
                    <li><a class="ubuntu" href="http://help.d2real.com" target="_blank">Help</a></li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-5" style="margin: 5% auto auto 8%">
                <div id="lms-vhome-left">
                    <h1 style="color:  #b941b9;" class="animated fadeInRight">Learning Management System<small> of NSBM</small></h1>
                    <img style="width: 120%; height: 120%; margin: 20px 0 0 -100px" class="image-responsive center-block hidden-xs" src="<?php echo base_url(); ?>assets_lms/img/lms.png" alt="NSBM-LMS" />
                </div>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4"  style="margin: 5% auto auto 8%">
                <br />
                <img class="img-rounded" style="width: 80px; height: 80px;" src="<?php echo base_url() ?>assets_lms/img/user.png" alt="NSBM-LMS" />
                <h4>Hi User ..</h4>
                <form action="http://connect.d2real.com" method="POST">
                    <input type="hidden" name="nexturl" value="lms.d2real.com" />
                    <input type="hidden" name="type" value="lms" />
                    <button type="submit" class="btn btn-default" style="width: 40%">Log in</button>
                </form>
                <h3 style="color: #0099ff">Get Started ...</h3>
                <p class="text-justify">Welcome to learning Management System of NSBM. This provides self learning facilities to all the students in NSBM. All of the extra curricular activities, assignments, projects and important news events will be displayed here. Academic staff members will facilitate you through this system 24/7.</p>
            </div>
        </div>

        <br /><br />
    </div>