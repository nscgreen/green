<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >, Anton Perera < antondilshanperera@gmail.com >
 * Description : 
 * Created on : Jun 9, 2014, 12:29:39 AM
 */

?>
<div class="container" style="margin-bottom: 30px; padding-top: 10px;">
    <header id="lms-header" class="row">
        <div class="col-sm-1 col-xs-3">
            <img class="img-rounded" style="width: 80px; height: 80px; padding-bottom: 10px" src="<?php if(isset($profile_pic)){ echo base_url().$profile_pic; } ?>" alt="NSBM-LMS" />
        </div>
        <div class="col-sm-3 col-xs-4" style="">

            <h4><!--FirstName--><?php if(isset($fname)){ echo $fname; } ?><br /><small><!--NickName--><?php if(isset($nick_name)){ echo $nick_name; } ?></small></h4>
            <h6><!--Student Index or My NSBM id-->
                <?php 
                if(isset($fname)){ 
                    if($user_type==="ST") {
                        echo $st_index;
                    }else{
                        echo $mynsbm_id;
                    }
                } 
                    ?>
            </h6>
        </div>
        <div class="col-sm-4 col-xs-5">
            <ul class="nav nav-pills animated fadeInUp" style="margin: 20px 0 0px 0; padding: 5px;">
                <li class=""><a href="<?php echo base_url();?>" class="btn "><img style="width: 32px; height: 32px" src="<?php echo base_url();?>assets_lms/img/home3.png"></a></li>
                <li class=""><a href="<?php echo base_url();?>home/logout" class="btn "><img style="width: 32px; height: 32px" src="<?php echo base_url();?>assets_lms/img/logout.png"></a></li>
            </ul>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 visible-lg visible-md">
            <ul id="lms-home-social-menu1" class="animated fadeInDown" style="margin: 10px auto 5px 190px">
                <li><a href="#"><img style="width: 25px; height: 25px" src="<?php echo base_url(); ?>assets_lms/img/facebook.png"></a></li>
                <li><a href="#"><img style="width: 25px; height: 25px" src="<?php echo base_url(); ?>assets_lms/img/twitter.png"></a></li>
                <li><a href="#"><img style="width: 25px; height: 25px" src="<?php echo base_url(); ?>assets_lms/img/linkedin.png"></a></li>
                <li><a href="#"><img style="width: 25px; height: 25px" src="<?php echo base_url(); ?>assets_lms/img/googleplusalt.png"></a></li>
            </ul>
            <ul id="lms-lgmenu">
                <li><a class="ubuntu" href="http://mynsbm.d2real.com" target="_blank">My NSBM</a></li>
                <li><a class="ubuntu" href="http://forum.d2real.com" target="_blank">Forum</a></li>
                <li><a class="ubuntu" href="http://events.d2real.com" target="_blank">Events</a></li>
                <li><a class="ubuntu" href="http://communities.d2real.com" target="_blank">Communities</a></li>
                <li><a class="ubuntu" href="http://help.d2real.com" target="_blank">Help</a></li>
            </ul>
        </div>
        <!--Visible for small devices-->
<!--        <div class="col-xs-12 visible-xs">
            <div class="row">
                <div class="col-xs-6">
                    <ul id="lms-home-social-menu1" class="animated fadeInDown" style="margin: 10px auto 5px 190px">
                        <li><a href="#"><img style="width: 25px; height: 25px" src="<?php echo base_url(); ?>assets_lms/img/facebook.png"></a></li>
                        <li><a href="#"><img style="width: 25px; height: 25px" src="<?php echo base_url(); ?>assets_lms/img/twitter.png"></a></li>
                        <li><a href="#"><img style="width: 25px; height: 25px" src="<?php echo base_url(); ?>assets_lms/img/linkedin.png"></a></li>
                        <li><a href="#"><img style="width: 25px; height: 25px" src="<?php echo base_url(); ?>assets_lms/img/googleplusalt.png"></a></li>
                    </ul>
                </div>
                <div class="col-xs-6">
                    <ul id="lms-lgmenu">
                        <li><a class="ubuntu" href="#">My NSBM</a></li>
                        <li><a class="ubuntu" href="#">Forum</a></li>
                        <li><a class="ubuntu" href="#">Events</a></li>
                        <li><a class="ubuntu" href="#">Communities</a></li>
                        <li><a class="ubuntu" href="#">Help</a></li>
                    </ul>
                </div>
            </div>
        </div>-->
    </header>
</div>
