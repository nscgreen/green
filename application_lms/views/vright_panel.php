<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >, Anton Perera < antondilshanperera@gmail.com >
 * Description : 
 * Created on : Jun 9, 2014, 12:33:43 AM
 */

?>
    <aside class="col-lg-4 col-md-4 col-sm-4 col-xs-12" id="right_panel" style="height: 100%;">
        <h3 class="alert alert-info" style="padding: 5px">News line</h3>
        <div class="row">
            <div class="col-xs-12 centered">
                <div id="nt-example1-container">
                    <i class="fa fa-angle-up fa-3x text-muted" id="nt-example1-prev"></i>
                    <ul id="nt-example1">
                    <?php 
                    foreach ($comments_for_users as $comments){
                    ?>
                        <li>
                            <img class="pull-left image-responsive" style="width: 42px; height: 42px; margin-right: 20px" src="<?php echo base_url().$comments->lec_profile_pic;?>" />
                            <h3 style="margin: 15px auto 10px auto"><small>Lecturer Name: <?php echo $comments->lec_name;?></small></h3>
                            
                            
                            <?php if(isset($comments_lecturers) && $comments_lecturers===TRUE) {  ?>
                            <h3 style="margin: 15px auto 10px auto"><small>Batch Name: <?php echo $comments->batch_name;?></small></h3>
                            <?php echo $comments->comment;?>
                            <?php }else{ ?>
                            <?php echo $comments->comment;?>
                            <?php } ?>
                        </li>
                    <?php
                    }
                    ?>
                    </ul>
                    <i class="fa fa-angle-down fa-3x text-muted" id="nt-example1-next"></i>
                </div>
            </div>
        </div>
        <h3 class="alert alert-info" style="padding: 5px">Calender</h3>
        <div id="cal"></div>
        <br />
        <h3 class="alert alert-success" style="padding: 5px">Time Table</h3>
        <div class="row center-block">
            
<?php   if(isset($time_table_coordinator) && $time_table_coordinator===TRUE){   ?>
            <div class="col-xs-12" style="background: rgba(0,0,0, 0.3); color: #ccccff; padding-bottom: 30px ">
                <h3 class="font-volkorn">Upload Recent Time Table</h3>
                <h4><small>Batch : </small><?php echo $time_table->batch_name ?></h4>
                <h4><small>Added Date : </small><?php echo $time_table->added_date ?></h4>
                
                <div class="row">
                    <div class="col-xs-7">
                        <img class="pull-right" src="<?php echo base_url(); ?>assets_lms/img/time_table.png" />
                    </div>
                    <div class="col-xs-5 pull-right">
                        <div style="margin: 70px auto 50px auto" class="pull-left">
                            <a data-toggle="modal" data-target="#upload" class="btn"><img class="pull-left" style="width: 32px; height: 32px;" src="<?php echo base_url(); ?>assets_lms/img/upload.png" /></a>
                        </div>
                    </div>
                </div>
            </div>
<?php   }elseif(isset($time_table_student) && $time_table_student===TRUE){   ?>
            <div class="col-xs-12" style="background: rgba(0,0,0, 0.3); color: #ccccff; padding-bottom: 30px ">
                <h3 class="font-volkorn">Download Recent Time Table</h3>
                <h4><small>Added Date: </small><?php echo $time_table->added_date ?></h4>
                <div class="row">
                    <div class="col-xs-7">
                        <img class="pull-right" src="<?php echo base_url(); ?>assets_lms/img/time_table.png" />
                    </div>
                    <div class="col-xs-5 pull-right">
                        <div style="margin: 70px auto 50px auto" class="pull-left">
                            <a href="<?php echo base_url().'download/time_table/id/'.$time_table->id; ?>" class="btn"><img class="pull-left" style="width: 32px; height: 32px;" src="<?php echo base_url(); ?>assets_lms/img/download3.png" /></a>
                        </div>
                    </div>
                </div>
            </div>
<?php   }   ?>            
            
        </div>
        <br />
    </aside>

    </div> <!--END row-->
</div> <!--END Container-->





<!-- Edit Modal Start-->
<div class="modal fade" id="upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #ffcc00">Upload the New Time Table</h4>
            </div>
            <?php   if(isset($time_table_coordinator) && $time_table_coordinator===TRUE){   ?>
            <form class="" role="form" action="<?php echo base_url().'coordinator/timetable/update_timetable' ?>" name="meditform" method="POST" enctype="multipart/form-data" >
                <!--   Hidden fields to store Batch,Academic Year,Semester and Module-->
                <input type="hidden" name="id" value="<?php echo $time_table->id ?>"/>
                <input type="hidden" name="file_path_old" value="<?php echo $time_table->file_path ?>"/>
                <input type="hidden" name="tt_batch_id" value="<?php echo $time_table->tt_batch_id ?>"/>
                
                <div class="modal-body">
                    
                    <div class="form-group">
                        <label for="fileName" class="lms-label">Select a file</label>
                        <input id="input-2" type="file" class="file" data-show-upload="false" data-show-caption="true" name="timetablefile">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" >Close</button>
                    <button type="submit" class="btn btn-warning" >Upload</button>
                </div>
            </form>
            <?php   }   ?>
        </div>
    </div>
</div> 
<!-- Edit Modal End-->