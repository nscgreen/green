<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/***************** Login Site Main Home Page********************/
class Home extends CI_Controller {
	
        public function index()
	{
            // echo $this->input->post('login_index',TRUE);
            // var_dump($this->input->post('password',TRUE));
            
            if(isset($_POST['login_index']) && !empty($_POST['login_index'])){
                //echo 'inside';
                $this->load->helper('html');
                $this->load->helper('url');
                $this->load->model('login_user_model');

                $login_index=$this->input->post('login_index',TRUE);
                $password=$this->input->post('password',TRUE);
                
                $nexturl=$this->input->post('nexturl',TRUE);
                $type=$this->input->post('type',TRUE);
                $community_id=$this->input->post('community_id',TRUE);
                $user_type_prefix=substr($login_index, 0, 3);

                //echo $user_type_prefix;

                if($user_type_prefix==="VL-"){
                    $user_type="VL";
                }elseif($user_type_prefix==="EI-"){
                    $user_type = $this->login_user_model->userType_of_employee($login_index,$nexturl,$type,$community_id);
                }else{
                    $user_type="ST";
                }

                $mynsbm_id=$this->login_user_model->validate_user_with_mynsbm($user_type,$login_index,$password);
                
                //echo $mynsbm_id;
                if(!$mynsbm_id===FALSE){
                    $url=$this->login_user_model->get_user_details($nexturl,$type,$user_type,$login_index,$password,$community_id);
                    //$this->load->library('nativesession');
                    $this->nativesession->set('mynsbm_id2',TRUE);
                    redirect('http://'.$url);
                }
                
            }else{
                if(isset($_POST['nexturl']) && !empty($_POST['nexturl']) && isset($_POST['type']) && !empty($_POST['type']) ){
                    $nexturl=$this->input->post('nexturl',TRUE);
                    $type=$this->input->post('type',TRUE);
                    //$community_id=$this->input->post('nexturl',TRUE);

                    $data = array(
                        'title' => 'NSBM',
                        'mDescription' => 'Meta desctiptions goes hera',
                        'mKeywords' => 'Meta Keywords goes hera',
                        'nexturl'=>$nexturl,
                        'type'=>$type
                    );

                    if(isset($_POST['community_id'])){
                        $data['community_id']=$this->input->post('community_id',TRUE);;
                    }
                }else{
                    $nexturl="my.d2real.com";
                    $type="my_nsbm";
                    //$community_id=$this->input->post('nexturl',TRUE);

                    $data = array(
                        'title' => 'NSBM',
                        'mDescription' => 'Meta desctiptions goes hera',
                        'mKeywords' => 'Meta Keywords goes hera',
                        'nexturl'=>$nexturl,
                        'type'=>$type
                    );
                }
                $this->load->view('vheader', $data);
                $this->load->view('vhome');
                $this->load->view('vfooter');
            }
	}
        
        public function validate_login($nexturl,$type,$community_id=NULL){
            $this->load->helper('html');
            $this->load->helper('url');
            $this->load->model('login_user_model');
            
            $login_index=$this->input->post('login_index',TRUE);
            $password=$this->input->post('password',TRUE);
            $user_type_prefix=substr($login_index, 0, 3);
            
            echo $user_type_prefix;

            if($user_type_prefix==="VL"){
                $user_type="VL";
            }elseif($user_type_prefix==="EI"){
                $user_type = $this->login_user_model->userType_of_employee($login_index,$nexturl,$type,$community_id);
            }else{
                $user_type="ST";
            }
            
            $mynsbm_id=$this->login_user_model->validate_user_with_mynsbm($user_type,$login_index,$password);
            
            var_dump($mynsbm_id);
            if(!$mynsbm_id===FALSE){
                $url=$this->login_user_model->get_user_details($nexturl,$type,$user_type,$login_index,$password,$community_id=NULL);
                //echo $url;
                $this->load->library('nativesession');
                //var_dump($this->nativesession->sessionIsset('mynsbm_id'));
                $this->nativesession->set('mynsbm_id2',TRUE);
                redirect('http://'.$url);
            }
                
            
            
            
//        $validation = $this->Login_user_model->validate($user_type_prefix);
//        
//        if($validation){
//            if ($type=="lms") {
//                //code related with LMS
//                $row=$this->Login_user_model->get_userss_data($login_index);
//                $user_id=$row->user_id;
//
//                $data = array(
//                    'user_id' => $user_id,
//                    'is_logged_in' => true
//                );
//
//                $this->session->set_userdata($data);
//        //            echo $this->session->userdata('user_id');
//                redirect($nexturl.'?user_id='.$user_id);
//                
//            }elseif($type=="community"){
//                //code related with communities
//                $row=$this->Login_user_model->get_userss_data($login_index);
//                $user_id=$row->user_id;
//
//                $index=$row->mynsbm_id;
//                $nick_name=$row->nick_name;
//                $fname=$row->fname;
//                $mobile=$row->mobile;
//                $profile_pic=$row->profile_pic;
//                $email=$row->email;
//
//
//                $data = array(
//                    'user_id' => $user_id,
//                    'is_logged_in' => true
//                );
//
//                $this->session->set_userdata($data);
//        //            echo $this->session->userdata('user_id');
//                redirect(
//                        $nexturl.
//                        '?index='.$index.
//                        '&nick_name='.$nick_name.
//                        '&fname='.$fname.
//                        '&mobile='.$mobile.
//                        '&profile_pic='.$profile_pic.
//                        '&email='.$email);
//            }
//        } else {
//            //$this->index();
//            echo "login Error";
//            redirect('http://login.d2real.com/?loginfailed=true');
//        }
        
        }
    
    
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */