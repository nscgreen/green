<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login_user_model extends CI_Model {

    function userType_of_employee($login_index,$nexturl,$type,$community_id) {
        $this->load->database();
        $this->db->where('employee_id', $login_index);
        $query = $this->db->get('permenent_employee');
        //echo $query;
        If($query->num_rows == 1){
            $result=$query->row();
            $emptype=$result->type;
            return $emptype;
        }else {
            if($community_id===NULL){
                redirect(base_url().$nexturl.'/'.$type);
            }else{
                redirect(base_url().$nexturl.'/'.$type.'/'.$community_id);
            }
        }
    }
    
    function validate_user_with_mynsbm($user_type,$login_index,$password) {
        $this->load->database();
        if($user_type==="CO" || $user_type==="PL" || $user_type==="VL" || $user_type==="PE" ){//If the user is a Permanent Employee or a Visiting Lecturer
           $query = $this->db->get_where('my_nsbm_pe', array('pe_mynsbm_id' => $login_index,'password' => $password));
           if ($query->num_rows == 1) {
               $row=$query->row();
               return $row->pe_mynsbm_id;
           }else{//If the user has no access
               return false;
           }
        }elseif($user_type==="ST"){//If the user is a Student $query = $this->db->get_where('my_nsbm_stu', array('st_index' => $login_index,'password' => $password));
           $query = $this->db->get_where('my_nsbm_stu', array('st_index' => $login_index,'password' => $password));
            if ($query->num_rows == 1) {
               $row=$query->row();
               return $row->st_mynsbm_id;
           }else{//If the user has no access
               return false;
           }
        }else{//If the user has no access
               return false;
        }
    }
    
    function get_user_details($nexturl,$type,$user_type,$login_index,$password) {
        $this->load->library('nativesession');
        if($type=="my_nsbm"){
if($user_type==="CO" || $user_type==="PL" || $user_type==="PE" ){//If the user is a Permanent Employee or a Visiting Lecturer
                                
                $this->db->select('m.pe_mynsbm_id,m.nick_name,m.nick_name,m.profile_pic,p.fname');
                $this->db->from('my_nsbm_pe as m');
                $this->db->join('permenent_employee as p', 'm.pe_mynsbm_id = p.employee_id');
                $this->db->where(array('pe_mynsbm_id' => $login_index,'password' => $password));
                $query = $this->db->get();
                
                $row=$query->row();
                $details['mynsbm_id']= $row->pe_mynsbm_id;
                $details['nick_name']= $row->nick_name;
                $details['fname']= $row->fname;
                $details['user_type']= $user_type;
                $details['profile_pic']= $row->profile_pic;
                
                
                
                $this->nativesession->set('mynsbm_id',$details['mynsbm_id']);
                
                $url= $nexturl.
                        '?mynsbm_id='.$details['mynsbm_id'].
                        '&nick_name='.$details['nick_name'].
                        '&fname='.$details['fname'].
                        '&user_type='.$details['user_type'].
                        '&profile_pic='.$details['profile_pic'];
                return $url;
                
            }elseif($user_type==="ST"){
                //$query = $this->db->get_where('my_nsbm_stu', array('st_index' => $login_index,'password' => $password));
                
                $this->db->select('m.st_mynsbm_id,m.st_index,m.nick_name,m.nick_name,m.profile_pic,s.fname');
//                $this->db->select('st_index');
                $this->db->from('my_nsbm_stu m');
                $this->db->join('student as s', 'm.st_index = s.index_nm');
                $this->db->where(array('st_index' => $login_index,'password' => $password));
                $query = $this->db->get();
                
                $row=$query->row();
                $details['mynsbm_id']= $row->st_mynsbm_id;
                $details['st_index']= $row->st_index;
                $details['nick_name']= $row->nick_name;
                $details['fname']= $row->fname;
                $details['user_type']= "ST";
                $details['profile_pic']= $row->profile_pic;
                                               
                $this->nativesession->set('mynsbm_id',$details['mynsbm_id']);
                $this->nativesession->set('st_index',$details['st_index']);
                $this->nativesession->set('nick_name',$details['nick_name']);
                $this->nativesession->set('fname',$details['fname']);
                $this->nativesession->set('profile_pic',$details['profile_pic']);
                $this->nativesession->set('user_type',"ST");
                
                $url= $nexturl.
                        '?mynsbm_id='.$details['mynsbm_id'].
                        '&st_index='.$details['st_index'].
                        '&nick_name='.$details['nick_name'].
                        '&fname='.$details['fname'].
                        '&user_type='.$details['user_type'].
                        '&profile_pic='.$details['profile_pic'];
                //echo '<img src="'.$details['profile_pic'].'"/>';
                return $url;
            }elseif($user_type==="VL"){//If the user is a Permanent Employee or a Visiting Lecturer
                                
                $this->db->select('m.vl_mynsbm_id as mynsbm_id,m.password,v.fname,m.nick_name,m.profile_pic');
                $this->db->from('my_nsbm_vl as m');
                $this->db->join('visiting_lecturer as v', 'm.vl_mynsbm_id = v.vlecturer_id');
                $this->db->where(array('vl_mynsbm_id' => $login_index,'password' => $password));
                $query = $this->db->get();
                
                $row=$query->row();
                $details['mynsbm_id']= $row->vl_mynsbm_id;
                $details['nick_name']= $row->nick_name;
                $details['fname']= $row->fname;
                $details['user_type']= $user_type;
                $details['profile_pic']= $row->profile_pic;
                
                
                $this->nativesession->set('mynsbm_id',$details['mynsbm_id']);
                
                $url= $nexturl.
                        '?mynsbm_id='.$details['mynsbm_id'].
                        '&nick_name='.$details['nick_name'].
                        '&fname='.$details['fname'].
                        '&user_type='.$details['user_type'].
                        '&profile_pic='.$details['profile_pic'];
                return $url;
            }                    
        }elseif($type=="lms"){
            if($user_type==="CO" || $user_type==="PL" || $user_type==="PE" ){//If the user is a Permanent Employee or a Visiting Lecturer
                                
                $this->db->select('m.pe_mynsbm_id,m.nick_name,m.nick_name,m.profile_pic,p.fname');
                $this->db->from('my_nsbm_pe as m');
                $this->db->join('permenent_employee as p', 'm.pe_mynsbm_id = p.employee_id');
                $this->db->where(array('pe_mynsbm_id' => $login_index,'password' => $password));
                $query = $this->db->get();
                
                $row=$query->row();
                $details['mynsbm_id']= $row->pe_mynsbm_id;
                $details['nick_name']= $row->nick_name;
                $details['fname']= $row->fname;
                $details['user_type']= $user_type;
                $details['profile_pic']= $row->profile_pic;
                
                
                
                $this->nativesession->set('mynsbm_id',$details['mynsbm_id']);
                
                $url= $nexturl.
                        '?mynsbm_id='.$details['mynsbm_id'].
                        '&nick_name='.$details['nick_name'].
                        '&fname='.$details['fname'].
                        '&user_type='.$details['user_type'].
                        '&profile_pic='.$details['profile_pic'];
                return $url;
                
            }elseif($user_type==="ST"){
                //$query = $this->db->get_where('my_nsbm_stu', array('st_index' => $login_index,'password' => $password));
                
                $this->db->select('m.st_mynsbm_id,m.st_index,m.nick_name,m.nick_name,m.profile_pic,s.fname');
//                $this->db->select('st_index');
                $this->db->from('my_nsbm_stu m');
                $this->db->join('student as s', 'm.st_index = s.index_nm');
                $this->db->where(array('st_index' => $login_index,'password' => $password));
                $query = $this->db->get();
                
                $row=$query->row();
                $details['mynsbm_id']= $row->st_mynsbm_id;
                $details['st_index']= $row->st_index;
                $details['nick_name']= $row->nick_name;
                $details['fname']= $row->fname;
                $details['user_type']= "ST";
                $details['profile_pic']= $row->profile_pic;
                                               
                $this->nativesession->set('mynsbm_id',$details['mynsbm_id']);
                $this->nativesession->set('st_index',$details['st_index']);
                $this->nativesession->set('nick_name',$details['nick_name']);
                $this->nativesession->set('fname',$details['fname']);
                $this->nativesession->set('profile_pic',$details['profile_pic']);
                $this->nativesession->set('user_type',"ST");
                
                $url= $nexturl.
                        '?mynsbm_id='.$details['mynsbm_id'].
                        '&st_index='.$details['st_index'].
                        '&nick_name='.$details['nick_name'].
                        '&fname='.$details['fname'].
                        '&user_type='.$details['user_type'].
                        '&profile_pic='.$details['profile_pic'];
                //echo '<img src="'.$details['profile_pic'].'"/>';
                return $url;
            }elseif($user_type==="VL"){//If the user is a Permanent Employee or a Visiting Lecturer
                                
                $this->db->select('m.vl_mynsbm_id as mynsbm_id,m.password,v.fname,m.nick_name,m.profile_pic');
                $this->db->from('my_nsbm_vl as m');
                $this->db->join('visiting_lecturer as v', 'm.vl_mynsbm_id = v.vlecturer_id');
                $this->db->where(array('vl_mynsbm_id' => $login_index,'password' => $password));
                $query = $this->db->get();
                
                $row=$query->row();
                $details['mynsbm_id']= $row->vl_mynsbm_id;
                $details['nick_name']= $row->nick_name;
                $details['fname']= $row->fname;
                $details['user_type']= $user_type;
                $details['profile_pic']= $row->profile_pic;
                
                
                $this->nativesession->set('mynsbm_id',$details['mynsbm_id']);
                
                $url= $nexturl.
                        '?mynsbm_id='.$details['mynsbm_id'].
                        '&nick_name='.$details['nick_name'].
                        '&fname='.$details['fname'].
                        '&user_type='.$details['user_type'].
                        '&profile_pic='.$details['profile_pic'];
                return $url;
            }
                
        }elseif($type=="community"){
//            if($user_type==="CO"){
//
//            }elseif($user_type==="PL"){
//
//            }elseif($user_type==="VL"){
//
//            }elseif($user_type==="PE"){
//
//            }elseif($user_type==="ST"){
//
//            }                 
        }
    }
    
    
    /**
     * Upload a Pro Image
     */
    public function get_userss_data($email){
       $query = $this->db->get_where('users', array('email' => $email));
       $result=$query->row();
       return $result;
    }

    /**
     * Chech whether you are log or not
     * if you are log it'll return true
     * @return boolean True if you are log
     */
    function is_Log() {
        if ($this->session->userdata('is_logged_in')) {
            return true;
        }
        return false;
    }

}

