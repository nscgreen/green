<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {
    
    /* function create_user($data){
      $this->db->insert('users', $data);
      }

      function login($username, $password){
      $where = array('username' => $username ,
      'password' => $password ,
      );
      $this->db->select()->from('user')->where($where);
      $query = $this->db->get();
      return $query->first_row('array');
      } */

    function validate() {
        $this->db->where('email', $this->input->post('login_username'));
        $this->db->where('password', $this->input->post('login_password'));

        $query = $this->db->get('users');

        if ($query->num_rows == 1) {
            return true;
        }
    }

    /**
     * Upload a Pro Image
     */
    public function get_userss_data($email){
       $query = $this->db->get_where('users', array('email' => $email));
       $result=$query->row();
       return $result;
    }
    
    
    function proImgUpload() {

        $query = $this->db->query("SELECT MAX(id_users) FROM users;");
        $row = $query->row();
        $file_name = $_FILES['pro_img']['name'];
        $config['upload_path'] = realpath(APPPATH . '../assets/img/uploads');
        $config['allowed_types'] = 'gif|jpg|png';
        $config['file_name'] = $row->id_users . $this->input->post('fname');
        //$config['max_size']  = '100';
        //$config['max_width']  = '1024';
        //$config['max_height']  = '768';

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('pro_img')) {
            $error = array('error' => $this->upload->display_errors());
            echo $error;
        } else {
            $data = array('upload_data' => $this->upload->data());
            echo "success";
        }

        return;
    }

    function create_user() {
        //$this->proImgUpload();

        $new_member_insert_data = array(
            'email' => $this->input->post('email'),
            'password' => $this->input->post('password'),
            'f_name' => $this->input->post('fname'),
            'l_name' => $this->input->post('lname'),
            'index_no' => $this->input->post('index_no'),
            'batch' => $this->input->post('batch'),
            'uni' => $this->input->post('uni'),
            'contact_no' => $this->input->post('phone'),
            'pro_img' => 'null',
            'address' => $this->input->post('address')
        );

        $insert = $this->db->insert('users', $new_member_insert_data);
        return $insert;
    }

    /**
     * Chech whether you are log or not
     * if you are log it'll return true
     * @return boolean True if you are log
     */
    function is_Log() {
        if ($this->session->userdata('is_logged_in')) {
            return true;
        }
        return false;
    }

}

/* End of file user.php */
/* created by TK */
/* Location: ./application/models/user.php */