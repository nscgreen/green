<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="container">
                    <a href="http://main.d2real.com" target="_blank"><img class="image-responsive center-block nsbmlogo" src="<?php echo base_url(); ?>logos/nsbmwhite.gif"></a>
                    <header style="margin-bottom: -10px">
                        <h1 style="margin-bottom: -15px"><span style="color: #0099cc">NSBM</span> <strong>Connect</strong></h1>
                        <h2>PLUGGED once, no HASSLE, CONNECT with ALL</h2>
                        <div class="support-note">
                            <span class="note-ie">Sorry, only modern browsers.</span>
                        </div>
                    </header>
                </div>
            </div>
        </div>

        <section class="main">
            <form class="form-2" method="POST" action="">
                <h1><span class="log-in">Connect</span> or <span class="sign-up">sign up</span></h1>
                <p class="float">
                    <label for="login"><i class="fa fa-user"></i>Username</label>
                    <input type="text" name="login_index" required="TRUE" placeholder="Username or email">
                </p>

                <p class="float">
                    <label for="password" class="pull-left"><i class="icon-lock "></i>Password</label>
                    <input type="password" name="password" required="TRUE" placeholder="Password" class="showpassword">
                </p>
                <!--Hidden Inputs-->
                <input type="hidden" name="nexturl" value="<?php echo $nexturl ?>" />
                <input type="hidden" name="type" value="<?php echo $type ?>" />

                <p class="clearfix"> 
                    <button type="submit" class="log-twitter">Connect</button>
                    <a class="btn btn-info" style="width: 150px; height: 38px" data-toggle="modal" data-target="#signup">Sign up</a>
                </p>
            </form>​​
        </section>

        <div class="container" style="margin-bottom: 10px; margin-top: -15px">
            <div class="row" style="margin: 0 auto; padding: 0">
                <div class="col-xs-12" style="margin: 0 auto; padding: 0">
                    <div class="center-block" style="margin: 0 auto; padding: 0">
                        <center>
                            <ul id="lms-home-social-menu2"  style="margin: 0 auto; padding: 0">
                                <li><a href="http://main.d2real.com"  target="_blank">NSBM</a></li>
                                <li><a href="http://mynsbm.d2real.com"  target="_blank">My NSBM</a></li>
                                <li><a href="http://lms.d2real.com"  target="_blank">L.M.S</a></li>
                                <li><a href="http://forum.d2real.com"  target="_blank">Forum</a></li>
                                <li><a href="http://events.d2real.com"  target="_blank">Events</a></li>
                                <li><a href="http://communities.d2real.com"  target="_blank">Communities</a></li>
                                <li><a href="http://help.d2real.com"  target="_blank">Help</a></li>
                            </ul>
                        </center> 
                    </div>
                </div>
            </div>
        </div>

        <div class="container hidden-xs">
            <div class="row col-sm-8 col-sm-offset-2">
                <div class="row well well-sm"  style="background:rgba(0,0,0, 0.3); max-height: 70px; border:0px; padding: 5px;">
                    <div  class="col-sm-1 col-xs-1 col-sm-offset-2" style="padding: 5px">
                        <a href="http://www.youthskillsmin.gov.lk/" target="_blank">
                            <img style="max-height: 60px" src="logos/new/gov.png" class="img-responsive center-block">
                        </a>
                    </div>
                    <div class="col-sm-1 col-xs-1" style="padding: 5px">
                        <a href="http://www.ugc.ac.lk" target="_blank">
                            <img style="max-height: 60px" src="logos/new/ugc.png" class="img-responsive center-block">
                        </a>
                    </div>
                    <div class="col-sm-1 col-xs-1" style="padding: 5px">
                        <a href="http://www.nibm.lk" target="_blank">
                            <img style="max-height: 60px; margin-top: 19px;" src="logos/new/nibm.png" class="img-responsive center-block">
                        </a>
                    </div>
                    <div class="col-sm-1 col-xs-1" style="padding: 5px">
                        <a href="http://www.ucd.ie" target="_blank">
                            <img style="max-height: 55px; margin-top: 0px;" src="logos/new/ucd.png" class="img-responsive center-block">
                        </a>
                    </div>
                    <div class="col-sm-1 col-xs-1" style="padding: 5px">
                        <a href="http://www5.plymouth.ac.uk" target="_blank">
                            <img style="max-height: 60px; margin-top: 15px;" src="logos/new/plymouth.png" class="img-responsive center-block">
                        </a>
                    </div>
                    <div class="col-sm-1 col-xs-1" style="padding: 5px">
                        <a href="#" target="_blank">
                            <img style="max-height: 60px; margin-top: 10px;" src="logos/new/linkm.png" class="img-responsive center-block">
                        </a>
                    </div>
                    <div class="col-sm-1 col-xs-1" style="padding: 5px">
                        <a href="#" target="_blank">
                            <img style="max-height: 60px; margin-top: 25px;" src="logos/new/edexcel.png" class="img-responsive center-block">
                        </a>
                    </div>
                    <div class="col-sm-1 col-xs-1" style="padding: 5px">
                        <a href="#" target="_blank">
                            <img style="max-height: 60px; margin-top: 0px;" src="logos/acca.png" class="img-responsive center-block">
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <img class="pull-right" style="position:absolute; z-index:-1; bottom:0 ; right:0; " src="<?php echo base_url(); ?>assets_login/img/welcome.png"/>
        <p class="text-center">Copyright &COPY; National School of Business Management</p>

    </div>

<!-- Password Reset Modal -->
<div class="modal fade" id="reset" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title text-primary" id="myModalLabel">Forget your password ..?</h4>
      </div>
      <div class="modal-body">
          <p>We'll send you a temporary password please enter your My-NSBM profile email</p>
          <form class="form" action="" method="" role='form'>
              <input class="form-control" type="text" placeholder="Enter your e-mail to reset password" />
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-info">Send</button>
      </div>
    </div>
  </div>
</div>  <!-- Password Reset Modal END -->

<!-- SignUp Modal Start -->
<div class="modal fade" id="signup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title text-primary" id="myModalLabel">Sign Up to NSBM Web Network</h4>
      </div>
      <div class="modal-body">
          <h5>Please enter your Student/ Lecturer/ Coordinator index to Sign Up</h5>
          <form class="form" action="" method="" role='form'>
              <input class="form-control" type="text" placeholder="Ex : BSC-UCD-CSC-13.1-008 (Student)" />
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-info">Sign Up</button>
      </div>
    </div>
  </div>
</div>  <!-- SignUp Modal END -->
