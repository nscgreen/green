<body>
    <div class="container">

        <a href="http://d2real.com" target="_blank"><img class="image-responsive center-block nsbmlogo" src="<?php echo base_url(); ?>logos/nsbmwhite.gif"></a>
        <header style="margin-bottom: -10px">
            <h1 style="margin-bottom: -15px"><span style="color: #0099cc">NSBM</span> <strong>Connect</strong></h1>
            <h2>Plug ones, No hassle, Connect with all ...</h2>
            <div class="support-note">
                <span class="note-ie">Sorry, only modern browsers.</span>
            </div>
        </header>


        <section class="main">
            <form class="form-2" method="POST" action="">
                <h1><span class="log-in">Connect</span> or <span class="sign-up">sign up</span></h1>
                <p class="float">
                    <label for="login"><i class="fa fa-user"></i>Username</label>
                    <input type="text" name="login_index" placeholder="Username or email">
                </p>

                <p class="float">
                    <label for="password"><i class="icon-lock "></i>Password</label>
                    <input type="password" name="password" placeholder="Password" class="showpassword">
                </p>
                <!--Hidden Inputs-->
                <input type="hidden" name="nexturl" value="<?php echo $nexturl ?>" />
                <input type="hidden" name="type" value="<?php echo $type ?>" />

                <p class="clearfix"> 
                    <button type="submit" class="log-twitter">Connect</button>
                    <button type="submit" class="btn btn-info" style="width: 150px; height: 38px">Sign up</button>
                </p>
            </form>​​
        </section>
        <div class="container" style="margin-bottom: 10px; margin-top: -15px">
            <div class="row" style="margin: 0 auto; padding: 0">
                <div class="col-xs-12" style="margin: 0 auto; padding: 0">
                    <div class="center-block" style="margin: 0 auto; padding: 0">
                        <center>
                            <ul id="lms-home-social-menu2"  style="margin: 0 auto; padding: 0">
                                <li><a href="http://d2real.com"  target="_blank">NSBM</a></li>
                                <li><a href="http://my.d2real.com"  target="_blank">My NSBM</a></li>
                                <li><a href="http://lms.d2real.com"  target="_blank">L.M.S</a></li>
                                <li><a href="http://forum.d2real.com"  target="_blank">Forum</a></li>
                                <li><a href="http://events.d2real.com"  target="_blank">Events</a></li>
                                <li><a href="http://communities.d2real.com"  target="_blank">Communities</a></li>
                                <li><a href="http://help.d2real.com"  target="_blank">Help</a></li>
                            </ul>
                        </center> 
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row well well-sm col-sm-8 col-sm-offset-2 hidden-xs" style="background:rgba(0,0,0, 0.3); max-height: 70px; border:0px; padding: 5px;">
                <div  class="col-md-1 col-sm-2 col-xs-2"></div>
                <div  class="col-md-1 col-sm-2 col-xs-2"></div>
                <div  class="col-md-1 col-sm-2 col-xs-2">
                    <a href="http://www.youthskillsmin.gov.lk/" target="_blank">
                        <center><img style="margin-top: 0px; height: 60px; width: 40px" src="logos/gov.png" class="img-responsive"></center>
                    </a>
                </div>
                <div class="col-md-1 col-sm-2 col-xs-2">
                    <a href="http://www.ugc.ac.lk" target="_blank">
                        <center><img style="margin-top: 5px; height: 50px; width: 40px" src="logos/ugc.png" class="img-responsive"></center>
                    </a>
                </div>
                <div class="col-md-1 col-sm-2 col-xs-2">
                    <a href="http://www.ugc.ac.lk" target="_blank">
                        <center><img style="margin-top: 17px; height: 35px; width: 50px" src="logos/nibm.png" class="img-responsive"></center>
                    </a>
                </div>
                <div class="col-md-1 col-sm-2 col-xs-2">
                    <a href="http://www.nibm.lk" target="_blank">
                        <center><img style="margin-top: 0px;" src="logos/ucd.png" class="img-responsive"></center>
                    </a>
                </div>
                <div class="col-md-1 col-sm-2 col-xs-2">
                    <a href="http://www.d2real.com" target="_blank">
                        <center><img style="margin-top: 10px; height: 40px; width: 50px" src="logos/plymouth.png" class="img-responsive"></center>
                    </a>
                </div>
                <div class="col-md-1 col-sm-2 col-xs-2">
                    <a href="http://www.ucd.ie" target="_blank">
                        <center><img style="margin-top: 5px; height: 50px; width: 50px" src="logos/linkm.png" class="img-responsive"></center>
                    </a>
                </div>
                <div class="col-md-1 col-sm-2 col-xs-2">
                    <a href="http://www5.plymouth.ac.uk" target="_blank">
                        <center><img style="margin-top: 35px; height: 10px; width: 50px" src="logos/edexcel.png" class="img-responsive"></center>
                    </a>
                </div>
                <div class="col-md-1 col-sm-2 col-xs-2">
                    <a href="http://www5.plymouth.ac.uk" target="_blank">
                        <center><img style="margin-top: 5px; height: 50px; width: 50px" src="logos/acca.png" class="img-responsive"></center>
                    </a>
                </div>

                <div  class="col-md-1 col-sm-2 col-xs-2">
                    <div  class="col-md-1 col-sm-2 col-xs-2"></div>
                </div>
            </div>
        </div>
        <img class="pull-right" style="position:absolute; z-index:-1; bottom:0 ; right:0; " src="<?php echo base_url(); ?>assets_login/img/welcome.png"/>
        <center>
            <p class="text-center">Copyright &COPY; National School of Business Management</p>
        </center>
    </div>
    
    
    
<!--<div>
    <?php
    //$formurl=  base_url();
    ?>
    <?php // echo $formurl ?>
    <form method="POST" action="">
        <input type="text" name="login_index"/>
        <input type="password" name="password"/>
        
        <input type="hidden" name="nexturl" value="<?php // echo $nexturl ?>" />
        <input type="hidden" name="type" value="<?php // echo $type ?>" />        
        
        <button type="submit">Login</button>
    </form>
</div>-->