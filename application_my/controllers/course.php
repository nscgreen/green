<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Course extends CI_Controller {
    
        public function index() {
            
            $data = array(
                'title' => 'my-NSBM',
                'mDescription' => 'Learning Management System of National School of business Management',
                'mKeywords' => 'LMS',
            );
            
            $this->load->model('mprofile');
            if(!$this->nativesession->get('islogged')===TRUE){
            redirect(base_url());
            }else{
                $dataall['password']= $this->mprofile->get_student_password($this->nativesession->get('mynsbm_id'));
                $dataall['profile_pic']= $this->nativesession->get('profile_pic');
                $dataall['fname']= $this->nativesession->get('fname');
                $dataall['nick_name']= $this->nativesession->get('nick_name');
            }
            
            $this->load->view('vmy_header', $data);
            $this->load->view('vmy_course',$dataall);
            $this->load->view('vmy_footer');
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */