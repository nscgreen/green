<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
    
    public function index() {
        $data = array(
            'title' => 'my-NSBM',
            'mDescription' => 'Learning Management System of National School of business Management',
            'mKeywords' => 'LMS',
        );
        
        if(isset($_GET['mynsbm_id'])){
        $this->load->model('logged/mlg_user','mlg_user');
        }
        //$lg_data=$this->mlg_user->islogged_user();
        //var_dump($lg_data);sessionIsset   login_user
        if(isset($_GET['mynsbm_id']) && !$this->nativesession->sessionIsset('just_logged')===TRUE && !$this->nativesession->get('just_logged')===TRUE){
            $this->mlg_user->login_user();
            redirect(base_url());
        }elseif($this->nativesession->sessionIsset('islogged')===TRUE && $this->nativesession->get('islogged')===TRUE && 
                $this->nativesession->sessionIsset('just_logged')===TRUE && $this->nativesession->get('just_logged')===TRUE){
            
            $this->nativesession->delete('just_logged');
            
            $dataall['profile_pic']= $this->nativesession->get('profile_pic');
            $dataall['fname']= $this->nativesession->get('fname');
            $dataall['nick_name']= $this->nativesession->get('nick_name');
            
            $this->load->view('vmy_header', $data);
            $this->load->model('mprofile');
            if($this->nativesession->get('user_type')==="ST"){
            $dataall['password']= $this->mprofile->get_student_password($this->nativesession->get('mynsbm_id'));
//            $dataall['profile_details']= $this->mprofile->get_student_profile_details($this->nativesession->get('mynsbm_id'));
            $this->load->view('vmy_home',$dataall);
            }
            
            $this->load->view('vmy_footer');
        }elseif($this->nativesession->sessionIsset('islogged')===TRUE && $this->nativesession->get('islogged')===TRUE){
            $this->nativesession->delete('just_logged');
            
            $dataall['profile_pic']= $this->nativesession->get('profile_pic');
            $dataall['fname']= $this->nativesession->get('fname');
            $dataall['nick_name']= $this->nativesession->get('nick_name');
            
            $this->load->view('vmy_header', $data);
            $this->load->model('mprofile');
            if($this->nativesession->get('user_type')==="ST"){
            $dataall['password']= $this->mprofile->get_student_password($this->nativesession->get('mynsbm_id'));
//            $dataall['profile_details']= $this->mprofile->get_student_profile_details($this->nativesession->get('mynsbm_id'));
            $this->load->view('vmy_home',$dataall);
            }
            
            $this->load->view('vmy_footer');

        }elseif(!$this->nativesession->sessionIsset('islogged')===TRUE && !$this->nativesession->get('islogged')===TRUE){
            $this->load->view('vmy_header', $data);
            $this->load->view('vmy_login');
            $this->load->view('vmy_footer');
            //redirect(base_url());
        }else{
            $this->load->view('vmy_header', $data);
            $this->load->view('vmy_login');
            $this->load->view('vmy_footer');
            //redirect(base_url());
        }

    }

    public function logged() {
        $data = array(
            'title' => 'my-NSBM',
            'mDescription' => 'Learning Management System of National School of business Management',
            'mKeywords' => 'LMS',
        );

            $this->load->view('vmy_header', $data);
            $this->load->view('vmy_home');
            $this->load->view('vmy_footer');
    }

    public function lecturer() {
        $data = array(
            'title' => 'my-NSBM',
            'mDescription' => 'Learning Management System of National School of business Management',
            'mKeywords' => 'LMS',
        );

            $this->load->view('vmy_header', $data);
            $this->load->view('vmy_lec_home');
            $this->load->view('vmy_footer');
    }
    public function student() {
        $data = array(
            'title' => 'my-NSBM',
            'mDescription' => 'Learning Management System of National School of business Management',
            'mKeywords' => 'LMS',
        );

//            $this->load->view('vmy_header', $data);
            $this->load->view('vmy_uni_profile_pub',$data);
//            $this->load->view('vmy_footer');
    }
        
    public function logout() {
        
        $this->nativesession->delete('islogged');
        $this->nativesession->set('islogged',FALSE);
        $this->nativesession->delete('mynsbm_id');
        $this->nativesession->delete('user_type');
        $this->nativesession->delete('st_index');
        $this->nativesession->delete('nick_name');
        
        redirect(base_url());
    }

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */