<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
    
    public function index() {
        $this->load->database();
        $data = array(
            'title' => 'my-NSBM',
            'mDescription' => 'Learning Management System of National School of business Management',
            'mKeywords' => 'LMS',
        );

        $this->load->model('logged/mlg_user','mlg_user');
        $lg_data=$this->mlg_user->islogged_user();
        //var_dump($lg_data);
        if(!$lg_data['islogged']===FALSE){
            if(!$this->nativesession->get('islogged')===TRUE){
                redirect(base_url());
            }else{
                $dataall['profile_pic']= $this->nativesession->get('profile_pic');
                $dataall['fname']= $this->nativesession->get('fname');
                $dataall['nick_name']= $this->nativesession->get('nick_name');
            }
            $this->load->view('vmy_header', $data);
            $this->load->view('vmy_home',$dataall);
            $this->load->view('vmy_footer');

        }else{
            //echo 'not logged';
            $this->load->view('vmy_header', $data);
            $this->load->view('vmy_login');
            $this->load->view('vmy_footer');

            // Unset all sesions
            $this->nativesession->delete('islogged');
            $this->nativesession->delete('mynsbm_id');
            $this->nativesession->delete('user_type');
            $this->nativesession->delete('st_index');
            $this->nativesession->delete('nick_name');
        }

    }

    public function logged() {
        $data = array(
            'title' => 'my-NSBM',
            'mDescription' => 'Learning Management System of National School of business Management',
            'mKeywords' => 'LMS',
        );

            $this->load->view('vmy_header', $data);
            $this->load->view('vmy_home');
            $this->load->view('vmy_footer');
    }

    public function lecturer() {
        $data = array(
            'title' => 'my-NSBM',
            'mDescription' => 'Learning Management System of National School of business Management',
            'mKeywords' => 'LMS',
        );

            $this->load->view('vmy_header', $data);
            $this->load->view('vmy_lec_home');
            $this->load->view('vmy_footer');
    }
    public function student() {
        $data = array(
            'title' => 'my-NSBM',
            'mDescription' => 'Learning Management System of National School of business Management',
            'mKeywords' => 'LMS',
        );

//            $this->load->view('vmy_header', $data);
            $this->load->view('vmy_uni_profile_pub',$data);
//            $this->load->view('vmy_footer');
    }
        
    public function logout()
    {
        $this->load->database();
        $this->load->helper('url');
        $this->load->helper('html');
        
        $this->nativesession->delete('islogged');
        $this->nativesession->set('islogged',FALSE);
        $this->nativesession->delete('mynsbm_id');
        $this->nativesession->delete('user_type');
        $this->nativesession->delete('st_index');
        $this->nativesession->delete('nick_name');
        
        redirect(base_url());
        
    }

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */