<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {
    
    public function index() {
        $this->load->database();
        $data = array(
            'title' => 'my-NSBM',
            'mDescription' => 'Learning Management System of National School of business Management',
            'mKeywords' => 'LMS',
        );

        $this->load->model('logged/mlg_user','mlg_user');
        //$lg_data=$this->mlg_user->islogged_user();
        //var_dump($lg_data);sessionIsset   login_user
        if(isset($_GET['mynsbm_id']) && !$this->nativesession->sessionIsset('just_logged')===TRUE && !$this->nativesession->get('just_logged')===TRUE){
            $this->mlg_user->login_user();
            redirect(base_url());
        }elseif($this->nativesession->sessionIsset('islogged')===TRUE && $this->nativesession->get('islogged')===TRUE && 
                $this->nativesession->sessionIsset('just_logged')===TRUE && $this->nativesession->get('just_logged')===TRUE){
            
            $this->nativesession->delete('just_logged');
            
            $dataall['profile_pic']= $this->nativesession->get('profile_pic');
            $dataall['fname']= $this->nativesession->get('fname');
            $dataall['nick_name']= $this->nativesession->get('nick_name');

            if($this->nativesession->sessionIsset('password_changed')===TRUE && $this->nativesession->get('password_changed')===TRUE){
                $this->nativesession->delete('password_changed');
                $dataall['password_changed']= TRUE;
                //var_dump($dataall);
            }else{
                $dataall['password_changed']= FALSE;
            }
            
            $this->load->model('mprofile');
            $this->load->view('vmy_header', $data);
            if($this->nativesession->get('user_type')==="ST"){
                $dataall['password']= $this->mprofile->get_student_password($this->nativesession->get('mynsbm_id'));
                $dataall['profile_details']= $this->mprofile->get_student_profile_details($this->nativesession->get('mynsbm_id'));
                $this->load->view('vmy_profile',$dataall);
            }
//                if($this->nativesession->get('user_type')==="PL" || $this->nativesession->get('user_type')==="PL"){
//                    $dataall['profile_details']= $this->mprofile->get_pl_profile_details($this->nativesession->get('mynsbm_id'));
//                    $this->load->view('vmy_profile',$dataall);
//                }
//                if($this->nativesession->get('user_type')==="ST"){
//                    $dataall['profile_details']= $this->mprofile->get_student_profile_details($this->nativesession->get('mynsbm_id'));
//                    $this->load->view('vmy_profile',$dataall);
//                }

            $this->load->view('vmy_footer');
        }elseif($this->nativesession->sessionIsset('islogged')===TRUE && $this->nativesession->get('islogged')===TRUE){
            
            $this->nativesession->delete('just_logged');
            
            $dataall['profile_pic']= $this->nativesession->get('profile_pic');
            $dataall['fname']= $this->nativesession->get('fname');
            $dataall['nick_name']= $this->nativesession->get('nick_name');

            if($this->nativesession->sessionIsset('password_changed')===TRUE && $this->nativesession->get('password_changed')===TRUE){
                $dataall['password_changed']= TRUE;
                $this->nativesession->delete('password_changed');
                //var_dump($dataall);
            }else{
                $dataall['password_changed']= FALSE;
            }
            
            $this->load->model('mprofile');
            $this->load->view('vmy_header', $data);
            if($this->nativesession->get('user_type')==="ST"){
                $dataall['password']= $this->mprofile->get_student_password($this->nativesession->get('mynsbm_id'));
                $dataall['profile_details']= $this->mprofile->get_student_profile_details($this->nativesession->get('mynsbm_id'));
                //var_dump($dataall);
                $this->load->view('vmy_profile',$dataall);
            }
//                if($this->nativesession->get('user_type')==="PL" || $this->nativesession->get('user_type')==="PL"){
//                    $dataall['profile_details']= $this->mprofile->get_pl_profile_details($this->nativesession->get('mynsbm_id'));
//                    $this->load->view('vmy_profile',$dataall);
//                }
//                if($this->nativesession->get('user_type')==="ST"){
//                    $dataall['profile_details']= $this->mprofile->get_student_profile_details($this->nativesession->get('mynsbm_id'));
//                    $this->load->view('vmy_profile',$dataall);
//                }

            $this->load->view('vmy_footer');

        }elseif(!$this->nativesession->sessionIsset('islogged')===TRUE && !$this->nativesession->get('islogged')===TRUE){
            //echo 'not logged';
            $this->load->view('vmy_header', $data);
            $this->load->view('vmy_login');
            $this->load->view('vmy_footer');

            // Unset all sesions
//            $this->nativesession->delete('islogged');
//            $this->nativesession->delete('mynsbm_id');
//            $this->nativesession->delete('user_type');
//            $this->nativesession->delete('st_index');
//            $this->nativesession->delete('nick_name');
        }else{
            //echo 'not logged';
            $this->load->view('vmy_header', $data);
            $this->load->view('vmy_login');
            $this->load->view('vmy_footer');

            // Unset all sesions
//            $this->nativesession->delete('islogged');
//            $this->nativesession->delete('mynsbm_id');
//            $this->nativesession->delete('user_type');
//            $this->nativesession->delete('st_index');
//            $this->nativesession->delete('nick_name');
        }
    }
    
    public function index2()
    {
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('url');

        $data = array(
            'title' => 'my-NSBM',
            'mDescription' => 'Learning Management System of National School of business Management',
            'mKeywords' => 'LMS',
        );

        if(!$this->nativesession->get('islogged')===TRUE){
        redirect(base_url());
        }else{
            $dataall['profile_pic']= $this->nativesession->get('profile_pic');
            $dataall['fname']= $this->nativesession->get('fname');
            $dataall['nick_name']= $this->nativesession->get('nick_name');

            $this->load->model('mprofile');
            $this->load->view('vmy_header', $data);
            if($this->nativesession->get('user_type')==="ST"){
                $dataall['password']= $this->mprofile->get_student_password($this->nativesession->get('st_index'));
                $dataall['profile_details']= $this->mprofile->get_student_profile_details($this->nativesession->get('st_index'));
                $this->load->view('vmy_uni_profile',$dataall);
            }
//                if($this->nativesession->get('user_type')==="PL" || $this->nativesession->get('user_type')==="PL"){
//                    $dataall['profile_details']= $this->mprofile->get_pl_profile_details($this->nativesession->get('mynsbm_id'));
//                    $this->load->view('vmy_profile',$dataall);
//                }
//                if($this->nativesession->get('user_type')==="ST"){
//                    $dataall['profile_details']= $this->mprofile->get_student_profile_details($this->nativesession->get('mynsbm_id'));
//                    $this->load->view('vmy_profile',$dataall);
//                }

            $this->load->view('vmy_footer');  


        }       

    }
        
    public function update_profile(){
        $this->load->helper('html');
        $this->load->helper('url');      


        $mynsbmdetails['st_mynsbm_id']=$this->input->post('st_mynsbm_id', TRUE);
        $mynsbmdetails['nick_name']=$this->input->post('nick_name', TRUE);
        $mynsbmdetails['mobile']=$this->input->post('mobile', TRUE);
        $mynsbmdetails['email']=$this->input->post('email', TRUE);
        $mynsbmdetails['my_description']=$this->input->post('my_description', TRUE);
        
        $mynsbmdetails['link_fb']=$this->input->post('link_fb', TRUE);
        $mynsbmdetails['link_tw']=$this->input->post('link_tw', TRUE);
        $mynsbmdetails['link_ln']=$this->input->post('link_ln', TRUE);
        $mynsbmdetails['link_gl']=$this->input->post('link_gl', TRUE);

        //$mynsbmdetails['profile_pic']=$this->input->post('profile_pic', TRUE);
        $mynsbmdetails['profile_pic_old']=$this->input->post('profile_pic_old', TRUE);

        $this->load->model('mfile_handler');
        
        $this->load->model('mprofile');
        if(isset($_FILES['profile_pic']['name']) && !empty($_FILES['profile_pic']['name'])) {
            $this->load->model('mfile_handler');
            $this->mfile_handler->delete_file($mynsbmdetails['profile_pic_old']);
            
            // Check and create the folders for the module
            $uploadertype='students';
    //        $uploadtype='assignments';
            $folders=array(
                        1=>$mynsbmdetails['st_mynsbm_id']
            );           

            $uploadpaths=$this->mfile_handler->create_folders($uploadertype,$folders);

            $folderupload=$uploadpaths['folderupload'];
            $folderdb=$uploadpaths['folderdb'];
            
            $mynsbmdetails['profile_pic_file_name']=$mynsbmdetails['st_mynsbm_id'];
            $uploaddetails=$this->mprofile->upload_profile_pic($mynsbmdetails['profile_pic_file_name'],$folderupload,$folderdb,'profile_pic');

            if ($this->nativesession->get('uploadfailed')==TRUE){                
                redirect(base_url());  
            }
           
            //echo $uploaddetails['profile_picpath'];
            $mynsbmdetails['profile_pic']= $uploaddetails['profile_pic_path'];
        }else{
            $mynsbmdetails['profile_pic']=$this->input->post('profile_pic_old', TRUE);
        }

        $this->mprofile->update_profile($mynsbmdetails);
        //var_dump($mynsbmdetails);
        redirect(base_url());  
    }
    
    public function change_password(){
        $this->load->helper('html');
        $this->load->helper('url');      

        $this->load->model('mprofile');
        $mynsbmdetails['st_mynsbm_id']=$this->input->post('st_mynsbm_id', TRUE);
        $mynsbmdetails['password']=$this->input->post('password', TRUE);

        $this->mprofile->update_password($mynsbmdetails);
        
        //var_dump($mynsbmdetails);
        
        $this->nativesession->set('password_changed',TRUE);
        redirect(base_url());
    }
        
        
        
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */