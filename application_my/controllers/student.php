<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Student extends CI_Controller {
    
    public function index()
    {
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('url');

        $data = array(
            'title' => 'my-NSBM',
            'mDescription' => 'Learning Management System of National School of business Management',
            'mKeywords' => 'LMS',
        );

        if(!$this->nativesession->get('islogged')===TRUE){
        redirect(base_url());
        }else{
            $dataall['profile_pic']= $this->nativesession->get('profile_pic');
            $dataall['fname']= $this->nativesession->get('fname');
            $dataall['nick_name']= $this->nativesession->get('nick_name');

            $this->load->model('mprofile');
            $this->load->view('vmy_header', $data);
            if($this->nativesession->get('user_type')==="ST"){
                $dataall['profile_details']= $this->mprofile->get_student_profile_details($this->nativesession->get('st_index'));
                $this->load->view('vmy_profile',$dataall);
            }
//                if($this->nativesession->get('user_type')==="PL" || $this->nativesession->get('user_type')==="PL"){
//                    $dataall['profile_details']= $this->mprofile->get_pl_profile_details($this->nativesession->get('mynsbm_id'));
//                    $this->load->view('vmy_profile',$dataall);
//                }
//                if($this->nativesession->get('user_type')==="ST"){
//                    $dataall['profile_details']= $this->mprofile->get_student_profile_details($this->nativesession->get('mynsbm_id'));
//                    $this->load->view('vmy_profile',$dataall);
//                }

            $this->load->view('vmy_footer');  


        }       

    }
        
    public function update_profile(){
        $this->load->helper('html');
        $this->load->helper('url');      


        $mynsbmdetails['st_mynsbm_id']=$this->input->post('st_mynsbm_id', TRUE);
        $mynsbmdetails['nick_name']=$this->input->post('nick_name', TRUE);
        $mynsbmdetails['mobile']=$this->input->post('mobile', TRUE);
        $mynsbmdetails['email']=$this->input->post('email', TRUE);
        
        $mynsbmdetails['link_fb']=$this->input->post('link_fb', TRUE);
        $mynsbmdetails['link_tw']=$this->input->post('link_tw', TRUE);
        $mynsbmdetails['link_ln']=$this->input->post('link_ln', TRUE);
        $mynsbmdetails['link_gl']=$this->input->post('link_gl', TRUE);

        //$mynsbmdetails['profile_pic']=$this->input->post('profile_pic', TRUE);
        $mynsbmdetails['profile_pic_old']=$this->input->post('profile_pic_old', TRUE);

        $this->load->model('mfile_handler');
                    
        // Check and create the folders for the module
        $uploadertype='students';
//        $uploadtype='assignments';
        $folders=array(
                    1=>$assignmentdetails['batch'],
                    2=>$assignmentdetails['acyear'],
        );           
        $uploadpaths=$this->mfile_handler->create_folders($uploadertype,$uploadtype,$folders);
        
        $this->load->model('admin/mschool','mschool');
        if(isset($_FILES['profile_pic']['name']) && !empty($_FILES['profile_pic']['name'])) {
            $this->load->model('mfile_handler');
            $this->mfile_handler->delete_file($mynsbmdetails['profile_pic_old']);

            $mynsbmdetails['profile_pic_file_name']=$mynsbmdetails['name'];

            $folderupload="profile_pics/schools/";
            $uploaddetails=$this->mschool->upload_profile_pic($mynsbmdetails['profile_pic_file_name'],$folderupload,'profile_pic');

            if ($this->nativesession->get('uploadfailed')==TRUE) {
                //redirect('admin/schools');  
            }
           
            //echo $uploaddetails['profile_picpath'];
            $mynsbmdetails['profile_pic']= $uploaddetails['profile_picpath'];
        }else{
            $mynsbmdetails['profile_pic']=$this->input->post('profile_pic_old', TRUE);
        }

        $this->mschool->update_school($mynsbmdetails);
        //var_dump($mynsbmdetails);
        //redirect('admin/schools');  
    }
        
        
        
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */