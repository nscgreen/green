<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class University extends CI_Controller {
    
    public function profile(){
        $this->load->database();
        $data = array(
            'title' => 'my-NSBM',
            'mDescription' => 'Learning Management System of National School of business Management',
            'mKeywords' => 'LMS',
        );

        $this->load->model('logged/mlg_user','mlg_user');
        //$lg_data=$this->mlg_user->islogged_user();
        //var_dump($lg_data);sessionIsset   login_user
        if(isset($_GET['mynsbm_id']) && !$this->nativesession->sessionIsset('just_logged')===TRUE && !$this->nativesession->get('just_logged')===TRUE){
            $this->mlg_user->login_user();
            redirect(base_url());
        }elseif($this->nativesession->sessionIsset('islogged')===TRUE && $this->nativesession->get('islogged')===TRUE && 
                $this->nativesession->sessionIsset('just_logged')===TRUE && $this->nativesession->get('just_logged')===TRUE){
            
            $this->nativesession->delete('just_logged');
            
            $dataall['mynsbm_id']= $this->nativesession->get('mynsbm_id');            
            $dataall['profile_pic']= $this->nativesession->get('profile_pic');
            $dataall['fname']= $this->nativesession->get('fname');
            $dataall['nick_name']= $this->nativesession->get('nick_name');
            
            $this->load->view('vmy_header', $data);
            $this->load->model('mprofile');
            if($this->nativesession->get('user_type')==="ST"){
                $dataall['password']= $this->mprofile->get_student_password($this->nativesession->get('mynsbm_id'));
                if($this->nativesession->sessionIsset('add_project_success')===TRUE && $this->nativesession->get('add_project_success')===TRUE){                   
                    $this->nativesession->delete('add_project_success');
                    $dataall['add_project_success']=TRUE;
                    $dataall['add_project_failed']=FALSE;
                }elseif($this->nativesession->sessionIsset('uploadfailed')===TRUE && $this->nativesession->get('uploadfailed')===TRUE){
                    $this->nativesession->delete('uploadfailed');
                    
                    $dataall['add_project_failed']=TRUE;
                    $dataall['add_project_success']=FALSE;
                }else{
                    $dataall['add_project_success']=FALSE;
                    $dataall['add_project_failed']=FALSE;
                }
                $dataall['user_name']= $this->mprofile->get_student_user_name($dataall['mynsbm_id']);
                $this->load->view('vmy_uni_profile',$dataall);
            }
            
            $this->load->view('vmy_footer');
        }elseif($this->nativesession->sessionIsset('islogged')===TRUE && $this->nativesession->get('islogged')===TRUE){
            $this->nativesession->delete('just_logged');

            $dataall['mynsbm_id']= $this->nativesession->get('mynsbm_id'); 
            $dataall['profile_pic']= $this->nativesession->get('profile_pic');
            $dataall['fname']= $this->nativesession->get('fname');
            $dataall['nick_name']= $this->nativesession->get('nick_name');
            
            $this->load->view('vmy_header', $data);
            $this->load->model('mprofile');
            if($this->nativesession->get('user_type')==="ST"){
                $dataall['password']= $this->mprofile->get_student_password($this->nativesession->get('mynsbm_id'));
                if($this->nativesession->sessionIsset('add_project_success')===TRUE && $this->nativesession->get('add_project_success')===TRUE){                   
                    $this->nativesession->delete('add_project_success');
                    $dataall['add_project_success']=TRUE;
                    $dataall['add_project_failed']=FALSE;
                }elseif($this->nativesession->sessionIsset('uploadfailed')===TRUE && $this->nativesession->get('uploadfailed')===TRUE){
                    $this->nativesession->delete('uploadfailed');
                    
                    $dataall['add_project_failed']=TRUE;
                    $dataall['add_project_success']=FALSE;
                }else{
                    $dataall['add_project_success']=FALSE;
                    $dataall['add_project_failed']=FALSE;
                }
                $dataall['user_name']= $this->mprofile->get_student_user_name($dataall['mynsbm_id']);
                $this->load->view('vmy_uni_profile',$dataall);
            }
            
            $this->load->view('vmy_footer');

        }elseif(!$this->nativesession->sessionIsset('islogged')===TRUE && !$this->nativesession->get('islogged')===TRUE){
            redirect(base_url());
        }else{
            redirect(base_url());
        }

    }
    
    public function public_profile($username){
        $this->load->database();
        $data = array(
            'title' => 'my-NSBM',
            'mDescription' => 'Learning Management System of National School of business Management',
            'mKeywords' => 'LMS',
        );

        $this->load->model('mprofile');
        $details=$this->mprofile->get_student_public_profile_details($username);
        
        $data['public_profile_details']=$details['public_profile_details'];
        $data['stu_projects_details']= $details['stu_projects_details'];
        $data['stu_experiences_details']= $details['stu_experiences_details'];
        $data['stu_skills_details']= $details['stu_skills_details'];

//        $this->load->view('vmy_header');            
        $this->load->view('vmy_uni_profile_pub', $data);
//        $this->load->view('vmy_footer'); 

    }
        
    public function add_project(){
        $this->load->helper('html');
        $this->load->helper('url');      


        $stuprojectdetails['st_mynsbm_id']=$this->input->post('st_mynsbm_id', TRUE);
        $stuprojectdetails['project_title']=$this->input->post('project_title', TRUE);
        $stuprojectdetails['descrption']=$this->input->post('descrption', TRUE);
        $stuprojectdetails['details_link']=$this->input->post('details_link', TRUE);
        
        //$stuprojectdetails['image_path']=$this->input->post('image_path', TRUE);

        //$stuprojectdetails['profile_pic']=$this->input->post('profile_pic', TRUE);
        
        $this->load->model('mfile_handler');
        
        $this->load->model('mprofile');
        if(isset($_FILES['image_path']['name']) && !empty($_FILES['image_path']['name'])) {
            $this->load->model('mfile_handler');
            
            $name = $_FILES["image_path"]["name"];
            $ext = end((explode(".", $name))); 
            $stuprojectdetails['file_name']=$stuprojectdetails['project_title'].'.'.$ext;
            
            // Check and create the folders for the module
            $uploadertype='students/stu_experiences';
            
            $folders=array(
                        1=>$stuprojectdetails['st_mynsbm_id']
            );           

            $uploadpaths=$this->mfile_handler->create_folders($uploadertype,$folders);

            $folderupload=$uploadpaths['folderupload'];
            $folderdb=$uploadpaths['folderdb'];
            
            $stuprojectdetails['profile_pic_file_name']=$stuprojectdetails['st_mynsbm_id'];
            $uploaddetails=$this->mprofile->upload_project_pic($stuprojectdetails['project_title'],$folderupload,$folderdb,'image_path');

            if ($this->nativesession->get('uploadfailed')==TRUE){                
                redirect(base_url().'university/profile');  
            }
           
            //echo $uploaddetails['profile_picpath'];
            $stuprojectdetails['image_path']= $uploaddetails['project_pic_path'];
        }else{
            $stuprojectdetails['image_path']='';
        }
        //var_dump($stuprojectdetails);
        $this->mprofile->add_project_uni_profile($stuprojectdetails);
        $this->nativesession->set('add_project_success',TRUE);
        //var_dump($stuprojectdetails);
        redirect(base_url().'university/profile');  
    }
    
    public function add_experience(){
        $this->load->helper('html');
        $this->load->helper('url');      


        $stuexperiencedetails['st_mynsbm_id']=$this->input->post('st_mynsbm_id', TRUE);
        $stuexperiencedetails['company_name']=$this->input->post('company_name', TRUE);
        $stuexperiencedetails['job_title']=$this->input->post('job_title', TRUE);
        $stuexperiencedetails['from']=$this->input->post('from', TRUE);
        $stuexperiencedetails['to']=$this->input->post('to', TRUE);
        $stuexperiencedetails['descrption']=$this->input->post('descrption', TRUE);
        $stuexperiencedetails['company_website']=$this->input->post('company_website', TRUE);
        
        //$stuexperiencedetails['image_path']=$this->input->post('image_path', TRUE);

        //$stuexperiencedetails['profile_pic']=$this->input->post('profile_pic', TRUE);
        
        $this->load->model('mfile_handler');
        
        $this->load->model('mprofile');
        if(isset($_FILES['image_path']['name']) && !empty($_FILES['image_path']['name'])) {
            $this->load->model('mfile_handler');
            
            $name = $_FILES["image_path"]["name"];
            $ext = end((explode(".", $name))); 
//            $stuexperiencedetails['file_name']=$stuexperiencedetails['project_title'].'.'.$ext;
            
            // Check and create the folders for the module
            $uploadertype='students/stu_experiences';
            
            $folders=array(
                        1=>$stuexperiencedetails['st_mynsbm_id']
            );           

            $uploadpaths=$this->mfile_handler->create_folders($uploadertype,$folders);

            $folderupload=$uploadpaths['folderupload'];
            $folderdb=$uploadpaths['folderdb'];
            
            $stuexperiencedetails['profile_pic_file_name']=$stuexperiencedetails['st_mynsbm_id'];
            $uploaddetails=$this->mprofile->upload_project_pic($stuexperiencedetails['company_name'],$folderupload,$folderdb,'image_path');

            if ($this->nativesession->get('uploadfailed')==TRUE){                
                redirect(base_url().'university/profile');  
            }
           
            //echo $uploaddetails['profile_picpath'];
            $stuexperiencedetails['image_path']= $uploaddetails['project_pic_path'];
        }else{
            $stuexperiencedetails['image_path']='';
        }
        //var_dump($stuexperiencedetails);
        $this->mprofile->add_experience_uni_profile($stuexperiencedetails);
        $this->nativesession->set('add_project_success',TRUE);
        //var_dump($stuexperiencedetails);
        redirect(base_url().'university/profile');  
    }
    
    public function add_skill(){
        $this->load->helper('html');
        $this->load->helper('url');      


        $stuprojectdetails['st_mynsbm_id']=$this->input->post('st_mynsbm_id', TRUE);
        $stuprojectdetails['skill_title']=$this->input->post('skill_title', TRUE);
        $stuprojectdetails['descrption']=$this->input->post('descrption', TRUE);
        $stuprojectdetails['details_link']=$this->input->post('details_link', TRUE);
        
        //$stuprojectdetails['image_path']=$this->input->post('image_path', TRUE);

        //$stuprojectdetails['profile_pic']=$this->input->post('profile_pic', TRUE);
        
        $this->load->model('mfile_handler');
        
        $this->load->model('mprofile');
        if(isset($_FILES['image_path']['name']) && !empty($_FILES['image_path']['name'])) {
            $this->load->model('mfile_handler');
            
            $name = $_FILES["image_path"]["name"];
            $ext = end((explode(".", $name))); 
           // $stuprojectdetails['file_name']=$stuprojectdetails['project_title'].'.'.$ext;
            
            // Check and create the folders for the module
            $uploadertype='students/stu_skills';
            
            $folders=array(
                        1=>$stuprojectdetails['st_mynsbm_id']
            );           

            $uploadpaths=$this->mfile_handler->create_folders($uploadertype,$folders);

            $folderupload=$uploadpaths['folderupload'];
            $folderdb=$uploadpaths['folderdb'];
            
            $stuprojectdetails['profile_pic_file_name']=$stuprojectdetails['st_mynsbm_id'];
            $uploaddetails=$this->mprofile->upload_project_pic($stuprojectdetails['skill_title'],$folderupload,$folderdb,'image_path');

            if ($this->nativesession->get('uploadfailed')==TRUE){                
                redirect(base_url().'university/profile');  
            }
           
            //echo $uploaddetails['profile_picpath'];
            $stuprojectdetails['image_path']= $uploaddetails['project_pic_path'];
        }else{
            $stuprojectdetails['image_path']='';
        }
        //var_dump($stuprojectdetails);
        $this->mprofile->add_skill_uni_profile($stuprojectdetails);
        $this->nativesession->set('add_project_success',TRUE);
        //var_dump($stuprojectdetails);
        redirect(base_url().'university/profile');  
    }
    
    
    
    
    
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */