<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mlg_user extends CI_Model {
        public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
                $this->load->library('nativesession');
	}
        
        public function load_menu_user_details()
	{
		if($this->nativesession->get('islogged')===TRUE){
//                    if($this->nativesession->sessionIsset('st_index')){

                    $user_details['mynsbm_id']=$this->nativesession->get('mynsbm_id');
                    if($this->nativesession->get('user_type')==="ST"){
                        $user_details['st_index']=$this->nativesession->get('st_index');
                    }
                    
                    $user_details['nick_name']=$this->nativesession->get('nick_name');
                    $user_details['fname']=$this->nativesession->get('fname');
                    $user_details['profile_pic']=$this->nativesession->get('profile_pic');
                    $user_details['user_type']=$this->nativesession->get('user_type');
                    
                    return $user_details;
                    
                }else{
                    return FALSE;
                }
	}
        
//        public function islogged_user()
//	{
//		if(isset($_GET['mynsbm_id'])){
//                    $data['islogged']=TRUE;
//                    $data['user_type']=$this->nativesession->get('user_type');
//                    
//                    $this->nativesession->set('islogged',TRUE);
//                    $this->nativesession->set('mynsbm_id',$this->input->get('mynsbm_id'));
//                    $this->nativesession->set('st_index',$this->input->get('st_index'));
//                    $this->nativesession->set('nick_name',$this->input->get('nick_name'));
//                    $this->nativesession->set('fname',$this->input->get('fname'));
//                    $this->nativesession->set('profile_pic',$this->input->get('profile_pic'));
//                    $this->nativesession->set('user_type',"ST");
//                    
//                    return $data;                    
//                }else{
//                    $data['islogged']=FALSE;
//                    return FALSE;
//                }
//	}
        
        public function login_user()
	{       
            //var_dump($_GET);
		if(isset($_GET['mynsbm_id'])){                    
                    //echo $this->input->get('user_type');
                    //Deletes if there are any previously created sessions    

                    if(!$this->nativesession->sessionIsset('islogged')===TRUE){
                        $this->nativesession->delete('islogged');
                        $this->nativesession->delete('mynsbm_id');
                        $this->nativesession->delete('user_type');
                        $this->nativesession->delete('st_index');
                        $this->nativesession->delete('nick_name');
                    }
                    $this->nativesession->set('islogged',TRUE);
                    $this->nativesession->set('mynsbm_id',$this->input->get('mynsbm_id'));
                    if($this->input->get('user_type')==="ST"){
                        $this->nativesession->set('st_index',$this->input->get('st_index'));
                        
//                        $this->db->select('m.st_index,b.batch_id,b.name as batch_name');
//                        $this->db->from('student as s');
//                        $this->db->join('batch as b', 's.stu_batch_id = b.batch_id');
//                        $this->db->join('my_nsbm_stu as m', 'm.st_index = s.index_nm');
//                        $this->db->where(array('st_index' => $this->input->get('st_index')));
//                        $query = $this->db->get();            
//                        $row=$query->row();
//                        
//                        $this->nativesession->set('batch_name',$row->batch_name);
//                        $this->nativesession->set('batch',$row->batch_id);                        
                    }
                    
                    $this->nativesession->set('nick_name',$this->input->get('nick_name'));
                    $this->nativesession->set('fname',$this->input->get('fname'));
                    $this->nativesession->set('profile_pic',$this->input->get('profile_pic'));
                    $this->nativesession->set('user_type',$this->input->get('user_type'));
                    
                    $this->nativesession->set('just_logged',TRUE);
                    
                    
                    //return $data;                
                }else{
                    //var_dump($data);
                    $this->nativesession->delete('islogged');
                    $this->nativesession->delete('mynsbm_id');
                    $this->nativesession->delete('user_type');
                    $this->nativesession->delete('st_index');
                    $this->nativesession->delete('nick_name');
                }
	}
        
        public function islogged_user()
	{       
            //var_dump($_GET);
		if($this->nativesession->sessionIsset('islogged')===TRUE && !isset($_GET['mynsbm_id'])){
                    
                    $data['islogged']=TRUE;
                    $data['user_type']=$this->nativesession->get('user_type');
                    return $data;
                }elseif(isset($_GET['mynsbm_id'])){
                    
                    $data['islogged']=TRUE;
                    $data['user_type']=$this->input->get('user_type');
                    
                    //echo $this->input->get('user_type');
                    if(!$this->nativesession->sessionIsset('islogged')===TRUE){
                        $this->nativesession->delete('islogged');
                        $this->nativesession->delete('mynsbm_id');
                        $this->nativesession->delete('user_type');
                        $this->nativesession->delete('st_index');
                        $this->nativesession->delete('nick_name');
                    }
                    $this->nativesession->set('islogged',TRUE);
                    $this->nativesession->set('mynsbm_id',$this->input->get('mynsbm_id'));
                    if($this->input->get('user_type')==="ST"){
                        $this->nativesession->set('st_index',$this->input->get('st_index'));
                        
                        $this->db->select('m.st_index,b.batch_id,b.name as batch_name');
                        $this->db->from('student as s');
                        $this->db->join('batch as b', 's.stu_batch_id = b.batch_id');
                        $this->db->join('my_nsbm_stu as m', 'm.st_index = s.index_nm');
                        $this->db->where(array('st_index' => $this->input->get('st_index')));
                        $query = $this->db->get();            
                        $row=$query->row();
                        
                        $this->nativesession->set('batch_name',$row->batch_name);
                        $this->nativesession->set('batch',$row->batch_id);                        
                    }
                    
                    $this->nativesession->set('nick_name',$this->input->get('nick_name'));
                    $this->nativesession->set('fname',$this->input->get('fname'));
                    $this->nativesession->set('profile_pic',$this->input->get('profile_pic'));
                    $this->nativesession->set('user_type',$this->input->get('user_type'));
                    
                    $this->nativesession->set('just_logged',TRUE);
                    
                    
                    return $data;                
                }else{
                    $data['islogged']=FALSE;
                    //var_dump($data);
                    $this->nativesession->delete('islogged');
                    $this->nativesession->delete('mynsbm_id');
                    $this->nativesession->delete('user_type');
                    $this->nativesession->delete('st_index');
                    $this->nativesession->delete('nick_name');
                return FALSE;
                }
	}
        
}