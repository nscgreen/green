<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mfile_handler extends CI_Model {
        public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}
        
        public function create_folder($folder) {

            //Create the folder for the new event
            if (!is_dir($folder)) {
                mkdir($folder,0777);
                return;
            }else{
                return;
            }

        }
        
        public function create_folders($uploadertype,$folders) {
            $numoffolders=  count($folders);
            $mainfolderdb=  'uploads_mynsbm/'.$uploadertype.'/';
//            $mainfolderdb=  'uploads_mynsbm/'.$uploadertype.'/'.$uploadtype.'/';

            $mainfolderupload= './uploads_mynsbm/'.$uploadertype.'/';
            for($i=1;$i<=$numoffolders;$i++){
                $folderdb=$mainfolderdb.$folders[$i];
                
                $folderupload= $mainfolderupload.$folders[$i];
                $this->create_folder($folderupload);

                $mainfolderupload=$folderupload.'/';
                 
                $mainfolderdb=$folderdb.'/';
            }
            return array(
                'folderdb'=>  $mainfolderdb,
                'folderupload'=>$mainfolderupload
            );

        }
        
        public function do_upload($inputfieldname,$config){
            $this->load->library('upload', $config);
            echo '<br>'.var_dump($config).  var_dump($inputfieldname).'<br>';

            if (!$this->upload->do_upload($inputfieldname))
            {
			$error = array('error' => $this->upload->display_errors());
                        echo 'error'.'<br>';
                        echo var_dump($error).'<br>';
                        echo $this->upload->file_type ;
                $this->load->library('nativesession');
                $this->nativesession->set('uploadfailed',TRUE);
            }
            else
            {
                    $uploadedData=$this->upload->data();
                    $filename=$uploadedData['file_name'];

            }
            return $filename;

        }
        
        public function delete_file($filepath) {
            //Delete Files
            unlink($filepath);

        }
    
}