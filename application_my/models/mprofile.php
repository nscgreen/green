<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MProfile extends CI_Model {
        public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
//                $this->load->library('database');
	}
        
        public function get_student_profile_details($student_id){
            
            $this->db->select('s.*,m.email as stu_email,m.*,sc.*,u.*,
                    pe.initial as co_initial,pe.fname as co_fname,pe.lname as co_lname,
                    d.name as degree_name,
                    b.batch_id,b.name as batch_name,b.batch_coordinator_id,b.ac_year,
                    u.name as uni_name,u.country uni_country,u.phone as uni_phone,
                        u.email as uni_email,u.website as uni_website,u.logo as uni_logo,
                    sc.name as sc_name,sc.phone as sc_phone,sc.email as sc_email,
                        sc.web_page as sc_web_page,sc.logo as sc_logo 
                    ' );
            $this->db->from('student as s');
            $this->db->join('my_nsbm_stu as m', 'm.st_mynsbm_id = s.student_id');
            $this->db->join('batch as b', 's.stu_batch_id = b.batch_id');
            $this->db->join('degree as d', 's.stu_degree_id = d.degree_id');
            $this->db->join('school as sc', 'sc.school_id = d.degree_school_id');
            $this->db->join('university as u', 'u.university_id = d.degree_university_id');
            $this->db->join('permenent_employee as pe', 'b.batch_coordinator_id = pe.employee_id');
            
            $this->db->where(array('s.student_id'=>$student_id));
            $query = $this->db->get();            
            $result=$query->row();
//            $v1= $this->db->last_query();
//            return $v1;
            return $result;
        }
        
        public function get_student_public_profile_details($user_name){
            
            $this->db->select('s.*,m.email as stu_email,m.*,d.name as degree_name');
            $this->db->from('student as s');
            $this->db->join('my_nsbm_stu as m', 'm.st_mynsbm_id = s.student_id');
            $this->db->join('degree as d', 's.stu_degree_id = d.degree_id');            
            $this->db->where(array('m.user_name'=>$user_name));
            $query1 = $this->db->get();            
            $result1=$query1->row();
            $result['public_profile_details']=$result1;
            
            $this->db->select('*');
            $this->db->from('stu_projects');
            $this->db->where(array('student_id'=>$result1->student_id));
            $query2 = $this->db->get();            
            $result2=$query2->result();
            $result['stu_projects_details']=$result2;
            
            $this->db->select('*');
            $this->db->from('stu_experiences');
            $this->db->where(array('student_id'=>$result1->student_id));
            $query3 = $this->db->get();            
            $result3=$query3->result();
            $result['stu_experiences_details']=$result3;
            
            $this->db->select('*');
            $this->db->from('stu_skills');
            $this->db->where(array('student_id'=>$result1->student_id));
            $query4 = $this->db->get();            
            $result4=$query4->result();
            $result['stu_skills_details']=$result4;
            
            return $result;
        }
        
        public function get_student_password($student_id){
            
            $this->db->select('st_mynsbm_id,password' );
            $this->db->from('my_nsbm_stu');            
            $this->db->where(array('st_mynsbm_id'=>$student_id));
            $query = $this->db->get();            
            $result=$query->row();
//            $v1= $this->db->last_query();
//            return $v1;
            return $result;
        }
        
        public function get_student_user_name($student_id){
            
            $this->db->select('st_mynsbm_id,user_name' );
            $this->db->from('my_nsbm_stu');            
            $this->db->where(array('st_mynsbm_id'=>$student_id));
            $query = $this->db->get();            
            $result=$query->row();
//            $v1= $this->db->last_query();
//            return $v1;
            return $result->user_name;
        }
        
        
        public function upload_profile_pic($mynsbmname,$folderupload,$folderdb,$inputfieldname){
            $this->load->model('mfile_handler');
            echo '<br>';
            echo $folderupload;
            $config['upload_path'] = $folderupload;
            $config['allowed_types'] = 'gif|jpg|png|pdf|zip|doc|docx|ppt|pptx|sql|txt';
            $config['file_name']	= $mynsbmname;
//		$config['max_size']	= '2048';
            $config['overwrite']=TRUE;
            $this->load->model('mfile_handler');
            $mynsbmfilename=$this->mfile_handler->do_upload($inputfieldname,$config);

            $uploaddetails['profile_pic_path']= $folderdb.$mynsbmfilename;
            $uploaddetails['filename']= $mynsbmfilename;

            return $uploaddetails;
        }
        
        public function update_profile($mynsbmdetails){
            $this->load->database();
            //Data set to insert into the Database
            $updatedata=array(
//                'id'=>$mynsbmdetails['id'],
                'nick_name'=>$mynsbmdetails['nick_name'],
                'mobile'=>$mynsbmdetails['mobile'],
                'email'=>$mynsbmdetails['email'],
                'my_description'=>$mynsbmdetails['my_description'],
                'link_fb'=>$mynsbmdetails['link_fb'],
                'link_tw'=>$mynsbmdetails['link_tw'],
                'link_ln'=>$mynsbmdetails['link_ln'],
                'link_gl'=>$mynsbmdetails['link_gl'],
                'profile_pic'=>$mynsbmdetails['profile_pic'],
                

            );
            //Update the submitted mynsbm details into the database
            $this->db->where('st_mynsbm_id', $mynsbmdetails['st_mynsbm_id']);
            $this->db->update('my_nsbm_stu', $updatedata);

        }    
        
        public function upload_project_pic($stuprojectname,$folderupload,$folderdb,$inputfieldname){
            $this->load->model('mfile_handler');
            //echo '<br>';
            //echo $folderupload;
            $config['upload_path'] = $folderupload;
            $config['allowed_types'] = '*';
            $config['file_name']	= $stuprojectname;
//		$config['max_size']	= '2048';
            $config['overwrite']=TRUE;
            $this->load->model('mfile_handler');
            $stuprojectfilename=$this->mfile_handler->do_upload($inputfieldname,$config);

            $uploaddetails['project_pic_path']= $folderdb.$stuprojectfilename;
            $uploaddetails['filename']= $stuprojectfilename;

            return $uploaddetails;
        }
        
        public function add_experience_uni_profile($stuprojectdetails){
            $this->load->database();
            //Data set to insert into the Database
            $insertdata=array(
//                'id'=>$stuprojectdetails['id'],
                'student_id'=>$stuprojectdetails['st_mynsbm_id'],
                'company_name'=>$stuprojectdetails['company_name'],
                'job_title'=>$stuprojectdetails['job_title'],
                'from'=>$stuprojectdetails['from'],
                'to'=>$stuprojectdetails['to'],
                'descrption'=>$stuprojectdetails['descrption'],
                'company_website'=>$stuprojectdetails['company_website'],
                'image_path'=>$stuprojectdetails['image_path'],                

            );
            //Insert the submitted mynsbm details into the database            
            $this->db->insert('stu_experiences', $insertdata);    

        }
        
        public function add_project_uni_profile($stuexperiencedetails){
            $this->load->database();
            //Data set to insert into the Database
            $insertdata=array(
//                'id'=>$stuexperiencedetails['id'],
                'student_id'=>$stuexperiencedetails['st_mynsbm_id'],
                'project_title'=>$stuexperiencedetails['project_title'],
                'descrption'=>$stuexperiencedetails['descrption'],
                'details_link'=>$stuexperiencedetails['details_link'],
                'image_path'=>$stuexperiencedetails['image_path'],                

            );
            //Insert the submitted mynsbm details into the database            
            $this->db->insert('stu_projects', $insertdata);

        }    
        
        public function add_skill_uni_profile($stuexperiencedetails){
            $this->load->database();
            //Data set to insert into the Database
            $insertdata=array(
//                'id'=>$stuexperiencedetails['id'],
                'student_id'=>$stuexperiencedetails['st_mynsbm_id'],
                'skill_title'=>$stuexperiencedetails['skill_title'],
                'descrption'=>$stuexperiencedetails['descrption'],
                'details_link'=>$stuexperiencedetails['details_link'],
                'image_path'=>$stuexperiencedetails['image_path'],                

            );
            //Insert the submitted mynsbm details into the database            
            $this->db->insert('stu_skills', $insertdata);

        }    
        
        public function update_password($mynsbmdetails){
            $this->load->database();
            //Data set to insert into the Database
            $updatedata=array(
//                'id'=>$mynsbmdetails['id'],
                'st_mynsbm_id'=>$mynsbmdetails['st_mynsbm_id'],
                'password'=>$mynsbmdetails['password'],               
            );
            //Update the submitted mynsbm details into the database
            $this->db->where('st_mynsbm_id', $mynsbmdetails['st_mynsbm_id']);
            $this->db->update('my_nsbm_stu', $updatedata);

        }        

        
        
}
