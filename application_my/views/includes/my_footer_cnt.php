<!-- JavaScript -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script> <!-- Boostrap JS -->

    <script src="<?php echo base_url(); ?>assets/js/bst_formhelp.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bst_fileinput.min.js"></script>
    <script src="<?php echo base_url(); ?>assets_my/js/my_snap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets_my/js/my_site.js"></script>
<!-- Ajax Wrapper loader -->
    <script>
        function loadwrap($div, $page) {
            $($div).load($page);
        }
        function calajax(page) { // for calendar
            $('#cal').load(page);
        }
    </script>
<!-- Ajax Wrapper loader End -->