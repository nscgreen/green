<!--Fonts Admin-->
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'> <!--for h1 h2-->
    <link href='http://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'> <!--for h3 h4 h5 h6-->
    <link href='http://fonts.googleapis.com/css?family=Play' rel='stylesheet' type='text/css'> <!--for Special-->
    
<!--CSS-->
    <?php echo link_tag('assets/css/normalize.css');?>
    <?php echo link_tag('assets/css/cstm_reset.css');?>
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
    <?php echo link_tag('assets/css/bst_formhelp.min.css');?>
    <?php echo link_tag('assets/css/bst_fileinput.min.css');?>
    <?php echo link_tag('assets/css/anime.css');?>
    
    <?php echo link_tag('assets_my/css/my_site.css');?>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->