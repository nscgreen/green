<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jun 13, 2014, 3:44:18 PM
 */

?>
<!--<body>-->
    <?php $this->load->view('vmy_snap_panel.php'); ?>  <!--include snap panel-->
    
    <div id="content" class="snap-content">
        <?php $this->load->view('vmy_topmenu.php'); ?> <!--include my_nsbm top menu-->

        <div class="row" style="margin: 0 10px;">
            <h1 class="alert alert-info">BSc in Computer Science <small>(3yr)</small></h1>
            <div class="col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title font-roboto">Academic Year1 Semester 1</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-responsive table-hover">
                            <tbody>
                                <tr>
                                    <td>Module:</td>
                                    <td>C Language</td>
                                </tr>
                                <tr>
                                    <td>Description:</td>
                                    <td>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</td>
                                </tr>
                                <tr>
                                    <td>Module:</td>
                                    <td>C Language</td>
                                </tr>
                                <tr>
                                    <td>Description:</td>
                                    <td>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title font-roboto">Academic Year1 Semester 2</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-responsive table-hover">
                            <tbody>
                                <tr>
                                    <td>Module:</td>
                                    <td>C Language</td>
                                </tr>
                                <tr>
                                    <td>Description:</td>
                                    <td>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</td>
                                </tr>
                                <tr>
                                    <td>Module:</td>
                                    <td>C Language</td>
                                </tr>
                                <tr>
                                    <td>Module:</td>
                                    <td>C Language</td>
                                </tr>
                                <tr>
                                    <td>Description:</td>
                                    <td>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row" style="margin: 0 10px">
            <div class="col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title font-roboto">Academic Year2 Semester 1</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-responsive table-hover">
                            <tbody>
                                <tr>
                                    <td>Module:</td>
                                    <td>C Language</td>
                                </tr>
                                <tr>
                                    <td>Description:</td>
                                    <td>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</td>
                                </tr>
                                <tr>
                                    <td>Description:</td>
                                    <td>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</td>
                                </tr>
                                <tr>
                                    <td>Module:</td>
                                    <td>C Language</td>
                                </tr>
                                <tr>
                                    <td>Description:</td>
                                    <td>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title font-roboto">Academic Year2 Semester 2</h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-responsive table-hover">
                            <tbody>
                                <tr>
                                    <td>Module:</td>
                                    <td>C Language</td>
                                </tr>
                                <tr>
                                    <td>Description:</td>
                                    <td>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</td>
                                </tr>
                                <tr>
                                    <td>Module:</td>
                                    <td>C Language</td>
                                </tr>
                                <tr>
                                    <td>Description:</td>
                                    <td>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
