<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jun 7, 2014, 10:25:18 PM
 */

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title><?php echo $title?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo $mDescription?>"/>
    <meta name="keywords" content="<?php echo $mKeywords?>">
    <link rel="icon" href="../../images/favicon.ico" type="image/x-icon"/>
    
    <!--include all header files-->
    <?php include 'includes/my_header_cnt.php'; ?>
    
  </head>