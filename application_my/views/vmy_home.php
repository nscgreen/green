<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jun 13, 2014, 3:44:18 PM
 */

?>
<body>
    <?php $this->load->view('vmy_snap_panel.php'); ?>  <!--include snap panel-->
    
    <div id="content" class="snap-content">
        <?php $this->load->view('vmy_topmenu.php'); ?> <!--include my_nsbm top menu-->

        <div class="row" style="margin: 0 10px">
            <div class="container">
                <div class="page-header">
                    <h1 style="color: #ffffff" class="animated lightSpeedIn">My <span style="color: #1ac6ff" class="animated pulse">NSBM</span> <small style="color: #ffffff">more ways in one place...</small></h1>
                </div>
                <div class="legend text-justify">
                    <p style="color: #BCCBFA">Welcome to My NSBM!! My NSBM is a place where all of you will be able to access NSBM 
                        LMS, NSBM EMS, My Nsbm and NSBM Forum. From My NSBM you can create the University Profile
                        where all of your information is saved. This feature will help outside personals to gain
                        your educational qualifications and information reguarding projects you have done. Join
                        to University Profile to day and enjoy perquisites..</p>
                </div>
                <div class="row">
                    <div class="col-md-3 col-md-offset-1 text-justify">
                        <div class="feature-item">
                            <img class="pull-left animated zoomIn" style="width: 128px; margin-right: 15px" src="logos/lms.png">
                            <h3><a href="http://communities.d2real.com" class="text-muted">L.M.S <i class="fa fa-angle-double-right"></i></a></h3>
                            <div><p>NSBM learning management system . This provides self learning facilities to all the students in NSBM. All of the assignments, projects and important news events will be displayed here. Academic staff members will facilitate you through this system 24/7.</p></div>
                        </div>
                        <div class="feature-item">							
                            <h3><a href="" class="text-muted">Communities <i class="fa fa-angle-double-right"></i></a></h3>								
                            <div><p>NSBM Community helps the vast amount of communities that have been introduced by NSBM students
                                    to gather in a one place. A place where all the communities are held, so the students can 
                                    easily find information about on going communities and participate accordingly to their likes.
                                    A place all the messages and notices can be added and viewed. Network with NSBM communities 
                                    now..</p></div>
                        </div>
                    </div>
                    <div class="col-md-4 text-right feature-picture">
                        <center>
                            <img class="img-responsive brzero" style="margin-top: 30px" src="<?php echo $profile_pic;?>">
                            <br />
                            <h3><a href="http://forum.d2real.com" class="text-muted">Forum <i class="fa fa-angle-double-right"></i></a></h3>								
                            <p class="text-justify">NSBM Forum is a new feature that we are going to intorduce the main website. This is to 
                                increase the communication between NSBM students and the communication between
                                students and lecturers. From this forum both students and lecturers will reap many uses,
                                like exchange of ideas, couse materials, discussions or even use as a bulletin board. 
                                Our developing team will hope to launch the NSBM forum soon enough.</p>
                        </center>
                    </div>
                    <div class="col-md-3 text-justify">
                        <div class="feature-item">
                            <img class="pull-left animated zoomIn" style="width: 128px; padding-right: 5px" src="logos/ems.png">
                            <h3><a href="http://events.d2real.com" class="text-muted">E.M.S <i class="fa fa-angle-double-right"></i></a></h3>
                            <div><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500.</p></div>
                        </div>
                        <div class="feature-item">
                            <h3><a href="http://help.d2real.com" class="text-muted">Help <i class="fa fa-angle-double-right"></i></a></h3>
                            <div><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500.</p></div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
