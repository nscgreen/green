<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jun 13, 2014, 3:44:18 PM
 */

?>
<body>
    <?php $this->load->view('vmy_snap_panel.php'); ?>  <!--include snap panel-->
    
    <div id="content" class="snap-content">
        <?php $this->load->view('vmy_topmenu.php'); ?> <!--include my_nsbm top menu-->

        <div class="row" style="margin: 0 10px">
            <div class="container">
                <div class="page-header">
                    <h1 style="color: #006699">My NSBM <small>More ways in one place...</small></h1>
                </div>
                <div class="legend text-justify">
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                </div>
                <div class="row">
                    <div class="col-md-3 col-md-offset-1 text-right">
                        <div class="feature-item">
                            <h3><i class="fa fa-arrow-left"></i> L.M.S</h3>
                            <div><p class="text-info">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500.</p></div>
                        </div>
                        <div class="feature-item">							
                            <h3><i class="fa fa-arrow-left"></i> E.M.S</h3>								
                            <div><p class="text-info">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500.</p></div>
                        </div>
                    </div>
                    <div class="col-md-4 text-right feature-picture">
                        <center>
                            <img class="img-responsive img-rounded" style="border: 7px solid #ffffff; margin-top: 30px" src="../../logos/pic.jpg">
                        </center> 
                    </div>
                    <div class="col-md-3 text-left">
                        <div class="feature-item">
                            <h3>e-Learning  <i class="fa fa-arrow-right"></i></span></h3>
                            <div><p class="text-info">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500.</p></div>
                        </div>
                        <div class="feature-item">
                            <h3>e-Learning <i class="fa fa-arrow-right"></i></span></h3>
                            <div><p class="text-info">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500.</p></div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
