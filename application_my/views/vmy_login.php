<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jun 13, 2014, 3:44:18 PM
 */

?>
<body>
    <div class="row" style="margin: 0 10px">
        <div class="container">
            <div class="page-header">
                <a href="http://main.d2real.com" target="_blank"><img class="image-responsive center-block nsbmlogo" src="<?php echo base_url(); ?>logos/nsbmwhite.gif"></a>
                <br /><br />
                <h1 style="color: #ffffff" class="animated lightSpeedIn text-center">My <span style="color: #1ac6ff" class="animated pulse">NSBM</span> <small style="color: #ffffff">more ways in one place...</small></h1>
                <br />
                <form action="http://connect.d2real.com" method="POST">
                    <input type="hidden" name="nexturl" value="mynsbm.d2real.com" />
                    <input type="hidden" name="type" value="my_nsbm" />
                    <button type="submit" class="btn btn-info btn-lg center-block" style="width: 40%">Log in</button>
                </form>
            </div>
            <div class="legend text-center">
                <p style="color: #ffffff">Welcome to My NSBM!! My NSBM is a place where all of you will be able to access NSBM 
                    LMS, NSBM EMS, My Nsbm and NSBM Forum. From My NSBM you can create the University Profile
                    where all of your information is saved. This feature will help outside personals to gain
                    your educational qualifications and information reguarding projects you have done. Join
                    to University Profile to day and enjoy perquisites..</p>
            </div>
            <br /><br /><br />
            <div class="container" style="margin-bottom: 10px; margin-top: -15px">
                <div class="row" style="margin: 0 auto; padding: 0">
                    <div class="col-xs-12" style="margin: 0 auto; padding: 0">
                        <div class="center-block" style="margin: 0 auto; padding: 0">
                            <center>
                                <ul id="lms-home-social-menu2"  style="margin: 0 auto; padding: 0">
                                    <li><a href="http://d2real.com"  target="_blank">NSBM</a></li>
                                    <li><a href="http://lms.d2real.com"  target="_blank">L.M.S</a></li>
                                    <li><a href="http://forum.d2real.com"  target="_blank">Forum</a></li>
                                    <li><a href="http://events.d2real.com"  target="_blank">Events</a></li>
                                    <li><a href="http://communities.d2real.com"  target="_blank">Communities</a></li>
                                    <li><a href="http://help.d2real.com"  target="_blank">Help</a></li>
                                </ul>
                            </center> 
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <center>
                <p class="text-center">Copyright &COPY; National School of Business Management</p>
            </center>
        </div>  

