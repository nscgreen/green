<?php
/*
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jun 13, 2014, 3:44:18 PM
 */
?>
<?php if(isset($password_changed) && $password_changed==TRUE){ ?>
<body onload='loadmodal("#password_changed_success");'>
<?php }else{ ?>
<body>
<?php } ?>
    <?php $this->load->view('vmy_snap_panel.php'); ?>  <!--include snap panel-->

    <div id="content" class="snap-content">
        <?php $this->load->view('vmy_topmenu.php'); ?> <!--include my_nsbm top menu-->

        <div class="row" style="margin: 0 10px">
            <div class="col-sm-4">
                <div class="thumbnail" style="padding: 5px 5px 10px 5px;">
                    <img class="image-responsive" src="<?php echo $profile_details->profile_pic; ?>" style="max-height: 128px;"/>
<!--                    <img class="image-responsive" src="<?php // echo base_url()  ?>logos/pic.jpg" />-->
                    <div class="caption" style="text-align: center">
                        <h3 class="font-roboto text-primary">my-NSBM Profile Details</h3>
                    </div>
                    <div class="caption">
                        <table class="table table-responsive">
                            <tbody>
                                <tr>
                                    <td>Index No:</td>
                                    <!--<td><?php // echo $profile_details;   ?></td>-->
                                    <td><?php echo $profile_details->index_nm; ?></td>
                                </tr>
                                <tr>
                                    <td>Display Name:</td>
                                    <td><?php echo $profile_details->nick_name; ?></td>
                                </tr>
                                <tr>
                                    <td>Mobile:</td>
                                    <td><?php echo $profile_details->mobile; ?></td>
                                </tr>
                                <tr>
                                    <td>Email:</td>
                                    <td><a href="mailto:<?php echo $profile_details->stu_email; ?>"><?php echo $profile_details->stu_email; ?></a></td>
                                </tr>
                                <tr>
                                    <td>I'm:</td>
                                    <td><?php echo $profile_details->my_description; ?></td>
                                </tr>
                                <tr>
                                    <td>Facebook:</td>
                                    <td><a href="<?php echo $profile_details->link_fb; ?>" target="_blank"><?php echo $profile_details->link_fb; ?></a></td>
                                </tr>
                                <tr>
                                    <td>Twitter:</td>
                                    <td><a href="<?php echo $profile_details->link_tw; ?>" target="_blank"><?php echo $profile_details->link_tw; ?></a></td>
                                </tr>
                                <tr>
                                    <td>Google+:</td>
                                    <td><a href="<?php echo $profile_details->link_ln; ?>" target="_blank"><?php echo $profile_details->link_ln; ?></a></td>
                                </tr>
                                <tr>
                                    <td>Linkdin:</td>
                                    <td><a href="<?php echo $profile_details->link_gl; ?>" target="_blank"><?php echo $profile_details->link_gl; ?></a></td>
                                </tr>
                            </tbody>
                        </table>
                        <a data-toggle="modal" data-target="#edit" class="btn btn-default btn-lg" style="width: 100%" role="button">Update</a>
                    </div>
                </div>
                <div class="alert alert-info">
                    <h4>Degree : <small><?php echo $profile_details->degree_name; ?></small></h4>
                    <h4>Batch : <small><?php echo $profile_details->batch_name; ?></small></h4>
<!--                    <h4>Batch ID : <small>CS-13.1</small></h4>-->
                    <h4>Academic Year of the Batch : <small><?php echo $profile_details->ac_year; ?></small></h4>
                    <h4>Batch Coordinator : <small><?php echo $profile_details->co_initial . ' ' . $profile_details->co_fname . ' ' . $profile_details->co_lname; ?></small></h4>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="row" style="padding: 0 15px">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title font-roboto">Registered University Details</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-4">
                                    <img src="<?php echo $profile_details->uni_logo; ?>" style="max-width: 128px;">
                                    <!--<img src="../../logos/computing-logo.png">-->
                                </div>
                                <div class="col-sm-8">
                                    <h4><?php echo $profile_details->uni_name; ?><br />
                                        <?php echo $profile_details->uni_country; ?></h4>
                                </div>
                            </div><br />
                            <table class="table table-responsive">
                                <tbody>
                                    <tr>
                                        <td>Mobile:</td>
                                        <td><?php echo $profile_details->uni_phone; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Email:</td>
                                        <td><a href="mailto:<?php echo $profile_details->uni_email; ?>"><?php echo $profile_details->uni_email; ?></a></td>
                                    </tr>
                                    <tr>
                                        <td>Web:</td>
                                        <td><a href="<?php echo $profile_details->uni_website; ?>"><?php echo $profile_details->uni_website; ?></a></td>
                                    </tr>
                                    <tr>
                                        <td>Facebook:</td>
                                        <td><a href="http://www.d2real.com" target="_blank">http://www.nsbm.lk</a></td>
                                    </tr>
                                    <tr>
                                        <td>Twitter:</td>
                                        <td><a href="http://www.d2real.com" target="_blank">http://www.nsbm.lk</a></td>
                                    </tr>
                                    <tr>
                                        <td>Google+:</td>
                                        <td><a href="http://www.d2real.com" target="_blank">http://www.nsbm.lk</a></td>
                                    </tr>
                                    <tr>
                                        <td>Linkdin:</td>
                                        <td><a href="http://www.d2real.com" target="_blank">http://www.nsbm.lk</a></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="panel-footer">
                        </div>
                    </div>
                </div>

                <div class="row" style="padding: 0 15px">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title font-roboto">Affiliated School Details</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-4">
                                    <img src="<?php echo $profile_details->sc_logo; ?>" style="max-width: 256px;">
                                </div>
                                <div class="col-sm-8">
                                    <h4>Name: <br /><?php echo $profile_details->sc_name; ?></h4>
                                </div>
                            </div><br />
                            <table class="table table-responsive">
                                <tbody>
                                    <tr>
                                        <td>Mobile:</td>
                                        <td><?php echo $profile_details->sc_phone; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Email:</td>
                                        <td><a href="mailto:<?php echo $profile_details->sc_email; ?>"><?php echo $profile_details->sc_email; ?></a></td>
                                    </tr>
                                    <tr>
                                        <td>Web Page:</td>
                                        <td><a href="<?php echo $profile_details->sc_web_page; ?>" target="_blank"><?php echo $profile_details->sc_web_page; ?></a></td>
                                    </tr>
                                    <tr>
                                        <td>Facebook:</td>
                                        <td><a href="http://www.d2real.com" target="_blank">http://www.nsbm.lk</a></td>
                                    </tr>
                                    <tr>
                                        <td>Twitter:</td>
                                        <td><a href="http://www.d2real.com" target="_blank">http://www.nsbm.lk</a></td>
                                    </tr>
                                    <tr>
                                        <td>Google+:</td>
                                        <td><a href="http://www.d2real.com" target="_blank">http://www.nsbm.lk</a></td>
                                    </tr>
                                    <tr>
                                        <td>Linkdin:</td>
                                        <td><a href="http://www.d2real.com" target="_blank">http://www.nsbm.lk</a></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="panel-footer">
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>

<!-- Edit Modal Start-->
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 3;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #277dcf">Update my-NSBM details</h4>
            </div>
            <form class="form" role="form" action="<?php echo base_url() . 'profile/update_profile'; ?>" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                <?php var_dump($password_changed); ?>
                   <input type="hidden" name="st_mynsbm_id" value="<?php echo $profile_details->st_mynsbm_id; ?>">
                   <input type="hidden" name="profile_pic_old" value="<?php echo $profile_details->profile_pic; ?>">
                   <div class="form-group">
                       <label class="font-label">Display Name :</label>
                       <input type="text" class="form-control" placeholder="Enter Display Name here" value="<?php echo $profile_details->nick_name; ?>" name="nick_name">
                   </div>

                    <div class="form-group">
                        <label class="font-label">Mobile :</label>
                        <input type="text" class="form-control" placeholder="Enter Mobile here" value="<?php echo $profile_details->mobile; ?>" name="mobile">
                    </div>

                    <div class="form-group">
                        <label class="font-label">Profile Picture :</label>
                        <input id="input-2" type="file" class="file" data-show-upload="false" data-show-caption="true" name="profile_pic" >
                    </div>

                    <div class="form-group">
                        <label class="font-label">Email :</label>
                        <input type="email" class="form-control" placeholder="example@example.com" value="<?php echo $profile_details->stu_email; ?>" name="email">
                    </div>
                    <div class="form-group">
                        <label class="font-label">I'm :</label>
                        <textarea class="form-control counted" name="my_description"><?php echo $profile_details->my_description; ?></textarea>
<!--                        <textarea class="form-control " placeholder="Type in your description" rows="5" style="margin-bottom:10px;" name="descrption" required></textarea>-->
                        <h6 class="pull-right" id="counter">320 characters remaining</h6>
                    </div>
                    <div class="form-group">
                        <label class="font-label">Facebook :</label>
                        <input type="text" class="form-control" placeholder="Facebook URL" value="<?php echo $profile_details->link_fb; ?>" name="link_fb">
                    </div>
                    <div class="form-group">
                        <label class="font-label">Twitter :</label>
                        <input type="text" class="form-control" placeholder="Twitter URL" value="<?php echo $profile_details->link_tw; ?>" name="link_tw">
                    </div>
                    <div class="form-group">
                        <label class="font-label">Google+ :</label>
                        <input type="text" class="form-control" placeholder="Google+ URL" value="<?php echo $profile_details->link_ln; ?>" name="link_ln">
                    </div>
                    <div class="form-group">
                        <label class="font-label">Linkedin :</label>
                        <input type="text" class="form-control" placeholder="Linkedin URL" value="<?php echo $profile_details->link_gl; ?>" name="link_gl">
                    </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-success">Update</button>

            </div>
            </form>
        </div>
    </div>
</div>
<!-- Edit Modal End-->

    
<!-- Password changed successfully Modal Start -->
<div class="modal fade" id="password_changed_success" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 3;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #ff0066">Password changed successfully.</h4>
            </div>
            <div class="modal-body">
                 <p class="text-primary">Your Password has been changed successfully.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>
<!-- Password changed successfully Modal End -->

<!-- Registration Done Modal Start-->
<div class="modal fade" id="successfull" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 0;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #00cc66">Successfully updated.</h4>
            </div>
            <div class="modal-body">
                <p>Done...!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Done !</button>
            </div>
        </div>
    </div>
</div>
<!-- Registration Done Modal End-->

<!-- Registration Failed Modal Start-->
<div class="modal fade" id="failed" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 0;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #ff0066">Registration failed</h4>
            </div>
            <div class="modal-body">
                <p>Error...!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-danger">Yes</button>
            </div>
        </div>
    </div>
</div>
<!-- Registration Failed Modal End-->