<?php
/*
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jun 21, 2014, 11:58:10 PM
 */
?>
<?php if(isset($password_changed) && $password_changed==TRUE){ ?>
<body onload='loadmodal("#edit");'>
<?php }else{ ?>
<body>
<?php } ?>
<div class="snap-drawers">
    <div class="snap-drawer snap-drawer-left">
        <!--<div class="">-->
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="http://d2real.com"><img style="margin: 0px auto auto 10px; width: 50%; height: 50%;" class="image-responsive" src="<?php echo base_url(); ?>logos/nsbm.gif"></a>
                        <div style="text-align: left; margin-left: 5px">
                            <img style="margin: 10px auto auto auto; width: 130px; height: 124px;" class="image-responsive img-rounded" src="<?php echo base_url() . $profile_pic; ?>">
                            <h4><?php echo $fname; ?><br /><small><?php echo $nick_name; ?></small></h4>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="<?php echo base_url(); ?>profile">
                                <span class="glyphicon glyphicon-user"></span> &nbsp;Profile
                            </a>
                        </h4>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="<?php echo base_url(); ?>course"><span class="fa fa-graduation-cap">
                                </span> Course</a>
                        </h4>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading"> 
                        <h4 class="panel-title">
                            <a href="<?php echo base_url(); ?>university/profile"><span class="fa fa-user">
                                </span> &nbsp;Uni Profile</a>
                        </h4>
                    </div>
                </div>

                <footer class="footer">
                    <div class="row" style="padding: 0; margin: 0">
                        <div class="col-xs-10" style="padding: 0; margin: 25px 15px 0 20px;">
                            <p style=" color: #ffffff">Copyright &COPY; National School of Business Management</p>
                        </div>
                    </div>
                </footer>
            </div>
        <!--</div>-->
    </div>

    <div class="snap-drawer snap-drawer-right">
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12">
                    <div class="col-sm-12" style="">
                        <div class="row alert alert-info" style=" padding: 0">
                            <div class="col-xs-12" style="text-align: center; padding-top: 5%">
                                <!--<span class="fa fa-graduation-cap fa-3x text-info"></span>-->
                            </div><br />
                            <div class="col-xs-9" style="">
                                <h3 class="font-play underline"><a href="http://lms.nsbm.kl" target="_blank"><span  class="text-info">L.M.S</span></a></h3>
                            </div>
                            <div class="col-xs-3" style="padding-top: 10px">
                                <a href="http://lms.nsbm.kl" target="_blank" class="text-info"><span class="fa fa-angle-double-right fa-3x"></span></a>
                            </div>
                        </div>
                        <div class="row alert alert-success" style=" padding: 0">
                            <div class="col-xs-12" style="text-align: center; padding-top: 5%">
                                <!--<span class="fa fa-graduation-cap fa-3x text-info"></span>-->
                            </div><br />
                            <div class="col-xs-9" style="">
                                <h3 class="font-play underline"><a href="http://forum.d2real.com" target="_blank"><span  class="text-info">Forum</span></a></h3>
                            </div>
                            <div class="col-xs-3" style="padding-top: 10px">
                                <a href="http://forum.d2real.com" target="_blank" class="text-info"><span class="fa fa-angle-double-right fa-3x"></span></a>
                            </div>
                        </div>
                        <div class="row alert alert-info" style=" padding: 0">
                            <div class="col-xs-12" style="text-align: center; padding-top: 5%">
                                <!--<span class="fa fa-graduation-cap fa-3x text-info"></span>-->
                            </div><br />
                            <div class="col-xs-9" style="">
                                <h3 class="font-play underline"><a href="http://events.d2real.com" target="_blank"><span  class="text-info">Events</span></a></h3>
                            </div>
                            <div class="col-xs-3" style="padding-top: 10px">
                                <a href="http://events.d2real.com" target="_blank" class="text-info"><span class="fa fa-angle-double-right fa-3x"></span></a>
                            </div>
                        </div>
                        <div class="row alert alert-success" style=" padding: 0">
                            <div class="col-xs-12" style="text-align: center; padding-top: 5%">
                                <!--<span class="fa fa-graduation-cap fa-3x text-info"></span>-->
                            </div><br />
                            <div class="col-xs-9" style="">
                                <h3 class="font-play underline"><a href="http://communities.d2real.com" target="_blank"><span  class="text-info">Communities</span></a></h3>
                            </div>
                            <div class="col-xs-3" style="padding-top: 10px">
                                <a href="http://communities.d2real.com" target="_blank" class="text-info"><span class="fa fa-angle-double-right fa-3x"></span></a>
                            </div>
                        </div>
                        <div class="row alert alert-info" style=" padding: 0">
                            <div class="col-xs-12" style="text-align: center; padding-top: 5%">
                                <!--<span class="fa fa-graduation-cap fa-3x text-info"></span>-->
                            </div><br />
                            <div class="col-xs-9" style="">
                                <h3 class="font-play underline"><a href="http://help.d2real.com" target="_blank"><span  class="text-info">Help</span></a></h3>
                            </div>
                            <div class="col-xs-3" style="padding-top: 10px">
                                <a href="http://help.d2real.com" target="_blank" class="text-info"><span class="fa fa-angle-double-right fa-3x"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>