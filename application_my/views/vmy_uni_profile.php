<?php

/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jun 13, 2014, 3:44:18 PM
 */

?>
<?php if(isset($add_project_success) && $add_project_success==TRUE){ ?>
<body onload='loadmodal("#add_project_success");'>
<?php }elseif(isset($add_project_failed) && $add_project_failed==TRUE){ ?>
<body onload='loadmodal("#failed");'>
<?php }else{ ?>
<body>
<?php } ?>
    <?php $this->load->view('vmy_snap_panel.php'); ?>  <!--include snap panel-->
    
    <div id="content" class="snap-content">
        <?php $this->load->view('vmy_topmenu.php'); ?> <!--include my_nsbm top menu-->

        <div class="row" style="margin: 0 10px">
            <div class="col-sm-7">
                <h2 class="alert alert-info">Manage your University Profile</h2>
                <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title font-roboto">Add a Project you have done.</h3>
                        </div>                        
                    <form class="" role="form" action="<?php echo base_url(); ?>university/add_project" method="POST" enctype="multipart/form-data">
                        <div class="panel-body">
                            <input type="hidden" name="st_mynsbm_id" value="<?php echo $mynsbm_id; ?>">
                                <div class="form-group">
                                    <label class="font-label">Title :</label>
                                    <input type="text" class="form-control" placeholder="Enter university name here" name="project_title" required>
                                </div>
                                <div class="form-group">
                                    <label class="font-label">Description :</label>
                                    <textarea class="form-control counted" placeholder="Type in your description" rows="5" style="margin-bottom:10px;" name="descrption" required></textarea>
                                    <h6 class="pull-right" id="counter">320 characters remaining</h6>
                                </div>
                                <div class="form-group">
                                    <label class="font-label">Link : (optional)</label>
                                    <input type="text" class="form-control" placeholder="http:// ..." name="details_link">
                                </div>
                                <div class="form-group">
                                    <label for="fileName" class="font-label"><small>For best results with your University Profile, we recommend you to add a related image below.</small></label>
                                    <br/>
                                    <label for="fileName" class="font-label">Image : </label>
                                    <input id="input-2" type="file" class="file" data-show-upload="false" data-show-caption="true" name="image_path">
                                </div>

                        </div>
                        <div class="panel-footer">
                            <div class="pull-right">
                                <button class="btn btn-success" type="submit">Add</button>
                            </div>
                        </div>
                    </form>
                </div>
                
                <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title font-roboto">Add Experience</h3>
                        </div>                        
                    <form class="" role="form" action="<?php echo base_url(); ?>university/add_experience" method="POST" enctype="multipart/form-data">
                        <div class="panel-body">
                            <input type="hidden" name="st_mynsbm_id" value="<?php echo $mynsbm_id; ?>">
                                <div class="form-group">
                                    <label class="font-label">Company Name :</label>
                                    <input type="text" class="form-control" placeholder="Enter Company name here" name="company_name" required>
                                </div>
                                <div class="form-group">
                                    <label class="font-label">Job Title :</label>
                                    <input type="text" class="form-control" placeholder="Enter Job Title here" name="job_title" required>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label for="fileName" class="font-label">From:</label><br />
                                            <div class="bfh-datepicker" data-format="y-m-d" data-date="today" style="float: left" data-name="from" required></div>
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="fileName" class="font-label">To:</label><br />
                                            <div class="bfh-datepicker" data-format="y-m-d" data-date="today" style="float: left" data-name="to" required></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="font-label">Description :</label>
                                    <textarea class="form-control counted" placeholder="Type in your description" rows="5" style="margin-bottom:10px;" name="descrption" required></textarea>
                                    <h6 class="pull-right" id="counter">320 characters remaining</h6>
                                </div>
                                <div class="form-group">
                                    <label class="font-label">Company Website : </label>
                                    <input type="text" class="form-control" placeholder="http:// ..." name="company_website">
                                </div>
                                <div class="form-group">
                                    <label for="fileName" class="font-label"><small>For best results with your University Profile, we recommend you to add a related image below.</small></label>
                                    <br/>
                                    <label for="fileName" class="font-label">Image : </label>
                                    <input id="input-2" type="file" class="file" data-show-upload="false" data-show-caption="true" name="image_path">
                                </div>

                        </div>
                        <div class="panel-footer">
                            <div class="pull-right">
                                <button class="btn btn-success" type="submit">Add</button>
                            </div>
                        </div>
                    </form>
                </div>
                
                <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title font-roboto">Add a Skill/ Extra Curicular activity/ Community Details</h3>
                        </div>                        
                    <form class="" role="form" action="<?php echo base_url(); ?>university/add_skill" method="POST" enctype="multipart/form-data">
                        <div class="panel-body">
                            <input type="hidden" name="st_mynsbm_id" value="<?php echo $mynsbm_id; ?>">
                                <div class="form-group">
                                    <label class="font-label">Title :</label>
                                    <input type="text" class="form-control" placeholder="Enter Title for Skill/Community name here" name="skill_title" required>
                                </div>
                                <div class="form-group">
                                    <label class="font-label">Description :</label>
                                    <textarea class="form-control counted" placeholder="Enter achievements you got with your skilland details" rows="5" style="margin-bottom:10px;" name="descrption" required></textarea>
                                    <h6 class="pull-right" id="counter">320 characters remaining</h6>
                                </div>
                                <div class="form-group">
                                    <label class="font-label">Link : (optional)</label>
                                    <input type="text" class="form-control" placeholder="http:// ..." name="details_link">
                                </div>
                                <div class="form-group">
                                    <label for="fileName" class="font-label"><small>For best results with your University Profile, we recommend you to add a related image below.</small></label>
                                    <br/>
                                    <label for="fileName" class="font-label">Image : </label>
                                    <input id="input-2" type="file" class="file" data-show-upload="false" data-show-caption="true" name="image_path">
                                </div>

                        </div>
                        <div class="panel-footer">
                            <div class="pull-right">
                                <button class="btn btn-success" type="submit">Add</button>
                            </div>
                        </div>
                    </form>
                </div>
                
                
            </div>
            
            
            <div class="col-sm-5">
                <br />
                <div class="row">
                    <div class="col-xs-6 col-xs-offset-3">
                        <a href="<?php echo base_url().$user_name;?>" target="_blank" class="btn-perpal">View <span class="glyphicon glyphicon-chevron-right "></span></a>
                    </div>
                </div><br /><br />
                <div class="panel panel-default">
                    <div class="panel-body">
                        <img class="image-responsive img-circle center-block" src="../../assets_my/img/user.png" style="width: 150px;">
                        <h3 class="text-center">Find Friend's Profiles</h3>
                        <br /><br />
                        <form class="" role="form" action="" method="">
                            <div class="form-group">
                                <label class="font-label">Name :</label>
                                <input type="text" class="form-control" placeholder="Enter university name here">
                            </div>
                    </div>
                    <div class="panel-footer">
                        <button class="btn btn-default btn-lg" style="width: 100%" data-toggle="modal" data-target="#successfull">Search</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
<!-- Registration Done Modal Start-->
<div class="modal fade" id="add_project_success" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 0;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #00cc66">The Project/Skill/Experience successfully added.</h4>
            </div>
            <div class="modal-body">
                <p class="text-primary">The Project/Skill/Experience added successfully.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Done !</button>
            </div>
        </div>
    </div>
</div>
<!-- Registration Done Modal End-->

<!-- Add Project Failed Modal Start-->
<div class="modal fade" id="failed" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 0;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel" style="color: #ff0066">Adding Project/Skill/Experience failed</h4>
            </div>
            <div class="modal-body">
                <p>Adding your Project/Skill/Experience to your University Profile failed. Try to add details again.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>
<!-- Add Project Failed Modal End-->