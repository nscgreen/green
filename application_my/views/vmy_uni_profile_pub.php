<!DOCTYPE html>
<!--
Copyright © 2012 - 2014 D2Real Solutions.
All Rights Reserved.

These materials are unpublished, proprietary, confidential source code of
D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.

Author : S.Priyanga < s.priyanga22@gmail.com >
Description : 
Created on : Jul 11, 2014, 12:03:11 PM
-->
<html>
    <head>
    <title>my-NSBM - <?php echo $title?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo $mDescription?>"/>
    <meta name="keywords" content="<?php echo $mKeywords?>">
    <link rel="icon" href="../../images/favicon.ico" type="image/x-icon"/>
    
    <!--Fonts Admin-->
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'> <!--for h1 h2-->
    <link href='http://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'> <!--for h3 h4 h5 h6-->
    <link href='http://fonts.googleapis.com/css?family=Play' rel='stylesheet' type='text/css'> <!--for Special-->
    
    <!--CSS-->
    <?php echo link_tag('assets/css/normalize.css');?>
    <?php echo link_tag('assets/css/cstm_reset.css');?>
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <style>
        body{
            background-color: #faf8f8
        }
        .container{
            box-shadow:0px 4px 25px #ACAAAD;
            background: #F2F2F2;
        }
        .thumbnail{
            border-radius: 0;
            padding: 0;
            box-shadow:-1px 0px 15px #36C6CC;
        }
        .thumbnail:hover{
            box-shadow:-1px 0px 25px #cc33ff;
        }
        .thumbnail > img{
            padding: 0;
            width: 100%;
        }
        .thumbnail > caption{
            padding: 10px;
        }
        .caption > h3{
            font-family: 'Play', sans-serif;
            text-align: center
        }
        .caption > p{
            font-family: 'Play', sans-serif;
            text-align: justify
        }
        .font-label{
            font-family: 'Roboto', sans-serif;
            color: #666666;
            font-weight: 300;
        }
        .font-play{
            font-family: 'Play', sans-serif;
        }
        a:hover{
            color: #999999;
            text-decoration: none;
        }
        .hr-style-two {
            border: 0;
            height: 1px;
            background-image: -webkit-linear-gradient(left, rgba(0,0,0,0), rgba(43, 166, 214, 0.75), rgba(0,0,0,0)); 
            background-image:    -moz-linear-gradient(left, rgba(0,0,0,0), rgba(43, 166, 214, 0.75), rgba(0,0,0,0)); 
            background-image:     -ms-linear-gradient(left, rgba(0,0,0,0), rgba(43, 166, 214, 0.75), rgba(0,0,0,0)); 
            background-image:      -o-linear-gradient(left, rgba(0,0,0,0), rgba(43, 166, 214, 0.75), rgba(0,0,0,0)); 
        }
        /* # Start
         * Footer Styles
         *****************************************************************************/
        #shadow-footer{
            border-top: 1px solid rgba(172,170,173, 0.5);
            height: 1px;
            background-image: -webkit-linear-gradient(left, rgba(172,170,173,0), rgba(172,170,173, 0.7), rgba(172,170,173,0)); 
            background-image:    -moz-linear-gradient(left, rgba(172,170,173,0), rgba(172,170,173, 0.7), rgba(172,170,173,0)); 
            background-image:     -ms-linear-gradient(left, rgba(172,170,173,0), rgba(172,170,173, 0.7), rgba(172,170,173,0)); 
            background-image:      -o-linear-gradient(left, rgba(172,170,173,0), rgba(172,170,173, 0.7), rgba(172,170,173,0)); 
            box-shadow: 0px 5px 20px #7B9FA8;
        }  /* # END Footer */
        
        /*--------------------------- Animating Link Underlines HOver Effect ---------*/
        .underline > a{
            position: relative;
            color: #0099ff;
            text-decoration: none;
        }

        .underline > a:hover {
            color: #0099ff;
        }
        .underline > a:before {
            content: "";
            position: absolute;
            width: 100%;
            height: 2px;
            bottom: 0;
            left: 0;
            background-color: #0099ff;
            visibility: hidden;
            -webkit-transform: scaleX(0);
            transform: scaleX(0);
            -webkit-transition: all 0.3s ease-in-out 0s;
            transition: all 0.3s ease-in-out 0s;
        }
        .underline > a:hover:before {
            visibility: visible;
            -webkit-transform: scaleX(1);
            transform: scaleX(1);
        } /*Animating Link Underlines END*/
    </style>
    </head>
    <body>
    <div class="container">
        <div class="row">
            <div class="center-block">
                <img class="center-block" src="../../logos/nsbm.gif">
                <h1 class="text-center font-roboto" style="font-weight: 400; color: #005085">Student University Profile</h1>
                <hr class="hr-style-two"/>
            </div>
            <div class="col-sm-5">
                <div class="panel panel-default">
                <div class="panel-body">
                    <img class="image-responsive img-circle center-block" src="<?php echo base_url().$public_profile_details->profile_pic; ?>" style="width: 128px; height: 128px"/>
                    <h3 class="text-muted text-center font-roboto" style="font-weight: 100"><?php echo $public_profile_details->initials.' '.$public_profile_details->fname.' '.$public_profile_details->mname.' '.$public_profile_details->lname; ?>
                        <br /><small>index: <?php echo $public_profile_details->st_index; ?></small></h3>
                    <br /><br />
                    <table class="table table-responsive font-label">
                        <tbody>
                                <tr>
                                    <td class="label-success font-play"><span style="color: #faf8f8">Index No</span></td>
                                    <td><?php echo $public_profile_details->st_index; ?></td>
                                </tr>
                                <tr>
                                    <td class="font-play active">I'm..</td>
                                    <td class="text-justify"><?php echo $public_profile_details->my_description; ?></td>
                                </tr>
                                <tr>
                                    <td class="font-play active">Degree</td>
                                    <td class="text-justify"><?php echo $public_profile_details->degree_name; ?></td>
                                </tr>
                                <tr>
                                    <td class="font-play active">Address</td>
                                    <td><?php echo $public_profile_details->address_line1.', '.$public_profile_details->address_line2.', '.$public_profile_details->town.', '.$public_profile_details->city.', '.$public_profile_details->state.', '.$public_profile_details->country; ?></td>
                                </tr>
                                <tr>
                                    <td class="font-play active">Mobile</td>
                                    <td><?php echo $public_profile_details->mobile; ?></td>
                                </tr>
                                <tr>
                                    <td class="font-play active">Email</td>
                                    <td class="underline"><a href="mailto:<?php echo $public_profile_details->stu_email; ?>"><?php echo $public_profile_details->stu_email; ?></a></td>
                                </tr>
                                <tr>
                                    <td class="label-primary font-play"><span style="color: #faf8f8">Facebook</span></td>
                                    <td class="underline"><a href="<?php echo $public_profile_details->link_fb; ?>" target="_blank"><?php echo $public_profile_details->link_fb; ?></a></td>
                                </tr>
                                <tr>
                                    <td class="label-info font-play"><span style="color: #faf8f8">Twitter</span></td>
                                    <td class="underline"><a href="<?php echo $public_profile_details->link_tw; ?>" target="_blank"><?php echo $public_profile_details->link_tw; ?></a></td>
                                </tr>
                                <tr>
                                    <td class="label-danger font-play"><span style="color: #faf8f8">Google+</span></td>
                                    <td class="underline"><a href="<?php echo $public_profile_details->link_gl; ?>" target="_blank"><?php echo $public_profile_details->link_gl; ?></a></td>
                                </tr>
                                <tr>
                                    <td class="label-info font-play"><span style="color: #faf8f8">Linkdin</span></td>
                                    <td class="underline"><a href="<?php echo $public_profile_details->link_ln; ?>" target="_blank"><?php echo $public_profile_details->link_ln; ?></a></td>
                                </tr>
                            </tbody>
                        </table>
                </div>
                </div>
            </div>
            <div class="col-sm-7">
                <!------------ Start View Student Projects ----------->
                <div class="row">
                    <div class="center-block">
                        <h1 class="text-center font-roboto" style="font-weight: 200; color: #0099ff">Projects</h1>
                        <hr class="hr-style-two"/>
                    </div>
                <?php 
                $i=1;
                foreach ($stu_projects_details as $projects) {
                ?>
            <?php if($i===3) { ?>
                    <div class="row">
            <?php } ?>
                        <div class="col-sm-6">
                            <div class="thumbnail">
                                <img src="<?php echo base_url().$projects->image_path; ?>" alt="..." style="max-height: 100px">
                                <div class="caption">
                                    <h3><?php echo $projects->project_title; ?></h3>
                                    <p><?php echo $projects->descrption; ?></p>
                                    <p class="underline text-muted">More : <br /><a href="<?php echo $projects->details_link; ?>"><?php echo $projects->details_link; ?></a>
                                </div>
                            </div>
                        </div>
                <?php if($i===2) { 
                    //echo 'always inside end';
                    ?>    
                        </div>    
                    <?php } ?>
                <?php
                    if($i===3){
                        $i=1;
                    }else{
                    $i++;  
                    }
                }
                ?>
                
                </div>
                <!------------ End View Student Projects ----------->
                
                <!------------ Start View Student Experiences ----------->
                <div class="row">
                    <div class="center-block">
                        <h1 class="text-center font-roboto" style="font-weight: 200; color: #0099ff">Experiences</h1>
                        <hr class="hr-style-two"/>
                    </div>
                <?php 
                $i=1;
                foreach ($stu_experiences_details as $experiences) {
                ?>
            <?php if($i===3) { ?>
                    <div class="row">
            <?php } ?>
                        <div class="col-sm-6">
                            <div class="thumbnail">
                                <img src="<?php echo base_url().$experiences->image_path; ?>" alt="..." style="max-height: 100px">
                                <div class="caption">
                                    <h3><?php echo $experiences->job_title; ?></h3>
                                    <p><?php echo $experiences->company_name; ?></p>                                    
                                    <p>From: <?php echo $experiences->from; ?></p>
                                    <p>To: <?php echo $experiences->to; ?></p>
                                    <p><?php echo $experiences->descrption; ?></p>
                                    <p class="underline text-muted">Company Website : <br /><a href="<?php echo $experiences->company_website; ?>"><?php echo $experiences->company_website; ?></a>
                                </div>
                            </div>
                        </div>
                <?php if($i===2) { 
                    //echo 'always inside end';
                    ?>    
                        </div>    
                    <?php } ?>
                <?php
                    if($i===3){
                        $i=1;
                    }else{
                    $i++;  
                    }
                }
                ?>
                
                </div>
                <!------------ End View Student Experiences ----------->    
                
                <!------------ Start View Student Skills ----------->
                <div class="row">
                    <div class="center-block">
                        <h1 class="text-center font-roboto" style="font-weight: 200; color: #0099ff">Skills/Activities/Community details</h1>
                        <hr class="hr-style-two"/>
                    </div>
                <?php 
                $i=1;
                foreach ($stu_skills_details as $skills) {
                ?>
            <?php if($i===3) { ?>
                    <div class="row">
            <?php } ?>
                        <div class="col-sm-6">
                            <div class="thumbnail">
                                <img src="<?php echo base_url().$skills->image_path; ?>" alt="..." style="max-height: 100px">
                                <div class="caption">
                                    <p><?php echo $skills->skill_title; ?></p>
                                    <p><?php echo $skills->descrption; ?></p>
                                    <p class="underline text-muted">More : <br /><a href="<?php echo $skills->details_link; ?>"><?php echo $skills->details_link; ?></a>
                                </div>
                            </div>
                        </div>
                <?php if($i===2) { 
                    //echo 'always inside end';
                    ?>    
                        </div>    
                    <?php } ?>
                <?php
                    if($i===3){
                        $i=1;
                    }else{
                    $i++;  
                    }
                }
                ?>
                
                </div>
                <!------------ End View Student Skills ----------->     
                
                
            </div>
        </div>
        
        <footer class="row" style="margin: 30px auto auto auto">
            <div class="col-md-6 col-md-offset-3 col-xs-6 col-xs-offset-3">
                <hr id="shadow-footer" />
                <ul class="list-inline text-center">
                    <li><a href="http://d2real.com"  target="_blank">NSBM</a></li>
                    <li><a href="http://my.d2real.com"  target="_blank">My NSBM</a></li>
                    <li><a href="http://lms.d2real.com"  target="_blank">L.M.S</a></li>
                    <li><a href="http://forum.d2real.com"  target="_blank">Forum</a></li>
                    <li><a href="http://events.d2real.com"  target="_blank">Events</a></li>
                    <li><a href="http://communities.d2real.com"  target="_blank">Communities</a></li>
                    <li><a href="http://help.d2real.com"  target="_blank">Help</a></li>
                </ul>
                <p class="text-center">Copyright © National School of Business Management</p>
            </div>
        </footer>
    </div>
        
    <!-- JavaScript -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script> <!-- Boostrap JS -->
    </body>
</html>
