/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jun 21, 2014, 11:42:11 PM
 */

//----------------------------Snapper Sliding Panel Script --------------------
var snapper = new Snap({
    element: document.getElementById('content'),
    disable: 'right' //Disable right
});

var addEvent = function addEvent(element, eventName, func) {
	if (element.addEventListener) {
    	return element.addEventListener(eventName, func, false);
    } else if (element.attachEvent) {
        return element.attachEvent("on" + eventName, func);
    }
};

addEvent(document.getElementById('open-left'), 'click', function(){
	snapper.open('left');
});

/* Prevent Safari opening links when viewing as a Mobile App */
(function (a, b, c) {
    if(c in b && b[c]) {
        var d, e = a.location,
            f = /^(a|html)$/i;
        a.addEventListener("click", function (a) {
            d = a.target;
            while(!f.test(d.nodeName)) d = d.parentNode;
            "href" in d && (d.href.indexOf("http") || ~d.href.indexOf(e.host)) && (a.preventDefault(), e.href = d.href)
        }, !1)
    }
})(document, window.navigator, "standalone");

settings = {
    element: null,
    dragger: null,
    disable: 'right', //Disable right
    addBodyClasses: true,
    hyperextensible: true,
    resistance: 0.5,
    flickThreshold: 50,
    transitionSpeed: 0.3,
    easing: 'ease',
    maxPosition: 266,
    minPosition: -266,
    tapToClose: true,
    touchToDrag: true,
    slideIntent: 40,
    minDragDistance: 5
};

//----------------------------Snapper Sliding Panel Script END -----------------

$('[data-toggle="tooltip"]').tooltip(); // Tooltip enable

/* # Start
 * Table Filter
 *****************************************************************************/
/**
*   <input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-table" placeholder="Filter Developers" />
*   $(input-element).filterTable()
*   The important attributes are 'data-action="filter"' and 'data-filters="#table-selector"'
*/
(function() {
    'use strict';
    var $ = jQuery;
    $.fn.extend({
        filterTable: function() {
            return this.each(function() {
                $(this).on('keyup', function(e) {
                    $('.filterTable_no_results').remove();
                    var $this = $(this), search = $this.val().toLowerCase(), target = $this.attr('data-filters'), $target = $(target), $rows = $target.find('tbody tr');
                    if (search == '') {
                        $rows.show();
                    } else {
                        $rows.each(function() {
                            var $this = $(this);
                            $this.text().toLowerCase().indexOf(search) === -1 ? $this.hide() : $this.show();
                        })
                        if ($target.find('tbody tr:visible').size() === 0) {
                            var col_count = $target.find('tr').first().find('td').size();
                            var no_results = $('<tr class="filterTable_no_results"><td colspan="' + col_count + '">No results found</td></tr>')
                            $target.find('tbody').append(no_results);
                        }
                    }
                });
            });
        }
    });
    $('[data-action="filter"]').filterTable();
})(jQuery);

$(function() {
    // attach table filter plugin to inputs
    $('[data-action="filter"]').filterTable();

    $('.container').on('click', '.panel-heading span.filter', function(e) {
        var $this = $(this),
                $panel = $this.parents('.panel');

        $panel.find('.panel-body').slideToggle();
        if ($this.css('display') != 'none') {
            $panel.find('.panel-body input').focus();
        }
    });
    $('[data-toggle="tooltip"]').tooltip();
});
/* # End Filter Table Scripts */

/* # Start
 * Clickable Panel Script
 *****************************************************************************/
$(document).on('click', '.panel-heading span.clickable-panel', function (e) {
    var $this = $(this);
    if (!$this.hasClass('panel-collapsed')) {
        $this.parents('.panel').find('.panel-body').slideUp();
        $this.addClass('panel-collapsed');
        $this.find('i').removeClass('glyphicon-minus').addClass('glyphicon-plus');
    } else {
        $this.parents('.panel').find('.panel-body').slideDown();
        $this.removeClass('panel-collapsed');
        $this.find('i').removeClass('glyphicon-plus').addClass('glyphicon-minus');
    }
});

$(document).ready(function () {
    $('.panel-heading span.clickable-panel').click();
});
/* # End Clickable Panel Script */

/* # Start
 * Dynamic Form Script
 *****************************************************************************/
$(function ()
{
    $(document).on('click', '.btn-add', function(e)
    {
        e.preventDefault();

        var controlForm = $('.controls form:first'),
            currentEntry = $(this).parents('.entry:first'),
            newEntry = $(currentEntry.clone()).appendTo(controlForm);

        newEntry.find('input').val('');
        controlForm.find('.entry:not(:last) .btn-add')
            .removeClass('btn-add').addClass('btn-remove')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<span class="glyphicon glyphicon-minus"></span>');
    }).on('click', '.btn-remove', function(e)
    {
		$(this).parents('.entry:first').remove();

		e.preventDefault();
		return false;
	});
});
/* # End Dynamic Form Script */



/* # Start Some script functions for Admin Panel
 * 
 *****************************************************************************/

        function loadmodal($modal) { // load bootstrap modals
            $($modal).modal({ show: true});            
        }
        
        function loadmodalwithdata($modal,$formid,$action) { // load bootstrap modals with data
            //$($fieldid).val($value);
            $($formid).attr('action', $action);
            $($modal).modal({ show: true});            
        }
        
        function loadmodalwithdata($modal,$id,$value) { // load bootstrap modals with data
            //$($fieldid).val($value);
            $($id).attr('value', $value);
            $($modal).modal({ show: true});            
        }
        
        function loadmodalwith_formaction($modal,$formid,$action) { // load bootstrap modals with data
            //$($fieldid).val($value);
            
            $($modal).modal({ show: true}); 
            $($formid).attr('action', $action);           
        }
        
/* # End Some script functions for Admin Panel Models
* 
******************************************************************************/


/* # Start Admin Help CMS Script
* 
******************************************************************************/
function for_edit(id, urlprefix,value) {
    category = $(value).val();
    url = urlprefix + '/' + category;
    //alert(url);
    $(id).load(url);
}
//---------------------------------------------------------------------------END