
 //*
 //*   Copyright © 2012 - 2013 D2Real Solutions.
 //*   All Rights Reserved.
 //*    
 //*   These materials are unpublished, proprietary, confidential source code of
 //*   D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 //*
 //*   
 //* 

//Validate Name onchange
function onchangename()
{
    var inputText=document.forms["submit_form"]["name"].value;
    var name=/^[A-Za-z]\w{1,}$/;
    if(name.test(inputText))
    {
        document.getElementById("namemsg").className = "no-display";    
    }
    else{
        document.getElementById("namemsg").className = "do-display";
    }
}

//Validate first name onchange
function onchangefname()
{   
//    alert("nooooooooooooooo");
    var inputText=document.forms["submit_form"]["fname"].value;
    var fname=/^[A-Za-z]\w{1,}$/;
    if(fname.test(inputText))
    {   
//        alert("nooooooooooooooo");
        document.getElementById("fnamemsg").className = "no-display";    
    }
    else{
//        alert("dooooooooooooooo");
        document.getElementById("fnamemsg").className = "do-display";
    }
}
//Validate last name onchange
function onchangelname()
{
    var inputText=document.forms["submit_form"]["lname"].value;
    var lname=/^[A-Za-z]\w{1,}$/;
    if(lname.test(inputText))
    {
        document.getElementById("lnamemsg").className = "no-display";    
    }
    else
    {
        document.getElementById("lnamemsg").className = "do-display"; 
    }
}
//Validate Phone number onchange
function onchangephone()
{
    var inputText=document.forms["submit_form"]["phone"].value;
    var phoneno = /^\+?([0-9]{4})\)?[-. ]?([0-9]{2,})[-. ]?([0-9]{2,})$/;
    if(phoneno.test(inputText))
    {  
        document.getElementById("phonemsg").className = "no-display";  
    }  
    else
    {
        document.getElementById("phonemsg").className = "do-display"; 
    }
}
//Validate email confirm onchange
function onchangeemailcon()
{
    var email1 = document.getElementById("email").value;
    var email2 = document.getElementById("emailcon").value;
    if (email1!== email2) 
    { 
        document.getElementById("emailconmsg").className = "do-display";
    }
	else
    { 
        document.getElementById("emailconmsg").className = "no-display";
    }
}
//Validate email onchange
function onchangeemail()
{
    var inputText=document.forms["submit_form"]["email"].value;
    var email =  /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (email.test(inputText))
    { 
        document.getElementById("emailmsg").className = "no-display";
    }
	else
    { 
        document.getElementById("emailmsg").className = "do-display";
    }
}


//Validate Password onchange
function onchangepass()   
{   
    var inputText=document.forms["submit_form"]["password"].value;
    var pass=/^.{6,}$/;
    if (pass.test(inputText))  
{ 
        document.getElementById("passmsg").className = "no-display";
    }
        else
    { 
        document.getElementById("passmsg").className = "do-display";
    }
}


//Validate Password confirm onchange
function onchangepasscon() {
        var pass1 = document.getElementById("password").value;
        var pass2 = document.getElementById("passwordcon").value;
        if (pass1!== pass2) 
    { 
        document.getElementById("passconmsg").className = "do-display";
    }
	else
    { 
        document.getElementById("passconmsg").className = "no-display";
    }
    }

//Validate Country onchange
function onchangecountry()
    {
        var e = document.getElementById("country");
        var country = e.options[e.selectedIndex].value;
        if(country==="-")
    {
        document.getElementById("countrymsg").className = "do-display";
    }
        else
    {
        document.getElementById("countrymsg").className = "no-display";    
    }
}


//Validate Country onchange
function onchangecountrytarget()
    {
        var e = document.getElementById("countrytarget");
        var country = e.options[e.selectedIndex].value;
        if(country==="-")
    {
        document.getElementById("countrytargetmsg").className = "do-display";
    }
        else
    {
        document.getElementById("countrytargetmsg").className = "no-display";    
    }
}



//Validation on submit of the sign up form

function validateSignUpForm()  
{  
//Creating a flag to check 
    var error =false;

//checking the fname
    var inputText1=document.forms["submit_form"]["fname"].value;
        var fname=/^[A-Za-z]\w{1,}$/;
        if(fname.test(inputText1))
    {
        document.getElementById("fnamemsg").className = "no-display";    
    }
    else{
        document.getElementById("fnamemsg").className = "do-display";
        error=true;  
    }

//checking the lname
        var inputText2=document.forms["submit_form"]["lname"].value;
        var lname=/^[A-Za-z]\w{1,}$/;
        if(lname.test(inputText2))
    {
        document.getElementById("lnamemsg").className = "no-display";    
    }
    else{
        document.getElementById("lnamemsg").className = "do-display";
        error=true;  
    }

//checking the phone
        var inputText3=document.forms["submit_form"]["phone"].value;
        var phone=/^\+?([0-9]{4})\)?[-. ]?([0-9]{2,})[-. ]?([0-9]{2,})$/;
        if(phone.test(inputText3))
    {
        document.getElementById("phonemsg").className = "no-display";    
    }
    else{
        document.getElementById("phonemsg").className = "do-display";
        error=true;  
    }

//checking the emailcon
        var email1 = document.getElementById("email").value;
        var email2 = document.getElementById("emailcon").value;
        if (email1!== email2)
    {
        document.getElementById("emailconmsg").className = "do-display";
        error=true; 
    }
    else{
        document.getElementById("emailconmsg").className = "no-display";
         
    }
    
//checking the email
        var inputText4=document.forms["submit_form"]["email"].value;
        var email =  /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (email.test(inputText4))
    {
        document.getElementById("emailmsg").className = "no-display";    
    }
    else
    {
        document.getElementById("emailmsg").className = "do-display";
        error=true;  
}
    
//checking the password
        var inputText5=document.forms["submit_form"]["password"].value;
        var pass=/^.{6,}$/;
        if (pass.test(inputText5))  
{ 
        document.getElementById("passmsg").className = "no-display";
    }
    else
    { 
        document.getElementById("passmsg").className = "do-display";
        error=true;
}
    
//checking the passwordcon
        var pass1 = document.getElementById("password").value;
        var pass2 = document.getElementById("passwordcon").value;
        if (pass1!== pass2) 
    { 
        document.getElementById("passconmsg").className = "do-display";
        error=true;
    }
    else
    { 
        document.getElementById("passconmsg").className = "no-display";
    }


//checking the country
        var e = document.getElementById("country");
        var country = e.options[e.selectedIndex].value;
        if(country==="-")
        {
        document.getElementById("countrymsg").className = "do-display";
        error=true;
        }else{
        document.getElementById("countrymsg").className = "no-display";    
        }



//        var checkemail=document.forms["submit_form"]["email"].value;
//
//        $.ajax({
//            type: "GET",
//            url: "../../../../control/reg_users/registration/checkemailAvailability.php?email="+checkemail
//        //    data: {catid:catid}
//         }).done(function( result ) {
//             $("#emailcheckmsg").html( result );
//             document.getElementById("emailcheckmsg").className = "do-display";
//             var response=result;
//             if(response==="Use another email. This email is Already Taken"){
//                error=true;
//            }
//         }
//        );
            
            





//Returning validation status
    if(error==false){
        return true;
    }else
    {
        return false;
//        document.getElementById("countrymsg").innerHTML = "Error"; 
    }
                
                    
                    

}



function validateSignInForm()  
{  
//Creating a flag to check 
    var error =false;
    
//checking the email
        var inputText4=document.forms["submit_form"]["email"].value;
        var email =  /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (email.test(inputText4))
    {
        document.getElementById("emailmsg").className = "no-display";    
    }
    else
    {
        document.getElementById("emailmsg").className = "do-display";
        error=true;  
}
    
//checking the password
        var inputText5=document.forms["submit_form"]["password"].value;
        var pass=/^.{6,}$/;
        if (pass.test(inputText5))  
{ 
        document.getElementById("passmsg").className = "no-display";
    }
    else
    { 
        document.getElementById("passmsg").className = "do-display";
        error=true;
}
    

//Returning validation status
    if(error==false){
        return true;
    }else
    {
        return false;
//        document.getElementById("countrymsg").innerHTML = "Error"; 
    }
                
                    
                    

}

//Validate Title 
function onchangetitle()
{
    var inputText=document.forms["submit_form"]["title"].value;
    //var name=/^[A-Za-z]\w{1,}$/;
	if(inputText==="")
    //if(name.test(inputText))
    {
        document.getElementById("titlemsg").className = "no-display";    
    }
    else{
        document.getElementById("titlemsg").className = "do-display";
    }
}

//Validate cars onchange
function onchangemakecars()
    {
        var e = document.getElementById("makecars");
        var makecars = e.options[e.selectedIndex].value;
        if(makecars==="")
    {
        document.getElementById("makecarsmsg").className = "do-display";
    }
        else
    {
        document.getElementById("makecarsmsg").className = "no-display";    
    }
}

//Validate bike onchange
function onchangemakebikes()
    {
        var e = document.getElementById("makebikes");
        var makebikes = e.options[e.selectedIndex].value;
        if(makebikes==="")
    {
        document.getElementById("makebikesmsg").className = "do-display";
    }
        else
    {
        document.getElementById("makebikesmsg").className = "no-display";    
    }
}


//Validate mobile onchange
function onchangebrandmobile()
    {
        var e = document.getElementById("brandmobile");
        var brandmobile = e.options[e.selectedIndex].value;
        if(brandmobile==="")
    {
        document.getElementById("brandmobilemsg").className = "do-display";
    }
        else
    {
        document.getElementById("brandmobilemsg").className = "no-display";    
    }
}

//Validate condition onchange
function onchangecondition()
    {
        var e = document.getElementById("condition");
        var condition = e.options[e.selectedIndex].value;
        if(condition==="")
    {
        document.getElementById("conditionmsg").className = "do-display";
    }
        else
    {
        document.getElementById("conditionmsg").className = "no-display";    
    }
}

//Validate transmission onchange
function onchangetransmission()
    {
        var e = document.getElementById("transmission");
        var transmission = e.options[e.selectedIndex].value;
        if(transmission==="")
    {
        document.getElementById("transmissionmsg").className = "do-display";
    }
        else
    {
        document.getElementById("transmissionmsg").className = "no-display";    
    }
}

//Validate fueltype onchange
function onchangefueltype()
    {
        var e = document.getElementById("fueltype");
        var fueltype = e.options[e.selectedIndex].value;
        if(fueltype==="")
    {
        document.getElementById("fueltypemsg").className = "do-display";
    }
        else
    {
        document.getElementById("fueltypemsg").className = "no-display";    
    }
}

//Validate year onchange
function onchangeyear()
    {
        var e = document.getElementById("year");
        var year = e.options[e.selectedIndex].value;
        if(year==="")
    {
        document.getElementById("yearmsg").className = "do-display";
    }
        else
    {
        document.getElementById("yearmsg").className = "no-display";    
    }
}

//Validate model 
function onchangemodel()
{
    var inputText=document.forms["submit_form"]["model"].value;
    //var name=/^[A-Za-z]\w{1,}$/;
	if(inputText==="")
    //if(name.test(inputText))
    {
        document.getElementById("modelmsg").className = "no-display";    
    }
    else{
        document.getElementById("modelmsg").className = "do-display";
    }
}

//Validate description 
function onchangedescriotion()
{
    var inputText=document.forms["submit_form"]["description"].value;   
	if(inputText==="")   
    {
        document.getElementById("descriptionmsg").className = "no-display";    
    }
    else{
        document.getElementById("descriptionmsg").className = "do-display";
    }
}






//Validation on submit of reset form


function signupreset()
{
    document.getElementById("phonemsg").className = "no-display";
    document.getElementById("emailmsg").className = "no-display";
    document.getElementById("fnamemsg").className = "no-display";
    document.getElementById("lnamemsg").className = "no-display";
    document.getElementById("emailconmsg").className = "no-display";
    document.getElementById("passmsg").className = "no-display";
    document.getElementById("passconmsg").className = "no-display";
    document.getElementById("countrymsg").className = "no-display";
}
