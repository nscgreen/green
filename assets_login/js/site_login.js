/* 
 * Copyright © 2012 - 2014 D2Real Solutions.
 * All Rights Reserved.
 * 
 * These materials are unpublished, proprietary, confidential source code of
 * D2Real Solutions (pvt) Limited and constitute a TRADE SECRET of D2Real Solutions (pvt) Limited.
 * 
 * Author : S.Priyanga < s.priyanga22@gmail.com >
 * Description : 
 * Created on : Jul 15, 2014, 12:32:38 AM
 */

$(function() {
    $(".showpassword").each(function(index, input) {
        var $input = $(input);
        $("<p class='opt'/>").append(
                $("<input type='checkbox' class='showpasswordcheckbox pull-left' id='showPassword'  style='width: 20px; margin-top:5px'/><a href='#' class='pull-right' style='margin: 5px 7px 0 0;' data-toggle='modal' data-target='#reset'><label style='text-transform: none;'>Forget your password ..?</label></a>").click(function() {
            var change = $(this).is(":checked") ? "text" : "password";
            var rep = $("<input placeholder='Password' type='" + change + "' />")
                    .attr("id", $input.attr("id"))
                    .attr("name", $input.attr("name"))
                    .attr('class', $input.attr('class'))
                    .val($input.val())
                    .insertBefore($input);
            $input.remove();
            $input = rep;
        })
                ).append($("<label for='showPassword'/>").text("Show password")).insertAfter($input.parent());
    });

    $('#showPassword').click(function() {
        if ($("#showPassword").is(":checked")) {
            $('.icon-lock').addClass('icon-unlock');
            $('.icon-unlock').removeClass('icon-lock');
        } else {
            $('.icon-unlock').addClass('icon-lock');
            $('.icon-lock').removeClass('icon-unlock');
        }
    });
});